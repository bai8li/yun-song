//
//  WeatherCell.swift
//  天气cell
//
//  Created by smile on 2020/10/21.
//

import UIKit

import SnapKit

class WeatherCell: UICollectionViewCell {
    /// 下拉回调方法
    var onRefresh:((_ data:Int)->Void)!
    
    var refreshControl:UIRefreshControl!
    var loading:UIActivityIndicatorView!
    var scrollView:UIScrollView!
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    func initViews() {
        //背景
        backgroundColor = .clear
        
        //加载控件
        loading = UIActivityIndicatorView()
        loading.style = .large
        loading.hidesWhenStopped=true
        contentView.addSubview(loading)
        
        //滚动控件
        scrollView = UIScrollView(frame: CGRect.zero)
        scrollView.isHidden = true
        contentView.addSubview(scrollView)
    
        //下拉刷新
        refreshControl=UIRefreshControl()
//        refreshControl.attributedTitle = NSAttributedString(string:  "最后更新：15分钟前")
        scrollView.refreshControl = refreshControl
        
        //刷新回调
        scrollView.refreshControl?.addTarget(self, action: #selector(onPullRefresh(sender:)), for: UIControl.Event.valueChanged)
        
        //contaienrView
        let containerView = UIView(frame: CGRect.zero)
        scrollView.addSubview(containerView)
        
        containerView.addSubview(tempView)
        containerView.addSubview(weatherView)
        containerView.addSubview(indexView)
        
        //自动布局
        loading.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        //scrollView frame就确定了
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        //contaienrView
        containerView.snp.makeConstraints { make in
            //4边和scrollView对齐
            make.edges.equalToSuperview()
            
            //宽度等于cell宽度
            make.width.equalTo(scrollView.snp.width)
        }
        
        //滚动控件
        
        //温度
        tempView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        //未来天气
        weatherView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(tempView.snp.bottom).offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        //生活指数
        indexView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(weatherView.snp.bottom).offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalTo(-16)
        }
    }
    
    func bindData(_ data:Area) {
        if let weather = data.weather {
            tempView.bindData(data)
            weatherView.bindData(weather.daily)
            indexView.bindData(weather.daily.life_index)
            
            loading.stopAnimating()
            loading.isHidden=true
            
            scrollView.isHidden=false
        }else{
            loading.startAnimating()
        }
        
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    lazy var tempView: TempView = {
        let view = TempView()
        
        return view
    }()
    
    lazy var weatherView: WeatherView = {
        let view = WeatherView()
        
        return view
    }()
    
    lazy var indexView: IndexView = {
        let view = IndexView()
        
        return view
    }()
    
    @objc func onPullRefresh(sender:UIRefreshControl) {
        print("WeatherCell onPullRefresh")
        
        //延迟在请求数据
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            //回调到外面
            self.onRefresh(self.tag)
        }
       
    }
}
