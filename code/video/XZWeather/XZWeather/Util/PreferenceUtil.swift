//
//  PreferenceUtil.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import Foundation

class PreferenceUtil {
    /// 保存用户选择城市
    ///
    /// - Parameter data: <#data description#>
    static func setAreas(_ data:[Area]) {
        let dataJSON=data.toJSONString()
        
        UserDefaults.standard.set(dataJSON, forKey: KEY_AREAS)
    }
    
    /// 获取用户选择城市
    ///
    /// - Returns: <#return value description#>
    static func getAreas() -> [Area]? {
        let dataJSON = UserDefaults.standard.string(forKey: KEY_AREAS)
        return [Area].deserialize(from: dataJSON) as? [Area]
    }
    
    // MARK: - 常量key
    
    /// 用户选择城市key
    private static let KEY_AREAS = "KEY_AREAS"
    
}
