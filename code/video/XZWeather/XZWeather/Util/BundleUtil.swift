//
//  BundleUtil.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import Foundation

class BundleUtil {
    
    /// app版本
    /// 在项目-targets-general-indenty-version设置
    ///
    /// - Returns: 一般都是3位：例如：2.0.1
    static func appVersion() -> String {
        //获取info文件字典
        let infoDictionary=Bundle.main.infoDictionary!
        
        //获取值并转为字符串
        return infoDictionary["CFBundleShortVersionString"] as! String
    }
    
    /// appBuild版本
    /// 在项目-targets-general-indenty-build设置
    ///
    /// - Returns: 一般是整形，例如：201；int更方便判断大小
    static func appBuildVersion() -> Int {
        //获取到info文件字典
        let infoDictionary=Bundle.main.infoDictionary!
        
        //获取值
        let buildVersion=infoDictionary["CFBundleVersion"] as! String
        
        //转为Int
        let buildVersionInt=Int(buildVersion)
        
        //返回
        return buildVersionInt!
    }
}
