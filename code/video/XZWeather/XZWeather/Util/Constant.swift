//
//  Constant.swift
//  weather
//
//  Created by smile on 2020/10/20.
//

import Foundation

/// 列表Cell复用字符串
let CELL = "Cell"

//天气API
let WEATHER_ENDPOINT = "https://api.caiyunapp.com"

//天气token
let WEATHER_TOKEN = "OHvsQlALaTxIIQXT"

/// 日期时间格式化字符串
let DATE_TIME_FORMAT1 = "yyyy-MM-dd'T'HH:mm+ss.SS"

let DATE_TIME_FORMAT2 = "yyyy-MM-dd"

//天气对照
//https://open.caiyunapp.com/%E9%80%9A%E7%94%A8%E9%A2%84%E6%8A%A5%E6%8E%A5%E5%8F%A3/v2.5#.E7.94.9F.E6.B4.BB.E6.8C.87.E6.95.B0.E7.AD.89.E7.BA.A7
let WEATHER_SKY = ["CLEAR_DAY":"晴",
                   "CLEAR_NIGHT":"晴",
                   "PARTLY_CLOUDY_DAY":"多云",
                   "PARTLY_CLOUDY_NIGHT":"多云",
                   "CLOUDY":"阴",
                   "LIGHT_HAZE":"轻度雾霾",
                   "MODERATE_HAZE":"中度雾霾",
                   "HEAVY_HAZE":"重度雾霾",
                   "LIGHT_RAIN":"小雨",
                   "MODERATE_RAIN":"中雨",
                   "HEAVY_RAIN":"大雨",
                   "STORM_RAIN":"暴雨",
                   "FOG":"雾",
                   "LIGHT_SNOW":"小雪",
                   "MODERATE_SNOW":"中雪",
                   "HEAVY_SNOW":"大雪",
                   "STORM_SNOW":"暴雪",
                   "DUST":"浮尘",
                   "SAND":"沙尘",
                   "WIND":"大风"
]


/// 选择了地区
let ON_SELECT_AREA = "ON_SELECT_AREA"

