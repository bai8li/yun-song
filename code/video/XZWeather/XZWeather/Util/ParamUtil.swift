//
//  ParamUtil.swift
//  weather
//
//  Created by smile on 2020/10/23.
//

import Foundation

//导入JSON解析框架
import HandyJSON

//导入网络框架
import Moya

class ParamUtil {
    /// 返回URL编码的参数
    ///
    /// - Parameter parameters: <#parameters description#>
    /// - Returns: <#return value description#>
    static func urlRequestParamters(_ parameters:[String:Any]) -> Task {
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
}
