//
//  TimeUtil.swift
//  weather
//
//  Created by smile on 2020/10/22.
//

import Foundation


class TimeUtil {
    /// 将2020-10-22T00:00+08:00字符串格式化为Date
    ///
    /// - Parameter _data: <#_data description#>
    /// - Returns: <#return value description#>
    static func toDate1(_ data:String) -> Date {
        //初始化格式化器
        let dateFormatter = DateFormatter.init()
        
        //设置时区
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        //设置格式化字符串
        dateFormatter.dateFormat = DATE_TIME_FORMAT1
        
        //转为date
        return dateFormatter.date(from: data)!
    }
    
    /// 将Date转为yyyyMMdd
    ///
    /// - Parameter _data: <#_data description#>
    /// - Returns: <#return value description#>
    static func toyyyyMMdd(_ data:Date) -> String {
        //初始化格式化器
        let dateFormatter = DateFormatter.init()
        
        //设置时区
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        //设置格式化字符串
        dateFormatter.dateFormat = DATE_TIME_FORMAT2
        
        //转为date
        return dateFormatter.string(from: data)
    }
    
    /// 将Date转为yyyyMMdd
    ///
    /// - Parameter _data: <#_data description#>
    /// - Returns: <#return value description#>
    static func toyyyyMMdd(_ data:String) -> String {
        
        return toyyyyMMdd(toDate1(data))
    }
}
