//
//  IndexView.swift
//  生活指数View
//
//  Created by smile on 2020/10/22.
//

import UIKit

class IndexView: CardView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        //背景
        backgroundColor = .white
        
        //圆角
        cornerRadius = 5
        
        //添加布局
        addSubview(titleView)
        
//        let stackView1 = UIStackView(frame: CGRect.zero)
//
//        //水平
//        stackView1.axis = .horizontal
//
//        //中心
//        stackView1.alignment = .center
//
//        //子view平分
//        stackView1.distribution = .fillEqually
//        addSubview(stackView1)
        
        let view1 = UIStackView(frame: CGRect.zero)
        addSubview(view1)
        
        view1.addSubview(index1)
        view1.addSubview(index2)
        view1.addSubview(index3)
        
        let view2 = UIStackView(frame: CGRect.zero)
        addSubview(view2)
        
        view2.addSubview(index4)
        view2.addSubview(index5)
        view2.addSubview(index6)
        
        //自动布局
        
        //标题
        titleView.snp.makeConstraints { make in
            make.top.equalTo(16)
            make.left.equalTo(16)
            
        }
        
        //view1
        view1.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(10)
            make.left.equalTo(0)
            make.right.equalTo(0)
//            make.bottom.equalTo(-16)
            
            make.height.equalTo(70)
        }
        
        //view2
        view2.snp.makeConstraints { make in
            make.top.equalTo(view1.snp.bottom).offset(5)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(-16)
            
            make.height.equalTo(70)
        }
        
        //index1
        index1.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(16)
        }
        
        //index2
        index2.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(index1.snp.right)
            
            make.width.equalTo(index1.snp.width)
        }
        
        //index3
        index3.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(index2.snp.right)
            make.right.equalTo(-16)
            
            make.width.equalTo(index2.snp.width)
        }
        //index4
        index4.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(16)
        }
        
        //index5
        index5.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(index4.snp.right)
            
            make.width.equalTo(index4.snp.width)
        }
        
        //index6
        index6.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(index5.snp.right)
            make.right.equalTo(-16)
            
            make.width.equalTo(index5.snp.width)
        }
        
    }
    
    /// 标题
    lazy var titleView: UILabel = {
        let label = UILabel()
        
        label.text="生活指数"
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 18)
        return label
    }()
    
    /// index1
    lazy var index1: ItemIndexView = {
        let view = ItemIndexView()
        
        return view
    }()
    
    /// index2
    lazy var index2: ItemIndexView = {
        let view = ItemIndexView()
        
        return view
    }()
    
    /// index3
    lazy var index3: ItemIndexView = {
        let view = ItemIndexView()
        
        return view
    }()
    
    /// index4
    lazy var index4: ItemIndexView = {
        let view = ItemIndexView()
        
        return view
    }()
    
    /// index5
    lazy var index5: ItemIndexView = {
        let view = ItemIndexView()
        
        return view
    }()
    
    /// index6
    lazy var index6: ItemIndexView = {
        let view = ItemIndexView()
        
        return view
    }()

    func bindData(_ data:LifeIndex?) {
        if let data = data {
            index1.bindData("紫外线",data.ultraviolet[0].desc)
            index2.bindData("洗车",data.carWashing[0].desc)
            index3.bindData("穿衣",data.dressing[0].desc)
            index4.bindData("舒适度",data.comfort[0].desc)
            index5.bindData("感冒",data.coldRisk[0].desc)
        }else{
            index1.bindData("紫外线","")
            index2.bindData("洗车","")
            index3.bindData("穿衣","")
            index4.bindData("舒适度","")
            index5.bindData("感冒","")
        }
       
    }
    
}
