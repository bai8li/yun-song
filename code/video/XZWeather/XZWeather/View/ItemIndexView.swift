//
//  ItemIndexView.swift
//  生活指数单个View
//
//  Created by smile on 2020/10/22.
//

import UIKit

class ItemIndexView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        //添加布局
        addSubview(titleView)
        addSubview(labelView)
        
        //自动布局
        
        //天气图标
//        iconView.snp.makeConstraints { make in
//            make.top.equalTo(0)
//            make.centerX.equalToSuperview()
////            make.bottom.equalTo(-10)
//
//            make.width.equalTo(35)
//            make.height.equalTo(35)
//        }
        
        titleView.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.centerX.equalToSuperview()
//            make.bottom.equalTo(-10)

//            make.width.equalTo(30)
            make.height.equalTo(15)
        }
        
        //标签
        labelView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(0)
        }

    }
    
    /// 图标
//    lazy var iconView: UIImageView = {
//        let view = UIImageView()
//
//        view.image =  UIImage(named: "ClearDay")
//
//        return view
//    }()
    
    lazy var titleView: UILabel = {
        let label = UILabel()
        
        label.text=""
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 12)
        
        label.textColor = .lightGray
        return label
    }()
    
    /// 值
    lazy var labelView: UILabel = {
        let label = UILabel()
        
        label.text=""
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 16)
        
        label.textColor = .darkGray
        return label
    }()
    
    
    func bindData(_ title:String,_ data:String) {
        titleView.text=title
        labelView.text = data
    }
    

}
