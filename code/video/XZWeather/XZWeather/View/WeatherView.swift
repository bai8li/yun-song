//
//  WeatherView.swift
//  未来天气
//
//  Created by smile on 2020/10/21.
//

import UIKit

class WeatherView: CardView {
    var weatherDay1:WeatherDayView!
    var weatherDay2:WeatherDayView!
    var weatherDay3:WeatherDayView!
    var weatherDay4:WeatherDayView!
    var weatherDay5:WeatherDayView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        //背景
        backgroundColor = .white
        
        //圆角
        cornerRadius = 5
        
        //添加布局
        addSubview(titleView)
        
        weatherDay1 = WeatherDayView(frame: CGRect.zero)
        addSubview(weatherDay1)
        
        weatherDay2 = WeatherDayView(frame: CGRect.zero)
        addSubview(weatherDay2)
        
        weatherDay3 = WeatherDayView(frame: CGRect.zero)
        addSubview(weatherDay3)
        
        weatherDay4 = WeatherDayView(frame: CGRect.zero)
        addSubview(weatherDay4)
        
        weatherDay5 = WeatherDayView(frame: CGRect.zero)
        addSubview(weatherDay5)
        
        //自动布局
        
        //标题
        titleView.snp.makeConstraints { make in
            make.top.equalTo(16)
            make.left.equalTo(16)

        }
        
        //天气1
        weatherDay1.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(30)
        }
        
        //天气2
        weatherDay2.snp.makeConstraints { make in
            make.top.equalTo(weatherDay1.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(30)
        }
        
        //天气3
        weatherDay3.snp.makeConstraints { make in
            make.top.equalTo(weatherDay2.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(30)
        }
        
        //天气4
        weatherDay4.snp.makeConstraints { make in
            make.top.equalTo(weatherDay3.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(30)
        }
        
        //天气5
        weatherDay5.snp.makeConstraints { make in
            make.top.equalTo(weatherDay4.snp.bottom).offset(5)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.bottom.equalTo(-16)
            make.height.equalTo(30)
        }
        
    }
    
    func bindData(_ data:Daily?) {
        if let data = data {
            weatherDay1.bindData("今天",WEATHER_SKY[data.skycon[0].value]!,data.temperature[0].min,data.temperature[0].max)
            
            weatherDay2.bindData("明天",WEATHER_SKY[data.skycon[1].value]!,data.temperature[1].min,data.temperature[1].max)
            
            weatherDay3.bindData(data.temperature[2].date[0 ..< 10],WEATHER_SKY[data.skycon[2].value]!,data.temperature[2].min,data.temperature[2].max)
            
            weatherDay4.bindData(data.temperature[3].date[0 ..< 10],WEATHER_SKY[data.skycon[3].value]!,data.temperature[3].min,data.temperature[3].max)
            
            weatherDay5.bindData(data.temperature[4].date[0 ..< 10],WEATHER_SKY[data.skycon[4].value]!,data.temperature[4].min,data.temperature[4].max)
        }else{
            weatherDay1.bindData("","",0,0)
            weatherDay2.bindData("","",0,0)
            weatherDay3.bindData("","",0,0)
            weatherDay4.bindData("","",0,0)
            weatherDay5.bindData("","",0,0)
        }
       
    }

    
    /// 标题
    lazy var titleView: UILabel = {
        let label = UILabel()
        
        label.text="未来天气"
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 18)
        return label
    }()

}
