//
//  TempView.swift
//  天气view-头部温度view
//
//  Created by smile on 2020/10/21.
//

import UIKit

import SnapKit

class TempView: CardView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    func initViews() {
        //背景
        backgroundColor = .white
        
        //圆角
        cornerRadius = 5
        
        //添加布局
        addSubview(currentTempView)
        addSubview(weatherView)
        addSubview(airView)
        
        //自动布局
        
        //温度
        currentTempView.snp.makeConstraints { make in
            make.top.equalTo(50)
            make.centerX.equalToSuperview()
            
        }
        
        //天气
        weatherView.snp.makeConstraints { make in
            make.top.equalTo(currentTempView.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            
        }
        
        //空气
        airView.snp.makeConstraints { make in
            make.top.equalTo(weatherView.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(-50)
           
        }

    }
    
    
    /// 当前温度
    lazy var currentTempView: UILabel = {
        let label = UILabel()
        
        label.text="-℃"
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 70)
        return label
    }()
    
    /// 天气；晴
    lazy var weatherView: UILabel = {
        let label = UILabel()
        
        label.text="-"
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 25)
        return label
    }()
    
    /// 空气
    lazy var airView: UILabel = {
        let label = UILabel()
        
        label.text="空气质量 -"
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 18)
        return label
    }()

    func bindData(_ data:Area) {
        if let data = data.weather {
            currentTempView.text = "\(data.realtime.temperature!)℃"
            weatherView.text = WEATHER_SKY[data.realtime.skycon]!
            airView.text="空气质量 \(data.realtime.air_quality.aqi.chn!)"
        }else{
            currentTempView.text = "-℃"
            airView.text="空气质量 -"
        }
       
    }

}
