//
//  WeatherDayView.swift
//  weather
//
//  Created by smile on 2020/10/21.
//

import UIKit

class WeatherDayView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func initViews() {
        //添加布局
        addSubview(dateView)
//        addSubview(weatchIconView)
        addSubview(airView)
        addSubview(tempView)
        
        //自动布局
        
        //日期
        dateView.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.centerY.equalToSuperview()
//            make.left.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        //天气图标
//        weatchIconView.snp.makeConstraints { make in
//            make.top.equalTo(10)
//            make.right.equalTo(airView.snp.left).offset(-20)
//            make.bottom.equalTo(-10)
//
//            make.width.equalTo(20)
//            make.height.equalTo(20)
//        }

//        //空气
        airView.snp.makeConstraints { make in
            make.right.equalTo(tempView.snp.left).offset(-30)
            make.centerY.equalToSuperview()
        }
        
        //温度
        tempView.snp.makeConstraints { make in
            make.right.equalTo(0)
            make.width.equalTo(80)
            make.centerY.equalToSuperview()
        }
        
    }
    
    func bindData(_ date:String,_ air:String,_ min:Int,_ max:Int) {
        dateView.text=date
        airView.text=air
        tempView.text="\(min)~\(max)℃"
    }
    
    /// 日期
    lazy var dateView: UILabel = {
        let label = UILabel()
        
        label.text="-"
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 16)
        
        label.textColor = .darkGray
        return label
    }()
    
    /// 天气图标
//    lazy var weatchIconView: UIImageView = {
//        let view = UIImageView()
//
//        view.image =  UIImage(named: "ClearDay")
//
//        return view
//    }()

    /// 空气
    lazy var airView: UILabel = {
        let label = UILabel()
        
        label.text="-"
        
        //字体大小
        label.font=UIFont.systemFont(ofSize: 16)
        
        label.textColor = .darkGray
        return label
    }()
    
    /// 温度
    lazy var tempView: UILabel = {
        let view = UILabel()
        
        view.text="-"
        
        //字体大小
        view.font=UIFont.systemFont(ofSize: 16)
        
        view.textColor = .darkGray
        
        view.textAlignment = .right
        return view
    }()
}
