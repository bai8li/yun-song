//
//  StringExtend.swift
//  weather
//
//  Created by smile on 2020/10/22.
//

import Foundation

extension String {
    /// 支持通过数组的方式几区文本
    ///
    /// - Parameter r: <#r description#>
    subscript (r: Range<Int>) -> String {
        //创建一个范围
        let range = Range(uncheckedBounds: (lower: max(0, min(count, r.lowerBound)), upper: min(count, max(0, r.upperBound))))
        
        //开始位置
        let start = index(startIndex, offsetBy: range.lowerBound)
        
        //结束位置
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        
        //截取字符串并创建一个字符串返回
        return String(self[start ..< end])
    }
    
    /// 去除字符串首尾的空格和换行
    ///
    /// - Returns: <#return value description#>
    func trim() -> String? {
        let whitespace=NSCharacterSet.whitespacesAndNewlines
        
        return trimmingCharacters(in: whitespace)
    }
}
