//
//  ViewExtend.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import UIKit

//小圆角
let SIZE_SMALL_RADIUS = 5

extension UIView{
    /// 设置圆角
    ///
    /// - Parameters:
    ///   - view: <#view description#>
    ///   - radius: <#radius description#>
    func showRadius(_ radius:Float) {
        layer.cornerRadius=CGFloat(radius)
        
        //裁剪多余的内容
        //例如：给ImageView设置了圆角
        //如果不裁剪多余的内容，就不会生效
        clipsToBounds=true
    }
    
    /// 显示小的圆角
    ///
    /// - Parameter view: <#view description#>
    func showSmallRadius() {
        showRadius(Float(SIZE_SMALL_RADIUS))
    }
}
