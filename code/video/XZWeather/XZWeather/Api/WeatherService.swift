//
//  WeatherService.swift
//  网络接口
//
//  Created by smile on 2020/10/22.
//

import Foundation

//导入网络框架
import Moya

/// 定义项目中的所有接口
///
/// - weather: 通用天气
/// - realtime: 实时天气
/// - daily: 为了天气
/// - place: 区域
enum WeatherService {
    case weather(lat:Double,lng:Double)
    case realtime(lat:Double,lng:Double)
    case daily(lat:Double,lng:Double)
    case place(data:String)
}

// MARK: - 实现TargetType协议
extension WeatherService: TargetType {
    
    /// 返回网址
    var baseURL: URL {
        return URL(string: WEATHER_ENDPOINT)!
    }
    
    /// 返回每个请求的路径
    var path: String{
        switch self {
        case .weather(let lat,let lng):
            return "/v2.5/\(WEATHER_TOKEN)/\(lng),\(lat)/weather.json"
        case .realtime(let lat,let lng):
            return "/v2.5/\(WEATHER_TOKEN)/\(lng),\(lat)/realtime.json"
        case .daily(let lat,let lng):
            return "/v2.5/\(WEATHER_TOKEN)/\(lng),\(lat)/daily.json"
        case .place(let data):
            return "/v2/place"
        default:
            return ""
        }
    }
    
    /// 请求方式
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }

    }
    
    /// 返回测试相关的数据
    var sampleData: Data {
        return Data()
    }
    
    /// 请求的参数
    var task: Task {
        switch self {
        case .place(let data):
            return ParamUtil.urlRequestParamters(["query":data,"token":WEATHER_TOKEN])
        default:
            //不传递任何参数
            return .requestPlain
        }
        
    }
    
    /// 请求头
    var headers: [String : String]? {
        
        return nil
    }

}
