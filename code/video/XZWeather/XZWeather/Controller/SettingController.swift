//
//  SettingController.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import UIKit

class SettingController: UIViewController {
    @IBOutlet weak var ivIcon: UIImageView!
    
    @IBOutlet weak var lbVersion: UILabel!
    
    var versionClickCount=0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ivIcon.showSmallRadius()
        
        let version = BundleUtil.appVersion()
        
        //显示版本
        lbVersion.text="    当前版本：V\(version)"
        
        //版本点击
        lbVersion.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onVersionClick)))
    }
    

    @objc func onVersionClick() {
        versionClickCount += 1
        if versionClickCount>3 {
            var temp:Int?
            print(temp!)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
