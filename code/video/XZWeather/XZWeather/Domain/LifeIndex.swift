//
//  LifeIndex.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import Foundation

class LifeIndex: BaseJSON {
    var ultraviolet:[LifeIndexData]!
    var carWashing:[LifeIndexData]!
    var dressing:[LifeIndexData]!
    var comfort:[LifeIndexData]!
    var coldRisk:[LifeIndexData]!
}
