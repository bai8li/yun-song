//
//  Location.swift
//  weather
//
//  Created by smile on 2020/10/20.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class Location: BaseModel {
    
    /// 经度
    var lat:Double!
    
    /// 纬度
    var lng:Double!
}
