//
//  PlaceResponse.swift
//  搜索区域返回
//
//  Created by smile on 2020/10/23.
//

import Foundation

class PlaceResponse: BaseJSON {
    /// 状态码
    var status:String!
    
    var places:[Area]?
}
