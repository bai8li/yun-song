//
//  Temperature.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import Foundation

class Temperature: BaseJSON {
    
    /// 日期
    var date:String!
    
    
    /// 最小
    var min:Int!
    
    /// 最大
    var max:Int!
    
}
