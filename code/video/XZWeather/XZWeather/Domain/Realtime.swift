//
//  WeatherRealtime.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import Foundation

class Realtime: BaseJSON {
    
    /// 温度
    var temperature:Int! = 0
    
    /// 主要天气现象
    var skycon:String!=""
    
    /// 空气质量
    var air_quality:AirQuality!
}
