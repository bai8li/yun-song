//
//  WeatherResponse.swift
//  通用天气返回bean
//  接口文档：https://open.caiyunapp.com/%E9%80%9A%E7%94%A8%E9%A2%84%E6%8A%A5%E6%8E%A5%E5%8F%A3/v2.5#.E7.94.9F.E6.B4.BB.E6.8C.87.E6.95.B0.E7.AD.89.E7.BA.A7
//
//  Created by smile on 2020/10/22.
//

import Foundation

/// 继承BaseResponse
/// 因为我们希望用户传递的类要能解析为JSON
class WeatherResponse: BaseResponse {
    /// 状态码
    var status:String!
    
    var result:Result!
    
    //服务器本次返回的utc时间戳
    var server_time:Int!=0
    
    
}
