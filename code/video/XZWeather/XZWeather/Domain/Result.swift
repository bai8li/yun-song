//
//  WeatherResult.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import Foundation

class Result:BaseJSON {
    
    /// 实时天气
    var realtime:Realtime!
    
    /// 按天气
    var daily:Daily!
}
