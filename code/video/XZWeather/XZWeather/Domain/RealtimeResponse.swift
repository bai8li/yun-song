//
//  RealtimeResponse.swift
//  实时天气返回bean
//  https://api.caiyunapp.com/v2.5/TAkhjf8d1nlSlspN/112.62774443271765,37.770765330118316/realtime.json
//  接口文档：https://open.caiyunapp.com/%E5%AE%9E%E5%86%B5%E5%A4%A9%E6%B0%94%E6%8E%A5%E5%8F%A3/v2.5
//
//  Created by smile on 2020/10/22.
//

import Foundation

/// 继承BaseResponse
/// 因为我们希望用户传递的类要能解析为JSON
class RealtimeResponse: BaseResponse {
    /// 状态码
    var status:String!
    
    var result:Result!
    
    //服务器本次返回的utc时间戳
    var server_time:Int!=0
    
    class Result:BaseJSON {
        var realtime:Realtime!
        
        class Realtime: BaseJSON {
            
            /// 温度
            var temperature:Int! = 0
            
            /// 主要天气现象
            var skycon:String!=""
            
            /// 空气质量
            var air_quality:AirQuality!
            
            class AirQuality: BaseJSON {
                var aqi:Aqi!
                
                class Aqi: BaseJSON {
                    //中国标准
                    var chn:Int!=0
                    
                }
            }
        }
    }
}
