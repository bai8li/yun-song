//
//  Daily.swift
//  weather
//
//  Created by smile on 2020/10/24.
//

import Foundation

class Daily: BaseJSON {
    //温度
    var temperature:[Temperature]!
    
    ///天气
    var skycon:[Skycon]!
    
    ///生活指数
    var life_index:LifeIndex!
}
