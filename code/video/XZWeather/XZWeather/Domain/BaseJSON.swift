//
//  BaseJSON.swift
//  weather
//
//  Created by smile on 2020/10/22.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class BaseJSON: HandyJSON {
    
    //JSON解析框架需要一个默认的init方法
    required init() {}
}
