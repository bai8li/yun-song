//
//  Area.swift
//  城市
//
//  Created by smile on 2020/10/20.
//

import Foundation

//导入JSON解析框架
import HandyJSON

//https://api.caiyunapp.com/v2/place?token=jZvT57Omw2fGyYrW&lang=zh_CN&query=%E5%8C%97%E4%BA%AC
class Area: BaseModel {
    /// 城市，例如：太原
    var name:String!
    
    /// 格式化的标题
    var formatted_address:String!
    
    /// 位置
    var location:Location!
    
    ///天气信息，是其他接口解析完，放到这里的，方便显示
    var weather:Result?
    
    /// 更新时间
//    var updatedAt:Date?
}
