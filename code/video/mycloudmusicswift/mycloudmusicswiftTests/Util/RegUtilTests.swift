//
//  RegUtilTests.swift
//  正则表达式工具类测试
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import XCTest

class RegUtilTests: XCTestCase {
    //测试字符串有1个mention和2个hashTag
    let str1 = "这是一条很长的评论，真很长，不信你看看，包含了#各种特性#，有话题，提醒人，例如：这是一个话题#电影教会你的人生道理#还有提醒人，例如：你好@爱学啊smile，评论完毕。"
    
    //测试字符串没有任何要高亮的信息
    let str2 = "人生苦短，我们只做好课！"

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    /// 测试查看mentions
    func testFindMentions() {
        //测试字符串有1个mentions
        XCTAssertEqual(RegUtil.findMentions(str1).count, 1)
        
        //测试字符串没有mentions
        XCTAssertEqual(RegUtil.findMentions(str2).count, 0)
    }
    
    /// 测试查找话题
    func testFindHashTag()  {
        //测试字符串有2个HashTag
        XCTAssertEqual(RegUtil.findHashTag(str1).count, 2)
        
        //测试字符串没有任何话题
        XCTAssertEqual(RegUtil.findHashTag(str2).count, 0)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
