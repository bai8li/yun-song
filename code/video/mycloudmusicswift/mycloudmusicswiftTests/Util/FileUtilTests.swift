//
//  FileUtilTests.swift
//  mycloudmusicswiftTests
//
//  Created by smile on 2019/6/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import XCTest

class FileUtilTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    /// 测试文件大小格式化
    func testFormatFileSize() {
        //第一个参数等于第二个参数
        XCTAssertEqual(FileUtil.formatFileSize(1), "1.00Byte")
        
        //第一个参数等于第二个参数
        //1234/1024=1.205078125
        XCTAssertEqual(FileUtil.formatFileSize(1234), "1.21K")
        
        //第一个参数不等于第二个参数
        XCTAssertNotEqual(FileUtil.formatFileSize(1234), "1.23K")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
