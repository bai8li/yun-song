//
//  DegestUtilTests.swift
//  加密工具类测试
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import XCTest

class DigestUtilTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /// 测试sha1签名
    func testSha1() {
        //相等测试
        XCTAssertEqual(DigestUtil.sha1("ixueaedu"), "c31c3e896a92ba11382ab4ec92ece29ebfd38ecc")
        
        //不相等测试
        XCTAssertNotEqual(DigestUtil.sha1("ixueaedu"), "ixueaedu")
    }
    
    /// 测试AES128加密
    func testEncrypAES() {
        //相等测试
        XCTAssertEqual(DigestUtil.encrypAES("ixueaedu"), "3gNwgHqyYLjPzO4xG8976w==")
        
        //不相等测试
        XCTAssertNotEqual(DigestUtil.encrypAES("ixueaedu"), "ixueaedu")
    }
    
    /// 测试AES128解密
    func testDecrypAES() {
        //相等测试
        XCTAssertEqual(DigestUtil.decryptAES("3gNwgHqyYLjPzO4xG8976w=="), "ixueaedu")
        
        //不相等测试
        XCTAssertNotEqual(DigestUtil.decryptAES("ixueaedu"), "ixueaedu")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
