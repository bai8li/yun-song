//
//  StringUtil.swift
//  字符串工具类
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class StringUtil {
    
    /// 点击回调方法别名
    public typealias onTagClick = (_ container:UIView,_ text:NSAttributedString,_ range:NSRange,_ rect:CGRect) -> Void
    
    /// 高亮mention和hashtag
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func processHighlight(_ data:String) -> NSMutableAttributedString {
        //创建一个结果属性字符串
        let result = NSMutableAttributedString(string: data)
        
        //设置字体大小
        result.yy_font = UIFont.systemFont(ofSize: 17)
        
        //查找mention
        let mentionResults = RegUtil.findMentions(data)
        
        //遍历
        for tag in mentionResults {
            processHighlightInner(result,tag.range)
        }
        
        //查找话题
        let hashTagResults = RegUtil.findHashTag(data)
        
        for tag in hashTagResults {
            processHighlightInner(result, tag.range)
        }
        
        return result
    }
    
    /// 内部高亮方法
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - range: <#range description#>
    static func processHighlightInner(_ data:NSMutableAttributedString,_ range:NSRange) {
        //设置颜色
        data.addAttribute(.foregroundColor, value: UIColor(hex: COLOR_HIGHLIGHT), range: range)
    }
    
    /// 处理文本
    /// 对关键内容高亮
    /// 并且添加点击事件
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - result: <#result description#>
    ///   - onMentionClick: <#onMentionClick description#>
    ///   - onHashTagClick: <#onHashTagClick description#>
    /// - Returns: <#return value description#>
    static func processContent(_ data:String,_ result:NSMutableAttributedString,_ onMentionClick:@escaping onTagClick,_ onHashTagClick:@escaping onTagClick) -> NSMutableAttributedString {
        //设置字体大小
        result.yy_font = UIFont.systemFont(ofSize: 17)
        
        //查找mention
        let mentionResults=RegUtil.findMentions(data)
        
        //遍历
        for tag in mentionResults {
            StringUtil.processInner(result,tag.range,onMentionClick)
        }
        
        //查找话题
        let hashTagResults = RegUtil.findHashTag(data)
        
        for tag in hashTagResults {
            StringUtil.processInner(result, tag.range, onHashTagClick)
        }
        
        return result
    }
    
    /// 内部处理字符串点击方法
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - range: <#range description#>
    ///   - onTagClick: <#onTagClick description#>
    private static func processInner(_ data:NSMutableAttributedString,_ range:NSRange,_ onTagClick:@escaping onTagClick) {
        //使用YYText框架提供的方法设置高亮和点击事件
        data.yy_setTextHighlight(range, color: UIColor(hex: COLOR_HIGHLIGHT), backgroundColor: nil) { (container, text, range, rect) in
            //点击回调
            print("StringUtil processInner:\(text),\(range)")
            
            //回调tag
            onTagClick(container,text,range,rect)
        }
    }
    
    /// 处理评论关键信息点击
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - range: <#range description#>
    /// - Returns: <#return value description#>
    static func processClickText(_ data:String,_ range:NSRange) -> String {
        //截取用户点击的文本
        let clickText = data[range.location..<range.location+range.length]
        
        //移除字符串前后的占位符
        return StringUtil.removePlaceholderString(clickText)
    }
    
    /// 移除字符串中的占位字符串
    /// @；#
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func removePlaceholderString(_ data:String) -> String {
        if data.hasPrefix("@") {
            //从1截取到最后
            return data.substring(fromIndex: 1)
        }else if data.hasPrefix("#"){
            //从1截取到最后一个字符串-1
            return data[1 ..< data.count-1]
        }else {
            return data
        }
    }
    
    /// 给用户Id添加填充字符
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func wrapperUserId(_ data:String) -> String {
        return "100\(data)"
    }
    
    /// 将用户Id填充字符移除
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func unwrap(_ data:String) -> String {
        return data.substring(fromIndex: 3)
    }
}
