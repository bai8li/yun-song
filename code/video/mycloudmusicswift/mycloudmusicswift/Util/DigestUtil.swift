//
//  DigestUtil.swift
//  加密算法工具类
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//常用加密算法实现
//aes,sha,md5
import CryptoSwift

class DigestUtil {
    
    /// sha1签名
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func sha1(_ data:String) -> String? {
        //前面和后面的是加盐
        
        //什么是加盐？
        //就是混入一些内容
        
        //加盐的好处是
        //如果攻击者知道了我们使用的签名算法
        //但他不知道盐
        //也就无法计算出相同的结果
        
        //这里的盐是随机生成的
        //真实项目中可以随便更改
        //但要和服务端协商好
        let string="wt5jKURZtH6RDtt\(data)uWg7xnE4Mr5Xwzm"
        let stringData=string.data(using: .utf8)
        let sha1Data=stringData?.sha1()
        return sha1Data?.toHexString()
    }
    
    /// AES128加密
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func encrypAES(_ data:String) -> String? {
        do {
            //使用PKCS5Padding
            //这是和服务端协商好的
            //所以这里不能随便更改
            
            //因为这里使用的是AES128算法
            //所以key，iv必须是16位
            //因为16位字符就是128位
            //16*8=128，每个字符占用8位
            //也就是一个Byte
            
            //CBC模式
            let cipher = try AES(key: AES128_KEY, iv: AES128_IV, padding: .pkcs5)
            
            //使用aes128加密算法加密
            return try cipher.encrypt(Array(data.utf8)).toBase64()
        } catch {
            
        }
        return nil
    }
    
    /// AES128解密
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func decryptAES(_ data:String) -> String? {
        do {
            //CBC模式
            let cipher = try AES(key: AES128_KEY, iv: AES128_IV, padding: .pkcs5)
            
            //先使用base64解码
            //然后在解密
            return try data.decryptBase64ToString(cipher: cipher)
        } catch {
            
        }
        return nil
    }
}
