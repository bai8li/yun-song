//
//  UUIDUtil.swift
//  UUID工具类
//
//  Created by smile on 2019/7/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class UUIDUtil {
    
    /// 生成UUID
    /// 例如：3a32e44543ad42f5b584531ed040c62a
    /// - Returns: <#return value description#>
    static func uuid() -> String {
        return NSUUID().uuidString.replacingOccurrences(of: "-", with: "").lowercased()
    }
}
