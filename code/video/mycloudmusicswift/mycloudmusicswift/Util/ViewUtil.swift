//
//  ViewUtil.swift
//  View相关的工具类，包括：圆角，边框
//
//  Created by smile on 2019/6/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit


class ViewUtil {
    /// 设置大圆角
    ///
    /// - Parameter view: 要设置的View
    static func showLargeRadius(view:UIView) {
//        view.layer.cornerRadius = CGFloat(SIZE_LARGE_RADIUS)
//
//        //裁剪多余的内容
//        //例如：给ImageView设置了圆角
//        //如果不裁剪多余的内容，就不会生效
//        view.clipsToBounds=true
        
        showRadius(view, Float(SIZE_LARGE_RADIUS))
    }
    
    /// 设置圆角
    ///
    /// - Parameters:
    ///   - view: <#view description#>
    ///   - radius: <#radius description#>
    static func showRadius(_ view:UIView,_ radius:Float) {
        view.layer.cornerRadius=CGFloat(radius)
        
        //裁剪多余的内容
        //例如：给ImageView设置了圆角
        //如果不裁剪多余的内容，就不会生效
        view.clipsToBounds=true
    }
    
    /// 显示小的圆角
    ///
    /// - Parameter view: <#view description#>
    static func showSmallRadius(_ view:UIView) {
        showRadius(view, Float(SIZE_SMALL_RADIUS))
    }
    
    /// 从ScrollView截图
    ///
    /// - Parameter scrollView: <#scrollView description#>
    /// - Returns: <#return value description#>
    static func captureScrollView(_ scrollView:UIScrollView) -> UIImage {
 
        //下面的代码大家不用理解
        //只需要知道这样写就行了
        //因为他是iOS中绘图相关的知识
        
        //获取绘图上下文
        UIGraphicsBeginImageContextWithOptions(scrollView.contentSize, false, 0.0)
        
        //获取ScrollView的偏移
        let savedContentOffset = scrollView.contentOffset
        
        //获取ScrollView的位置
        let savedFrame = scrollView.frame
        
        //重新设置ScrollView的Frame
        scrollView.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        
        //调用渲染方法
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        //然后就可以获取图片
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        //恢复ScrollView的偏移
        scrollView.contentOffset = savedContentOffset
        
        //恢复ScrollView的frame
        scrollView.frame = savedFrame
        
        //标记绘图结束
        UIGraphicsEndImageContext()
        
        //返回Image
        return image!
    }
}
