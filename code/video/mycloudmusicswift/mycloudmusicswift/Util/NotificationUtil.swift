//
//  NotificationUtil.swift
//  通知相关工具类
//
//  Created by smile on 2019/7/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入通知中心
import UserNotifications

class NotificationUtil {
    
    /// 显示聊天消息到通知栏
    ///
    /// - Parameter data: <#data description#>
    static func showMessage(_ data:JMSGMessage) {
        //获取用户
        let user=data.fromUser
        
        //获取用户信息
        UserManager.shared().getUser(StringUtil.unwrap(user.username)) { (userDetail) in
            //用户信息获取成功
            
            //获取通知中心
            let center=UNUserNotificationCenter.current()
            
            //创建通知
            let content=UNMutableNotificationContent()
            
            //扩展信息
            //用来实现点击通知做相应的处理
            content.userInfo=[CUSTOM_KEY:[ACTION_KEY:ACTION_MESSAGE,EXTRA_ID:user.username]]
                
            //内容
            //格式为：用户名：消息内容
            content.body="\(userDetail.nickname!)：\(MessageUtil.getContent(data))"
            
            //默认提示音
            content.sound = .default
            
            //创建一个通知请求
            let request=UNNotificationRequest(identifier: "IxueaMyCloudMusic", content: content, trigger: nil)
            
            //显示通知
            center.add(request, withCompletionHandler: { (error) in
                if let error=error{
                    print("NotificationUtil showMessage failed:\(error)")
                }else{
                    print("NotificationUtil showMessage success")
                }
            })
        }
    }
}
