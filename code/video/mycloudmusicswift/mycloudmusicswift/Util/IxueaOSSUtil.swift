//
//  IxueaOSSUtil.swift
//  阿里云OSS工具类
//
//  Created by smile on 2019/7/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class IxueaOSSUtil {
    private static var instance:OSSClient?
    
    static func shared() -> OSSClient {
        if instance==nil {
            //推荐使用OSSAuthCredentialsProvider
            //token过期可以及时更新
            //我们这里为了课程难度
            //就直接使用AK和SK
            let credentialProvider = OSSPlainTextAKSKPairCredentialProvider(plainTextAccessKey: ALIYUN_OSS_AK, secretKey: ALIYUN_OSS_SK)
            
            //bucket地址；认证信息
            instance=OSSClient(endpoint: RESOURCE_ENDPOINT, credentialProvider: credentialProvider)
        }
        
        return instance!
    }
}
