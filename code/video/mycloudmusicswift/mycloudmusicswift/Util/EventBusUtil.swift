//
//  EventBusUtil.swift
//  发布订阅通知工具类
//
//  Created by smile on 2019/7/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入发布订阅框架
import SwiftEventBus

class EventBusUtil {
    
    /// 发送一个事件
    ///
    /// - Parameter name: <#name description#>
    static func post(_ name: String) {
        SwiftEventBus.post(name)
    }
    
    
    /// 发送消息未读数改变通知
    ///
    /// - Returns: <#return value description#>
    static func postMessageCountChanged() {
        EventBusUtil.post(ON_MESSAGE_COUNT_CHANGED)
    }
}
