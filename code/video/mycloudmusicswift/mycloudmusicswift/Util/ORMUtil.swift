//
//  ORMUtil.swift
//  数据库工具类
//
//  Created by smile on 2019/6/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入数据库框架
import Realm
import RealmSwift

class ORMUtil {
    private static var instance:ORMUtil?
    
    /// 数据库
    var database:Realm!
    
    
    /// 单例设计模式
    ///
    /// - Returns: <#return value description#>
    static func shared() -> ORMUtil {
        if instance == nil {
            instance = ORMUtil()
        }
        
        return instance!
    }
    
    init() {
        //创建数据库
        database = try! Realm()
    }
    
    /// 保存歌曲
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - userId: <#userId description#>
    func saveSong(_ data:Song,_ userId:String) {
        //将Song转为SongLocal
        let songLocal = data.toSongLocal()
        
        //保存用户Id
        songLocal.user_id = userId
        
        //生成主键
        songLocal.generateId()
        
        //保存数据到数据库
        try! database.write {
            database.add(songLocal, update: .modified)
        }
        
        //打印日志是为了方便调试
        print("ORMUtil saveSong:\(songLocal.id),\(songLocal.progress),\(songLocal.duration)")
    }
    
    /// 查询指定用户的播放列表
    ///
    /// - Parameter userId: <#userId description#>
    /// - Returns: <#return value description#>
    func queryPlayList(_ userId:String) -> [Song] {
        //创建一个查询条件字符串
        let whereString = String(format: "user_id = '%@' and playList = 1", userId)
        
        //查询数据
        let songLocals = database.objects(SongLocal.self).filter(whereString)
        
        print("ORMUtil queryPlayList:\(songLocals.count)")
        
        //将songLocal转为Song
        var results:[Song] = []
        
        for songLocal in songLocals {
            //遍历每一个本地音乐对象
            //转成song对象
            let song = songLocal.toSong()
            
            results.append(song)
        }
        
        return results
    }
    
    /// 根据Id查询音乐对象
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func findSongById(_ id:String) -> Song? {
        //创建一个查询条件字符串
        let whereString = String(format: "sid = '%@'", id)
        
        let song = database.objects(SongLocal.self).filter(whereString).first
        
        if let song = song {
            return song.toSong()
        }
        
        return nil
    }
    
    /// 从数据库中删除该音乐
    ///
    /// - Parameter data: <#data description#>
    func deleteSong(_ data:Song) {
        //删除
//        try! database.write {
//            //没找到该框架通过Id删除数据
//            //所有要先查询到要删除的数据
//            //然后在删除
//            let whereString = "user_id = '\(PreferenceUtil.userId()!)' and id = '\(data.id!)'"
//
//            let songLocal = database.objects(SongLocal.self).filter(whereString).first
//
//            if let songLocal = songLocal {
//                //查询到了该音乐
//                database.delete(songLocal)
//            }
//        }
        
        //更新
        let songLocal = data.toSongLocal()
        
        //设置UserId
        songLocal.user_id = PreferenceUtil.userId()!
        
        //生成主键
        songLocal.generateId()
        
        songLocal.playList = false
        
        try! database.write {
            database.add(songLocal)
        }
    }
}
