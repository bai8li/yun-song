//
//  AnalysisUtil.swift
//  统计相关方法
//
//  Created by smile on 2019/7/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class AnalysisUtil {
    
    /// 登录事件
    ///
    /// - Parameters:
    ///   - success: <#success description#>
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - qqId: <#qqId description#>
    ///   - weiboId: <#weiboId description#>
    static func onLogin(success:Bool,phone:String?=nil,email:String?=nil,qqId:String?=nil,weiboId:String?=nil) {
        
        //创建登录事件
        let event=JANALYTICSLoginEvent()
        
        //是否成功
        event.success=success
        
        //计算登录类型
        let info=getMethod(phone: phone, email: email, qqId: qqId, weiboId: weiboId)
        
        //设置方式
        event.method=info.0
        
        //扩展信息
        event.extra=info.1
        
        //记录事件
        JANALYTICSService.eventRecord(event)
    }
    
    /// 注册事件
    ///
    /// - Parameters:
    ///   - success: <#success description#>
    ///   - avatar: <#avatar description#>
    ///   - nickname: <#nickname description#>
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - qqId: <#qqId description#>
    ///   - weiboId: <#weiboId description#>
    static func onRegister(success:Bool,method:String,avatar:String?=nil,nickname:String,phone:String,email:String,qqId:String?=nil,weiboId:String?=nil){
        //创建注册事件
        let event=JANALYTICSRegisterEvent()
        
        //是否成功
        event.success=success
        
        //计算注册方法
//        let info=getMethod(phone: phone, email: email, qqId: qqId, weiboId: weiboId)
        
        //设置方式
//        event.method=info.0
        event.method=method
        
        //设置扩展信息
        var extra:[String:String]=[:]
        
        //头像
        if let avatar = avatar {
            extra["avatar"]=avatar
        }
        
        extra["nickname"]=nickname
        extra["phone"]=phone
        extra["email"]=email
        
        //qq
        if let qqId = qqId {
            extra["qq"]=DigestUtil.sha1(qqId)
        }
        
        //weibo
        if let weiboId = weiboId {
            extra["weibo"]=DigestUtil.sha1(weiboId)
        }
        
        event.extra=extra
        
        //记录事件
        JANALYTICSService.eventRecord(event)
    }
    
    /// 根据信息计算登录/注册方式
    ///
    /// - Parameters:
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - qqId: <#qqId description#>
    ///   - weiboId: <#weiboId description#>
    /// - Returns: <#return value description#>
    static func getMethod(phone:String?,email:String?,qqId:String?,weiboId:String?) -> (String,[String:String]) {
        //扩展信息
        var extra:[String:String] = [:]
        
        //登录类型
        var loginType=""
        
        if let phone = phone {
            //手机号
            loginType="phone"
            
            //扩展信息
            extra["phone"]=phone
        }else if let email = email {
            //邮箱
            loginType="email"
            extra["email"]=email
        }else if let qqId=qqId{
            //QQ
            loginType="qq"
            extra["qq"]=DigestUtil.sha1(qqId)
        }else if let weiboId=weiboId {
            //微博
            loginType="weibo"
            extra["weibo"]=DigestUtil.sha1(weiboId)
        }
        
        return (loginType,extra)
    }
    
    /// 购买事件
    ///
    /// - Parameters:
    ///   - success: <#success description#>
    ///   - data: <#data description#>
    static func onPurchase(_ success:Bool,_ data:Order) {
        //创建事件
        let event=JANALYTICSPurchaseEvent()
        
        //是否成功
        event.success=success
        
        //商品价格
        event.price=CGFloat(data.price)
        
        //商品名称
        event.goodsName=data.book.title
        
        //商品类型
        //一般有多种商品
        //例如：有课程，有电子书
        event.goodsType="book"
        
        //商品Id
        event.goodsID=data.book.id
        
        //币种
        event.currency = .currencyCNY
        
        //数量
        //由于我们这里没有数据
        //所有随便写一个值
        event.quantity=1
        
        //扩展信息
        event.extra=["order":data.id]
        
        //记录事件
        JANALYTICSService.eventRecord(event)
    }
    
    /// 跳过广告事件
    ///
    /// - Parameter userId: <#userId description#>
    static func onSkipAd(_ userId:String) {
        //创建事件
        let event=JANALYTICSCountEvent()
        
        //自定义事件名称
        event.eventID="SkipAd"
        
        //扩展信息
        //传递了用户Id
        event.extra=["user":userId]
        
        //记录事件
        JANALYTICSService.eventRecord(event)
    }
}
