//
//  Constant.swift
//  常量类文件
//  主要用来保存项目中用到的字符串，颜色，尺寸，网络请求地址
//
//  Created by smile on 2019/6/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

/// 是否是调试模式
let DEBUG = true

// MARK: - 开发环境
/// 网络接口的地址
///Rails课程API端点
//let ENDPOINT="http://dev-my-cloud-music-api-rails.ixuea.com"

/// SpringBoot课程API地址
let ENDPOINT="http://my-cloud-music-api-sp-dev.ixuea.com"

/// 局域网电脑api地址
//let ENDPOINT="http://192.168.50.51:8080"

/// 资源接口地址
let RESOURCE_ENDPOINT="http://dev-courses-misuc.ixuea.com"

// MARK: - 阿里云OSS配置
///企业项目中也应该区分环境
/// AK
let ALIYUN_OSS_AK = "LTAI4Fr1njmWpE4E5uGrMtqk"

/// SK
let ALIYUN_OSS_SK="XLCBiAGLN1ad1DWUE3ExAux9lxmRue"

//bucket name
//可以简单理解为上传到那个硬盘（不是目录）
//企业项目中也应该像上面区分环境
let ALIYUN_OSS_BUCKET_NAME = "dev-courses-misuc"

// MARK: - 正式环境
///// 网络接口的地址
//let ENDPOINT="http://my-cloud-music-api-rails.ixuea.com"

/// 资源接口地址
//let RESOURCE_ENDPOINT="http://courses-misuc.ixuea.com"

// MARK: - 高德地图
/// 高德地图
let AMAP_KEY="b118833fa0ed59f545a571a1d7c620af"

// MARK: - 尺寸
/// 立即体验按钮的宽度
let SIZE_BUTTON_ENTER_WIDTH = 130.0

/// 立即体验按钮的高度
let SIZE_BUTTON_ENTER_HEIGHT = 44.0

/// 大圆角
let SIZE_LARGE_RADIUS = 22

//小圆角
let SIZE_SMALL_RADIUS = 5

/// 边框宽度
let SIZE_BORDER = 1.0

/// 发现界面；Cell标题高度
let SIZE_TITLE_HEIGHT:CGFloat = 40

/// 大分割线
let SIZE_LARGE_DIVIDER:CGFloat = 10

/// 发现界面头部高度
let SIZE_DISCOVERY_HEADER_HEIGHT:CGFloat = 270

/// 黑胶唱片每16毫秒旋转的弧度
let SIZE_ROTATE:CGFloat = 0.004021238596595

///评论界面行高
let SIZE_COMMENT_LINE_HEIGHT:CGFloat=27

/// 标题Cell高度
let SIZE_TITLE_CELL_HEIGHT:CGFloat=75

/// 用户详情Header高度
let SIZE_HEADER_HEIGHT:CGFloat=270

/// 用户详情指示器高度
let SIZE_INDICATOR_HEIGHT:CGFloat=44

// MARK: - 颜色
/// 全局主色调
/// PRIMARY后缀其实是参考了Android中颜色的命名
let COLOR_PRIMARY=0xdd0000

/// 亮灰色
let COLOR_LIGHT_GREY=0xaaaaaa

/// 高亮颜色
let COLOR_HIGHLIGHT = 0x3385ff

// MARK: - 颜色字符串（主要用来实现夜间模式）

/// 白色
let COLOR_STRING_WHITE="#ffffff"

/// TableView分组颜色字符串
let COLOR_STRING_GROUP_TABLE_VIEW_BACKGROUND="#efeff4"

/// 灰色颜色
let COLOR_STRING_LIGHT_GREY="#AAAAAA"

/// 发现页面搜索按钮夜间模式下背景颜色字符串
let COLOR_STRING_SEARCH_BACKGROUND="#2E2E2E"

/// 深黑
/// 值和xcode不一样
/// 他的值和黑色一样
let COLOR_STRING_DARK_BLACK="#232323"

/// 黑色
let COLOR_STRING_BLACK="#000000"

/// 主色调
let COLOR_STRING_PRIMARY="#dd0000"

/// 主色调深色字符串
/// 用来实现夜间模式
/// 按钮按下
let COLOR_STRING_DARK_PRIMARY="#7B22222"

/// 展位图
let IMAGE_PLACE_HOLDER="PlaceHolder"

/// 手机号
/// 移动：134 135 136 137 138 139 147 150 151 152 157 158 159 178 182 183 184 187 188 198
/// 联通：130 131 132 145 155 156 166 171 175 176 185 186
/// 电信：133 149 153 173 177 180 181 189 199
/// 虚拟运营商: 170
let REGX_PHONE="^(0|86|17951)?(13[0-9]|15[012356789]|16[6]|19[89]]|17[01345678]|18[0-9]|14[579])[0-9]{8}$"

//邮箱正则表达式
let REGX_EMAIL="^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$"


// MARK: - 提示字符串

/// 密码格式不正确
let ERROR_PASSWORD_FORMAT = "密码格式不正确！"

// MARK: - QQ第三方登录，分享
let QQ_APP_ID = "101481482"
let QQ_APP_SECRET = "b4304961e7299dde6b7b6964bfbb4a7f"

// MARK: - 新浪微博登录，分享
let WEIBO_APP_KEY = "1638318172"
let WEIBO_APP_SECRET = "5e265999bb4eb7c3c1eed34d658e0b0e"
let WEIBO_REDIRECT_URI = "http://www.ixuea.com"

// MARK: - 常量字符串
//离线通知自定义信息key
let CUSTOM_KEY="custom"

// Universal Links
let APP_UNIVERSAL_LINK="https://dev-courses-misuc.oss-cn-beijing.aliyuncs.com/mycloudmusic/"

/// 广告点击了发送的事件
let AD_CLICK = "AD_CLICK"

/// 列表Cell复用字符串
let CELL = "Cell"

/// 开始旋转黑胶唱片事件
let ON_START_RECORD = "ON_START_RECORD"

/// 停止旋转黑胶唱片事件
let ON_STOP_RECORD = "ON_STOP_RECORD"

/// 选择了话题
let ON_SELECT_TOPIC = "ON_SELECT_TOPIC"

/// 选择了好友
let ON_SELECT_USER = "ON_SELECT_USER"

/// 歌单数据改变了
/// 取消收藏
/// 收藏歌单
/// 创建歌单
let ON_SHEET_CHANGED = "ON_SHEET_CHANGED"

/// 下载状态改变了事件
let ON_DOWNLOAD_STATUS_CHANGED = "ON_DOWNLOAD_STATUS_CHANGED"

/// 动态改变了事件
let ON_FEED_CHANGED="ON_FEED_CHANGED"

/// 用户资料改变了
let ON_USER_INFO_CHANGED="ON_USER_INFO_CHANGED"

/// 点击了搜索
let ON_SEARCH_CLICK="ON_SEARCH_CLICK"

// MARK: - 网络请求参数

/// 最热
let ORDER_HOT = 10

/// 最新
let ORDER_NEW = 0


// MARK: - 日期和时间格式化字符串

/// ISO8601格式化字符串
let ISO8601_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSzzz"

/// 年-月-日 时:分:秒 格式化字符串
let DATE_TIME_FORMAT_yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss"

/// 年-月-日 时:分:秒:毫秒 格式化字符串
let DATE_TIME_FORMAT_yyyyMMddHHmmsssss = "yyyy-MM-dd HH:mm:ss:sss"

/// 年-月-日 格式化字符串
let DATE_FORMAT_yyyyMMdd = "yyyy-MM-dd"

// MARK: - 第三方登录平台
//微信
let WECHAT_APP_ID = "wx672a5ce2ea3a3f4f"
let WECHAT_APP_SECRET = "04fcd35c088f1bef28af2f1c0bc26654"

// QQ登录
let PLATFORM_QQ=20

// 微信
let PLATFORM_WECHAT=30

// 微博
let PLATFORM_WEIBO=40

/// 二维码地址
/// 因为我们没有做网页所以随便写一个地址
/// 真实项目中可能会使用主页，或者APP下载界面
let QRCODE_URL="\(ENDPOINT)/v1/monitors/version?u="

/// MARK: - 支付宝
/// 支付宝支付回调
/// 不能和其他应用有重复
/// 用于支付宝客户端回调我们应用
let ALIPAY_CALLBACK_SCHEME = "ixueacourseswift"

/// 支付宝回调
let ON_ALIPAY_CALLBACK = "ON_ALIPAY_CALLBACK"

/// 微信支付回调
let ON_WECHATA_PAY_CALLBACK = "ON_WECHATA_PAY_CALLBACK"

/// 支付成功事件
let ON_PAY_SUCCESS="ON_PAY_SUCCESS"

//当前应用的AppStore地址
//用来点击版本跳转到AppStore
//由于当前应用没有上传到应用商店
//所有随便选择一个地址
//这里就用节奏大师
let MY_APPSTORE_URL = "itms-apps://itunes.apple.com/cn/app/jie-zou-da-shi/id493901993?mt=8"

// MARK: - 加解密相关

/// AES128算法key
let AES128_KEY="wqfrwOSH*gN%I2v6"

/// AES128算法IV
let AES128_IV="VO*1sxQO5nDkcMyj"

// MARK: - 腾讯Bugly

/// AppId
let BUGLY_APP_KEY="d963027715"

// MARK: - 极光统计

/// 极光AppKey
let JIGUANG_APP_KEY="ba3f08e44440a942db10b46e"

/// 渠道
let CHANNEL="default"

// MARK: - 通知

/// 通知类型
let ACTION_KEY="ACTION_KEY"

/// 表示这是聊天消息通知
let ACTION_MESSAGE="ACTION_MESSAGE"

/// Id
let EXTRA_ID="EXTRA_ID"

// MARK: - 聊天

/// 聊天消息改变了通知
let ON_MESSAGE_COUNT_CHANGED="ON_MESSAGE_COUNT_CHANGED"
