//
//  MessageUtil.swift
//  消息工具类
//
//  Created by smile on 2019/7/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class MessageUtil {
    
    /// 获取聊天内容
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func getContent(_ data:JMSGMessage) -> String {
        if data.contentType == .image{
            //图片消息
            return "[图片]"
        }else if data.contentType == .text{
            //文本消息
            return (data.content as! JMSGTextContent).text
        }
        
        return "[其他消息]"
    }
    
    /// 获取全局消息未读数
    /// 也可以在用到的地方
    /// 直接调用SDK接口
    /// 这里还写到工具类的好处是
    /// 使用的时候不用在拆包等一些列判断
    /// - Returns: <#return value description#>
    static func getUnreadMessageCount() -> Int {
//        let count = JMSGConversation.getAllUnreadCount().intValue
//        if count>0 {
//            return count
//        }
//
//        return 0
        
        return JMSGConversation.getAllUnreadCount().intValue
    }
}
