//
//  DataUtil.swift
//  数据工具类
//
//  Created by smile on 2019/6/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class DataUtil: Any {

    /// 更改playList标记
    ///
    /// - Parameters:
    ///   - datum: <#datum description#>
    ///   - playList: <#playList description#>
    static func changePlayListFlag(_ datum:[Song],_ playList:Bool) {
        for data in datum {
            data.playList = playList
        }
    }
    
    /// 根据用户昵称计算出拼音
    ///
    /// - Parameter datas: <#datas description#>
    /// - Returns: <#return value description#>
    static func processUserPinyin(_ datas:[User]) -> [User] {
        //创建拼音格式化输出对象
        // PinyinToneType: none, toneNumber
        // PinyinVCharType: vCharacter, uUnicode, uAndColon
        // PinyinCaseType: lowercased, uppercased, capitalized
        let outputFormat = PinyinOutputFormat(toneType: .none, vCharType: .vCharacter, caseType: .lowercased)
        
        for data in datas {
            //获取全拼
            data.pinyin = data.nickname.toPinyin(withFormat: outputFormat, separator: "")
            
            //获取拼音首字母
            data.pinyinFrist = data.nickname!.toPinyinAcronym()
            
            //拼音首字母的首字母
            data.frist = data.pinyinFrist.substring(toIndex: 1)
        }
        
        return datas
    }
    
    /// 根据用户首字母分组
    ///
    /// - Parameter datas: <#datas description#>
    /// - Returns: <#return value description#>
    static func processUser(_ datas:[User]) -> [UserGroup] {
        //创建结果数组
        var results:[UserGroup] = []
        
        //根据昵称计算拼音
        var users = processUserPinyin(datas)
        
        //按照第一个字母排序
        users = users.sorted(by: { (obj1, obj2) -> Bool in
            return obj1.frist < obj2.frist
        })
        
        //按照首字母进行分组
        //这些操作都可以使用谓词
        //这里为了简单使用了最普通的方法
        //因为只要明白了原理
        //使用谓词就是语法不同而已
        var lastUser:User?
        
        var group:UserGroup!
        
        //遍历所有用户
        for user in users {
            if lastUser != nil && lastUser!.frist == user.frist {
                //同一组
            }else {
                //新建组
                group = UserGroup()
                group.title = user.frist
                group.datas = []
                results.append(group)
            }
            
            //添加用户到组
            group.datas.append(user)
            
            lastUser = user
            
        }
        
        return results
    }
    
    /// 获取用户组标题
    ///
    /// - Parameter datas: <#datas description#>
    /// - Returns: <#return value description#>
    static func processUserLetter(_ datas:[UserGroup]) -> [String] {
        var results:[String] = []
        
        for data in datas {
            results.append(data.title)
        }
        
        return results
    }
    
    /// 过滤用户数据
    ///
    /// - Parameter datas: <#datas description#>
    /// - Returns: <#return value description#>
    static func filterUser(_ datas:[UserGroup],_ searchText:String) -> [UserGroup] {
        //创建一个结果列表
        var results:[UserGroup] = []
        
        //转为小写
        let searchText = searchText.lowercased()
        
        //判断是否是空字符串
        if searchText.isEmpty {
            return results
        }
        
        var lastUser:User?
        
        var group:UserGroup!
        
        for userGroup in datas {
            let users = userGroup.datas
            
            for user in users! {
                //nickanme是否包含搜索的字符串
                //全拼是否包含
                //首字母是否包含
                
                //如果需要更多的条件可以在加
                //条件越多
                //就更容易搜索到
                //但结果就越多
                if user.nickname!.containsIgnoringCase(find: searchText) || user.pinyin.containsIgnoringCase(find: searchText) || user.pinyinFrist.containsIgnoringCase(find: searchText) {
                    
                    //搜索结果分组
                    if lastUser != nil && lastUser!.frist == user.frist {
                        //同一个组
                    }else {
                        //创建一个组
                        group = UserGroup()
                        group.title=user.frist
                        group.datas=[]
                        results.append(group)
                    }
                    
                    group.datas.append(user)
                    
                    lastUser = user
                }
                
            }
        }
        
        return results
    }
    
    /// 对聊天消息按照时间从小到大排序
    /// 也就说昨天的消息在最前面
    /// 今天的消息在最后面
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func sortMessage(_ data:[JMSGMessage]) -> [JMSGMessage] {
        let results=data.sorted { (obj1, obj2) -> Bool in
            return obj1.timestamp.intValue < obj2.timestamp.intValue
        }
        
        return results
    }
}
