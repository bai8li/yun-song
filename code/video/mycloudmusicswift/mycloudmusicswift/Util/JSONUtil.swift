//
//  JSONUtil.swift
//  JSON工具类
//
//  Created by smile on 2020/1/27.
//  Copyright © 2020 ixuea. All rights reserved.
//

import Foundation

class JSONUtil {
    
    /// 将json字符串转为Dictionary
    /// - Parameter data: <#data description#>
    static func toDictionary(_ data:String) -> Dictionary<String,Any>? {
        //转为data
        let jsonData=data.data(using: .utf8)!
        
        //转为字典
        let dict=try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        
        return dict as? Dictionary<String,Any>
    }
}
