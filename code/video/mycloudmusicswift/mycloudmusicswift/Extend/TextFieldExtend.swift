//
//  TextFieldExtend.swift
//  扩展TextField
//
//  Created by smile on 2019/6/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

extension UITextField {
    
    /// 显示左侧图标
    ///
    /// - Parameter name: 图标名称
    func showLeftIcon(name:String) {
        //设置显示图片
        leftView=UIImageView(image: UIImage(named: name))
        
        //设置显示方法
//        leftViewMode = UITextField.ViewMode.always
        leftViewMode = .always
    }
}
