//
//  ThemeManagerExtend.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入主题框架
import SwiftTheme

extension ThemeManager {
    
    /// 是否是夜间模式
    ///
    /// - Returns: <#return value description#>
    static func isNight() -> Bool {
        return ThemeManager.currentThemeIndex==1
    }
}
