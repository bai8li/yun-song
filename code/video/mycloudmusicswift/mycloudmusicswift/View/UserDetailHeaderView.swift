//
//  UserDetailHeaderView.swift
//  用户详情头部View
//
//  Created by smile on 2019/7/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class UserDetailHeaderView: BaseView {
    static let NAME = "UserDetailHeaderView"
    
    /// 背景
    @IBOutlet weak var ivBackground: UIImageView!
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 信息
    @IBOutlet weak var lbInfo: UILabel!
    
    /// 关注按钮
    @IBOutlet weak var btFollow: UIButton!
    
    /// 发送消息按钮
    @IBOutlet weak var btSendMessage: UIButton!
    
    /// 用户
    var data:User!
    
    /// 关注按钮回调方法
    var onFollowClick:((_ data:User)->Void)!
    
    /// 发送消息按钮点击回调方法
    var onSendMessageClick:((_ data:User)->Void)!
    
    override func initViews() {
        super.initViews()
        
        //加载Nib
        let nib = UINib(nibName: UserDetailHeaderView.NAME, bundle: nil)
        
        //创建View
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        //更改frame
        view.frame=bounds
        
        //添加到当前view中
        addSubview(view)
        
        //圆角
        ViewUtil.showRadius(ivAvatar, 45)
        
        ViewUtil.showRadius(btFollow, 20)
        ViewUtil.showRadius(btSendMessage, 20)
        
        //显示边框
        btFollow.showColorPrimaryBorder()
        
        //显示背景
        //由于服务端没有实现用户背景
        //所以这里显示一个默认的背景
        ImageUtil.show(ivBackground, "assets/default_user_detail_background.jpg")
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:User) {
       self.data = data
        
        //显示头像
        ImageUtil.showAvatar(ivAvatar, data.avatar)
        
        //显示昵称
        lbNickname.text=data.nickname
        
        //信息
        lbInfo.text="好友 \(data.followers_count) | 粉丝 \(data.followings_count)"

        //是否显示关注按钮
        if PreferenceUtil.userId()! == data.id {
            //是我们自己
            
            //隐藏关注按钮
            //隐藏发送消息按钮
            btFollow.isHidden=true
            btSendMessage.isHidden=true
        }else {
            btFollow.isHidden=false
            showFollowStatus()
        }
    }

    /// 显示关注相关状态逻辑
    func showFollowStatus() {
        if data.isFollowing() {
            //已经关注了
            
            btFollow.setTitle("取消关注", for: .normal)
            
            //弱化取消关注
            btFollow.backgroundColor=UIColor.clear
            
            //显示发送消息按钮
            btSendMessage.isHidden=false
        } else {
            //没有关注
            
            btFollow.setTitle("关注", for: .normal)
            
            //强化关注
            btFollow.backgroundColor=UIColor(hex: COLOR_PRIMARY)
            
            //隐藏发送消息按钮
            btSendMessage.isHidden=true
        }
    }
    
    /// 关注按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onFollowClick(_ sender: UIButton) {
        print("UserDetailHeadrView onFollowClick")
        
        onFollowClick(data)
    }
    
    
    /// 发送消息按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSendMessageClick(_ sender: UIButton) {
        print("UserDetailHeaderView onSendMessageClick")
        
        onSendMessageClick(data)
    }
}
