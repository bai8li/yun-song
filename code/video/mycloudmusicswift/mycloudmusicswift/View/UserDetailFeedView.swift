//
//  UserDetailFeedView.swift
//  用户详情动态
//
//  Created by smile on 2019/7/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入列表框架
import JXPagingView

class UserDetailFeedView: BaseView {
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 列表数据源
    var dataArray:[Topic] = []
    
    /// 列表的回调
    var listViewDidScrollCallback:((UIScrollView)->())?

    override func initViews() {
        super.initViews()
        
        //从Nib中加载View
        let view = Bundle.main.loadNibNamed("UserDetailFeedView", owner: self, options: nil)!.first as! UIView
        
        addSubview(view)
        
        
        //注册cell
        tableView.register(UINib(nibName: TopicCell.NAME, bundle: nil), forCellReuseIdentifier: TopicCell.NAME)
        
        //设置TableView数据源和代理
        tableView.dataSource=self
        tableView.delegate=self
    }
    
    override func initDatas() {
        super.initDatas()
        
        //添加一些测试数据
        //如果要显示真实数据
        //和歌单列表一样
        //直接从网络获取就行了
        for i in 0..<100 {
            dataArray.append(Topic())
        }
        
        //加载数据源
        tableView.reloadData()
    }

}

// MARK: - 列表数据源和代理
extension UserDetailFeedView:UITableViewDataSource,UITableViewDelegate {
    
    /// 返回有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME, for: indexPath)
        
        return cell
    }
    
    /// 列表滚动
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        listViewDidScrollCallback?(scrollView)
    }
}

// MARK: - JXPagingViewListView代理
extension UserDetailFeedView:JXPagingViewListViewDelegate {
    
    /// 返回要显示的控件
    ///
    /// - Returns: <#return value description#>
    func listView() -> UIView {
        return self
    }
    
    /// 返回内容滚动的控件
    ///
    /// - Returns: <#return value description#>
    func listScrollView() -> UIScrollView {
        return tableView
    }
    
    /// 保存回调的代理
    ///
    /// - Parameter callback: <#callback description#>
    func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        listViewDidScrollCallback=callback
    }
}


