//
//  UserDetailAboutView.swift
//  用户详情关于我
//
//  Created by smile on 2019/7/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入列表框架
import JXPagingView

class UserDetailAboutView: BaseView {

    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 列表的回调
    var listViewDidScrollCallback:((UIScrollView)->())?
    
    
    override func initViews() {
        super.initViews()
        
        //加载view
        let view = Bundle.main.loadNibNamed("UserDetailAboutView", owner: self, options: nil)!.first as! UIView
        
        //添加到当前View中
        addSubview(view)
    }

}

// MARK: - JXPagingViewListView代理
extension UserDetailAboutView:JXPagingViewListViewDelegate{
    
    /// 返回要显示的控件
    ///
    /// - Returns: <#return value description#>
    func listView() -> UIView {
        return self
    }
    
    /// 返回内部的滚动控件
    ///
    /// - Returns: <#return value description#>
    func listScrollView() -> UIScrollView {
        return tableView
    }
    
    /// listView已经滚动回调
    ///
    /// - Parameter callback: <#callback description#>
    func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback=callback
    }
}
