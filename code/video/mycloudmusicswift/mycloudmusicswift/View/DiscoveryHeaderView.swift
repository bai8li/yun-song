//
//  DiscoveryHeaderView.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/6/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class DiscoveryHeaderView: BaseCollectionReusableView {
    
    static let NAME = "DiscoveryHeaderView"
    
    /// 轮播图
    @IBOutlet weak var bannerView: YJBannerView!
    
    /// 显示今天是多少号
    @IBOutlet weak var lbDay: UILabel!
    
    /// 每日推荐标题
    @IBOutlet weak var lbDayTitle: UILabel!
    
    /// 歌单标题
    @IBOutlet weak var lbSheet: UILabel!
    
    /// 私人FM标题
    @IBOutlet weak var lbFM: UILabel!
    
    /// 排行榜
    @IBOutlet weak var lbRank: UILabel!
    
    /// 每日推荐按钮
    @IBOutlet weak var btDay: UIButton!
    
    /// 歌单按钮
    @IBOutlet weak var btSheet: UIButton!
    
    /// 私人FM按钮
    @IBOutlet weak var btFM: UIButton!
    
    /// 排行榜按钮
    @IBOutlet weak var btRank: UIButton!
    
    /// 轮播图点击回调方法
    var onBannerClick:((_ data:Ad) -> Void)!
    
    
    /// 轮播图的数据
    var dataArray:[String] = []
    
    /// 用来保存广告数据
    var data:[Ad]!
    
    /// 当系统创建完成后Nib后
    /// 因为我们在可视化中关联他
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//        //设置轮播图圆角
//        ViewUtil.showSmallRadius(bannerView)
//
//        initBannerView()
//    }
    
    override func initViews() {
        super.initViews()
        
        //设置轮播图圆角
        ViewUtil.showSmallRadius(bannerView)

        initBannerView()
        
        //主题
        //文本
        lbDay.theme_textColor=[COLOR_STRING_PRIMARY,COLOR_STRING_DARK_BLACK]
        lbDayTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        lbSheet.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        lbFM.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        lbRank.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //图片
        //每日推荐
        btDay.theme_setImage(["Day","DayGrey"], forState: .normal)
        btDay.theme_setImage(["DaySelected","DaySelectedGrey"], forState: .highlighted)
        
        //歌单
        btSheet.theme_setImage(["Sheet","SheetGrey"], forState: .normal)
        btSheet.theme_setImage(["SheetSelected","SheetSelectedGrey"], forState: .highlighted)
        
        //私人FM
        btFM.theme_setImage(["FM","FMNight"], forState: .normal)
        btFM.theme_setImage(["FMSelected","FMNightSelected"], forState: .highlighted)
        
        //排行榜
        btRank.theme_setImage(["Rank","RankGrey"], forState: .normal)
        btRank.theme_setImage(["RankSelected","RankSelectedGrey"], forState: .highlighted)
    }
    
    /// 初始化BannerView
    func initBannerView() {
        //设置轮播图数据源为当前类
        bannerView.dataSource=self
        
        //设置轮播图的代理为当前类
        bannerView.delegate=self
        
        //设置如果找不到图片显示的图片
        bannerView.emptyImage=UIImage(named: IMAGE_PLACE_HOLDER)
        
        //设置占位图
        bannerView.placeholderImage=UIImage(named: IMAGE_PLACE_HOLDER)
        
        //设置轮播图内部显示图片的时候调用什么方法
        bannerView.bannerViewSelectorString="sd_setImageWithURL:placeholderImage:"
        
        //设置指示器默认颜色
        bannerView.pageControlNormalColor=UIColor(hex: COLOR_LIGHT_GREY)
        
        //高亮的颜色
        bannerView.pageControlHighlightColor=UIColor(hex: COLOR_PRIMARY)
        
        //重新载入数据
//        bannerView.reloadData()
    }
    
    override func initDatas() {
        super.initDatas()
        
        //设置每日推荐按钮显示的数据
        
        //获取当前日期
        let now = Date()
        
        //创建一个日期格式化器
        let formatter = DateFormatter()
        
        //只获取天（2位）
        formatter.dateFormat = "dd"
        
        //格式化日期
        let day = formatter.string(from: now)
        
        //将格式化后的字符串设置label
        lbDay.text=day
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:[Ad]) {
        self.data=data
        
        //清除原来的数据
        dataArray.removeAll()
        
        //循环每一个广告
        //取出广告的地址
        //放到一个数组中
        for ad in data {
//            dataArray.append("\(RESOURCE_ENDPOINT)/\(ad.banner!)")
            dataArray.append(ResourceUtil.resourceUri(ad.banner))
        }
        
        //通知轮播图框架从新加载数据
        bannerView.reloadData()
    }
    
    
    // MARK: - 快捷按钮点击事件
    
    /// 每日推荐按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDayClick(_ sender: Any) {
        print("DiscoveryHeaderView onDayClick")
    }
    
    
    /// 歌单按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSheetClick(_ sender: Any) {
        print("DiscoveryHeaderView onSheetClick")
    }
    
    
    /// 私人FM点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onFMClick(_ sender: Any) {
        print("DiscoveryHeaderView onFMClick")
    }
    
    /// 排行榜点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onRankClick(_ sender: Any) {
        print("DiscoveryHeaderView onRankClick")
    }
    
}


// MARK: - banner数据源和代理
extension DiscoveryHeaderView:YJBannerViewDataSource,YJBannerViewDelegate{
    
    /// 返回BannerView要显示的数据
    ///
    /// - Parameter bannerView: <#bannerView description#>
    /// - Returns: <#return value description#>
    func bannerViewImages(_ bannerView: YJBannerView!) -> [Any]! {
        return dataArray
    }
    
    /// 自定义Cell
    /// 复写该方法的目的是
    /// 设置图片的缩放模式
    ///
    /// - Parameters:
    ///   - bannerView: <#bannerView description#>
    ///   - customCell: <#customCell description#>
    ///   - index: <#index description#>
    /// - Returns: <#return value description#>
    func bannerView(_ bannerView: YJBannerView!, customCell: UICollectionViewCell!, index: Int) -> UICollectionViewCell! {
        //将cell类型转为YJBannerViewCell
        let cell = customCell as! YJBannerViewCell
        
        //设置图片的缩放模式为
        //从中心填充
        //多余的裁剪掉
        cell.showImageViewContentMode = .scaleAspectFill
        
        return cell
    }
    
    /// banner点击回调方法
    ///
    /// - Parameters:
    ///   - bannerView: <#bannerView description#>
    ///   - index: <#index description#>
    func bannerView(_ bannerView: YJBannerView!, didSelectItemAt index: Int) {
        print("DiscoveryHeaderView didSelectItemAtIndex:\(index)")
        
        //获取当前点击的广告对象
        let ad = data[index]
        
        //调用轮播图回调方法
        onBannerClick(ad)
    }
}
