//
//  VideoDetailUserInfoHeaderView.swift
//  视频详情用户信息HeaderView
//
//  Created by smile on 2019/7/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoDetailUserInfoHeaderView: BaseTableViewHeaderFooterView {
    static let NAME = "VideoDetailUserInfoHeaderView"
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 关注按钮
    @IBOutlet weak var btFollow: UIButton!
    
    override func initViews() {
        super.initViews()
        
        //头像圆角
        ViewUtil.showRadius(ivAvatar, 20)
        
        //显示边框
        btFollow.showColorPrimaryBorder()
    }
    
    func bindData(_ data:User) {
        ImageUtil.showAvatar(ivAvatar, data.avatar)
        lbNickname.text=data.nickname
    }

    
    /// 关注按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onFollowClick(_ sender: Any) {
        print("VideoDetailUserInfoHeaderView onFollowClick")
    }
}
