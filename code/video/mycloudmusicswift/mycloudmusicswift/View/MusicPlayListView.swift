//
//  MusicPlayListView.swift
//  播放列表控件
//
//  Created by smile on 2019/6/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class MusicPlayListView: BaseView {
    
    var contentView:UIView!
    
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 播放列表管理器
    var playListManager:PlayListManager!
    
    /// 点击音乐回调
    var onSongClick:((_ data:Song) -> Void)!
    
    
    override func initViews() {
        super.initViews()
        
        //加载Nib控件
        contentView = Bundle.main.loadNibNamed("MusicPlayListView", owner: self, options: nil)!.first as! UIView
        
        //添加到当前View中
        addSubview(contentView)
        
        //设置数据源和代理
        tableView.dataSource=self
        tableView.delegate=self
        
        //注册Cell
        tableView.register(UINib(nibName: SongListCell.NAME, bundle: nil), forCellReuseIdentifier: SongListCell.NAME)
    }
    
    override func initDatas() {
        super.initDatas()
        //初始化播放列表管理器
        playListManager = PlayListManager.shared
        
        //选中当前播放的音乐
        scrollPositionAsync()
    }
    
    /// 选中当前播放的这首音乐
    func scrollPosition() {
        let dataArray = playListManager.getPlayList()
        
        var index = -1
        
        //获取当前播放的这首音乐
        let data = playListManager.data!
        
        //变量所有音乐
        //找到当前这首音乐
        for e in dataArray.enumerated() {
            if e.element.id == data.id {
                index = e.offset
                break
            }
        }
        
        //创建一个IndexPath
        let indexPath = IndexPath(item: index, section: 0)
        
        //选中这一行
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
        
    }
    
    /// 异步选中当前播放的音乐
    func scrollPositionAsync() {
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.scrollPosition()
            
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //设置内容View的尺寸
        contentView.frame = bounds
    }
    
    

    /// 删除所有点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDeleteAllClick(_ sender: Any) {
        print("MusicPlayListView onDeleteAllClick")
    }
}

// MARK: - 列表数据源和代理
extension MusicPlayListView: UITableViewDataSource,UITableViewDelegate {
    
    /// 返回有多个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playListManager.getPlayList().count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: SongListCell.NAME) as! SongListCell
        
        //取出当前位置的数据
        let data = playListManager.getPlayList()[indexPath.row]
        
        //设置Tag
        cell.tag = indexPath.row
        
        //绑定数据
        cell.bindData(data)
        
        //返回Cell
        return cell
    }
    
    /// 点击Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //取出数据
        let data = playListManager.getPlayList()[indexPath.row]
        
        //回调方法
        onSongClick(data)
    }
}
