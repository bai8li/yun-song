//
//  BaseTableViewHeaderFooterView.swift
//  通用TableView头部和底部
//
//  Created by smile on 2019/6/13.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseTableViewHeaderFooterView: UITableViewHeaderFooterView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        initViews()
        initDatas()
        initListeners()
    }

    /// 初始化控件
    func initViews() {
        
    }
    
    /// 初始化数据
    func initDatas() {
        
    }
    
    /// 初始化监听器
    func initListeners() {
        
    }
}
