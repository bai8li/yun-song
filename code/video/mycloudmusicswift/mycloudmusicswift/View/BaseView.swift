//
//  BaseView.swift
//  通用View
//
//  Created by smile on 2019/6/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入RxSwift
import RxSwift

class BaseView: UIView {
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag=DisposeBag()

    
    /// 使用代码创建一个View会调用该构造方法
    ///
    /// - Parameter frame: <#frame description#>
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        innerInit()
    }
    
    /// 可视化布局的时候
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        innerInit()
    }

    func innerInit() {
        initViews()
        initDatas()
        initListeners()
    }
    
    /// 初始化控件
    func initViews() {
        
    }
    
    /// 初始化数据
    func initDatas() {
        
    }
    
    /// 初始化监听器
    func initListeners() {
        
    }
}
