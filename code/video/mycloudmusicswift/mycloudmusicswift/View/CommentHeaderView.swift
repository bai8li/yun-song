//
//  CommentHeaderView.swift
//  评论分组标题
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class CommentHeaderView: BaseTableViewHeaderFooterView {
    static let NAME = "CommentHeaderView"
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    func bindData(_ data:String) {
        lbTitle.text = data
    }
}
