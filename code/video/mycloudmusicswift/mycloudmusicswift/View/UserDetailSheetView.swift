//
//  UserDetailSheetView.swift
//  用户详情音乐
//
//  Created by smile on 2019/7/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入列表框架
import JXPagingView

class UserDetailSheetView: BaseView {
    
    /// 列表控件
    var tableView:UITableView!
    
    /// 用户Id
    var userId:String!
    
    /// 列表数据源
    var dataArray:[SheetGroup] = []
    
    /// 列表的回调
    var listViewDidScrollCallback:((UIScrollView)->())?
    
    /// 歌单点击回调事件
    var onSheetClick:((_ data:Sheet)->Void)!
    
    override func initViews() {
        super.initViews()
        
        //创建TableView
        tableView=UITableView(frame: frame)
        
        //设置Cell高度
        tableView.rowHeight=SIZE_TITLE_CELL_HEIGHT
        
        //注册header
        tableView.register(UINib(nibName: CommentHeaderView.NAME, bundle: nil), forHeaderFooterViewReuseIdentifier: CommentHeaderView.NAME)
        
        //注册歌单Cell
        tableView.register(UINib(nibName: TopicCell.NAME, bundle: nil), forCellReuseIdentifier: TopicCell.NAME)
        
        //设置数据源和代理
        tableView.delegate=self
        tableView.dataSource=self
        
        //添加tableView到当前View
        addSubview(tableView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        //设置tableview的大小
        tableView.frame=bounds
    }
    
    /// 自定义初始化方法
    func customInit() {
        fetchData()
    }
    
    /// 获取数据
    func fetchData() {
        //请求创建的歌单
        Api.shared.createSheets(userId).subscribeOnSuccess { (data) in
            let group = SheetGroup()
            group.title="创建的歌单"
            group.datas=data?.data?.data ?? []
            
            self.dataArray.append(group)
            
            //请求收藏的歌单
            Api.shared.collectSheets(self.userId).subscribeOnSuccess({ (data) in
                let group = SheetGroup()
                group.title="收藏的歌单"
                group.datas=data?.data?.data ?? []
                
                self.dataArray.append(group)
                
                self.tableView.reloadData()
            }).disposed(by: self.disposeBag)
            
        }.disposed(by: disposeBag)
    }
}

// MARK: - 列表数据源和代理
extension UserDetailSheetView:UITableViewDataSource,UITableViewDelegate {
    
    /// 有多少组
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    /// 有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group=dataArray[section]
        
        return group.datas.count
    }
    
    /// 返回当前位置cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME, for: indexPath) as! TopicCell
        
        let group=dataArray[indexPath.section]
        let data=group.datas[indexPath.row]
        
        //绑定数据
        cell.bindData(data)
        
        //返回cell
        return cell
    }
    
    /// cell点击
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onSheetClick(dataArray[indexPath.section].datas[indexPath.row])
    }
    
    /// 返回header
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        //获取header
        let header=tableView.dequeueReusableHeaderFooterView(withIdentifier: CommentHeaderView.NAME) as! CommentHeaderView
        
        //绑定数据
        header.bindData(dataArray[section].title)
        
        //返回header
        return header
        
    }
    
    /// 列表滚动
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        listViewDidScrollCallback?(scrollView)
    }
}

// MARK: - JXPagingViewListView代理
extension UserDetailSheetView:JXPagingViewListViewDelegate{
    
    /// 返回要显示的控件
    ///
    /// - Returns: <#return value description#>
    func listView() -> UIView {
        return self
    }
    
    /// 返回内部的滚动控件
    ///
    /// - Returns: <#return value description#>
    func listScrollView() -> UIScrollView {
        return tableView
    }
    
    /// listView已经滚动回调
    ///
    /// - Parameter callback: <#callback description#>
    func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback=callback
    }
}
