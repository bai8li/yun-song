//
//  TitleCell.swift
//  发现页面标题Cell
//
//  Created by smile on 2019/6/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TitleCell: BaseCollectionViewCell {
    
    /// 当前Cell的名称
    static let NAME = "TitleCell"
    
    /// 显示标题
    @IBOutlet weak var lbTitle: UILabel!
    
    override func initViews() {
        super.initViews()
        
        //主题
        lbTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:String) {
        lbTitle.text=data
    }

}
