//
//  ShopCell.swift
//  商品列表Cell
//
//  Created by smile on 2019/7/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ShopCell: BaseTableViewCell {
    static let NAME = "ShopCell"
    
    /// 封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 价格
    @IBOutlet weak var lbPrice: UILabel!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(ivBanner)
    }
    
    func bindData(_ data:Book) {
        ImageUtil.show(ivBanner, data.banner)
        lbTitle.text=data.title
        lbPrice.text="￥\(String(data.price))"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
