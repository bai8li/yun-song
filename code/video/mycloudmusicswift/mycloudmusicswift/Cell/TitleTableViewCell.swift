//
//  TitleTableViewCell.swift
//  首页-我的-标题Cell
//
//  Created by smile on 2019/7/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TitleTableViewCell: BaseTableViewCell {
    static let NAME="TitleTableViewCell"
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 更多按钮
    @IBOutlet weak var btMore: UIButton!
    
    /// 更多点击回调方法
    var onMoreClick:(()->Void)?
    
   
    func bindData(_ data:Title) {
        lbTitle.text=data.title
        btMore.isHidden = !data.isShowMore
    }
    
    func bindData(_ data:String) {
        lbTitle.text=data
        btMore.isHidden=true
    }
    
    /// 更多按钮点击回调
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onMoreClick(_ sender: UIButton) {
        print("TitleTableViewCell onMoreClick")
        
        if let onMoreClick = onMoreClick {
            onMoreClick()
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
