//
//  BaseFeedCell.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/7/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseFeedCell: BaseTableViewCell {

    /// 用户信息容器
    @IBOutlet weak var svUserInfo: UIStackView!
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 时间
    @IBOutlet weak var lbTime: UILabel!
    
    /// 内容
    @IBOutlet weak var lbContent: UILabel!
    
    /// 动态
    var data:Feed!
    
    /// 用户信息点击回调
    var onUserInfoClick:((_ data:User)->Void)!
    
    override func initViews() {
        super.initViews()
        
        //圆角
        ViewUtil.showRadius(ivAvatar, 17.5)
    }
    
    override func initListeners() {
        super.initListeners()
        
        //用户信息点击事件
        svUserInfo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onUserInfoInnerClick)))
    }
    
    /// 用户信息点击
    @objc func onUserInfoInnerClick() {
        onUserInfoClick(data.user)
    }
    
    func bindData(_ data:Feed) {
        self.data=data
        
        //头像
        ImageUtil.showAvatar(ivAvatar, data.user.avatar)
        
        //昵称
        lbNickname.text=data.user.nickname
        
        //时间
        lbTime.text=TimeUtil.iso8601ToCommonFormat(data.created_at)
        
        //动态文本
        //lbContent.text=data.content
        
        //因为要设置行高
        //所以用属性文本
        
        //创建属性文本
        let contentAttribute = NSMutableAttributedString(string: data.content)
        
        //判断是否有地理位置
        if let province = data.province {
            contentAttribute.yy_appendString("\n来自：\(province).\(data.city!)")
        }
        
        //创建一个行样式
        let style = NSMutableParagraphStyle.init()
        
        //设置行高
        style.lineSpacing = 8.0
        
        //将样式添加到属性文本中
        contentAttribute.addAttribute(.paragraphStyle, value: style, range: NSRange(location: 0, length: contentAttribute.length))
        
        //设置到控件
        lbContent.attributedText = contentAttribute
    }

}
