//
//  BaseChatImageCell.swift
//  通用图片类型cell
//
//  Created by smile on 2019/7/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseChatImageCell: BaseChatCell {

    /// 图片
    @IBOutlet weak var ivBanner: UIImageView!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(ivBanner)
    }
    
    override func bindData(_ data: JMSGMessage) {
        super.bindData(data)
        
        //清除原来的图片
        ivBanner.image=UIImage(named: "PlaceHolder")
        
        //将内容转为图片内容
        let content = data.content as! JMSGImageContent
        
        //获取图片
        content.largeImageData(progress: nil) { (imageData, msgId, error) in
            if msgId == data.msgId {
                //防止图片错误
                
                //判断消息Id和下载图片的消息Id是否一样
                
                //显示图片
                if let imageData=imageData{
                    let image=UIImage(data: imageData)
                    self.ivBanner.image=image
                }
            }
        }
    }
}
