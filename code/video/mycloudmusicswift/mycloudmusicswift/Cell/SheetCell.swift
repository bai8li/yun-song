//
//  SheetCell.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/6/12.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SheetCell: BaseCollectionViewCell {
    
    static let NAME = "SheetCell"
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    
    /// 封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 点击量图标
    @IBOutlet weak var ivClickCount: UIImageView!
    
    /// 点击量
    @IBOutlet weak var lbCount: UILabel!
    
    override func initViews() {
        super.initViews()
        
        //设置封面圆角
        ViewUtil.showSmallRadius(ivBanner)
        
        //主题
        //标题
        lbTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //点击数文本
        lbCount.theme_textColor=[COLOR_STRING_WHITE,COLOR_STRING_LIGHT_GREY]
        
        //点击数图标
        ivClickCount.theme_image=["ClickCount","ClickCountNight"]
    }
    
    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Sheet)  {
        //显示标题
        lbTitle.text=data.title
        
        //设置点击量
        lbCount.text="\(data.clicks_count)"
        
        //设置图片
        if let imageUri = data.banner {
            //有图片
            
//            //把图片地址转为绝对路径
//            let imageUri=ResourceUtil.resourceUri(imageUri)
//
//            //创建URL
//            let url = URL(string: imageUri)
//
//            //占位图
//            let placeholderImage = UIImage(named: "PlaceHolder")
//
//            //显示网络图片
//            ivBanner.sd_setImage(with: url, placeholderImage: placeholderImage, options: [], completed: nil)

            //使用重构后的方法
            ImageUtil.show(ivBanner, imageUri)
        }else {
            //没有图片
            
            //设置默认图片
            ivBanner.image = UIImage(named: "PlaceHolder")
        }
    }

}
