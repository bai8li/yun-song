//
//  SelectImageCell.swift
//  选择图片后预览图片Cell
//
//  Created by smile on 2019/7/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SelectImageCell: BaseCollectionViewCell {
    
    /// 图片控件
    @IBOutlet weak var ivImage: UIImageView!
    
    
    /// 删除按钮
    @IBOutlet weak var btDelete: UIButton!
    
    /// 图片点击删除回调
    var onDeleteClick:((_ index:Int)->Void)!
    
    override func initViews() {
        super.initViews()
        
        //显示圆角
        ViewUtil.showSmallRadius(ivImage)
    }
    
    func bindData(_ data:Any) {
        
        if data is String {
            //添加图片按钮
            btDelete.isHidden=true
            ivImage.image=UIImage(named: data as! String)
        } else {
            //真实的图片
            btDelete.isHidden=false
            ivImage.image=data as! UIImage
        }
    }
    
    /// 点击删除回调
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDeleteClick(_ sender: UIButton) {
        onDeleteClick(tag)
    }
    
}
