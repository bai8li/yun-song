//
//  UserCell.swift
//  用户Cell
//
//  Created by smile on 2019/7/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class UserCell: BaseTableViewCell {
    static let NAME = "UserCell"
    

    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 信息
    @IBOutlet weak var lbInfo: UILabel!
    
    override func initViews() {
        super.initViews()
        
        //圆角
        ViewUtil.showRadius(ivAvatar, 30)
    }
    
    func bindData(_ data:User) {
        //显示头像
        ImageUtil.showAvatar(ivAvatar, data.avatar)
        
        //显示昵称
        lbNickname.text=data.nickname
        
        //显示描述
        lbInfo.text = data.formatDescription
    }
}
