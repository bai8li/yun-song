//
//  ButtonCell.swift
//  首页-我的-顶部按钮Cell
//
//  Created by smile on 2019/7/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ButtonCell: BaseTableViewCell {
    static let NAME="ButtonCell"

    /// 图标
    @IBOutlet weak var ivIcon: UIImageView!
    
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    func bindData(_ data:ButtonInfo) {
        ivIcon.image=UIImage(named: data.icon)
        lbTitle.text=data.title
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
