//
//  OrderCell.swift
//  订单Cell
//
//  Created by smile on 2019/7/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class OrderCell: BaseTableViewCell {
    static let NAME = "OrderCell"
    
    /// 订单号
    @IBOutlet weak var lbNumber: UILabel!
    
    /// 状态
    @IBOutlet weak var lbStatus: UILabel!
    
    /// 商品图片
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 商品标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 价格
    @IBOutlet weak var lbPrice: UILabel!
    
    override func initViews() {
        super.initViews()
        //圆角
        ViewUtil.showSmallRadius(ivBanner)
    }
    
    func bindData(_ data:Order) {
        //订单号
        lbNumber.text=data.number
        
        //状态
        lbStatus.text=data.formatStatus
        
        if data.status == .payed {
            //已经支付
            lbStatus.textColor=UIColor(named: "ColorPass")
        } else {
            //其他状态
            lbStatus.textColor=UIColor.lightGray
        }
        
        //显示封面
        ImageUtil.show(ivBanner, data.book.banner)
        
        //标题
        lbTitle.text=data.book.title
        
        //价格
        lbPrice.text="优惠价：￥\(String(data.price))"
    }
    
}
