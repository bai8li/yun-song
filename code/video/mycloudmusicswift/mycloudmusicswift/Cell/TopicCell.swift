//
//  TopicCell.swift
//  话题Cell
//
//  Created by smile on 2019/7/1.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TopicCell: BaseTableViewCell {
    static let NAME = "TopicCell"

    /// 封面图
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 描述
    @IBOutlet weak var lbDescription: UILabel!
    
    override func initViews() {
        super.initViews()
        
        //圆角
        ViewUtil.showSmallRadius(ivBanner)
    }
    
    /// 显示话题数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Topic) {
        ImageUtil.show(ivBanner, data.banner)
        lbTitle.text="#\(data.title!)#"
        lbDescription.text="\(data.joins_count)人参与"
    }
    
    /// 显示歌单数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Sheet) {
       ImageUtil.show(ivBanner, data.banner)
        lbTitle.text=data.title
        lbDescription.text="\(data.songs_count)首"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
