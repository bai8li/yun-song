//
//  LyricCell.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/6/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class LyricCell: BaseTableViewCell {

    
    /// 歌词控件
//    @IBOutlet weak var lbContent: UILabel!
    
    
    /// 歌词控件（自定义）
    @IBOutlet weak var line: LyricLineView!
    
    func bindData(_ data:Any,_ accurate:Bool) {
        
        //使用Label实现
//        if data is String {
//            //是占位数据
//            lbContent.text = ""
//        } else {
//            //真实歌词数据
//            let lyricLine = data as! LyricLine
//            lbContent.text=lyricLine.data
//        }
        
        //使用自定义控件实现
        if data is String {
            //是占位数据
            line.data = nil
            line.isAccurate = false
        } else {
            //真实歌词数据
            line.data = data as! LyricLine
            line.isAccurate = accurate
        }
    }

    /// 当TableView选中或者取消选中调用
    ///
    /// - Parameters:
    ///   - selected: <#selected description#>
    ///   - animated: <#animated description#>
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        //逐行滚动歌词
//        if selected {
//            //选中状态
//
//            //主色调
//            lbContent.textColor = UIColor(hex: COLOR_PRIMARY)
//        } else {
//            //未选中
//
//            //默认颜色
//            lbContent.textColor = UIColor.lightGray
//        }
        
        //自定义控件实现
        line.isSelected = selected
        
        //标志位需要绘制
        line.setNeedsDisplay()
    }
    
    /// 设置播放进度
    ///
    /// - Parameter progress: <#progress description#>
    func show(_ progress:Float) {
        line.show(progress)
        
        line.setNeedsDisplay()
    }

}
