//
//  DownloadCell.swift
//  下载Cell
//
//  Created by smile on 2019/7/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

class DownloadCell: BaseTableViewCell {

    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 信息
    @IBOutlet weak var lbInfo: UILabel!
    
    /// 下载进度
    @IBOutlet weak var pvProgress: UIProgressView!
    
    /// 下载信息
    var data:DownloadInfo!
    
    
    func bindData(_ data:DownloadInfo) {
        self.data=data
        
        //根据Id查询业务信息
        let song = ORMUtil.shared().findSongById(data.id)!
        
        lbTitle.text=song.title
        
        setDownloadCallback()
        
        refresh(false)
    }
    
    /// 设置下载回调
    func setDownloadCallback() {
        weak var weakSelf = self
        
        //设置下载回调
        data.downloadBlock = {
            downloadInfo in
            weakSelf?.refresh(true)
        }
    }
    
    /// 刷新状态
    ///
    /// - Parameter isDownloadManagerNotify: <#isDownloadManagerNotify description#>
    func refresh(_ isDownloadManagerNotify:Bool) {
        switch data.status {
        case .paused:
            //暂停
            pvProgress.isHidden=true
            lbInfo.text="已暂停，点击继续下载"
        case .error:
            //下载失败了
            pvProgress.isHidden=true
            lbInfo.text="下载失败，点击重试"
        case .downloading, .prepareDownload:
            //下载中，准备下载
            pvProgress.isHidden=false
            
            //计算进度
            if data.size > 0 {
                //计算下载百分比
                //0~1
                pvProgress.progress=Float(Double(data.progress)/Double(data.size))
                
                //格式化进度
                let start=FileUtil.formatFileSize(data.progress)
                let size=FileUtil.formatFileSize(data.size)
                lbInfo.text="\(start)/\(size)"
            }
        case .completed:
            //下载完成
            
            //该界面不会显示该状态的任务
            //所以要发送通知
            //然界面接收到
            //从而移除这样的任务
            publishDownloadStatusChangedEvent(isDownloadManagerNotify)
        case .wait:
            //等待中
            pvProgress.isHidden=true
            lbInfo.text="等待中，点击暂停"
        default:
            //未下载状态
            
            //点击删除任务后
            publishDownloadStatusChangedEvent(isDownloadManagerNotify)
        }
    }
    
    /// 发送数据改变了通知
    func publishDownloadStatusChangedEvent(_ isDownloadManagerNotify:Bool) {
        if isDownloadManagerNotify {
            SwiftEventBus.post(ON_DOWNLOAD_STATUS_CHANGED)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
