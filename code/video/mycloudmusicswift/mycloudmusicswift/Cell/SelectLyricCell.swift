//
//  SelectLyricCell.swift
//  选择歌词Cell
//
//  Created by smile on 2019/6/29.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SelectLyricCell: BaseTableViewCell {
    
    
    /// 选中标识
    @IBOutlet weak var ivSelected: UIImageView!
    
    /// 歌词标题
    @IBOutlet weak var lbTitle: UILabel!

    
    func bindData(_ data:LyricLine) {
        lbTitle.text = data.data
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            ivSelected.isHidden=false
        } else {
            ivSelected.isHidden=true
        }
    }

}
