//
//  VideoDetailInfoCell.swift
//  视频详情Cell
//  就是显示视频标题；发布日期的这个Cell
//
//  Created by smile on 2019/7/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入Tag框架
import TagListView

class VideoDetailInfoCell: BaseTableViewCell {
    static let NAME = "VideoDetailInfoCell"

    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 创建时间
    @IBOutlet weak var lbCreatedAt: UILabel!
    
    /// 播放次数
    @IBOutlet weak var lbPlayCount: UILabel!
    
    
    /// Tag控件
    @IBOutlet weak var tlTag: TagListView!
    
    /// 点赞数
    @IBOutlet weak var lbLikeCount: UILabel!
    
    /// 点赞容器
    @IBOutlet weak var svLike: UIStackView!
    
    override func initListeners() {
        super.initListeners()
        
        //点赞点击事件
        svLike.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onLikeClick)))
    }
    
    /// 点赞按钮回调
    @objc func onLikeClick() {
        print("VideoDetailInfoCell onLikeClick")
    }
    
    func bindData(_ data:Video) {
        //标题
        lbTitle.text=data.title
        
        //创建时间
        lbCreatedAt.text="创建时间：\(TimeUtil.date2yyyyMMddHHmmss(data.created_at))"
        
        //播放次数
        lbPlayCount.text="播放：\(data.clicks_count)"
        
        //点赞数
        lbLikeCount.text="\(data.likes_count)"
        
        //先移除原来的Tag
        tlTag.removeAllTags()
        
        //添加一个Tag
        tlTag.addTag("爱学啊")
        
        //添加多个Tag
        tlTag.addTags(["创意音乐","幽默","说唱"])
        tlTag.addTags(["流行音乐","搞笑","古典音乐"])
        tlTag.addTags(["乡村","大闹","民谣音乐"])
    }
    
}
