//
//  MessageCell.swift
//  会话Cell
//
//  Created by smile on 2019/7/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class MessageCell: BaseTableViewCell {
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 时间
    @IBOutlet weak var lbTime: UILabel!
    
    /// 消息
    @IBOutlet weak var lbMessage: UILabel!
    
    /// 未读消息数
    @IBOutlet weak var lbCount: UILabel!
    
    override func initViews() {
        super.initViews()
        
        //圆角
        ViewUtil.showRadius(ivAvatar, 30)
        ViewUtil.showRadius(lbCount, 10)
    }
    
    func bindData(_ data:JMSGConversation) {
        //获取最后一条时间
        
        //将毫秒转为秒
        let time=data.latestMsgTime.intValue/1000
        
        //将时间戳解析为date
        let date=Date(timeIntervalSince1970: TimeInterval(time))
        
        //格式化日期
        lbTime.text=TimeUtil.iso8601ToCommonFormat(date)
        
        //显示最后一条消息内容
        lbMessage.text=data.latestMessageContentText()
        
        //获取会话目标用户
        let user=data.target as! JMSGUser
        
        //获取用户信息
        UserManager.shared().getUser(StringUtil.unwrap(user.username)) { (data) in
            self.lbNickname.text=data.nickname
            ImageUtil.showAvatar(self.ivAvatar, data.avatar)
        }
        
        //未读消息数
        if let count=data.unreadCount?.intValue,count > 0 {
            //有未读消息
            lbCount.isHidden=false
            
            //判断是否大于99
            if count > 99 {
                lbCount.text="99+"
            }else{
                lbCount.text="\(count)"
            }
        }else{
            //没有未读消息
            lbCount.isHidden=true
        }
    }
}
