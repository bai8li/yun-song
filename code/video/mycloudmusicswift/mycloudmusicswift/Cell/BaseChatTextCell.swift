//
//  BaseChatTextCell.swift
//  通用文本类型cell
//
//  Created by smile on 2019/7/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseChatTextCell: BaseChatCell {

    /// 消息内容
    @IBOutlet weak var lbContent: UILabel!
    
    override func bindData(_ data: JMSGMessage) {
        super.bindData(data)
        
        //消息内容
        let content=data.content as! JMSGTextContent
        
        //显示消息内容
        lbContent.text=content.text
    }
}
