//
//  VideoDetailVideoCell.swift
//  相关视频Cell
//
//  Created by smile on 2019/7/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoDetailVideoCell: BaseTableViewCell {
    static let NAME = "VideoDetailVideoCell"
    
    /// 封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 信息
    @IBOutlet weak var lbInfo: UILabel!
    
    override func initViews() {
        super.initViews()
        ViewUtil.showSmallRadius(ivBanner)
    }
    
    func bindData(_ data:Video)  {
        ImageUtil.show(ivBanner, data.banner)
        lbTitle.text=data.title
        
        let timeString=TimeUtil.second2MinuteAndSecond(Float(data.duration))
        lbInfo.text="\(timeString)，by \(data.user.nickname!)"
    }
    
}
