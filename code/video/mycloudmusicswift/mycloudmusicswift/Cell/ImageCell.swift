//
//  ImageCell.swift
//  动态列表-图片动态-图片
//
//  Created by smile on 2019/7/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ImageCell: BaseCollectionViewCell {
    static let NAME = "ImageCell"
    
    @IBOutlet weak var ivImage: UIImageView!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(ivImage)
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Resource) {
        ImageUtil.show(ivImage, data.uri)
    }

}
