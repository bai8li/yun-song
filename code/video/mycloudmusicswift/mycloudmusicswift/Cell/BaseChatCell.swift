//
//  BaseChatCell.swift
//  通用聊天消息Cell
//
//  Created by smile on 2019/7/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseChatCell: BaseTableViewCell {
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    override func initViews() {
        super.initViews()
        
        //圆角
        ViewUtil.showRadius(ivAvatar, 20)
    }
    
    func bindData(_ data:JMSGMessage) {
        let user=data.fromUser
        
        //获取用户信息
        UserManager.shared().getUser(StringUtil.unwrap(user.username)) { (data) in
            //显示头像
            ImageUtil.showAvatar(self.ivAvatar, data.avatar)
        }
    }
}
