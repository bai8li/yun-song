//
//  SongCell.swift
//  歌单Cell
//
//  Created by smile on 2019/6/12.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SongCell: BaseCollectionViewCell {
    static let NAME = "SongCell"
    

    /// 封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 点击量
    @IBOutlet weak var lbCount: UILabel!
    
    
    /// 歌手头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 歌手昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 分割线
    @IBOutlet weak var vwDivider: UIView!
    
    override func initViews() {
        super.initViews()
        
        //设置封面圆角
        ViewUtil.showSmallRadius(ivBanner)
        
        //歌手头像圆角
        ViewUtil.showRadius(ivAvatar, 12)
        
        //主题
        //标题
        lbTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //昵称
        lbNickname.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //分割线
        vwDivider.theme_backgroundColor=[COLOR_STRING_GROUP_TABLE_VIEW_BACKGROUND,COLOR_STRING_SEARCH_BACKGROUND]
    }
    
    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Song) {
        //显示图片
        ImageUtil.show(ivBanner, data.banner)
        
        //标题
        lbTitle.text=data.title
        
        //点击量
        lbCount.text="\(data.clicks_count)"
        
        //歌手头像
        ImageUtil.showAvatar(ivAvatar,data.singer.avatar)
        
        //歌手昵称
        lbNickname.text=data.singer.nickname
    }

}
