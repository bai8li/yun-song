//
//  VideoCell.swift
//  视频Cell
//
//  Created by smile on 2019/7/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoCell: BaseTableViewCell {
    
    /// 封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 类型
    @IBOutlet weak var btType: UIButton!
    
    /// 播放数
    @IBOutlet weak var lbPlayCount: UILabel!
    
    /// 时长
    @IBOutlet weak var lbTime: UILabel!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 用户信息容器
    @IBOutlet weak var svUserInfo: UIStackView!
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 点赞数
    @IBOutlet weak var btLikeCount: UIButton!
    
    /// 评论数
    @IBOutlet weak var btCommentCount: UIButton!
    
    /// 视频
    var data:Video!
    
    override func initViews() {
        super.initViews()
        
        //banner圆角
        ViewUtil.showSmallRadius(ivBanner)
        
        //类型按钮
        ViewUtil.showRadius(btType, 11)
        btType.showColorPrimaryBorder()
        
        ViewUtil.showRadius(ivAvatar, 20)
    }
    
    
    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Video) {
        self.data=data
        
        //显示封面
        ImageUtil.show(ivBanner, data.banner)
        
        //显示类型
        
        //显示播放量
        lbPlayCount.text="\(data.clicks_count)"
        
        //时长
        lbTime.text=TimeUtil.second2MinuteAndSecond(Float(data.duration))
        
        //标题
        lbTitle.text=data.title
        
        //用户头像
        ImageUtil.showAvatar(ivAvatar, data.user.avatar)
        
        //昵称
        lbNickname.text=data.user.nickname
        
        //点赞数
        btLikeCount.setTitle(" \(data.likes_count)", for: .normal)
        
        //评论数
        btCommentCount.setTitle(" \(data.comments_count)", for: .normal)
    }
    
    
    /// 类型点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onTypeClick(_ sender: UIButton) {
        print("VideoCell onTypeClick")
    }
    
    
    /// 点赞回调
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLikeClick(_ sender: UIButton) {
        print("VideoCell onLikeClick")
    }
    
    /// 评论回调
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCommentClick(_ sender: UIButton) {
        print("VideoCell onCommentClick")
    }
    
    /// 更多回调
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onMoreClick(_ sender: UIButton) {
        print("VideoCell onMoreClick")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
