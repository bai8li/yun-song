//
//  CommentCell.swift
//  评论Cell
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class CommentCell: BaseTableViewCell {
    
    static let NAME = "CommentCell"
    
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 时间
    @IBOutlet weak var lbTime: UILabel!
    
    /// 点赞数
    @IBOutlet weak var lbLikeCount: UILabel!
    
    /// 点赞按钮
    @IBOutlet weak var btLike: UIButton!
    
    /// 评论控件
    @IBOutlet weak var lbContent: YYLabel!
    
    /// 被回复的评论
    @IBOutlet weak var lbReplyContent: YYLabel!
    
    
    /// 回复评论这部分到顶部距离的自动约束
    /// 为什么要找到他呢？
    /// 是因为在iOS中没有像Android中隐藏控件
    /// 并不占用位置的功能
    /// 而我们这里，回复评论这部分距离顶部有10
    /// 在有回复评论的时候，这个距离没问题
    /// 没有回复评论的时候底部就会多出10
    /// 解决方法是没有回复评论的时候设置为0
    @IBOutlet weak var replyContainerMarginTop: NSLayoutConstraint!
    
    /// 昵称点击回调方法
    var onNicknameClick:((_ data:String)->Void)!
    
    /// 话题点击回调方法
    var onHashTagClick:((_ data:String)->Void)!
    
    /// 点赞回调
    var onLikeClick:((_ data:Comment)->Void)!
    
    /// 头像点击回调
    var onAvatarClick:((_ data:User)->Void)!
    
    /// 评论
    var data:Comment!
    
    override func initViews() {
        super.initViews()
        
        //头像圆角
        ViewUtil.showRadius(ivAvatar, 17.5)
        
        //设置自动换行
        lbContent.numberOfLines = 0
        lbReplyContent.numberOfLines=0
        
        //设置行高
        var modifier = YYTextLinePositionSimpleModifier()
        modifier.fixedLineHeight = SIZE_COMMENT_LINE_HEIGHT
        lbContent.linePositionModifier=modifier
        
        modifier=YYTextLinePositionSimpleModifier()
        modifier.fixedLineHeight=SIZE_COMMENT_LINE_HEIGHT
        lbReplyContent.linePositionModifier=modifier
        
        //设置宽度
        //20+45是左侧；20是右侧
        lbContent.preferredMaxLayoutWidth=frame.width-20-45-20
        lbReplyContent.preferredMaxLayoutWidth=frame.width-20-45-20-2-10
    }

    func bindData(_ data:Comment) {
        self.data=data
        
        //显示头像
        ImageUtil.showAvatar(ivAvatar, data.user.avatar)
        
        //显示昵称
        lbNickname.text=data.user.nickname
        
        //显示时间
        lbTime.text = TimeUtil.iso8601ToCommonFormat(data.created_at)
        
        //显示点赞数
        lbLikeCount.text="\(data.likes_count)"
        
        //点赞状态
        if data.isLiked() {
            lbLikeCount.textColor=UIColor(hex: COLOR_PRIMARY)
            btLike.setImage(UIImage(named: "CommentLiked"), for: .normal)
        }else {
            lbLikeCount.textColor=UIColor.lightGray
            btLike.setImage(UIImage(named: "CommentLike"), for: .normal)
        }
        
        //显示评论内容
//        lbContent.text = data.content
        
        //高亮
//        let result = StringUtil.processHighlight(data.content)
//        lbContent.attributedText = result
        
        //高亮和点击事件
        let result = NSMutableAttributedString(string: data.content)
        lbContent.attributedText=processContent(data.content, result)
        
        //显示被回复的评论
        if let parent = data.parent {
            //有回复评论
            
            let replyContent = "@\(parent.user.nickname!): \(parent.content!)"
            
            //创建一个属性文本
            let result = NSMutableAttributedString(string: replyContent)
            
            //设置颜色为灰色
            result.yy_color=UIColor.lightGray
            
            //设置值到控件
            lbReplyContent.attributedText=processContent(replyContent, result)
            
            //有回复评论设置距离顶部距离为10
            replyContainerMarginTop.constant=10
        }else{
            //没有回复评论
            lbReplyContent.text=""
            
            //没有回复评论设置距离顶部距离为0
            replyContainerMarginTop.constant=0
        }
    }
    
    
    /// 头像点击
    /// 真实项目中
    /// 可能还会实现
    /// 用户名也可以点击
    /// 或者顶部除了点赞那部分
    /// 其他的位置点击都是查看用户信息
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onAvatarClick(_ sender: UIButton) {
        print("CommentCell onAvatarClick:\(data.user.nickname)")
        
        onAvatarClick(data.user)
    }
    
    
    /// 点赞点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLikeClick(_ sender: UIButton) {
        print("CommentCell onLikeClick:\(data.content)")
        
        onLikeClick(data)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// 处理文本高亮和点击事件
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - result: <#result description#>
    /// - Returns: <#return value description#>
    func processContent(_ data:String,_ result:NSMutableAttributedString) -> NSMutableAttributedString {
        StringUtil.processContent(data, result, { (container, text, range, rect) in
            
            let clickText = StringUtil.processClickText(data,range)
            print("CommentCell processContent mention click:\(clickText)")
            
            self.onNicknameClick(clickText)
        }) { (container, text, range, rect) in
            let clickText = StringUtil.processClickText(data, range)
            
            print("CommentCell processContent hash tag click:\(clickText)")
            
            self.onHashTagClick(clickText)
        }
        
        return result
    }
    
}
