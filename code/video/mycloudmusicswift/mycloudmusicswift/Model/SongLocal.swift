//
//  SongLocal.swift
//  保存歌曲信息
//  为什么单独创建一个对象保存歌曲信息呢？
//  第一个原因是：因为Realm框架，从数据库中查询回来的对象
//  如果更改字段的值，他会自动同步到数据中
//  第二个原因是：因为目前的版本和JSON解析协议有点冲突
//
//  Created by smile on 2019/6/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入数据库框架
import Realm
import RealmSwift

class SongLocal: Object {

    //主键
    //当前对象的Id+用户Id
    @objc dynamic var sid:String = ""
    
    /// 对象Id
    @objc dynamic var id:String = ""
    
    /// 标题
    @objc dynamic var title:String = ""
    
    /// 大图
    @objc dynamic var banner:String = ""
    
    /// 歌曲地址
    @objc dynamic var uri:String = ""
    
    /// 歌手Id
    @objc dynamic var singer_id:String = ""
    
    /// 歌手昵称
    @objc dynamic var singer_nickname:String = ""
    
    /// 歌手头像
    @objc dynamic var singer_avatar:String? = nil
    
    /// 用户Id
    @objc dynamic var user_id:String!
    
    /// 是否在播放列表
    /// true:在
    @objc dynamic var playList:Bool = false
    
    /// 设置主键字段
    ///
    /// - Returns: <#return value description#>
    override class func primaryKey() -> String? {
        return "sid"
    }

    /// 生成sid
    func generateId() {
        sid = "\(id)\(user_id!)"
    }
    
    // MARK: - 播放后才有值
    
    /// 时长
    @objc dynamic var duration:Float = 0
    
    /// 播放进度
    @objc dynamic var progress:Float = 0
    
    /// 将SongLocal转为Song
    ///
    /// - Returns: <#return value description#>
    func toSong() -> Song {
        //创建对象
        let song = Song()
        
        //赋值
        song.id = id
        song.title=title
        song.banner=banner
        song.uri=uri
        
        //歌手
        let singer = User()
        singer.id = singer_id
        singer.nickname=singer_nickname
        singer.avatar=singer_avatar
        
        song.singer = singer
        
        song.playList = playList
        
        //音乐时长
        song.duration = duration
        
        //播放进度
        song.progress = progress
        
        return song
    }
}
