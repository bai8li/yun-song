//
//  UserGroup.swift
//  用户列表分组模型
//
//  Created by smile on 2019/7/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class UserGroup {
    
    /// 标题
    var title:String!
    
    /// 当前这一组的数据
    var datas:[User]!
    
}
