//
//  LyricType.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/6/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

/// 歌词类型
///
/// - lrc: LRC歌词
/// - ksc: KSC歌词
enum LyricType:Int,HandyJSONEnum {
    case lrc=0
    case ksc=10
}
