//
//  WechatPay.swift
//  微信支付参数模型，服务端生成，客户端直接设置到sdk
//
//  Created by smile on 2021/1/4.
//  Copyright © 2021 ixuea. All rights reserved.
//

import HandyJSON

class WechatPay:HandyJSON{
    var appid:String!
    var partnerid:String!
    var prepayid:String!
    var noncestr:String!
    var timestamp:String!
    var package:String!
    var sign:String!
    
    //JSON解析框架需要一个默认的init方法
    required init() {}
}

