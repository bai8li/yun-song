//
//  Person.swift
//  人的模型
//  该类在应用中无实际意义
//  只是用来测试Realm ORM框架的
//
//  Created by smile on 2019/6/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入数据库框架
import RealmSwift

class Person: Object {
    
    /// 姓名
    @objc dynamic var name = ""
    
    /// 年龄
    @objc dynamic var age = 0
}
