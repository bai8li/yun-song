//
//  Video.swift
//  视频模型
//
//  Created by smile on 2019/7/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Video: BaseModel {
    
    /// 标题
    var title:String!
    
    /// 视频地址
    var uri:String!
    
    /// 封面
    var banner:String!
    
    /// 视频时长
    /// 单位：秒
    var duration = 0
    
    /// 点击数
    var clicks_count = 0
    
    /// 点赞数
    var likes_count = 0
    
    /// 评论数
    var comments_count = 0
    
    /// 谁发布了这个视频
    var user:User!
    
    // MARK: - 播放后才有值
    /// 播放进度
    var progress:Float = 0
    
}


