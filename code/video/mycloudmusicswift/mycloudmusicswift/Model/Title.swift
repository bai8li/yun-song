//
//  Title.swift
//  首页-我的-标题模型
//
//  Created by smile on 2019/7/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Title: Any {
    
    /// 标题
    var title:String!
    
    /// 是否显示右侧更多按钮
    var isShowMore:Bool = false
    
    /// 用来区分点击
    var tag:Int!
    
    /// 构造方法
    ///
    /// - Parameters:
    ///   - title: <#title description#>
    ///   - isShowMore: <#isShowMore description#>
    ///   - tag: <#tag description#>
    init(_ title:String,_ isShowMore:Bool,_ tag:Int) {
        self.title=title
        self.isShowMore=isShowMore
        self.tag=tag
    }
}
