//
//  Feed.swift
//  动态模型
//
//  Created by smile on 2019/7/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Feed: BaseModel {
    
    /// 动态内容
    var content:String!
    
    /// 省
    var province:String?
    
    /// 市
    var city:String?
    
    /// 谁发布的动态
    var user:User!
    
    /// 图片
    var images:Array<Resource>?
}

