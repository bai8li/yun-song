//
//  SheetWrapper.swift
//  歌单详情外部包裹对象
//
//  Created by smile on 2019/6/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class SheetWrapper:HandyJSON {
    
    /// 歌单数据
    var data:Sheet!
    
    //因为HandyJSON解析框架要求有一个init方法
    required init() {}
}
