//
//  BaseModel.swift
//  通用对象模型
//
//  Created by smile on 2019/6/9.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class BaseModel: HandyJSON {
    
    /// Id
    var id:String!
    
    /// 创建时间
    var created_at:String = ""
    
    /// 创建时间
    var updated_at:String = ""
    
    
    //JSON解析框架需要一个默认的init方法
    required init() {}
}
