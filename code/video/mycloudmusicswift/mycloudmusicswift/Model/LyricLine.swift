//
//  LyricLine.swift
//  一行歌词
//
//  Created by smile on 2019/6/27.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

/// 单位都是毫秒
class LyricLine {
    
    /// 这一行歌词的内容
    var data:String!
    
    /// 开始时间
    var startTime:Int = 0
    
    /// 每一个字
    var words:[String]?
    
    /// 每一个字对一个的时间
    var wordDurations:[Int]?
    
    /// 结束时间
    var endTime:Int = 0
    
    
}
