//
//  Ad.swift
//  广告模型
//
//  Created by smile on 2019/6/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Ad: BaseModel {
    
    /// 标题
    var title:String!
    
    /// 图片
    var banner:String!
    
    /// 点击广告后跳转的地址
    var uri:String!
    
    
}
