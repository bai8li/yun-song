//
//  Suggest.swift
//  搜索建议模型
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Suggest: BaseModel {
    
    /// 歌单搜索建议
    var sheets:[SearchTitle] = []
    
    /// 用户搜索建议
    var users:[SearchTitle] = []

}
