//
//  Topic.swift
//  话题模型
//
//  Created by smile on 2019/7/1.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Topic: BaseModel {
    
    /// 标题
    var title:String!
    
    /// 封面图
    var banner:String!
    
    /// 参数人数
    var joins_count = 0
    
}

