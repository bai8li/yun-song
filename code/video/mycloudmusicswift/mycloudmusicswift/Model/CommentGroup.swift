//
//  CommentGroup.swift
//  评论分组模型
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class CommentGroup {
    
    /// 这一组标题
    var title:String!
    
    /// 这一组评论
    var comments:[Comment]!
    
}
