//
//  SearchTitle.swift
//  搜索建议内容模型
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class SearchTitle: BaseModel {
    
    /// 标题
    var title:String!
    
}
