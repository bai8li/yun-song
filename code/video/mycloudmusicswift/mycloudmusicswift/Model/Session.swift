//
//  Session.swift
//  登录成功后返回的信息
//
//  Created by smile on 2019/6/9.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation


//{
//    "user": 1,
//    "session": "lt1KaAf7tx0IesWBylBn4FFXsjSn_COFvuou_KhemuI",
//    "im_token": "A5uX/U/elKC6w34rAEJOpr/9g/Qr+macnzRSjm0SBueczXTPLmsKBa9cPptdfoFPMioIBCPu2n0Huo5JNVD9iA=="
//}
class Session: BaseModel {
    
    /// 用户Id
    var user:String!
    
    /// 登录标识
    var session:String!
    
    /// 第三方聊天用的标识
    /// 这里没有用到他
    /// 可以不解析
    var im_token:String?
    
    
}
