//
//  SheetGroup.swift
//  歌单分组
//
//  Created by smile on 2019/7/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class SheetGroup {
    
    /// 标题
    var title:String!
    
    /// 数据
    var datas:[Sheet]!
    
}
