//
//  Order.swift
//  订单模型
//
//  Created by smile on 2019/7/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class Order: BaseModel {
    
    /// 订单状态；0:待支付；10:支付完成；20:订单关闭
    var status:Status = .wait_pay
    
    /// 订单状态格式化
    var formatStatus:String{
        get {
            switch status {
            case .payed:
                return "支付完成"
            case .close:
                return "订单关闭"
            default:
                return "待支付"
            }
        }
    }
    
    
    /// 订单状态枚举
    ///
    /// - wait_pay: 待支付
    /// - payed: 支付完成
    /// - close: 订单关闭
    enum Status:Int,HandyJSONEnum {
        case wait_pay=0
        case payed=10
        case close=20
    }
    
    /// 订单价格
    var price:Double!
    
    /// 订单来源
    /// 一般订单来源不会返回给客户端
    /// 我们这样返回来只是给大家演示如何显示这些字段而已
    /// 也就是在那个平台创建的订单
    var source:Source = .unknown
    
    /// 订单来源格式化
    var formatSource:String{
        get {
            switch source {
            case .android:
                return "Android"
            case .ios:
                return "iOS"
            case .web:
                return "Web"
            default:
                return ""
            }
        }
    }
    
    /// 订单来源枚举
    ///
    /// - unknown: 未知
    /// - android: Android
    /// - ios: iOS
    /// - web: Web
    enum Source:Int,HandyJSONEnum {
        case unknown=0
        case android=10
        case ios=20
        case web=30
        
    }
    
    /// 支付来源
    //  因为可能有创建订单是web网站
    //  用户是在手机上支付的
    var origin:Source = .unknown
    
    /// 支付来源格式化
    var formatOrigin:String{
        get {
            switch source {
            case .android:
                return "Android"
            case .ios:
                return "iOS"
            case .web:
                return "Web"
            default:
                return ""
            }
        }
    }
    
    /// 支付渠道
    var channel:Channel = .unknow
    
    /// 支付渠道格式化
    var formatChannel:String {
        get {
            switch channel {
            case .alipay:
                return "支付宝"
            case .wechat:
                return "微信"
            default:
                return ""
            }
        }
    }
    
    
    /// 支付渠道枚举
    ///
    /// - unknow: 未知
    /// - alipay: 支付宝
    /// - wechat: 微信
    enum Channel:Int,HandyJSONEnum {
        case unknow=0
        case alipay=10
        case wechat=20
    }
    
    /// 订单号
    var number:String!
    
    /// 订单所关联的商品
    var book:Book!

}
