//
//  Pay.swift
//  支付参数模型
//
//  Created by smile on 2019/7/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Pay: BaseModel {
    
    /// 支付渠道
    var channel:Order.Channel!
    
    /// 支付宝支付参数
    var pay:String?
    
    /// 微信支付参数
    var wechatPay:WechatPay?
    
}
