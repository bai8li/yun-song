//
//  ButtonInfo.swift
//  首页-我的-顶部按钮模型
//
//  Created by smile on 2019/7/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class ButtonInfo {
    
    /// 按钮图标
    var icon:String!
    
    /// 标题
    var title:String!
    
    /// 构造方法
    ///
    /// - Parameters:
    ///   - icon: <#icon description#>
    ///   - title: <#title description#>
    init(_ icon:String,_ title:String) {
        self.icon=icon
        self.title=title
    }
}
