//
//  ListResponse.swift
//  解析列表网络请求
//
//  Created by smile on 2019/6/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class ListResponse<T: HandyJSON>: BaseResponse {
    /// 分页元数据
    var data:Meta<T>?
    
}
