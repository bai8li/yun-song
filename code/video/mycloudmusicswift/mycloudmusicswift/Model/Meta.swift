//
//  Meta.swift
//  分页元数据
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class Meta<T>: HandyJSON {
    /// 当前是第几页
    var current_page:Int = 0
    
    /// 下一页
    var next_page:Int?
    
    /// 上一页
    var prev_page:Int?
    
    /// 有多少页
    var total_pages = 1
    
    /// 有多少个
    var total_count = 0
    
    ///定义一个列表
    ///里面使用了泛型
    var data:Array<T>?
    
    //JSON解析框架需要一个默认的init方法
    required init() {}
    
    /// 返回下一页
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func nextPage(_ data:Meta?) -> Int {
        if let data = data {
            return data.current_page+1
//            return data.next_page
        }
        
        return 1
    }
}
