//
//  Book.swift
//  商品模型
//
//  Created by smile on 2019/7/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Book: BaseModel {
    
    /// 标题
    var title:String!
    
    /// 封面
    var banner:String!
    
    /// 价格
    var price:Double!
    
    /// 谁创建了该商品
    var user:User!
    
    /// 有值表示购买
    var buy:Int?
    
    /// 是否购买
    ///
    /// - Returns: true:购买；false:没有购买
    func isBuy() -> Bool {
//        if buy != nil {
//            return true
//        }
//
//        return false
        
        return buy==nil ? false:true
    }
    
}
