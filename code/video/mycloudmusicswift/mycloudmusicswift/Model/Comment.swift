//
//  Comment.swift
//  评论模型
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Comment: BaseModel {
    
    /// 评论内容
    var content:String!
    
    /// 点赞数
    var likes_count = 0
    
    /// 创建评论的用户
    var user:User!
    
    /// 歌单
    var sheet:Sheet?
    
    /// 被回复的评论
    var parent:Comment?
    
    /// 是否点赞
    /// 有值表示点赞
    var like_id:String?
    
    /// 是否点赞
    ///
    /// - Returns: true:点赞；false:没有点赞
    func isLiked() -> Bool {
        return like_id != nil
    }
}

