//
//  VideoController.swift
//  视频列表界面
//
//  Created by smile on 2019/7/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoController: BaseTitleController {
    
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 列表数据源
    var dataArray:[Video] = []
    
    override func initViews() {
        super.initViews()
        
        setTitle("视频")
    }
    
    override func initDatas() {
        super.initDatas()
        
        Api.shared.videos().subscribeOnSuccess { (data) in
            if let data = data?.data?.data {
                self.dataArray=self.dataArray+data
                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
    }
    
    

}

// MARK: - 列表数据源和代理
extension VideoController:UITableViewDataSource,UITableViewDelegate {
    
    /// 有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //取出cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath) as! VideoCell
        
        //绑定数据
        cell.bindData(dataArray[indexPath.row])
        
        //返回cell
        return cell
    }
    
    
    /// cell点击
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        VideoDetailController.start(navigationController!, dataArray[indexPath.row].id)
    }
    
}
