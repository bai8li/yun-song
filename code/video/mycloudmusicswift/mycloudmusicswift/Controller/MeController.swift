//
//  MeController.swift
//  首页-我的界面
//
//  Created by smile on 2019/7/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

//导入响应式编程框架
import RxSwift

class MeController: BaseTitleController {
    
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 列表数据
    var dataArray:[Any] = []
    
    /// 创建歌单标题
    let CREATE_SHEET=0
    
    /// 收藏歌单标题
    let COLLECTION_SHEET=10
    
    override func initViews() {
        super.initViews()
        setTitle("我的音乐")
        
        //注册标题
        tableView.register(UINib(nibName: TitleTableViewCell.NAME, bundle: nil), forCellReuseIdentifier: TitleTableViewCell.NAME)
        
        //注册歌单Cell
        tableView.register(UINib(nibName: TopicCell.NAME, bundle: nil), forCellReuseIdentifier: TopicCell.NAME)
        
        //注册按钮cell
        tableView.register(UINib(nibName: ButtonCell.NAME, bundle: nil), forCellReuseIdentifier: ButtonCell.NAME)
    }
    
    override func initDatas() {
        super.initDatas()
        
        fetchData()
    }
    
    func fetchData() {
        //添加按钮
        dataArray.append(ButtonInfo("LocalMusic", "本地音乐"))
        dataArray.append(ButtonInfo("LocalMusic", "下载管理"))
        dataArray.append(ButtonInfo("LocalMusic", "最近播放"))
        dataArray.append(ButtonInfo("LocalMusic", "我的电台"))
        dataArray.append(ButtonInfo("LocalMusic", "我的收藏"))
        dataArray.append(ButtonInfo("LocalMusic", "Sati 空间"))
        
        //创建的歌单
        let createSheetsApi = Api.shared.createSheets(PreferenceUtil.userId()!)
        
        let collectSheetsApi = Api.shared.collectSheets(PreferenceUtil.userId()!)
        
        //添加标题
        self.dataArray.append(Title("创建的歌单", true, self.CREATE_SHEET))
        
        //使用RxSwift
        //将两个请求实现为并发
        //两个请求都请求成功后回调
        Observable.zip(createSheetsApi,collectSheetsApi){
            (createSheetsApi,collectSheetsApi) -> (ListResponse<Sheet>?,ListResponse<Sheet>?) in
            //将两个信号合并成一个信号
            //并压缩一个元组返回
            //两个信号均执行成功才会回调 回调方法
            return (createSheetsApi,collectSheetsApi)
        }
            //在子线程中执行
            .observeOn(MainScheduler.instance)

            //这里不能使用subscribeOnSuccess
            //因为他是专门处理单个请求的
            .subscribe(onNext: { (createSheetResponse, collectSheetsResponse) in
                //处理数据

                //添加创建的歌单
                self.dataArray=self.dataArray+(createSheetResponse?.data?.data ?? [])

                //添加收藏歌单标题
                self.dataArray.append(Title("收藏的歌单", true, self.COLLECTION_SHEET))

                //添加收藏的歌单
                self.dataArray=self.dataArray+(collectSheetsResponse?.data?.data ?? [])

                //重新加载数据
                self.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        //普通的回调方式
        //请求是线性的
//        createSheetsApi.subscribeOnSuccess { (data) in
//            if let data = data?.data {
//                //添加标题
////                self.dataArray.append(Title("创建的歌单", true, self.CREATE_SHEET))
//                self.dataArray = self.dataArray+data
//
//                //请求收藏的歌单
//                collectSheetsApi.subscribeOnSuccess({ (data) in
//                    if let data = data?.data {
//                        //添加标题
//                        self.dataArray.append(Title("收藏的歌单", true, self.COLLECTION_SHEET))
//                        self.dataArray = self.dataArray+data
//
//                        self.tableView.reloadData()
//                    }
//                }).disposed(by: self.disposeBag)
//
//            }
//            }.disposed(by: disposeBag)
    }
    
    override func initListeners() {
        super.initListeners()
        
        //接收歌单数据更改了通知
        SwiftEventBus.onMainThread(self, name: ON_SHEET_CHANGED) { (sender) in
            //清空原来的数据
            self.dataArray.removeAll()
            
            //重新获取
            self.fetchData()
        }
    }
    
    /// 根据数据获取类型
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    func typeForItemAtData(_ data:Any) -> CellType {
        if data is Title {
            return .title
        }else if data is ButtonInfo {
            return .button
        }
        
        return .sheet
    }
    
    /// cell类型
    ///
    /// - title: 标题
    /// - sheet: 歌单
    /// - button: 按钮
    enum CellType {
        case title
        case sheet
        case button
    }
}

// MARK: - 实现列表数据源和代理
extension MeController:UITableViewDataSource,UITableViewDelegate {
    
    /// 有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取数据
        let data = dataArray[indexPath.row]
        
        //获取Cell类型
        let type = typeForItemAtData(data)
        
        switch type {
        case .title:
            //标题
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleTableViewCell.NAME, for: indexPath) as! TitleTableViewCell
            
            //绑定数据
            cell.bindData(data as! Title)
            
            //设置右侧点击回调方法
            cell.onMoreClick = {
                print("MeController title cell onMoreClick")
                
                self.processOnMoreClick(indexPath)
            }
            
            //返回cell
            return cell
        case .sheet:
            //获取Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME, for: indexPath) as! TopicCell
            
            //绑定数据
            cell.bindData(data as! Sheet)
            
            //返回cell
            return cell
        default:
            //按钮
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.NAME, for: indexPath) as! ButtonCell
            
            //绑定数据
            cell.bindData(data as! ButtonInfo)
            
            //设置tag
            cell.tag=indexPath.row
            
            return cell
        }
        
    }
    
    
    /// 标题Cell更多按钮点击处理方法
    ///
    /// - Parameter indexPath: <#indexPath description#>
    func processOnMoreClick(_ indexPath:IndexPath) {
        let data = dataArray[indexPath.row] as! Title
        
        if data.tag == CREATE_SHEET {
            //创建歌单
            print("MeController processOnMoreClick create sheet click")
            
            CreateSheetController.start(navigationController!)
        } else {
            //收藏歌单
            print("MeController processOnMoreClick collection sheet click")
        }
    }
    
    /// cell点击回调方法
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = dataArray[indexPath.row]
        
        let type = typeForItemAtData(data)
        
        switch type {
        case .title:
            //标题不处理
            break
        case .sheet:
            //歌单
            SheetDetailController.start(navigationController!, (data as! Sheet).id)
        default:
            //按钮
            if indexPath.row == 0{
                //本地音乐
                print("MeController local music click")
                
                LocalMusicController.start(navigationController!)
            }else if indexPath.row == 1 {
                //下载管理
                print("MeController download manager click")
                
                DownloadController.start(navigationController!)
            }
        }
    }
    
}
