//
//  CreaetSheetController.swift
//  创建歌单界面
//
//  Created by smile on 2019/7/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅
import SwiftEventBus

class CreateSheetController: BaseTitleController {
    
    
    /// 输入框
    @IBOutlet weak var tfContent: UITextField!
    
    override func initViews() {
        super.initViews()
        
        setTitle("创建歌单")
        
        //创建一个保存按钮
        let saveBarItem = UIBarButtonItem(title: "保存", style: .plain, target: self, action: #selector(onSaveClick))
        
        navigationItem.rightBarButtonItem = saveBarItem
    }
    
    /// 保存按钮点击回调
    @objc func onSaveClick() {
        print("CreateSheetController onSaveClick")
        
        let title = tfContent.text!.trim()!
        if title.isEmpty {
            ToastUtil.short("请输入歌单名称.")
            return
        }
        
        Api.shared.createSheet(title).subscribeOnSuccess { (data) in
            //发布通知
            SwiftEventBus.post(ON_SHEET_CHANGED)
            
            //关闭界面
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动方法
extension CreateSheetController {
    static func start(_ nav:UINavigationController) {
        //获取控制器
        let controller = nav.storyboard!.instantiateViewController(withIdentifier: "CreateSheet")
        
        //将控制器放入导航控制器
        nav.pushViewController(controller, animated: true)
    }
}
