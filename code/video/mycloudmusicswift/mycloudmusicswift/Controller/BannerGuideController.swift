//
//  BannerGuideController.swift
//  扩展引导界面轮播图的数据源和代理
//
//  Created by smile on 2019/6/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

// MARK: - 初始化轮播图组件
extension GuideController {
    /// 初始化轮播图组件
    func initBannerView() {
        //设置轮播图数据源
        bannerView.dataSource=self
        
        //设置代理
        bannerView.delegate=self
        
        //设置如果图片为空显示的图片
        bannerView.emptyImage=UIImage(named: IMAGE_PLACE_HOLDER)
        
        //设置占位图
        //其实就是还没有显示真正的图片前显示的这个图片
        bannerView.placeholderImage=UIImage(named: IMAGE_PLACE_HOLDER)
        
        //显示图片的时候具体调用什么方法
        bannerView.bannerViewSelectorString="sd_setImageWithURL:placeholderImage:"
        
        //指示器默认颜色
        bannerView.pageControlNormalColor=UIColor(hex: COLOR_LIGHT_GREY)
        
        //高亮颜色
        bannerView.pageControlHighlightColor=UIColor(hex: COLOR_PRIMARY)
        
        //指示器位置
        //        bannerView.pageControlAliment = .right
        
        //禁用自动滚动
        bannerView.autoScroll=false
        
        //重新加载数据
        bannerView.reloadData()
    }
}


// MARK: - 实现轮播图数据源和代理方法
extension GuideController:YJBannerViewDataSource,YJBannerViewDelegate{
    
    /// banner数据源
    ///
    /// - Parameter bannerView: <#bannerView description#>
    /// - Returns: <#return value description#>
    func bannerViewImages(_ bannerView: YJBannerView!) -> [Any]! {
        return ["Guide1","Guide2","Guide3","Guide4","Guide5"]
    }
    
    /// 自定义Cell
    /// 设置图片的缩放模式
    ///
    /// - Parameter bannerView: <#bannerView description#>
    /// - Returns: <#return value description#>
    func bannerView(_ bannerView: YJBannerView!, customCell: UICollectionViewCell!, index: Int) -> UICollectionViewCell! {
        //将Cell转为YJBannerViewCell
        let cell=customCell as! YJBannerViewCell
        
        //按比例
        //从中心填充
        //多余的内容裁减掉
        cell.showImageViewContentMode = .scaleAspectFill
        
        return cell
    }
    
    
    /// banner点击方法
    ///
    /// - Parameters:
    ///   - bannerView: <#bannerView description#>
    ///   - index: <#index description#>
    func bannerView(_ bannerView: YJBannerView!, didSelectItemAt index: Int) {
        print("GuideController on banner view click:\(index)")
    }
}
