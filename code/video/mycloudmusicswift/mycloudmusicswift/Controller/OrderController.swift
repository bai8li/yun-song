//
//  OrderController.swift
//  我的订单列表界面
//
//  Created by smile on 2019/7/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

//导入发布订阅框架
import SwiftEventBus

class OrderController: UITableViewController {
    
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag=DisposeBag()
    
    /// 列表数据源
    var dataArray:[Order] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        //设置标题
        navigationItem.title="订单列表"
        
        //分割线间距
        tableView.separatorInset=UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        //注册cell
        tableView.register(UINib(nibName: OrderCell.NAME, bundle: nil), forCellReuseIdentifier: OrderCell.NAME)
        
        //请求数据
        fetchData()
        
        //监听支付成功事件
        SwiftEventBus.onMainThread(self, name: ON_PAY_SUCCESS) { (sender) in
            self.fetchData()
        }
    }
    
    func fetchData() {
        Api.shared.orders().subscribeOnSuccess { (data) in
            if let data=data?.data?.data{
                self.dataArray=data
                self.tableView.reloadData()
            }
            }.disposed(by: disposeBag)
    }

}

// MARK: - 启动方法
extension OrderController{
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=OrderController(style: .plain)
        
        //将控制器添加到导航控制器中
        nav.pushViewController(controller, animated: true)
    }
}

// MARK: - 实现列表数据源和代理
extension OrderController{
    
    /// 有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    
     /// 返回cell
     ///
     /// - Parameters:
     ///   - tableView: <#tableView description#>
     ///   - indexPath: <#indexPath description#>
     /// - Returns: <#return value description#>
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderCell.NAME, for: indexPath) as! OrderCell
     
        //绑定数据
        cell.bindData(dataArray[indexPath.row])
     
        //返回cell
        return cell
     }
    
    /// cell点击
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        OrderDetailController.start(navigationController!, dataArray[indexPath.row].id)
    }
 
}
