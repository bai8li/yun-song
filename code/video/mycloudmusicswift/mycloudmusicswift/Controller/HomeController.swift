//
//  HomeController.swift
//  首页界面
//
//  Created by smile on 2019/6/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

class HomeController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //设置TabBar高亮颜色
        tabBar.tintColor = UIColor(hex: COLOR_PRIMARY)
        
        //主题
        //TabBar背景颜色
        tabBar.theme_barTintColor=[COLOR_STRING_WHITE,COLOR_STRING_DARK_BLACK]
        
        //TabBar选中高亮颜色
        tabBar.theme_tintColor=[COLOR_STRING_PRIMARY,COLOR_STRING_DARK_PRIMARY]
        
        //监听消息数改变了通知
        SwiftEventBus.onMainThread(self, name: ON_MESSAGE_COUNT_CHANGED) { (sender) in
            self.updateMessageUnreadCount()
        }
        
        updateMessageUnreadCount()
    }
    
    /// 更新消息未读数
    func updateMessageUnreadCount() {
        //获取未读消息数
        let unreadMessageCount=MessageUtil.getUnreadMessageCount()
        
        //获取到账号TabBar
        let accountTabBarItem=tabBar.items![4]
        
        if unreadMessageCount>0 {
            //有消息未读数
            
            //显示消息未读数
            accountTabBarItem.badgeValue="\(unreadMessageCount)"
        } else {
            //没有消息未读数
            
            //清除红点
            accountTabBarItem.badgeValue=nil
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
