//
//  ScanController.swift
//  扫描二维码
//
//  Created by smile on 2019/7/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入AVFoundation
//设置扫描类型
import AVFoundation

class ScanController: BaseTitleController {

    /// 扫描预览容器
    @IBOutlet weak var scanContainer: UIView!
    
    /// 我的二维码按钮
    @IBOutlet weak var btMyCode: UIButton!
    
    /// 扫描样式
    var scanStyle:LBXScanViewStyle!
    
    /// 扫描二维码的View
    /// 就是全屏预览的View
    /// 不是中间扫描的输入框
    var scanView:LBXScanView?
    
    /// 扫描包裹对象
    var scanWrapper:LBXScanWrapper?
    
    /// 识别码类型
    var arrayCodeType = [AVMetadataObject.ObjectType.qr as NSString ,AVMetadataObject.ObjectType.ean13 as NSString ,AVMetadataObject.ObjectType.code128 as NSString] as [AVMetadataObject.ObjectType]
    
    override func initViews() {
        super.initViews()
        
        setTitle("扫一扫")
        
        ViewUtil.showRadius(btMyCode, 20)
        
        //创建扫描样式
        scanStyle=LBXScanViewStyle()
        
        //设置扫描时显示的动画图片
        scanStyle.animationImage=UIImage(named: "QRScanLine")
    }
    
    override func initDatas() {
        super.initDatas()
        
        
    }

    
    /// 点击我的二维码
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onMyCodeClick(_ sender: Any) {
        CodeController.start(navigationController!, PreferenceUtil.userId()!)
    }
    
    /// 视图即将显示
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置标题栏为亮色
        setTitleBarLight()
    }
    
    
    /// 视图已经显示
    ///
    /// - Parameter animated: <#animated description#>
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //绘制扫描的View
        drawScanView()
        
        //0.3秒中后开始扫描
        perform(#selector(ScanController.startScan), with: nil, afterDelay: 0.3)
    }
    
    /// 视图即将消息
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //设置标题栏为默认颜色
        setTitleBarDefault()
        
        //取消定时
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        
        //停止扫描动画
        scanView?.stopScanAnimation()
        
        //停止扫描
        scanWrapper?.stop()
        
    }
    
    /// 绘制扫描的View
    func drawScanView() {
        if scanView == nil {
            scanView=LBXScanView(frame: scanContainer.frame, vstyle: scanStyle)
            scanContainer.addSubview(scanView!)
        }
        
        //开始准备扫描
        scanView!.deviceStartReadying(readyStr: "加载中.")
    }
   
    /// 开始扫描
    @objc func startScan() {
        if scanWrapper==nil {
            var cropRect = CGRect.zero
            
            //裁剪矩形
            cropRect=LBXScanView.getScanRectWithPreView(preView: scanContainer, style: scanStyle)
            
            //创建一个包裹对象
            scanWrapper=LBXScanWrapper(videoPreView: scanContainer, objType: arrayCodeType, isCaptureImg: false, cropRect: cropRect, success: { (arrayResults) in
                
//                if let strongSelf = self {
                    //扫描成功
                    
                    //停止扫描
                    self.scanView!.stopScanAnimation()
                
                    //处理扫描结果
                    self.handleCodeResult(arrayResults)
//                }
                
            })
        }
        
        //结束相机等待提示
        scanView?.deviceStopReadying()
        
        //开始扫描动画
        scanView?.startScanAnimation()
        
        //开始扫描
        scanWrapper?.start()
    }
    
    /// 处理扫描结果
    ///
    /// - Parameter arrayResults: <#arrayResults description#>
    /// - Returns: <#return value description#>
    func handleCodeResult(_ arrayResults:[LBXScanResult]) {
//        for result:LBXScanResult in arrayResults {
//            print("ScanController handleCodeResult:\(result.strScanned)")
//        }
        
        //取第一个数据就行了
        let result=arrayResults[0]
        if let str = result.strScanned {
            if str.isEmpty {
                //内容为空字符串
                
                //显示不支持该类型
                showUnsupportFormat()
                return
            }
            
            //有内容
            handleScanString(str)
        }else{
            //显示不支持该类型
            showUnsupportFormat()
        }
    }
    
    /// 处理扫描结果
    ///
    /// - Parameter data: <#data description#>
    func handleScanString(_ data:String) {
        //从扫描的字符串中解析我们需要的参数
        //我们的二维码格式如下：
        //http://dev-my-cloud-music-api-rails.ixuea.com/v1/monitors/version?u=6
        
        //表示用我的云音乐软件扫描后
        //跳转到Id为6用户详情
        
        //其实就是我们应用的下载地址
        //我们要用到的参数放到了查询参数
        //生成网址的好处是
        //如果使用其他软件扫描的时候
        //会自动打开网址
        //大部分二维码扫描软件（QQ，微信）都会这样
        
        //这样就可以引导用户下载我们的应用
        //另外在真实项目中
        //会将上面的网址转短一点
        //因为二维码内容太多了
        //生成的二维码格子就会很小
        //如果显示的图片过小
        //就会非常难识别
        
        //另外不同的方式生成二维码
        //也可能不同
        
        //解析出网址中的查询参数
        let component = URLComponents(string: data)
        
        if let component = component {
            //解析成功
            
            //获取查询参数
            if let queryItems = component.queryItems{
                //有查询参数
                
                var userId:String?
                
                //遍历查询参数
                for queryItem in queryItems{
                    if queryItem.name=="u"{
                        //找到用户id属性
                        
                        userId=queryItem.value
                        
                        break
                    }
                }
                
                //判断是否找到了用户Id
                if let userId = userId {
                    //参数没问题
                    processUserCode(userId)
                    return
                }
            }
        }
        
        //显示不支持该类型
        showUnsupportFormat()
    }
    
    /// 处理用户二维码
    ///
    /// - Parameter data: <#data description#>
    func processUserCode(_ data:String) {
        //关闭当前界面
        navigationController?.popViewController(animated: true)
        
        //打开用户详情
        UserDetailController.start(navigationController!, userId: data)
    }
    
    /// 显示不支持该格式
    func showUnsupportFormat() {
        ToastUtil.short("不支持该格式！")
        
        //继续扫描
        startScan()
    }
}

// MARK: - 启动方法
extension ScanController{
    
    /// 启动方法
    ///
    /// - Returns: <#return value description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "Scan")
        
        //将控制器放入导航控制器
        nav.pushViewController(controller, animated: true)
    }
}
