//
//  SettingController.swift
//  设置界面
//
//  Created by smile on 2019/7/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入日志框架
import CocoaLumberjack

class SettingController: BaseTitleController {
    
    
    /// 拔掉耳机停止音乐播放
    @IBOutlet weak var stRemoveHeadsetStopMusic: UISwitch!
    
    override func initViews() {
        super.initViews()
        
        setTitle("设置")
        
        //从偏好设置中恢复设置状态
        stRemoveHeadsetStopMusic.setOn(PreferenceUtil.isRemoveHeadsetStopMusic(), animated: true)
    }
    
    /// 拔掉耳机停止音乐播放开关状态改变时调用
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onRemoveHeadsetStopMusicChanged(_ sender: UISwitch) {
        print("SettingController onRemoveHeadsetStopMusicChanged:\(sender.isOn)")
        
        //保存到偏好设置
        PreferenceUtil.setRemoveHeadsetStopMusic(sender.isOn)
    }
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "Setting")
        
        //将控制器放入导航控制器
        nav.pushViewController(controller, animated: true)
    }

    deinit {
        DDLogDebug("SettingController deinit")
    }
}
