//
//  ShareLyricImageController.swift
//  分享歌词图片界面
//
//  Created by smile on 2019/6/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ShareLyricImageController: BaseTitleController {
    
    
    /// 滚动试图容器控件
    @IBOutlet weak var svContaiiner: UIScrollView!
    
    
    /// 封面图片
    @IBOutlet weak var ivCover: UIImageView!
    
    
    /// 歌词控件
    @IBOutlet weak var blLyric: UILabel!
    
    
    /// 歌曲名称
    @IBOutlet weak var lbSongName: UILabel!
    
    /// 分享歌词图片
    @IBOutlet weak var btShareLyricImage: UIButton!
    
    
    /// 保存歌词图片
    @IBOutlet weak var btSaveLyricImage: UIButton!
    
    /// 音乐
    var song:Song!
    
    /// 歌词
    var lyric:String!
    

    override func initViews() {
        super.initViews()
        
        setTitle("分享歌词图片")
        
        //边框
        btShareLyricImage.showColorPrimaryBorder()
        btSaveLyricImage.showColorPrimaryBorder()
    }
    
    override func initDatas() {
        super.initDatas()
        
        //显示封面
        ImageUtil.show(ivCover, song.banner)
        
        //使用富文本
        let attrString = NSMutableAttributedString(string: lyric)
        
        let style = NSMutableParagraphStyle.init()
        
        //设置行高是多少倍
        style.lineHeightMultiple = 1.5
        
        //设置样式到属性文本
        attrString.addAttribute(.paragraphStyle, value: style, range: NSRange(location: 0, length: lyric.count))
        
        blLyric.attributedText = attrString
        
        //设置歌曲名称
        lbSongName.text = "——「\(song.title!)」"
    }
    
    /// 分享歌词图片点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onShareLyricImageClick(_ sender: Any) {
        print("ShareLyricImageController onShareLyricImageClick")
        
        //获取ScrollView截图
        let image = ViewUtil.captureScrollView(svContaiiner)
        
        ShareUtil.shareImage(image)
    }
    

    /// 保存歌词图片到相册
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSaveLyricImageClick(_ sender: Any) {
        print("ShareLyricImageController onSaveLyricImageClick")
        
        let image = ViewUtil.captureScrollView(svContaiiner)
        
        saveImage(image)
    }
    
    /// 保存图片到相册
    ///
    /// - Parameter data: <#data description#>
    func saveImage(_ data:UIImage) {
        UIImageWriteToSavedPhotosAlbum(data, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    /// 保存图片到相册回调
    ///
    /// - Parameters:
    ///   - image: <#image description#>
    ///   - error: <#error description#>
    ///   - contextInfo: <#contextInfo description#>
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        var message = ""
        
        if error == nil {
            message="保存成功！"
        } else {
            message="保存失败，请稍后再试！"
        }
        
        ToastUtil.short(message)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension ShareLyricImageController {
    
    /// 启动界面
    ///
    /// - Parameters:
    ///   - navigationController: <#navigationController description#>
    ///   - song: <#song description#>
    ///   - lyric: <#lyric description#>
    static func start(_ navigationController:UINavigationController,_ song:Song,_ lyric:String) {
        //创建控制器
        let controller = navigationController.storyboard!.instantiateViewController(withIdentifier: "ShareLyricImage") as! ShareLyricImageController
        
        //传递数据
        controller.song = song
        controller.lyric = lyric
        
        navigationController.pushViewController(controller, animated: true)
        
    }
}
