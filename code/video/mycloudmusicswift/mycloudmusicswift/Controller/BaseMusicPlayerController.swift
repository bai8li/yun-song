//
//  BaseMusicPlayerController.swift
//  通用音乐播放控制器
//  主要是显示右上角播放状态
//
//  Created by smile on 2019/6/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入主题框架
import SwiftTheme

class BaseMusicPlayerController: BaseTitleController {
    /// 播放状态的容器
    var vwPlayContainer:UIView!
    
    /// 播放状态的图片控件
    var ivPlaying:UIImageView!
    
    /// 列表列表管理器
    var playListManager:PlayListManager!
    
    /// 播放管理器
    var musicPlayerManager:MusicPlayerManager!
    
    override func initDatas() {
        super.initDatas()
        
        //初始化播放列表管理器
        playListManager = PlayListManager.shared
        
        //初始化播放管理器
        musicPlayerManager = MusicPlayerManager.shared()
    }

    /// 创建播放状态按钮
    func createPlayingBar() -> UIBarButtonItem {
        //创建自定义类型的按钮
        //创建一个矩形
        let frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        
        //创建一个View
        vwPlayContainer = UIView(frame: frame)
        
        //创建ImageView
        ivPlaying = UIImageView(frame: frame)
        
        //设置默认显示的图片
        ivPlaying.image = UIImage(named: "TatbarPlayingWhite1")
        
        //创建button
        let btPlaying = UIButton(type: .custom)
        btPlaying.frame = frame
        
        //设置按钮点击事件
        btPlaying.addTarget(self, action: #selector(onPlayingClick(sender:)), for: .touchUpInside)
        
        //添加这些控件到容器中
        vwPlayContainer.addSubview(ivPlaying)
        vwPlayContainer.addSubview(btPlaying)
        
        //创建BarButtonItem
        let playingBarItem = UIBarButtonItem(customView: vwPlayContainer)
        
        return playingBarItem
    }
    
    /// 进入播放界面
    func pushMusicPlayerController() {
        //启动简单播放界面
//        SimplePlayerController.start(navigationController!)
        
        //启动黑胶唱片界面
        PlayerController.start(navigationController!)
    }
    
    /// 播放状态点击了
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onPlayingClick(sender:UIButton) {
        print("SheetDetailController onPlayingClick")
        pushMusicPlayerController()
    }
    
    /// 界面即将显示的时候
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //判断是否显示播放界面入口
        if playListManager.getPlayList().count > 0 {
            //有音乐
            vwPlayContainer.isHidden = false
            
            //判断是否有音乐在播放
            
            if musicPlayerManager.isPlaying() {
                //正在播放音乐
                
                //启动动画
                showPlayingBarStatus()
            } else {
                //暂停中
                
                //停止动画
                showPauseBarStatus()
            }
        } else {
            //没有音乐
            vwPlayContainer.isHidden = true
        }
        
    }
    
    /// 显示播放状态的动画
    func showPlayingBarStatus() {
        //无限循环
        ivPlaying.animationRepeatCount = 0
        
        //将动画图片添加到数组中
        var animationImages:[UIImage] = []
        
        //顺序添加图片
        for i in 1...6 {
            let imageName = "\(getPlayingBarImagePrefix())\(i)"
            animationImages.append(UIImage(named: imageName)!)
        }
        
        //倒序添加图片
        for i in (1...6).reversed() {
            let imageName = "\(getPlayingBarImagePrefix())\(i)"
            animationImages.append(UIImage(named: imageName)!)
        }
        
        //设置图片到ImageView
        ivPlaying.animationImages = animationImages
        
        //开始动画
        ivPlaying.startAnimating()
    }
    
    /// 显示播放暂停的图片
    func showPauseBarStatus() {
        if ivPlaying.isAnimating {
            //如果正在播放动画
            //就停止
            ivPlaying.stopAnimating()
        }
        
        //设置一张静态的图片
        ivPlaying.image = UIImage(named: "\(getPlayingBarImagePrefix())1")
    }
    
    /// 返回播放动画图片的前缀
    ///
    /// - Returns: <#return value description#>
    func getPlayingBarImagePrefix() -> String {
        if ThemeManager.isNight() {
            //夜间模式
            return "TatbarPlayingNight"
        } else {
            //日间模式
            return "TatbarPlaying"
        }
        
    }
}
