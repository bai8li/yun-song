//
//  BaseController.swift
//  所有控制器通用父类
//
//  Created by smile on 2019/6/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
        initDatas()
        initListeners()
    }
    
    /// 初始化控件
    func initViews() {
        
    }
    
    /// 初始化数据
    func initDatas() {
        
    }
    
    /// 初始化监听器
    func initListeners()  {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
