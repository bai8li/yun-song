//
//  NewOrderController.swift
//  演示订单接口签名和加密功能界面
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class NewOrderController: BaseTitleController {
    
    
    /// 显示接口信息
    @IBOutlet weak var lbInfo: UILabel!
    
    override func initViews() {
        super.initViews()
        
        setTitle("我的订单（接口签名和加密）")
    }
    
    
    /// 订单列表（响应签名）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onOrderListSignClick(_ sender: Any) {
        print("NewOrderController onOrderListSignClick")
        
        Api.shared.ordersV2().subscribeOnSuccess { (data) in
            if let data=data?.data?.data{
                self.lbInfo.text="(签名接口)订单数量：\(data.count)"
            }
        }.disposed(by: disposeBag)
        
        
    }
    
    
    /// 创建订单（参数签名）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCreateOrderSignClick(_ sender: Any) {
        print("NewOrderController onCreateOrderSignClick")
        
        Api.shared.createOrderV2("1").subscribeOnSuccess { (data) in
            if let _ = data?.data{
                self.lbInfo.text="(签名接口)订单创建成功！"
            }
        }.disposed(by: disposeBag)
    }
    
    
    /// 订单列表（响应加密）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onOrderListEncryptClick(_ sender: Any) {
        print("NewOrderController onOrderListEncryptClick")
        
        Api.shared.orderV3().subscribeOnSuccess { (data) in
            if let data = data?.data?.data{
                self.lbInfo.text="(加密接口)订单数量：\(data.count)"
            }
        }.disposed(by: disposeBag)
    }
    
    /// 创建订单（参数加密）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCreateOrderEncryptClick(_ sender: Any) {
        print("NewOrderController onCreateOrderEncryptClick")
        
        Api.shared.createOrderV3("1").subscribeOnSuccess { (data) in
            if let _ = data?.data{
                self.lbInfo.text="(加密接口)订单创建成功！"
            }
        }.disposed(by: disposeBag)
    }
    
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "NewOrder")
        
        //将控制器放入导航控制器
        nav.pushViewController(controller, animated: true)
    }

}
