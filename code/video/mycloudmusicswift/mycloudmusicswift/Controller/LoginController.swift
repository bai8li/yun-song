//
//  LoginController.swift
//  登录界面
//
//  Created by smile on 2019/6/9.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class LoginController: BaseLoginController {
   
    /// 用户名
    @IBOutlet weak var tfUsername: UITextField!
    
    
    /// 密码
    @IBOutlet weak var tfPassword: UITextField!
    
    
    /// 登录按钮
    @IBOutlet weak var btLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        navigationItem.title="登录"
        
        setTitle("登录")
        
        //输入框左侧显示图标
        tfUsername.showLeftIcon(name: "LoginItemPhone")
        tfPassword.showLeftIcon(name: "LoginItemPhone")
        
        //按钮圆角
        ViewUtil.showLargeRadius(view: btLogin)
    }
    
    
    /// 登录按钮点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLoginClick(_ sender: Any) {
        print("LoginController onLoginClick")
        
        //获取用户名
        let username = tfUsername.text!.trim()!
        
        if username.isEmpty {
            ToastUtil.short("请输入用户名！")
            return
        }
        
//        guard username.isPhone() || username.isEmail() else {
//            //如果用户名不是手机号
//            //如果用户名不是邮箱
//            ToastUtil.short("用户名格式不正确！")
//            return
//        }
        
        var isPhone = false
        if username.isPhone() {
            //手机号
            isPhone=true
        }else if username.isEmail() {
            //邮箱
            isPhone=false
        }else {
            ToastUtil.short("用户名格式不正确！")
            return
        }
        
        //获取密码
        let password=tfPassword.text!.trim()!
        
        if password.isEmpty {
            ToastUtil.short("请输入密码！")
            return
        }
        
        guard password.isPassword() else {
            ToastUtil.short(ERROR_PASSWORD_FORMAT)
            return
        }
        
//        var phone:String?
//        var email:String?
//
//        //判断是手机号还是邮箱
//        if username.isPhone() {
//            //手机号
//            phone=username
//        } else {
//            //邮箱
//            email=username
//        }
//
//        login(phone: phone,email: email, password: password)
        
//        if username.isPhone() {
//            //手机号
//            login(phone: username, password: password)
//        } else {
//            //邮箱
//            login(email: username, password: password)
//        }
        
        if isPhone {
            //手机号
            login(phone: username, password: password)
        } else {
            //邮箱
            login( email: username, password: password)
        }
        
    }
    
    //状态栏样式
//    override var preferredStatusBarStyle: UIStatusBarStyle{
//        //内容为白色
//        return UIStatusBarStyle.lightContent
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
