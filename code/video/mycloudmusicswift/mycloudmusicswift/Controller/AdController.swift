//
//  AdController.swift
//  启动页面后面的广告页面
//
//  Created by smile on 2019/6/10.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class AdController: BaseCommonController {
    
    
    /// 跳过广告按钮
    @IBOutlet weak var btSkipAd: UIButton!
    
    /// 异步任务
    var task:DispatchWorkItem!
    
    /// 广告地址
    /// 只有点击了启动页后面的广告才有值
    var adUri:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        //设置跳过广告按钮圆角
        ViewUtil.showRadius(btSkipAd,15)
    }
    
    override func initDatas() {
        super.initDatas()
        
        //TODO 设置广告
        //如果通过服务端获取到广告图片
        //可以在这设置到ImageView
        
        //创建一个闭包
        //如果要取消一个延时就要使用这种方式创建
        task=DispatchWorkItem {
            //时间到了就会执行这个闭包
            self.next()
        }
        
        //添加任务到队列
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3, execute: task)
    }
    
    /// 进入首页
    func next() {
        print("AdController next")
        
        AppDelegate.shared.toHome(adUri)
    }
    
    /// 状态栏样式
    override var preferredStatusBarStyle: UIStatusBarStyle {
        //内容为白色
        return UIStatusBarStyle.lightContent
    }
    
    /// 点击了广告
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onAdClick(_ sender: Any) {
        print("AdController onAdClick")
        
        //正常情况，该界面的广告来自一个接口
        //他应该包含显示的图片，点击跳转的网址
        //这里就写死
        
        //由于当前界面和首页的导航控制器不是同一个
        //所以不能再这里启动显示广告的页面
        //而应该在显示首页后，在显示广告页面
        //因为我们希望用户看完广告，返回的是首页，而不是广告页面
        
        adUri = "http://www.ixuea.com"
        
        onSkipAdClick(sender)
        
    }
    
    
    /// 点击了跳过广告按钮
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSkipAdClick(_ sender: Any) {
        print("AdController onSkipAdClick")
        
        //取消延时
        task.cancel()
        
        //马上进入下一步操作
        next()
        
        //统计广告跳过事件
        AnalysisUtil.onSkipAd(PreferenceUtil.userId()!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func pageId() -> String? {
        return "Ad"
    }
}
