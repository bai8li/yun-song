//
//  ShopDetailController.swift
//  商品详情界面
//
//  Created by smile on 2019/7/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

class ShopDetailController: BaseTitleController {
    
    /// 封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 价格
    @IBOutlet weak var lbPrice: UILabel!
    
    /// 控制按钮
    @IBOutlet weak var btControl: UIButton!
    
    /// Id
    var id:String!
    
    /// 商品
    var data:Book!
    
    override func initViews() {
        super.initViews()
        
        setTitle("商品详情")
        
        //圆角
        ViewUtil.showSmallRadius(ivBanner)
    }
    
    override func initDatas() {
        super.initDatas()
        
        Api.shared.shopDetail(id).subscribeOnSuccess { (data) in
            if let data=data?.data{
                self.showData(data)
            }
        }.disposed(by: disposeBag)
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听订单支付成功
        SwiftEventBus.onMainThread(self, name: ON_PAY_SUCCESS) { (sender) in
            self.data.buy=0
            
            self.showBuySuccess()
        }
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func showData(_ data:Book) {
        self.data=data
        
        //封面
        ImageUtil.show(ivBanner, data.banner)
        
        //标题
        lbTitle.text=data.title
        
        //价格
        lbPrice.text="优惠价：￥\(String(data.price))"
        
        if data.isBuy() {
            showBuySuccess()
        }
    }
    
    /// 显示购买成功的样式
    func showBuySuccess() {
        btControl.setTitle("已经购买了", for: .normal)
        btControl.setTitleColor(UIColor(named: "ColorPass"), for: .normal)
    }
    
    /// 控制按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onControlClick(_ sender: Any) {
        print("ShopDetailController onControlClick")
        
        if data.isBuy() {
            //已经购买了
            
            ToastUtil.short("已经购买了！")
            
            //真实项目中
            //如果是虚拟物品
            //例如：电子书，视频，课程
            //在这里就是学习入口
            
            //当然为了安全
            //就算这里是学习入口
            //里面接口也要判断
            //只有服务端判断是购买了才返回数据
            //好处是如果有人破解了客户端
            //能进入学习界面
            //但里面的接口不会返回数据
        } else {
            //未购买
            
            //创建订单
            createOrder()
        }
    }
    
    /// 创建订单
    func createOrder() {
        Api.shared.createOrder(data.id).subscribeOnSuccess { (data) in
            if let data=data?.data{
                //订单创建成功
                print("ShopDetailController createOrder succes:\(data.id)")
                
                //跳转到订单详情
                OrderDetailController.start(self.navigationController!, data.id)
            }
            
        }.disposed(by: disposeBag)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    static func start(_ nav:UINavigationController,_ id:String) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "ShopDetail") as! ShopDetailController
        
        //传递数据
        controller.id=id
        
        //添加控制器到导航控制器中
        nav.pushViewController(controller, animated: true)
    }
}
