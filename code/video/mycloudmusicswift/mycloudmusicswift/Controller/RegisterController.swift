//
//  RegisterController.swift
//  注册界面
//
//  Created by smile on 2019/6/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入RxSwift
import RxSwift

//导入网络框架
import Moya

class RegisterController: BaseLoginController {
    /// 昵称控件
    @IBOutlet weak var tfNickname: UITextField!
    
    /// 手机号控件
    @IBOutlet weak var tfPhone: UITextField!
    
    /// 邮箱控件
    @IBOutlet weak var tfEmail: UITextField!
    
    /// 密码控件
    @IBOutlet weak var tfPassword: UITextField!
    
    /// 确认密码控件
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    /// 注册按钮
    @IBOutlet weak var btRegister: UIButton!
    
    /// 昵称
    var nickname:String?
    
    /// 头像
    var avatar:String?
    
    /// 第三方登录后的OpenId
    var openId:String?
    
    /// 第三方登录类型
    var type:SSDKPlatformType?
    
    /// 注册方法
    var method:String = "phone"
    
    override func initViews() {
        super.initViews()
        
//        //设置导航栏标题颜色
//        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.red]
//
//        //设置导航栏返回按钮颜色
//        navigationController!.navigationBar.tintColor=UIColor.red
        
        //设置圆角
        ViewUtil.showLargeRadius(view: btRegister)
        
        //设置左侧显示的图标
        tfNickname.showLeftIcon(name: "LoginItemPhone")
        tfPhone.showLeftIcon(name: "LoginItemPhone")
        tfEmail.showLeftIcon(name: "LoginItemPhone")
        tfPassword.showLeftIcon(name: "LoginItemPhone")
        tfConfirmPassword.showLeftIcon(name: "LoginItemPhone")
    }
    
    override func initDatas() {
        super.initDatas()
        
        if let _ = openId {
            //是第三方登录后跳转过来的
            
            //设置标题
//            navigationItem.title="补充资料"
            setTitle("补充资料")
            
            //将昵称显示到输入框
            tfNickname.text = nickname
            
            //更改注册按钮标题
            btRegister.setTitle("完成注册", for: .normal)
        }else {
            //不是第三方登录
            
//            navigationItem.title="注册"
            setTitle("注册")
            
            //测试设置标题文本颜色的方法
//            setTitleTextColor(UIColor.red)
        }
    }
    
    
    /// 注册按钮点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onRegisterClick(_ sender: Any) {
        print("RegisterController onRegisterClick")
        
        //测试网络框架
        //请求歌单详情
//        let provider=MoyaProvider<Service>()
        
        //使用网络活动指示器插件
//        let networkActivityPlugin = NetworkActivityPlugin { (changeType, targetType) in
//            //changeType他的类似是NetworkActivityChangeType
//
//            //targetType的类型是TargetType
//            //就是我们项目中的service枚举
//            if changeType == .began {
//                //开始请求
//                print("RegisterControler began request:\(targetType.path)")
//
//                ToastUtil.showLoading()
//            } else {
//                //结束请求
//                print("RegisterController end request:\(targetType.path)")
//
//                ToastUtil.hideLoading()
//            }
//
//        }
//
        
        //使用网络日志插件
//        let provider=MoyaProvider<Service>(plugins: [NetworkLoggerPlugin(),networkActivityPlugin])
        
        //发送一个网络请求
//        provider.request(.sheetDetail(id: "1")) { result in
//            //result类型是Result<Response,MoyaError>
//            switch result {
//            case let .success(response):
//                //请求成功
//                let data=response.data
//                let code=response.statusCode
//
//                //将data转为字符串
//                let dataString=String(data: data, encoding: .utf8)
//
//                print("RegisterController request sheet detail success:\(code),\(dataString)")
//
//            case let .failure(error):
//                //请求失败
//                print("RegisterController request sheet detail failed:\(error)")
//            }
//        }
        
        //错误判断
//        provider.request(.sheetDetail(id: "1")) { (result) in
//            switch result {
//            case let .success(response):
//                //请求成功
//                let data = response.data
//                let code = response.statusCode
//
//                //将data转为字符串
//                let dataString=String(data: data, encoding: .utf8)
//
//                print("RegisterController request sheet detail success:\(code),\(dataString)")
//
//            case let .failure(error):
//                //请求失败
//                print("RegisterController request sheet detail failed:\(error)")
//
//                //错误类型转为MoyaError
//                let error=error as! MoyaError
//
//                //MoyaError是一个枚举
//                switch error {
//                case .imageMapping(let response):
//                    print("图片解析错误")
//                case .jsonMapping(let response):
//                    print("JSON解析错误")
//                case .statusCode(let response):
//                    print("状态错误")
//                case .stringMapping(let response):
//                    print("字符串映射错误")
//                case .underlying(let nsError as NSError, let response):
//                    print("这里将错误转为了NSError")
//
//                    switch nsError.code {
//                    case NSURLErrorNotConnectedToInternet:
//                        print("网络不太好，请稍后再试！")
//                    case NSURLErrorTimedOut:
//                        print("连接超时，请稍后再试！")
//                    default:
//                        print("未知错误，请稍后再试！")
//                    }
//
//                case .objectMapping(_, _):
//                    print("对象解码错误")
//                case .encodableMapping(_):
//                    print("对象编码错误")
//                case .requestMapping(_):
//                    print("请求映射错误")
//                case .parameterEncoding(_):
//                    print("参数编码错误")
//                }
//
//            }
//        }
        
        
        //使用RxSwift方法是请求网络
//        provider
//            .rx
//            .request(.sheetDetail(id: "1"))
//            .subscribe { (event) in
//            //event类型是SingleEvent<Response>
//
//            switch event {
//            case let .success(response):
//                //请求成功
//                let data = response.data
//                let code=response.statusCode
//
//                //将data转为字符串
//                let dataString=String(data: data, encoding: .utf8)
//
//                print("RegisterController request sheet detail success:\(code),\(dataString)")
//
//            case let .error(error):
//                print("RegisterController request sheet detail failed:\(error)")
//            }
//        }
        
        //测试将网络请求JSON转为对象
//        provider.rx.request(.sheetDetail(id: "1")).subscribe { (event) in
//            switch event {
//            case let .success(response):
//                //请求成功
//                let data=response.data
//                let code=response.statusCode
//
//                let dataString=String(data: data, encoding: .utf8)
//
//                //解析JSON为对象
//                let sheetWrapper=SheetWrapper.deserialize(from: dataString)
//
//                //解析完成了
//                print("RegisterController request sheet detail succes:\(sheetWrapper?.data.title)")
//            case .error(_):
//                print("RegisterController request sheet detail failed")
//            }
//        }
        
        
        //测试扩展RxSwift添加的JSON解析为对象的方法
//        provider.rx.request(.sheetDetail(id: "1")).asObservable().mapString().mapObject(SheetWrapper.self).subscribe { (event) in
//            if let data = event.element {
//                print("RegiseterController request sheet detail success:\(data?.data.title)")
//            }
//        }
        
        //使用DetailResponse解析详情网络请求数据
//        provider
//            .rx
//            .request(.sheetDetail(id: "1"))
//            .asObservable()
//            .mapString()
//            .mapObject(DetailResponse<Sheet>.self)
//            .subscribe(onNext: { data in
//            print("RegisterController request sheet detail success:\(data?.data?.title)")
//        }, onError: { error in
//            print("RegisterController request sheet detail faield:\(error)")
//        }, onCompleted: {
//            print("onCompleted")
//        }) {
//            print("OnDisposed")
//        }
        
        //使用ListResponse解析列表网络请求
//        provider.rx.request(.sheets).filterSuccessfulStatusCodes().asObservable().mapString().mapObject(ListResponse<Sheet>.self).subscribe(onNext: { (data) in
//            print("RegisterController request sheet list success:\(data?.data?.count)")
//        }, onError: { (error) in
//            print("RegisterController request sheet list failed:\(error)")
//        }, onCompleted: {
//
//        }) {
//
//        }
        
        //使用封装后的Provider
        
        //歌单详情
//        Api
//            .shared
//            .sheetDetail(id: "1")
//            .subscribe(onNext: { data in
//                print("RegisterController request sheet detail succes:\(data?.data?.title)")
//        }, onError: { error in
//
//        }, onCompleted: {
//
//        }) {
//
//        }
        
        //歌单列表
//        Api.shared.sheets().subscribe(onNext: { data in
//            print("RegisterController request sheet list success:\(data?.data?.count)")
//        }, onError: { error in
//
//        }, onCompleted: {
//
//        }) {
//
//        }
        
        //只关注请求请求
//        Api
//            .shared
//            .sheets()
//            .subscribeOnSuccess { data in
//            if let data = data?.data {
//                print("RegisterController request sheet list success:\(data.count)")
//            }
//
////                if let data = data {
////                    if let sheets = data.data {
////                        //有歌单数据
////                    }else {
////                        //没有歌单数据
////                    }
////                }else {
////                    //JSON有问题
////                }
//        }.disposed(by: disposeBag)
    
    //测试自动处理网络请求错误
//    Api
//    .shared
//    .sheetDetail(id: "100000")
//    .subscribeOnSuccess { data in
//        if let data = data?.data {
//        //请求歌单列表成功了
//        //并且有歌单数据
//            print("RegisterController request sheet detail success:\(data.title)")
//        }
//    }.disposed(by: disposeBag)
        
        //测试监听错误
//        Api.shared.sheetDetail(id: "100000").subscribe({ data in
//            if let data = data?.data {
//                print("RegisterController request sheet detail success:\(data.title)")
//            }
//        }) { (baseResponse,error) -> Bool in
//            print("RegisterController request sheet detail failed:\(baseResponse),\(error)")
//
//            //返回false表示让框架自动处理错误
//            return true
//        }.disposed(by: disposeBag)
        
        //使用响应式编程使用过滤出有评论的歌单
//        Api
//            .shared
//            .sheets()
//            .map { data -> ListResponse<Sheet>? in
//                //判断是否有歌单数据
//                if let lists = data?.data {
//                    //有歌单数据
//
//                    //过滤操作
//                    data!.data = lists.filter { sheet -> Bool in
//                        //返回true的元素会被选中
//                        //返回false的元素会被过滤
//                        return sheet.comments_count > 0
//                    }
//                }
//
//                //没有歌单
//                return data
//            }
//            .subscribeOnSuccess { data in
//                print("RegisterController request sheet list success:\(data?.data?.count)")
//        }.disposed(by: disposeBag)
        
        //获取昵称
        let nickname=tfNickname.text!.trim()!

//        //去掉字符串中前后的空格
//        let whitespace=NSCharacterSet.whitespacesAndNewlines
//
//        nickname=nickname.trimmingCharacters(in: whitespace)
//
        //判断是否输入了昵称
        if nickname.isEmpty {
            ToastUtil.short("请输入昵称！")
            return
        }

//        //判断昵称格式
//        if nickname.count<2 || nickname.count>15 {
//            ToastUtil.short("昵称长度不对！")
//            return
//        }

        guard nickname.isNickname() else {
            ToastUtil.short("昵称长度不对！")
            return
        }
//
//
//        //获取到手机号
        let phone=tfPhone.text!.trim()!

//        //去掉前后的空白字符
//        phone=phone.trimmingCharacters(in: whitespace)
//
        if phone.isEmpty {
            ToastUtil.short("请输入手机号！")
            return
        }

        //使用重构后的方法

        //判断昵称格式
//        if !nickname.isNickname() {
//            ToastUtil.short("昵称长度不对！")
//            return
//        }

        //判断手机号格式
        guard phone.isPhone() else {
            ToastUtil.short("手机号格式不正确！")
            return
        }


        //获取邮箱
        let email=tfEmail.text!.trim()!

        //为空判断
        if email.isEmpty {
            ToastUtil.short("请输入邮箱！")
            return
        }

        //判断邮箱格式
        guard email.isEmail() else {
            ToastUtil.short("邮箱格式不正确！")
            return
        }

        //密码
        let password=tfPassword.text!.trim()!

        if password.isEmpty {
            ToastUtil.short("请输入密码！")
            return
        }

        guard password.isPassword() else {
//            ToastUtil.short("密码格式不正确！")
            ToastUtil.short(ERROR_PASSWORD_FORMAT)
            return
        }

        //确认密码
        let confirmPassword=tfConfirmPassword.text!.trim()!

        if confirmPassword.isEmpty {
            ToastUtil.short("请输入确认密码！")
            return
        }

        guard confirmPassword.isPassword() else {
            ToastUtil.short("确认密码格式不正确！")
            return
        }

        //判断确认密码和密码是否一样
        guard password==confirmPassword else {
            ToastUtil.short("两次密码不一致！")
            return
        }
        
        var qq_id:String?
        var weibo_id:String?
        var wechat_id:String?
        
        //判断第三方登录类型
        if type == .typeQQ {
            //QQ第三方登录
            qq_id = openId
            
            method="qq"
        }else if type == SSDKPlatformType.typeWechat {
            //微信第三方登录
            wechat_id = openId
            
            method="wechat"
        }else if type == SSDKPlatformType.typeSinaWeibo {
            //微博第三方登录
            weibo_id=openId
            
            method="weibo"
        }
        
        //参数都没有问题
        
        //调用网络接口注册
        
        Api
            .shared
            .createUser(avatar: avatar,nickname: nickname, phone: phone, email: email, password: password,qq_id: qq_id,weibo_id: weibo_id,wechat_id:wechat_id).subscribe({ data in
                if let data = data?.data {
                    //注册成功
                    AnalysisUtil.onRegister(success: true,method:self.method, avatar: self.avatar, nickname: nickname, phone: phone, email: email, qqId: qq_id, weiboId: weibo_id)
                    
                    //进行后续的处理
                    
                    print("RegisterController onRegisterClick create user succes:\(data.id)")
                    
                    //注册成功后自动登录
//                    self.login(phone: phone, email: email, password: password)
                    
                    //注册用户到聊天服务端
                    //由于注册的时候不能上传头像
                    //所以我们就干脆不让sdk管理任何用户信息
                    //只传递用户名（我们这里传递的是用户Id）
                    
                    //这里不能直接传递密码
                    //因为第三方登陆的时候没有密码
                    //所以如果是第三方登陆
                    //那么就会导致无法登陆聊天服务器
                    //这里我们就简单实现
                    //直接将用户Id当密码
                    //但这样实现其实是有重大Bug
                    //因为用户的Id不会变
                    //真实项目中应该换其他方式实现
                    let id=StringUtil.wrapperUserId(data.id)
                    JMSGUser.register(withUsername: id, password: id, completionHandler: { (data, error) in
                        if let error=error{
                            print("RegisterController message register failed:\(error)")
                        }else{
                            print("RegisterController message register success")
                            
                            //注册成功后自动登录
                            self.login(phone: phone, email: email, password: password)
                        }
                    })
                }else {
                    //注册失败
                    self.onRegisterFailed(avatar: self.avatar, nickname: nickname, phone: phone, email: email, qqId: qq_id, weiboId: weibo_id)
                    
                    ToastUtil.short("注册失败，请稍后再试！")
                }
            }, { (baseResponse, error) -> Bool in
                //注册失败
                
                self.onRegisterFailed(avatar: self.avatar, nickname: nickname, phone: phone, email: email, qqId: qq_id, weiboId: weibo_id)
                
                //让父类自动处理错误
                return false
            }).disposed(by: disposeBag)
    }
    
    /// 注册失败事件
    ///
    /// - Parameters:
    ///   - avatar: <#avatar description#>
    ///   - nickname: <#nickname description#>
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - qqId: <#qqId description#>
    ///   - weiboId: <#weiboId description#>
    func onRegisterFailed(avatar:String?=nil,nickname:String,phone:String,email:String,qqId:String?=nil,weiboId:String?=nil) {
        AnalysisUtil.onRegister(success: false,method:method, avatar: avatar, nickname: nickname, phone: phone, email: email, qqId: qqId, weiboId: weiboId)
    }
    
    
    /// 注册协议点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onAgreementClick(_ sender: Any) {
        print("RegisterController onAgreementClick")
        
        //使用web控制器打开用户协议
        //这里打开的是爱学啊官网用户协议
        //大家可以根据实际情况修改
        WebController.start(navigationController!, "用户协议", "http://www.ixuea.com/posts/1")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
