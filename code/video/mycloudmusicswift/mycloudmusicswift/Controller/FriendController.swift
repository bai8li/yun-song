//
//  FriendController.swift
//  首页-朋友界面
//
//  Created by smile on 2019/7/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

class FriendController: BaseTitleController {
    
    /// 附近列表控件
    @IBOutlet weak var nearbyTableView: UITableView!
    
    /// 动态容器
    @IBOutlet weak var svFeedContainer: UIStackView!
    
    /// 动态列表控件
    @IBOutlet weak var feedTableView: UITableView!
    
    /// 附近数据源
    var nearbyDataArray:[String] = []
    
    /// 动态数据源
    var feedDataArray:[Feed] = []
    
    /// 当前预览图片列表
    var images:[UIImage]?
    
    
    override func initViews() {
        super.initViews()
        
        //分段控件
        let segmentedControl=TwicketSegmentedControl(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        //未选中文本颜色
        segmentedControl.defaultTextColor=UIColor(hex: COLOR_PRIMARY)
        
        //选中后的颜色
        segmentedControl.highlightTextColor=UIColor.white
        
        //未选中的背景颜色
//        segmentedControl.segmentsBackgroundColor=UIColor.clear
        
        //选中的背景颜色
        segmentedControl.sliderBackgroundColor=UIColor(hex: COLOR_PRIMARY)
        
        //是否显示阴影
        segmentedControl.isSliderShadowHidden=true
        
        //设置代理
        segmentedControl.delegate=self
        
        //显示标题
        let titles=["动态","附近"]
        
        //设置标题
        segmentedControl.setSegmentItems(titles)
        
        //将分段控件添加到导航栏
        navigationController?.navigationBar.topItem?.titleView=segmentedControl
        
        //注册Cell
        //注册文本动态
        feedTableView.register(UINib(nibName: FeedTextCell.NAME, bundle: nil), forCellReuseIdentifier: FeedTextCell.NAME)
        
        //注册图片动态
        feedTableView.register(UINib(nibName: FeedImageCell.NAME, bundle: nil), forCellReuseIdentifier: FeedImageCell.NAME)
    }
    
    override func initDatas() {
        super.initDatas()
        
        
        
        //附近
        //由于没有什么难点
        //就随便创建一点模拟数据
        //让界面显示出来就行了
        nearbyDataArray.append("由于该界面没有什么难点，所以就随便创建一点模拟数据")
        
        nearbyDataArray.append("你好，这是附近人的界面！")
        nearbyDataArray.append("人生苦短，我们只做好课！")
        nearbyDataArray.append("我们是爱学啊！")
        nearbyDataArray.append("学习成就更好的自己！")
        
        nearbyTableView.reloadData()
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听动态改版了
        SwiftEventBus.onMainThread(self, name: ON_FEED_CHANGED) { (sender) in
            self.feedDataArray.removeAll()
            self.fetchData()
        }
    }
    
    func fetchData() {
        Api.shared.feeds().subscribeOnSuccess { (data) in
            if let data=data?.data?.data{
                self.feedDataArray=self.feedDataArray+data
                self.feedTableView.reloadData()
            }
        }.disposed(by: disposeBag)
    }

    /// 视图即将显示
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置导航栏为透明
        setTitleBarTransparet()
        
        fetchData()
    }
    
    
    /// 发动态点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPublishFeedClick(_ sender: Any) {
        print("FriendController onPublishFeedClick")
    }
    
    /// 发视频点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPublishFeedVideoClick(_ sender: Any) {
        print("FriendController onPublishFeedVideoClick")
    }
    
    /// 返回cell类型
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    func typeForItemAtData(_ data:Feed) -> CellType {
        if let _ = data.images {
            //有图片
            return .image
        }
//        else if let _ = data.video {
//            //TODO 有视频
//        }
        
        //文本动态
        return .text
    }

    /// 动态Cell类型
    ///
    /// - text: 文本动态
    /// - image: 图片动态
    enum CellType {
        case text
        case image
    }
    
    /// 图片预览
    ///
    /// - Parameters:
    ///   - images: <#images description#>
    ///   - index: <#index description#>
    func imagePreview(_ images:[UIImage],_ index:Int) {
        self.images = images
        
        //启动图片预览框架
        let galleryViewController = GalleryViewController(startIndex: index, itemsDataSource: self)
        
        //显示图片预览框架
        presentImageGallery(galleryViewController)
    }
}

// MARK: - 分段控件代理
extension FriendController:TwicketSegmentedControlDelegate {
    
    /// 选择了某一段
    ///
    /// - Parameter segmentIndex: <#segmentIndex description#>
    func didSelect(_ segmentIndex: Int) {
        print("FriendController segment didSelect:\(segmentIndex)")
        
        if segmentIndex==0 {
            //动态列表
            svFeedContainer.isHidden=false
            nearbyTableView.isHidden=true
        } else {
            //附近列表
            svFeedContainer.isHidden=true
            nearbyTableView.isHidden=false
        }
    }
    
    
}

// MARK: - 列表数据源和代理
extension FriendController:UITableViewDataSource,UITableViewDelegate {
    
    /// 返回有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView ==  feedTableView {
            //动态
            return feedDataArray.count
        } else {
            //附近
            return nearbyDataArray.count
        }
        
    }
    
    /// 返回cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == feedTableView {
            //动态
            
            let data = feedDataArray[indexPath.row]
            
            //获取类型
            let type=typeForItemAtData(data)
            
            switch type {
            case .image:
                //图片动态
                let cell = tableView.dequeueReusableCell(withIdentifier: FeedImageCell.NAME, for: indexPath) as! FeedImageCell
                
                //设置图片点击回调
                cell.onImageClick = {
                   images,index in
                    self.imagePreview(images,index)
                }
                
                //绑定数据
                cell.bindData(data)
                
                //返回cell
                return cell
            default:
                //文本动态
                let cell = tableView.dequeueReusableCell(withIdentifier: FeedTextCell.NAME, for: indexPath) as! BaseFeedCell
                
                //设置用户信息点击回调
                cell.onUserInfoClick = {
                    data in
                    UserDetailController.start(self.navigationController!, userId: data.id)
                }
                
                //绑定数据
                cell.bindData(data)
                
                //返回Cell
                return cell
            }
        } else {
            //附近
            //获取cell
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath)
            
            //绑定数据
            cell.textLabel?.text = nearbyDataArray[indexPath.row]
            
            //返回cell
            return cell
        }
        
        
    }
}

// MARK: - 图片预览框架数据源
extension FriendController:GalleryItemsDataSource{
    
    /// 有多少张图片
    ///
    /// - Returns: <#return value description#>
    func itemCount() -> Int {
        return images!.count
    }
    
    /// 返回当前位置的Image
    ///
    /// - Parameter index: <#index description#>
    /// - Returns: <#return value description#>
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        //这里返回的是本地图片
        //        return GalleryItem.image {
        //            let image=UIImage(named: "Guide1")
        //            $0(image)
        //        }
        
        //返回从Cell中获取的图片
        return GalleryItem.image(fetchImageBlock: {
            let image = self.images![index]
            $0(image)
        })
    }
    
    
}
