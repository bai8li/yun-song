//
//  WebController.swift
//  通用的显示网页的界面
//
//  Created by smile on 2019/6/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入WKWebView
import WebKit

class WebController: BaseController {
    
    
    /// WebView控件
    @IBOutlet weak var wv: WKWebView!
    
    /// 要显示的地址
    var uri:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        //设置UI代理
        wv.uiDelegate=self
        
        //导航代理
        wv.navigationDelegate=self
    }
    
    override func initDatas() {
        super.initDatas()
        
        //将字符串创建为URL
        let url=URL(string: uri)!
        
        //创建request
        let request = URLRequest(url: url)
        
        //使用WebView加载这个请求
        wv.load(request)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - WKUIDelegate代理
extension WebController:WKUIDelegate{
    
}

// MARK: - WKNavigationDelegate代理
extension WebController: WKNavigationDelegate{
    
}

// MARK: - 启动界面
extension WebController {
    
    /// 启动界面
    ///
    /// - Parameters:
    ///   - navigationController: 导航控制器
    ///   - title: 显示的标题
    ///   - uri: 显示的网址
    static func start(_ navigationController:UINavigationController, _ title:String,_ uri:String) {
        //创建控制器
        let controller = navigationController.storyboard?.instantiateViewController(withIdentifier: "Web") as! WebController
        
        //传递参数
        controller.title=title
        controller.uri=uri
        
        //将控制器压入导航控制器中
        navigationController.pushViewController(controller, animated: true)
    }
}
