//
//  SearchResultController.swift
//  搜索结果展示控制器
//
//  Created by smile on 2019/7/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

//类似Android中ViewPager效果
import Tabman

//Tabman依赖他
import Pageboy

class SearchResultController: TabmanViewController {
    
    /// 要显示的控制器
    private var viewControllers=[SheetController(),UserListController(),UIViewController(),UIViewController(),UIViewController(),UIViewController(),UIViewController()]
    
    /// 指示器标题
    private var titles=["歌单","用户","单曲","视频","歌手","专辑","主播电台"]
    
    /// 搜索关键字
    private var query:String?
    
    /// 当前搜索控制器
    private var currentSearchController:UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        print("SearchResultController viewDidLoad")

        //监听搜索事件
        SwiftEventBus.onMainThread(self, name: ON_SEARCH_CLICK) { (sender) in
            let data=sender?.object as! String
            
            print("SearchResultController search:\(data)")
            
            //保存搜索关键字
            self.query=data
            
            //搜索
            self.onSearch()
        }
        
        //设置数据源
        self.dataSource=self

        //创建指示器
        let bar=TMBar.ButtonBar()

        //自定义按钮
        bar.buttons.customize { (button) in
            //默认颜色
            button.tintColor = .black

            //选中后颜色
            button.selectedTintColor=UIColor(hex: COLOR_PRIMARY)
        }

        //指示器颜色
        bar.indicator.tintColor = UIColor(hex: COLOR_PRIMARY)

        //设置指示器里面的内容排列方式
        bar.layout.transitionStyle = .progressive

        //指示器到外部的边距
        bar.layout.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)

        //将指示器添加到当前控制器中
        //显示位置是顶部
        addBar(bar, dataSource: self, at: .top)
    }
    
    /// 搜索
    func onSearch() {
        if let query = query {
            if currentSearchController is SheetController {
                (currentSearchController as! SheetController).fetchData(query)
            }else if currentSearchController is UserListController{
                (currentSearchController as! UserListController).fetchData(query)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 指示器数据源和代理
extension SearchResultController:PageboyViewControllerDataSource,TMBarDataSource{
    
    /// 有多少个列表
    ///
    /// - Parameter pageboyViewController: <#pageboyViewController description#>
    /// - Returns: <#return value description#>
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    /// 返回当前位置的控制器
    ///
    /// - Parameters:
    ///   - pageboyViewController: <#pageboyViewController description#>
    ///   - index: <#index description#>
    /// - Returns: <#return value description#>
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        //从其他界面切换到未显示界面
        //该方法会调用两次
        print("SearchResultController index:\(index)")
        
        //保存当前搜索结果控制器
        currentSearchController = viewControllers[index]
        
        //搜索
        onSearch()
        
        return currentSearchController
    }
    
    /// 默认页面
    ///
    /// - Parameter pageboyViewController: <#pageboyViewController description#>
    /// - Returns: <#return value description#>
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
    /// 当前指示器
    ///
    /// - Parameters:
    ///   - bar: <#bar description#>
    ///   - index: <#index description#>
    /// - Returns: <#return value description#>
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
       return TMBarItem(title: titles[index])
    }
    
    
}
