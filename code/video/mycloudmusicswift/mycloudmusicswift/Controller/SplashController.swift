//
//  SplashController.swift
//  第二个启动界面；例如：可以添加判断用户是否登录从而跳转到不同的界面，判断是否显示引导界面，获取广告等功能
//
//  Created by smile on 2019/6/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SplashController: BaseCommonController {

    
    /// 加载视图成功后调用该方法
    /// 类似Android中Activity的onCreate方法
    override func viewDidLoad() {
        super.viewDidLoad()

        //延时3秒钟
//        DispatchQueue.main.asyncAfter(deadline: .now()+3.0) {
            self.next()
//        }
        
//        //测试偏好设置
//
//        //获取偏好设置对象
//        let userDefaults=UserDefaults.standard
//
//        //保存一个字符串
//        userDefaults.set("我们是爱学啊！", forKey: "username")
//
//        //获取上面保存的值
//        var username=userDefaults.string(forKey: "username")
//
//        //打印取出来的值
//        print("SplashController viewDidLoad:\(username)")
//
//        //删除一个key
//        userDefaults.removeObject(forKey: "username")
//
//        //再次获取
//        username = userDefaults.string(forKey: "username")
//
//        //打印删除后在获取的值
//        print("SplashController viewDidLoad delete:\(username)")
//
//        //测试获取用户登录信息
//        print("SplashController viewDidLoad userId:\(PreferenceUtil.userId())")
    }
    
    /// 延时3秒钟后调用
    func next() {
        print("SplashController next")
        
//        //获取当前应用的AppDelegate对象
//        //全局只有一个
//
//        //这里可能会报错找不到AppDelegate
//        //这是正常的
//        let appDelegate=UIApplication.shared.delegate as! AppDelegate
//
//        //调用它里面的方法显示引导界面
//        appDelegate.toGuide()
        
        //显示引导界面
//        AppDelegate.shared.toGuide()
        
        if PreferenceUtil.isShowGuide() {
            //显示引导界面
            AppDelegate.shared.toGuide()
        } else if PreferenceUtil.isLogin() {
            //表示已经登录了

            //所以直接跳转到首页
//            AppDelegate.shared.toHome()
            
            //显示广告页面
            //就显示广告页面
            //在广告页面再进入主界面
            //可以根据自己的需求来更改
            //同时只有用户登陆了
            //才显示也给用户有更好的体验
            AppDelegate.shared.toAd()
        } else {
            //显示登录/注册界面
            AppDelegate.shared.toLoginOrRegister()
        }
    }

    /// 返回当前页面标识
    ///
    /// - Returns: <#return value description#>
    override func pageId() -> String? {
        return "Splash"
    }
}

//// MARK: - 极光统计
//extension SplashController{
//
//    /// 界面即将显示的时候调用
//    ///
//    /// - Parameter animated: <#animated description#>
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        //使用极光统计
//        //页面开始事件
//        JANALYTICSService.startLogPageView("Splash")
//
//    }
//
//    /// 界面已经消失调用
//    ///
//    /// - Parameter animated: <#animated description#>
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//
//        //页面结束事件
//        JANALYTICSService.stopLogPageView("Splash")
//    }
//}
