//
//  SelectLyricController.swift
//  选择歌词界面
//
//  Created by smile on 2019/6/29.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SelectLyricController: BaseTitleController {
    
    
    /// 背景
    @IBOutlet weak var ivBackground: UIImageView!
    
    /// 歌词列表
    @IBOutlet weak var tableView: UITableView!
    
    /// 分享歌词文本
    @IBOutlet weak var btShareLyric: UIButton!
    
    
    /// 分享歌词图片
    @IBOutlet weak var btShareLyricImage: UIButton!
    
    
    /// 分享歌词视频
    @IBOutlet weak var btShareLyricVideo: UIButton!
    
    
    /// 歌曲
    var song:Song!
    
    /// 歌词
    var lyric:Lyric!
    

    override func initViews() {
        super.initViews()
        setTitle("选择歌词")
        
        //按钮圆角
        ViewUtil.showRadius(btShareLyric, 15)
        ViewUtil.showRadius(btShareLyricImage, 15)
        ViewUtil.showRadius(btShareLyricVideo, 15)
        
        //边框
        btShareLyric.showColorPrimaryBorder()
        btShareLyricImage.showColorPrimaryBorder()
        btShareLyricVideo.showColorPrimaryBorder()
    }
    
    override func initDatas() {
        super.initDatas()
        
        //显示背景图片
        ImageUtil.show(ivBackground, song.banner)
    }
    
    /// 获取选择的歌词
    ///
    /// - Parameter separator: <#separator description#>
    /// - Returns: <#return value description#>
    func getSelectLyricString(_ separator:String) -> String {
        var lyricStringArray:[String] = []
        
        //获取选中的歌词索引
        var indexPaths = tableView.indexPathsForSelectedRows
        
        if indexPaths == nil {
            return ""
        }
        
        //排序
        //由于该方法获取的indexPath是选择顺序
        //而我们需要的是列表顺序
        //所以按照row从小到大排序
        indexPaths = indexPaths!.sorted { (obj1, obj2) -> Bool in
            return obj1.row<obj2.row
        }
        
        //遍历
        for indexPath in indexPaths! {
            let line = lyric.datas[indexPath.row]
            
            //把歌词添加到数组中
            lyricStringArray.append(line.data)
        }
        
        //将字符串使用分隔符连接
        return lyricStringArray.joined(separator: separator)
    }
    
    /// 分享歌词
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onShareLyricClick(_ sender: Any) {
        let lyricString = getSelectLyricString("，")
        
        if lyricString.isEmpty {
            ToastUtil.short("请选择歌词！")
            return
        }
        
        //分享
        ShareUtil.shareLyricText(song, lyricString)
        
        print("SelectLyricController onShareLyricClick:\(lyricString)")
        
    }
    
    
    /// 分享歌词图片
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onShareLyricImageClick(_ sender: Any) {
        //获取选中的歌词
        let lyricString = getSelectLyricString("\n")
        
        if lyricString.isEmpty {
            ToastUtil.short("请选择歌词！")
            return
        }
        
        print("SelectLyricController onShareLyricImageClick:\(lyricString)")
        
        ShareLyricImageController.start(navigationController!, song, lyricString)
        
    }
    

    /// 界面即将显示
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置标题栏为亮色
        setTitleBarLight()
    }
    
    /// 界面即将消失
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //设置标题栏为暗色
        setTitleBarDefault()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension SelectLyricController {
    
    /// 启动界面
    ///
    /// - Parameters:
    ///   - navigationController: <#navigationController description#>
    ///   - song: <#song description#>
    ///   - lyric: <#lyric description#>
    static func start(_ navigationController:UINavigationController,_ song:Song,_ lyric:Lyric) {
        //创建控制器
        let controller = navigationController.storyboard!.instantiateViewController(withIdentifier: "SelectLyric") as! SelectLyricController
        
        //传递数据
        controller.song = song
        controller.lyric = lyric
        
        navigationController.pushViewController(controller, animated: false)
        
    }
}

// MARK: - 列表数据源和代理
extension SelectLyricController:UITableViewDataSource,UITableViewDelegate {
    
    /// 有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lyric.datas.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath) as! SelectLyricCell
        
        //绑定数据
        cell.bindData(lyric.datas[indexPath.row])
        
        return cell
    }
    
    
}
