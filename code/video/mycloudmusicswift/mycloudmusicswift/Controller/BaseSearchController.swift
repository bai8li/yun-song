//
//  BaseSearchController.swift
//  项目全局通用搜索控制器
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

//导入响应式编程框架
import RxSwift

class BaseSearchController<T>: UITableViewController {
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag=DisposeBag()
    
    /// 列表数据源
    var dataArray:[T] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //监听点击了搜索
//        SwiftEventBus.onMainThread(self, name: ON_SEARCH_CLICK) { (sender) in
//            let data=sender?.object as! String
//            
//            print("SheetController search:\(data)")
//            
//            self.fetchData(data)
//        }
    }

    /// 获取数据
    ///
    /// - Parameter query: <#query description#>
    func fetchData(_ query:String) {
        
    }
    
    /// 返回有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    
}
