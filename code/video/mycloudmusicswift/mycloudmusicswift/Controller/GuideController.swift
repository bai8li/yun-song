//
//  GuideController.swift
//  引导界面
//
//  Created by smile on 2019/6/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class GuideController: UIViewController {
    
    // MARK: - 字段
    // MARK: 顶部的轮播图
    @IBOutlet weak var bannerView: YJBannerView!
    
    // MARK: 底部容器
    @IBOutlet weak var vwFooterContainer: UIView!
    
    // MARK: 登录注册按钮
    @IBOutlet weak var btLoginOrRegister: UIButton!
    
    
    // MARK: 立即体验按钮
    @IBOutlet weak var btEnter: UIButton!
    
    // MARK: - 初始化
    override func viewDidLoad() {
        super.viewDidLoad()

        //初始化轮播图组件
        initBannerView()
    }
    
    /// 当布局完成后
    override func viewDidLayoutSubviews() {
        //获取当前View的frame
//        let contentFrame=view.frame
        
        //底部按钮垂直位置，y
        let buttonY=(vwFooterContainer.frame.height-CGFloat(SIZE_BUTTON_ENTER_HEIGHT))/2
        
        //登录注册按钮水平位置，x
        let btLoginOrRegisterX=(vwFooterContainer.frame.width-(CGFloat(SIZE_BUTTON_ENTER_WIDTH)*2))/3
        
        //设置登录注册按钮位置和大小
        btLoginOrRegister.frame=CGRect(x: btLoginOrRegisterX, y: buttonY, width: CGFloat(SIZE_BUTTON_ENTER_WIDTH), height: CGFloat(SIZE_BUTTON_ENTER_HEIGHT))
        
        //立即体验按钮水平位置，x
        let btEnterX=btLoginOrRegisterX*2+CGFloat(SIZE_BUTTON_ENTER_WIDTH)
        
        //设置立即体验按钮的位置和大小
        btEnter.frame=CGRect(x: btEnterX, y: buttonY, width: CGFloat(SIZE_BUTTON_ENTER_WIDTH), height: CGFloat(SIZE_BUTTON_ENTER_HEIGHT))
        
//        //登录注册按钮圆角为22
//        btLoginOrRegister.layer.cornerRadius=22
        ViewUtil.showLargeRadius(view: btLoginOrRegister)

//        //立即体验按钮圆角为22
//        btEnter.layer.cornerRadius=22
        ViewUtil.showLargeRadius(view: btEnter)
        
        
        //立即体验按钮边框，1
//        btEnter.layer.borderWidth=1
//
//        //立即体验按钮边框颜色
//        btEnter.layer.borderColor=UIColor(red: 221/255, green: 0, blue: 0, alpha: 1.0).cgColor
        
        btEnter.showColorPrimaryBorder()
        
//        btEnter.layer.borderColor=UIColor(hex: 0x00ff00).cgColor
        
//        btEnter.layer.borderColor=UIColor(named: "ColorPrimary")!.cgColor
        
        
    }
    
    
    // MARK: - 事件
    /// 登录注册按钮点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLoginOrRegisterClick(_ sender: UIButton) {
        print("GuideController onLoginOrRegisterClick")
        
//        PreferenceUtil.setShowGuide(true)
        
        setShowGuide()
        
        //调用AppDelegate中跳转到登录/注册界面的方法
        AppDelegate.shared.toLoginOrRegister()
    }
    
    
    /// 立即体验按钮点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onEnterClick(_ sender: Any) {
        print("GuideController onEnterClick")
        
//        PreferenceUtil.setShowGuide(false)
        
        setShowGuide()
        
        //调用用AppDelegate中跳转到首页的方法
        AppDelegate.shared.toHome()
    }
    
    /// 设置不显示引导界面
    func setShowGuide()  {
        PreferenceUtil.setShowGuide(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
