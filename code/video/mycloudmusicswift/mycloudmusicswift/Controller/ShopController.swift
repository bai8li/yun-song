//
//  ShopController.swift
//  商品列表界面
//
//  Created by smile on 2019/7/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class ShopController: UITableViewController {
    
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag=DisposeBag()
    
    /// 列表数据源
    var dataArray:[Book] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title="商品列表"
        
        //注册cell
        tableView.register(UINib(nibName: ShopCell.NAME, bundle: nil), forCellReuseIdentifier: ShopCell.NAME)
        
        //请求商品数据
        Api.shared.shops().subscribeOnSuccess { (data) in
            if let data=data?.data?.data{
                self.dataArray=self.dataArray+data
                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: ShopCell.NAME, for: indexPath) as! ShopCell

        //绑定数据
        cell.bindData(dataArray[indexPath.row])
        
        //返回cell
        return cell
    }
    
    /// cell点击
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //取出数据
        let data=dataArray[indexPath.row]
        
        ShopDetailController.start(navigationController!, data.id)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=ShopController(style: .plain)
        
        //将控制器添加到导航控制器中
        nav.pushViewController(controller, animated: true)
    }

}

