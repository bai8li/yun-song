//
//  UserListController.swift
//  搜索界面用户列表界面
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class UserListController: BaseSearchController<User> {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //注册cell
        tableView.register(UINib(nibName: UserCell.NAME, bundle: nil), forCellReuseIdentifier: UserCell.NAME)

    }
    
    /// 获取数据
    ///
    /// - Parameter query: <#query description#>
    override func fetchData(_ query:String) {
        Api.shared.searchUsers(query).subscribeOnSuccess { (data) in
            if let data=data?.data?.data{
                self.dataArray=data
                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
    }

}

// MARK: - 列表数据源和代理
extension UserListController{
    
    /// 返回cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.NAME, for: indexPath) as! UserCell
        
        //获取数据
        let data=dataArray[indexPath.row]
        
        //绑定数据
        cell.bindData(data)
        
        //返回cell
        return cell
    }
    
    /// cell选中
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //取出数据
        let data=dataArray[indexPath.row]
        
        //跳转到用户详情
        UserDetailController.start(navigationController!, userId: data.id)
    }
}
