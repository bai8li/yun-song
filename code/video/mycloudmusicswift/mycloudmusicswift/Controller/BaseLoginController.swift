//
//  BaseLoginController.swift
//  通用登录方法
//
//  Created by smile on 2019/6/9.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入RxSwift框架
import RxSwift

class BaseLoginController: BaseTitleController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    /// 登录
    ///
    /// - Parameters:
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - password: <#password description#>
    ///   - qq_id: <#qq_id description#>
    ///   - weibo_id: <#weibo_id description#>
    func login(phone:String?=nil,email:String?=nil,password:String?=nil,qq_id:String?=nil,weibo_id:String?=nil) {
//        Api.shared.login(phone: phone, email: email, password: password).subscribeOnSuccess { data in
//            if let data = data?.data {
//                //登录成功
//                print("RegisterController onRegisterClick login success:\(data.user)")
//
//                //把登录成功的事件通知到AppDelegate
//                AppDelegate.shared.onLogin(data)
//            }
//            }.disposed(by: disposeBag)
        
        Api.shared.login(phone: phone, email: email, password: password, qq_id: qq_id, weibo_id: weibo_id).subscribe({ (data) in
            
            if let data = data?.data {
                //登录成功
                print("RegisterController onRegisterClick login success:\(data.user)")

                //把登录成功的事件通知到AppDelegate
                AppDelegate.shared.onLogin(data)
                
                //统计登录成功事件
                AnalysisUtil.onLogin(success: true, phone: phone, email: email, qqId: qq_id, weiboId: weibo_id)
            }
        }) { (baseResponse, error) -> Bool in
            //统计登录失败事件
            AnalysisUtil.onLogin(success: false, phone: phone, email: email, qqId: qq_id, weiboId: weibo_id)
            
            //像无网络
            //用户名密码格式不正确这些就不统计了
            //当然真实项目中可能也需要统计
            
            //返回false让框架自动处理错误
            return false
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
