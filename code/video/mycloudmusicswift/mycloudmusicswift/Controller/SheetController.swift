//
//  SheetController.swift
//  搜索界面歌单列表界面
//
//  Created by smile on 2019/7/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SheetController: BaseSearchController<Sheet> {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //注册cell
        tableView.register(UINib(nibName: TopicCell.NAME, bundle: nil), forCellReuseIdentifier: TopicCell.NAME)

    }
    
    /// 获取数据
    ///
    /// - Parameter query: <#query description#>
    override func fetchData(_ query:String) {
        super.fetchData(query)
        
        Api.shared.searchSheets(query).subscribeOnSuccess { (data) in
            if let data=data?.data?.data{
                self.dataArray=data
                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
    }

    /// 返回cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME, for: indexPath) as! TopicCell

        //绑定数据
        cell.bindData(dataArray[indexPath.row])

        //返回cell
        return cell
    }
    
    /// cell点击
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data=dataArray[indexPath.row]
        
        SheetDetailController.start(navigationController!, data.id)
    }

}
