//
//  TopicDetailController.swift
//  话题详情界面
//
//  Created by smile on 2019/7/1.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TopicDetailController: BaseTitleController {
    
    /// 标题
    var topicTitle:String?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /// 根据话题标题获取话题详情
    ///
    /// - Parameters:
    ///   - navigationController: <#navigationController description#>
    ///   - title: <#title description#>
    static func start(_ navigationController:UINavigationController,title:String) {
        //创建控制器
        let controller = navigationController.storyboard!.instantiateViewController(withIdentifier: "TopicDetail") as! TopicDetailController
        
        //传递参数
        controller.topicTitle=title
        
        //将控制器压入导航控制器
        navigationController.pushViewController(controller, animated: true)
    }
}
