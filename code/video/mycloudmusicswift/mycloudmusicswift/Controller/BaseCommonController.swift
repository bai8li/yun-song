//
//  BaseCommonController.swift
//  通用的业务控制器
//
//  Created by smile on 2019/6/9.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入RxSwift
import RxSwift

//导入发布订阅框架
import SwiftEventBus

class BaseCommonController: BaseController {
    
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag=DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    /// 返回页面标识
    ///
    /// - Returns: <#return value description#>
    func pageId() -> String? {
        return nil
    }
    
    /// 页面即将不可见
    /// - Parameter animated: <#animated description#>
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //必须手动解除注册（如果没有注册，在解除注册也不会报错）
        SwiftEventBus.unregister(self)
    }
}

/// MARK: - 极光统计
extension BaseCommonController{

    /// 界面即将显示的时候调用
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let pageId = pageId() {
            //使用极光统计
            //页面开始事件
            JANALYTICSService.startLogPageView(pageId)
        }
    }

    /// 界面已经消失调用
    ///
    /// - Parameter animated: <#animated description#>
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        if let pageId = pageId() {
            //页面结束事件
            JANALYTICSService.stopLogPageView(pageId)
        }
        
    }
}

