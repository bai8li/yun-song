//
//  AboutController.swift
//  关于爱学啊（可视化布局）
//
//  Created by smile on 2019/7/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class AboutController: BaseTitleController {
    
    /// 版本
    @IBOutlet weak var lbVersion: UILabel!
    
    /// 版本容器
    @IBOutlet weak var vwVersion: UIView!
    
    /// 关于爱学啊容器
    @IBOutlet weak var vwAbout: UIView!
    
    override func initViews() {
        super.initViews()
        
        setTitle("关于爱学啊（可视化布局）")
    }
    
    override func initDatas() {
        super.initDatas()
        
        let version=BundleUtil.appVersion()

        let buildVersion=BundleUtil.appBuildVersion()

        //显示版本
        lbVersion.text="V\(version).\(buildVersion)"
    }

    override func initListeners() {
        super.initListeners()
        
        //版本点击
        vwVersion.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onVersionClick)))
        
        //关于爱学啊
        vwAbout.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onAboutClick)))
    }
    
    /// 版本点击
    @objc func onVersionClick() {
        ToastUtil.short("你点击了当前版本！")
        
        //如果要上架App Store不能内部检查是否有新版本
        //否则审核可能不会通过
        //跳转到App Store中当前App地址
        
        //跳转到当前App Store地址
        //真机才能测试
        UIApplication.shared.openURL(URL(string: MY_APPSTORE_URL)!)
    }
    
    /// 关于我们点击
    @objc func onAboutClick() {
        print("AboutController onAboutClick")
        
        WebController.start(navigationController!, "关于我们", "http://www.ixuea.com/posts/3")
    }
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "About")
        
        //将控制器添加到导航控制中
        nav.pushViewController(controller, animated: true)
    }

}
