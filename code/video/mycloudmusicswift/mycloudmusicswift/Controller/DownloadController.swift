//
//  DownloadController.swift
//  首页-我的-下载管理
//
//  Created by smile on 2019/7/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅框架
import SwiftEventBus

class DownloadController: BaseTitleController {
    
    
    /// 下载按钮
    @IBOutlet weak var btDownloadClick: UIButton!
    
    /// 删除全部按钮
    @IBOutlet weak var btDeleteAllClick: UIButton!
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 下载器
    var downloader:DownloadManager!
    
    /// 数据库工具类
    var orm:ORMUtil!
    
    /// 列表数据源
    var dataArray:[DownloadInfo] = []
    
    /// 是否有正在下载的任务
    var hasDownloading = false
    
    
    override func initViews() {
        super.initViews()
        
        setTitle("下载管理")
    }
    
    override func initDatas() {
        super.initDatas()
        
        //初始化下载器
        downloader = DownloadManager.sharedInstance()
        
        //初始化数据库工具类
        orm=ORMUtil.shared()
        
        fetchData()
    }
    
    /// 获取数据
    func fetchData() {
        let downloadInfos = downloader.findAllDownloading() as? [DownloadInfo]
        
        if downloadInfos != nil && downloadInfos!.count > 0 {
            //判断是否有正在下载的任务
            for downloadInfo in downloadInfos! {
                if downloadInfo.status == .downloading{
                    //如果有一个状态是下载
                    //按钮的状态就是暂停所有
                    hasDownloading=true
                    break
                }
            }
            
            //有下载任务
            dataArray=dataArray+downloadInfos!
        } else {
            //没有下载任务
        }
        
        tableView.reloadData()
        
        //设置按钮的状态
        setPauseOrResumeButtonStatus()
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听下载改变状态
        //目的是要从当前列表移除
        //因为当前列表中只显示除下载完成的任务
        SwiftEventBus.onMainThread(self, name: ON_DOWNLOAD_STATUS_CHANGED) { (sender) in
            self.dataArray.removeAll()
            self.fetchData()
        }
    }
    
    
    /// 开始所有/暂停所有
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDownloadClick(_ sender: Any) {
        print("DownloadController onDownloadClick")
        
        //无数据
        //可以按照需求来处理
        //原版是没有下载任务不能进入到该界面
        //我们这里就显示一个提示就行了
        
        if dataArray.count == 0 {
            ToastUtil.short("没有下载任务！")
            return
        }
        
        pauseOrResumeAll()
    }
    
    /// 删除所有任务
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDeleteClick(_ sender: Any) {
        print("DownloadController onDeleteClick")
        
        if dataArray.count == 0 {
            ToastUtil.short("没有下载任务！")
            return
        }
        
        for data in dataArray {
            //删除所有下载任务
            downloader.remove(data)
        }
        
        //删除数据源
        dataArray.removeAll()
        
        //列表控件重新加载数据
        tableView.reloadData()
    }
    
    /// 暂停/继续所有任务
    func pauseOrResumeAll() {
        if hasDownloading {
            pauseAll()
            hasDownloading=false
        } else {
            resumeAll()
            hasDownloading=true
        }
        
        setPauseOrResumeButtonStatus()
    }
    
    /// 设置下载/暂停所有任务按钮状态
    func setPauseOrResumeButtonStatus() {
        if hasDownloading {
            btDownloadClick.setTitle("全部暂停", for: .normal)
        } else {
            btDownloadClick.setTitle("全部开始", for: .normal)
        }
    }
    
    /// 暂停所有
    func pauseAll() {
        downloader.pauseAll()
        tableView.reloadData()
    }
    
    /// 继续所有
    func resumeAll() {
        downloader.resumeAll()
        tableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动方法
extension DownloadController{
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "Download")
        
        //将控制器放入导航控制器
        nav.pushViewController(controller, animated: true)
    }
}

// MARK: - 列表数据源和代理
extension DownloadController :UITableViewDataSource,UITableViewDelegate {
    
    /// 返回有多个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath) as! DownloadCell
        
        //获取数据
        let data = dataArray[indexPath.row]
        
        //绑定数据
        cell.bindData(data)
        
        //返回cell
        return cell
    }
    
    /// cell点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //获取数据
        let data = dataArray[indexPath.row]
        
        if data.status == .paused || data.status == .error {
            //暂停中
            //错误
            
            //继续下载
            downloader.resume(data)
        } else {
            //暂停
            downloader.pause(data)
        }
    }
    
    // MARK: - 列表编辑相关
    
    /// 当前Index对应的Cell是否可以编辑
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    /// 编辑样式
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    /// 编辑按钮的标题
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "删除"
    }
    
    /// 点击编辑按钮后的动作
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - editingStyle: <#editingStyle description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let data = dataArray[indexPath.row]
        
        //从下载框架中移除
        downloader.remove(data)
        
        //从数据源中移除
        dataArray.remove(at: indexPath.row)
        
        //删除cell
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        print("DownloadController commit edit delete success:\(data.uri)")
    }
}
