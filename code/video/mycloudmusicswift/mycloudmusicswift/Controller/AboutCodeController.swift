//
//  AboutCodeController.swift
//  关于爱学啊（代码自动布局）
//
//  Created by smile on 2019/7/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入自动布局框架
import SnapKit

class AboutCodeController: BaseTitleController {
    
    /// 版本按钮
    var btVersion:UIButton!
    
    /// 版本
    var lbVersion:UILabel!
    
    /// 我的云音乐新手指南按钮
    var btGuide:UIButton!
    
    /// 关于爱学啊按钮
    var btAbout:UIButton!
    
    override func initViews() {
        super.initViews()
        
        //设置背景颜色
        view.backgroundColor=UIColor.groupTableViewBackground
        
        setTitle("关于爱学啊（代码自动布局）")
        
        //使用自动布局框架
        //一定要将view添加到视图中才能设置约束
        
        //Logo
        let aboutLogo=UIImage(named: "LoginLogo")
        let aboutImageView=UIImageView(image: aboutLogo)
        view.addSubview(aboutImageView)
        
        //当前版本
        btVersion=UIButton(type: .system)
        btVersion.setTitle("当前版本", for: .normal)
//        btVersion.setTitleColor(UIColor.black, for: .normal)
//
//        //设置字体大小
//        btVersion.titleLabel?.font=UIFont.systemFont(ofSize: 17)
//
//        //背景颜色
//        btVersion.backgroundColor=UIColor.white
//
//        //文本居左
//        btVersion.contentHorizontalAlignment = .left
//
//        //左侧padding
//        btVersion.titleEdgeInsets=UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        setItemButton(btVersion)

        view.addSubview(btVersion)
        
        //右侧版本
        lbVersion=UILabel.init()
        
        //文本颜色
        lbVersion.textColor=UIColor.lightGray
        
        //测试内容
//        lbVersion.text="V1.0.0"
        
        view.addSubview(lbVersion)
        
        //我的云音乐新手指南按钮
        btGuide=UIButton(type: .system)
        btGuide.setTitle("我的云音乐新手指南", for: .normal)
        
        setItemButton(btGuide)
        
        view.addSubview(btGuide)
        
        //我的云音乐新手指南按钮 右侧图标
        let imageGuideRightMore=UIImage(named: "ArrowRight")
        let ivGuideRight=UIImageView(image: imageGuideRightMore)
        view.addSubview(ivGuideRight)
        
        //关于爱学啊按钮
        btAbout=UIButton(type: .system)
        btAbout.setTitle("关于爱学啊", for: .normal)
        
        setItemButton(btAbout)
        
        view.addSubview(btAbout)
        
        //关于爱学啊按钮 右侧图标
        let imageAboutRightMore=UIImage(named: "ArrowRight")
        let ivAboutRight=UIImageView(image: imageAboutRightMore)
        view.addSubview(ivAboutRight)
        
        //设置约束
        aboutImageView.snp.makeConstraints { (make) in
            //宽高
            make.width.equalTo(80)
            make.height.equalTo(80)
            
            //顶部
            //equalTo后面是对象
            //距离View的顶部安全区
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(40)
            
            //水平居中
            make.centerX.equalTo(self.view)
        }
        
        //当前版本
        btVersion.snp.makeConstraints { (make) in
            //宽为父布局宽度
            make.width.equalTo(self.view)
            
            //高为50
            make.height.equalTo(50)
            
            //顶部距离logo底部向下40
            make.top.equalTo(aboutImageView.snp.bottom).offset(40)
            
            //如果这样写
            //aboutImageView的top向下40
//            make.top.equalTo(aboutImageView).offset(40)
        }
        
        //版本
        lbVersion.snp.makeConstraints { (make) in
            //右侧位置
            //所在按钮的右侧-20
            make.right.equalTo(self.btVersion).offset(-20)
            
            //在所在按钮垂直方向居中
            make.centerY.equalTo(self.btVersion)
        }
        
        //我的云音乐新手指南按钮
        //距离上面button 10
        //由于背景是灰色
        //所以距离10就有分割线了
        btGuide.snp.makeConstraints { (make) in
            //宽为父布局宽度
            make.width.equalTo(self.view)
            
            //高为50
            make.height.equalTo(50)
            
            //顶部距离
            //上一个按钮底部向下10
            make.top.equalTo(btVersion.snp.bottom).offset(10)
        }
        
        //我的云音乐新手指南按钮 右侧图标
        ivGuideRight.snp.makeConstraints { (make) in
            //宽高
            make.width.equalTo(16)
            make.height.equalTo(16)
            
            //右侧位置
            make.right.equalTo(btGuide).offset(-20)
            
            //垂直方向
            //和所在按钮中心对齐
            make.centerY.equalTo(btGuide)
        }
        
        //关于爱学啊按钮
        btAbout.snp.makeConstraints { (make) in
            //宽为父布局宽度
            make.width.equalTo(self.view)
            
            //高为50
            make.height.equalTo(50)
            
            //顶部距离
            //上一个按钮底部向下1
            make.top.equalTo(btGuide.snp.bottom).offset(1)
        }
        
        //关于爱学啊 右侧图标
        ivAboutRight.snp.makeConstraints { (make) in
            //宽高
            make.width.equalTo(16)
            make.height.equalTo(16)
            
            //右侧位置
            make.right.equalTo(btAbout).offset(-20)
            
            //垂直方向
            //和所在按钮中心对齐
            make.centerY.equalTo(btAbout)
        }
    }
    
    /// 设置Item按钮通用样式
    ///
    /// - Parameter button: <#button description#>
    func setItemButton(_ button:UIButton) {
        button.setTitleColor(UIColor.black, for: .normal)

        //设置字体大小
        button.titleLabel?.font=UIFont.systemFont(ofSize: 17)

        //背景颜色
        button.backgroundColor=UIColor.white

        //文本居左
        button.contentHorizontalAlignment = .left

        //左侧padding
        button.titleEdgeInsets=UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)

    }
    
    override func initDatas() {
        super.initDatas()
        
        let version=BundleUtil.appVersion()
        let buildVersion=BundleUtil.appBuildVersion()
        
        lbVersion.text="V\(version).\(buildVersion)"
    }
    
    override func initListeners() {
        super.initListeners()
        
        //版本按钮点击
        btVersion.addTarget(self, action: #selector(onVersionClick), for: .touchUpInside)
        
        //关于爱学啊按钮点击
        btAbout.addTarget(self, action: #selector(onAboutClick), for: .touchUpInside)
    }
    
    /// 版本点击
    @objc func onVersionClick(){
        ToastUtil.short("你点击当前版本！")
        
        //如果要上架App Store不能内部检查是否有新版本
        //否则审核可能不会通过
        //跳转到App Store中当前App地址
        
        //跳转到当前App Store地址，真机才能测试
        UIApplication.shared.open(URL(string: MY_APPSTORE_URL)!)
    }
    
    /// 关于爱学啊点击
    @objc func onAboutClick() {
        WebController.start(navigationController!, "关于我们", "http://www.ixuea.com/posts/3")
    }
    

    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=AboutCodeController.init()
        
        //隐藏bottombar
        controller.hidesBottomBarWhenPushed=true
        
        //添加到导航控制器
        nav.pushViewController(controller, animated: true)
    }
}
