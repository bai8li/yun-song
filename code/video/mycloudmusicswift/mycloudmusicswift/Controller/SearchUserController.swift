//
//  SearchUserController.swift
//  搜索用户
//  其实搜索用户在首页的搜索已经实现了
//  这里主要是演示RxCocoa/RxSwift
//  对界面，TableView和网络请求的配合使用
//
//  Created by smile on 2019/7/16.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

//导入响应式UI编程框架
//像下面的searchBar使用的响应式
//来自这个框架
import RxCocoa

class SearchUserController: BaseTitleController {
    
    /// 搜索控件
    @IBOutlet weak var searchBar: UISearchBar!
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 用户列表数据
    var data:[User] = []
    
    override func initViews() {
        super.initViews()
        
        setTitle("搜索用户")
        
        //注册Cell
        tableView.register(UINib(nibName: UserCell.NAME, bundle: nil), forCellReuseIdentifier: UserCell.NAME)
    }
    
    override func initDatas() {
        super.initDatas()
        
        //获取搜索结果
        let searchResults=searchBar
            //调用rx扩展方法
            .rx
            
            //表示获取到searchBar里面输入的文本
            .text
        
            //为空也调用
            .orEmpty
            
            //过滤1000毫秒内的重复回调
            //现在实现的效果是
            //输入内容后1000毫秒后才会请求数据
            //因为这里是请网络
            //选择好友列表不限流是因为是本地过滤数据
            .throttle(.milliseconds(1000), scheduler: MainScheduler.instance)
            
            //返回一个可观察的列表
            .distinctUntilChanged()
            
            //使用变换操作
            .flatMapLatest { (query) -> Observable<[User]> in
                
                print("SearchUserController search:\(query)")
                
                //判断是否输入了搜索关键字
                if query.isEmpty{
                    //返回一个空数组的Observable
                    
                    //注意：不是返回一个空数组
                    return .just([])
                }
                
                //调用api搜索
                return Api.shared.searchUsers(query).flatMap({ (data) -> Observable<[User]> in
                    
                    //把用户列表获取到
                    if let data=data?.data?.data{
                        //有数据
                        
                        //将用来列表保存下来
                        //因为一会点击的时候会用到
                        
                        //通过RxSwift还有其他的方式实现
                        //但比较复杂
                        //这里就这样简单实现了
                        self.data=data
                        
                        //使用just创建一个观察者
                        //他会创建Observable<[User]>
                        return .just(data)
                    }
                    
                    //没有数据
                    
                    //或者是请求失败了
                    
                    //就返回一个空数组的Observable
                    return .just([])
                    
                })
            }
            
            //在主线程中观察
            .observeOn(MainScheduler.instance)
            
            //如果有错误就返回一个空数组的Observable
            .catchErrorJustReturn([])
        
        //显示搜索结果
        searchResults.bind(to: tableView
            .rx
            
            //UserCell表示要绑定的Cell
            //前面要先注册
            //可以理解为这一步就是从TableView中取Cell
            .items(cellIdentifier: UserCell.NAME)){
            (index,user:User,cell:UserCell) in
            
                //获取到cell后调用
                
                print("SearchUserController bind data:\(user.nickname)")
                
                //绑定数据
                cell.bindData(user)
        }.disposed(by: disposeBag)
    }
    
    override func initListeners() {
        super.initListeners()
        
        tableView.rx.itemSelected.map { (indexPath) in
            //当点击了Cell
            //这里返回来点击的索引
            //还返回了这条数据
                return (indexPath,self.data[indexPath.row])
            }.subscribe(onNext: {
                data in
                //在这里观察事件
                
                //获取点击的数据
                let user = data.1
                
                print("SearchController on item click:\(user.nickname)")
                
                //进入用户详情
                UserDetailController.start(self.navigationController!, userId: user.id)
            }).disposed(by: disposeBag)
    }
}

// MARK: - 启动方法
extension SearchUserController{
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "SearchUser")
        
        //添加到导航控制器
        nav.pushViewController(controller, animated: true)
    }
}
