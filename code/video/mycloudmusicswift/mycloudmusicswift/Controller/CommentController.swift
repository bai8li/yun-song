//
//  CommentController.swift
//  评论列表
//
//  Created by smile on 2019/6/14.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入下拉刷新
//上拉加载更多框架
import ESPullToRefresh

//导入TextView提示
import KMPlaceholderTextView

//导入键盘管理器
//弹出软键盘的时候不遮住输入框
import KeyboardLayoutGuide

//导入发布订阅
import SwiftEventBus

//导入日志框架
import CocoaLumberjack

class CommentController: BaseTitleController {
    
    
    /// 列表
    @IBOutlet weak var tableView: UITableView!
    
    /// 输入框最外层容器
    @IBOutlet weak var vwInputContainer: UIView!
    
    
    /// 输入框边框
    @IBOutlet weak var vwInputBorder: UIView!
    
    /// 输入框
    @IBOutlet weak var textView: KMPlaceholderTextView!
    
    /// 歌单Id
    var sheet_id:String?
    
    /// 被回复评论的Id
    var parent_id:String?
    
    /// 列表数据
    var dataArray:[CommentGroup] = []
    
    /// 分页元数据
    var meta:Meta<Comment>?

    override func initViews() {
        super.initViews()
        
        setTitle("评论")
        
        //设置输入框提示文本
//        setPlaceholder()
        
        //设置圆角
        ViewUtil.showSmallRadius(vwInputBorder)
        
        //设置边框
        vwInputBorder.showColorPrimaryBorder()
        
        //注册Header
        tableView.register(UINib(nibName: CommentHeaderView.NAME, bundle: nil), forHeaderFooterViewReuseIdentifier: CommentHeaderView.NAME)
        
        //注册评论Cell
        tableView.register(UINib(nibName: CommentCell.NAME, bundle: nil), forCellReuseIdentifier: CommentCell.NAME)
        
        //添加一条自动布局约束
        //含义是：输入框的底部锚点
        //等于当前界面中View的底部锚点
        vwInputContainer.bottomAnchor.constraint(equalTo: view.keyboardLayoutGuide.topAnchor).isActive=true
    }

    
    override func initDatas() {
        super.initDatas()
        
        fetchData()
    }
    
    func fetchData() {
        //将分页对象设置空
        //这样在加载更多的方法中
        //就会获取第一页评论
        meta = nil
        
        //移除原来数组中的内容
        dataArray.removeAll()
        
        Api
            .shared
            .comments(sheet_id: sheet_id, order: ORDER_HOT)
            .subscribeOnSuccess { data in
                if let data = data?.data?.data {
                //刷新完成
                self.tableView.es.stopPullToRefresh()
                
                //精彩评论组
                let commentGroup = CommentGroup()
                commentGroup.title="精彩评论"
                commentGroup.comments=data
                
                self.dataArray.append(commentGroup)
                
                //最新评论
                //先创建一个空数组
                let newCommentGroup = CommentGroup()
                newCommentGroup.title="最新评论"
                newCommentGroup.comments=[]
                
                self.dataArray.append(newCommentGroup)
                
                //加载更多数据（最新评论）
                self.loadMore()
                
                //有数据
//                self.dataArray = self.dataArray + data
//
//                //重新加载数据
//                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
    }
    
    /// 加载最新评论
    func loadMore() {
        //获取下一页
        let page = Meta.nextPage(meta)
        
        Api
            .shared
            .comments(sheet_id: sheet_id,page:page)
            .subscribeOnSuccess { (listResponse) in
                if let data = listResponse?.data?.data {
                //保存分页元数据
                self.meta = listResponse!.data
                
                //设置标题
                self.setTitle("评论(\(self.meta!.total_count))")
                
                //取出第二组评论
                let newCommentGroup = self.dataArray[1]
                
                //把评论添加到该列表中
                newCommentGroup.comments = newCommentGroup.comments+data
                
                //重新刷新数据
                self.tableView.reloadData()
                
                //判断是否加载完毕
                //控制是否还可以上拉
                if let _ = self.meta!.next_page{
                    //还有数据
                    self.tableView.es.stopLoadingMore()
                }else {
                    //没有更多数据了
                    self.tableView.es.noticeNoMoreData()
                }
            }
        }.disposed(by: disposeBag)
    }
    
    override func initListeners() {
        super.initListeners()
        
        //设置下拉刷新监听器
        tableView.es.addPullToRefresh {[weak self] in
            self?.fetchData()
        }
        
        //设置上拉加载更多监听器
        tableView.es.addInfiniteScrolling {[weak self] in
            self?.loadMore()
        }
        
        //设置TextView代理
        textView.delegate=self
        
        //监听选择了话题
        SwiftEventBus.onMainThread(self, name: ON_SELECT_TOPIC) { (sender) in
            //获取发送过来的数据
            let data = sender?.object as! Topic
            
            //获取输入框中原来的内容
            let source = self.textView.text!
            
            //最终的字符串
            let result = "\(source)#\(data.title!)#"
            
            //设置到文本输入框
            self.setText(result)
        }
        
        //监听选择了话题
        SwiftEventBus.onMainThread(self, name: ON_SELECT_USER) { (sender) in
            //获取数据
            let data = sender?.object as! User
            
            //获取输入框中的内容
            let source = self.textView.text!
            
            //最终的字符串
            let result="\(source)@\(data.nickname!) "
            
            //设置到输入框
            self.setText(result)
        }
    }
    
    /// 设置文本到输入框
    ///
    /// - Parameter data: <#data description#>
    func setText(_ data:String) {
        //高亮
        let result = StringUtil.processHighlight(data)
        
        //设置到输入框
        textView.attributedText = result
    }
    
    /// 显示评论更多对话框
    ///
    /// - Parameter data: <#data description#>
    func showCommentMoreDialog(_ data:Comment) {
        //创建控制器
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //回复评论
        let replyAction = UIAlertAction(title: "回复", style: .default) { (action) in
            print("CommentController showCommentMoreDialog reply:\(data.content)")
            
            self.parent_id = data.id
            self.textView.placeholder="回复\(data.user.nickname!): "
        }
        
        //分享
        let shareAction = UIAlertAction(title: "分享", style: .default) { (action) in
            print("CommentController showCommentMoreDialog share:\(data.content)")
        }
        
        //拷贝
        let copyAction = UIAlertAction(title: "拷贝", style: .default) { (action) in
            print("CommentController showCommentMoreDialog copy:\(data.content)")
            
            //创建一个剪贴板对象
            let pasteboard = UIPasteboard.general
            pasteboard.string=data.content
            
            ToastUtil.short("拷贝成功！")
        }
        
        /// 最后一个动作
        var lastAction:UIAlertAction!
        
        //这里只进行了简单判断
        //例如：用户未登陆，该动作怎么处理
        //具体的就是业务逻辑了
        //现在的实现是当前评论是自己的
        //就是显示删除
        //如果不是就是举报
        if PreferenceUtil.userId()! == data.user.id {
            //自己的评论
            
            //显示删除按钮
            lastAction = UIAlertAction(title: "删除", style: .default, handler: { (action) in
                print("CommentController showCommentMoreDialog delete:\(data.content)")
            })
        }else {
            //其他人发布的评论
            
            //显示举报按钮
            lastAction = UIAlertAction(title: "举报", style: .default, handler: { (action) in
                print("CommentController showCommentMoreDialog report:\(data.content)")
            })
            
        }
        
        //把action添加到控制器中
        controller.addAction(replyAction)
        controller.addAction(shareAction)
        controller.addAction(copyAction)
        controller.addAction(lastAction)
        
        //添加一个取消按钮
        controller.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        
        //显示对话框
        present(controller, animated: true, completion: nil)
    }
    
    /// 清除回复评论
    func clearReplyComment() {
        parent_id = nil
        textView.text=""
        setPlaceholder()
    }
    
    /// 发布评论
    func sendComment()  {
        //获取评论内容
        let content = textView.text.trim()
        
        if content!.isEmpty {
            ToastUtil.short("请输入评论.")
            return
        }
        
        Api.shared.createComment(content: content!, sheet_id: sheet_id, parent_id: parent_id).subscribeOnSuccess { (data) in
            //清除评论框
            self.clearReplyComment()
            
            //关闭键盘
            //要先隐藏键盘
            //在请求数据
            //因为隐藏键盘的时候
            //TableView的布局会变动
            //而我们在fetchData方法中清除了数组的数据
            //会导致TableView刷新是报错
            self.hideKeyboard()
            
            //拉去新评论
            self.fetchData()
            }.disposed(by: disposeBag)
    }
    
    /// 关闭键盘
    func hideKeyboard() {
        textView.resignFirstResponder()
    }
    
    /// 设置提示文本
    func setPlaceholder() {
        textView.placeholder="人生苦短，我们只做好课！"
    }
    
    /// 评论点赞
    ///
    /// - Parameter data: <#data description#>
    func onLikeClick(_ comment:Comment) {
        //判断当前评论是否点赞
        if comment.isLiked() {
            //已经点赞了
            
            //取消点赞
            //传递的Id是这条关系的Id
            Api.shared.deleleLike(comment.like_id!).subscribeOnSuccess { (data) in
                //取消点赞后
                
                //可以调用接口
                //也可以在本地加减
                //但本地加减效率更高
                comment.like_id = nil
                comment.likes_count -= 1
                
                //重新刷新数据
                //也可以直接获取到这个Cell
                //直接更改Cell
                //这样理论上效率高一点
                self.tableView.reloadData()
            }.disposed(by: self.disposeBag)
        } else {
            //没有点赞
            
            //开始点赞
            Api.shared.like(comment.id)
                .subscribeOnSuccess { (data) in
                    //将返回的Id设置到like_id上
                    comment.like_id = data!.data!.id
                    
                    comment.likes_count += 1
                    
                    self.tableView.reloadData()
            }.disposed(by: disposeBag)
        }
    }
    
    /// 选择话题
    func selectTopic() {
       SelectTopicController.start(navigationController!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    deinit {
        DDLogDebug("CommentController deinit")
    }
}

// MARK: - 启动方法
extension CommentController {
    
    /// 启动方法
    ///
    /// - Returns: <#return value description#>
    static func start(_ navigationController:UINavigationController,sheet:String) {
        //创建控制器
        let controller = navigationController.storyboard!.instantiateViewController(withIdentifier: "Comment") as! CommentController
        
        //传递参数
        controller.sheet_id = sheet
        
        //将控制器压入导航控制器
        navigationController.pushViewController(controller, animated: true)
    }
}

// MARK: - 实现列表数据源和代理
extension CommentController: UITableViewDataSource,UITableViewDelegate {
    
    /// 返回有多少组
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    /// 返回有多个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let commentGroup = dataArray[section]
        
        return commentGroup.comments.count
    }
    
    /// 返回当前位置Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentCell.NAME, for: indexPath) as! CommentCell
        
        //获取数据
        let commentGroup = dataArray[indexPath.section]
        
        let data =  commentGroup.comments[indexPath.row]
        
        //绑定数据
        cell.bindData(data)
        
        //设置昵称点击回调方法
        cell.onNicknameClick = {[weak self]
            data in
            print("CommentController onNicknameClick:\(data)")

            if let self = self {
                UserDetailController.start(self.navigationController!, nickname: data)
            }
        }

        //设置话题点击回调方法
        cell.onHashTagClick = {[weak self]
            data in
            print("CommentController onHashTagClick:\(data)")
            
            if let self = self {
                TopicDetailController.start(self.navigationController!, title: data)
            }
            
        }

        //设置点赞回调方法
        cell.onLikeClick = {[weak self]
            data in
            print("CommentController onLikeClick:\(data.content)")

            self?.onLikeClick(data)
        }

        //设置头像点击回调方法
        cell.onAvatarClick = {[weak self]
            data in
            print("CommentController onAvatarClick:\(data.nickname)")

            if let self = self {
                UserDetailController.start(self.navigationController!, userId: data.id)
            }
            
        }
        
        //返回cell
        return cell
    }
    
    
    /// 返回组标题
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
//    /// - Returns: <#return value description#>
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let commentGroup = dataArray[section]
//        return commentGroup.title
//    }
    
    /// 返回Header
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //获取Header
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: CommentHeaderView.NAME) as! CommentHeaderView
        
        //取出数据
        let commentGroup = dataArray[section]
        
        //绑定数据
        header.bindData(commentGroup.title)
        
        //返回Cell
        return header
    }
    
    /// 点击Cell回调
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //取出数据源
        let commentGroup = dataArray[indexPath.section]
        let data = commentGroup.comments[indexPath.row]
        
        //显示评论更多对话框
        showCommentMoreDialog(data)
    }
    
    /// 滚动中
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //滑动中,如果没有输入内容，就清除恢复，隐藏键盘
        //当然也可以判断，例如：滚动记录达到了一个阈值
        let content = textView.text.trim()!
        if content.isEmpty{
            clearReplyComment()
            hideKeyboard()
        }
    }
}

// MARK: - TextView代理
extension CommentController: UITextViewDelegate{
    
    /// 当文本改变的时候调用
    ///
    /// - Parameters:
    ///   - textView: <#textView description#>
    ///   - range: <#range description#>
    ///   - text: <#text description#>
    /// - Returns: <#return value description#>
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("CommentController shouldChangeTextIn:\(text)")
        
        //判断输入的字是否是回车
        //即按下return
        if text == "\n" {
            print("CommentController shouldChangeTextIn return key")
            
            sendComment()
            
            //这里返回false
            //就代表return键值失效
            //即页面上按下return
            //不会出现换行
            //如果为true
            //则输入页面会换行
            //但是如果想用return发送
            //但又想实现换行，就无法实现了
            return false
        }else if text == "#" {
            //选择话题
            print("CommentController shouldChangeTextIn select topic")
            
            self.selectTopic()
            
            return false
        }else if text=="@" {
            //选择好友
            print("CommentController shouldChangeTextIn select friend")
            
            SelectFriendController.start(self.navigationController!)
            
            //返回false后输入框就不会有@
            //所以需要选择人后手动添加前面的@符号
            
            //如果这里返回true
            //实现的效果就是如果不选择人
            //直接关闭选择好友界面
            //输入框中会有@
            //具体那种实现看需求
            return false
        }
        
        //其他情况返回true
        //表示其他字符正常输入
        return true
    }
    
    
}
