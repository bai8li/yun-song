//
//  PlayerController.swift
//  黑胶唱片界面
//
//  Created by smile on 2019/6/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入发布订阅
import SwiftEventBus

class PlayerController: BaseTitleController {
    
    // MARK: - 背景
    
    /// 黑胶唱片背景
    @IBOutlet weak var recordBackgroundBorder: UIImageView!
    
    
    /// 封面背景
    @IBOutlet weak var ivBackground: UIImageView!
    
    // MARK: - 歌词
    
    @IBOutlet weak var lyricContainer: UIView!
    
    
    /// 歌词列表控件
    @IBOutlet weak var tableView: UITableView!
    
    
    /// 歌词为空提示
    @IBOutlet weak var lyricEmpty: UILabel!
    
    
    /// 歌词拖拽状态容器
    @IBOutlet weak var dragContainer: UIStackView!
    
    
    /// 当前拖拽位置的歌词开始时间
    @IBOutlet weak var lbTime: UILabel!
    
    // MARK: -  黑胶唱片CollectionView
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    /// 唱片指针
    @IBOutlet weak var recordThumb: UIImageView!
    
    // MARK: - 迷你控制栏
    
    /// 喜欢按钮
    @IBOutlet weak var btLike: UIButton!
    
    
    /// 下载按钮
    @IBOutlet weak var btDownload: UIButton!
    
    
    /// 均衡器按钮
    /// 未实现
    /// 因为AVPlayer无法实现均衡器
    /// 可以用系统的另一个控件实现
    /// 但他不能直接播放在线音乐
    /// 所以这一块还是比较复杂的
    /// 如果有需要开一个小课程单独讲解
    /// 因为如果不是专门做一个商业用的音乐软件
    /// 不需要均衡器
    @IBOutlet weak var btEqualizer: UIButton!
    
    
    /// 评论按钮
    @IBOutlet weak var btComment: UIButton!
    
    
    /// 更多按钮
    @IBOutlet weak var btMore: UIButton!
    
    // MARK: - 进度相关
    
    /// 播放时间
    @IBOutlet weak var lbStart: UILabel!
    
    
    /// 进度条
    @IBOutlet weak var sdProgress: UISlider!
    
    /// 音乐总时间
    @IBOutlet weak var lbEnd: UILabel!
    
    // MARK: - 控制按钮
    
    /// 循环模式
    @IBOutlet weak var btLoopModel: UIButton!
    
    
    /// 上一曲
    @IBOutlet weak var btPrevious: UIButton!
    
    
    /// 播放按钮
    @IBOutlet weak var btPlay: UIButton!
    
    
    /// 下一曲
    @IBOutlet weak var btNext: UIButton!
    
    /// 播放列表
    @IBOutlet weak var btList: UIButton!
    
    
    /// 播放列表管理器
    var playListManager:PlayListManager!
    
    /// 播放管理器
    var musicPlayerManager:MusicPlayerManager!
    
    /// 是否是第一次滚动
    var isFirstScroll = true
    
    // MARK: - 歌词
    
    /// 歌词行
    var dataArray:[Any] = []
    
    /// 解析后的歌词对象
    var lyric:Lyric?
    
    /// 当前时间歌词行
    var lineNumber = 0
    
    /// 歌词填充多个占位数据
    private var lyricPlaceholderSize = 4
    
    /// 是否拖拽了歌词列表
    var isDrag = false
    
    /// 延迟滚动歌词异步任务
    var task:DispatchWorkItem?
    
    /// 当前歌词拖拽位置的歌词
    var scrollSelectedLyricLine:LyricLine?
    
    /// MARK: - 下载相关
    
    /// 下载器
    var downloader:DownloadManager!
    
    /// 数据库工具类
    var orm:ORMUtil!
    
    /// 下载信息
    var downloadInfo:DownloadInfo?
    

    /// MARK: - 初始化
    
    override func initViews() {
        super.initViews()
        
        //进度条
        //设置进度条颜色
        sdProgress.minimumTrackTintColor = UIColor(hex: COLOR_PRIMARY)
        
        //设置拖动点颜色
        sdProgress.thumbTintColor = UIColor(hex: COLOR_PRIMARY)
        
        //创建右侧按钮
        //分享按钮
        let shareBarItem = UIBarButtonItem(image: UIImage(named: "Share"), style: .plain, target: self, action: #selector(onShareClick(sender:)))
        
        //更多按钮
        let moreBarItem = UIBarButtonItem(image: UIImage(named: "MoreWhite"), style: .plain, target: self, action: #selector(onMoreClick))
        
        //添加到导航栏
        //排序为：从右到左排序
        navigationItem.rightBarButtonItems = [moreBarItem,shareBarItem]
        
        //使用一个异步任务
        //就可以获取到AutoLayout计算后尺寸
        //当然还有其他方法也可以
        DispatchQueue.main.async {
            //根据黑胶唱片指针图片计算
            //锚点为0.181，0.120
            self.recordThumb.setViewAnchorPoint(CGPoint(x: 0.181, y: 0.12))
        }
    }
    
    
    override func initDatas() {
        super.initDatas()
        
        //初始化下载管理器
        downloader = DownloadManager.sharedInstance()
        
        //初始化ORMUtil
        //业务数据需要我们自己保持
        orm = ORMUtil.shared()
        
        //创建播放列表管理器
        playListManager = PlayListManager.shared
        
        //创建播放管理器
        musicPlayerManager = MusicPlayerManager.shared()
        
        //显示播放数据
        initPlayData()
        
        
        //显示循环模式
        showLoopModel()
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听APP进入后台
        NotificationCenter.default.addObserver(self, selector: #selector(onEnterForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        //监听进入后台
        NotificationCenter.default.addObserver(self, selector: #selector(onEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        
        //CollectionView点击
        //显示歌词
        let collectionViewGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showLyricView))
        collectionView.addGestureRecognizer(collectionViewGestureRecognizer)
        
        //歌词容器点击
        //显示黑胶唱片
        let lyricContainerGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showRecordView))
        
        lyricContainer.addGestureRecognizer(lyricContainerGestureRecognizer)
        
        //collectionView长按事件
        let collectionViewLongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(onCollectionViewLongClick(gesture:)))
        
        //设置最小按下时间
        collectionViewLongPressGestureRecognizer.minimumPressDuration = 1.0
        collectionView.addGestureRecognizer(collectionViewLongPressGestureRecognizer)
        
        //歌词长按
        let onLyricLongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(onLyricLongClick(gesture:)))
        onLyricLongPressGestureRecognizer.minimumPressDuration = 1.0
        tableView.addGestureRecognizer(onLyricLongPressGestureRecognizer)
    }
    
    /// 歌词长按事件回调
    ///
    /// - Parameter gesture: <#gesture description#>
    @objc func onLyricLongClick(gesture:UILongPressGestureRecognizer) {
        //会调用多次
        //通过如下代码过滤掉多次
        if gesture.state != .began {
            return
        }
        
        print("PlayerController onLyricLongClick")
        
        if let lyric = lyric {
            //启动歌词选择界面
            SelectLyricController.start(navigationController!, playListManager.data!, lyric)
        }
        
    }
    
    /// 封面长按事件
    ///
    /// - Parameter gusture: <#gusture description#>
    @objc func onCollectionViewLongClick(gesture:UILongPressGestureRecognizer) {
        //会调用多次
        //通过如下代码过滤掉多次
        if gesture.state != UIGestureRecognizer.State.began {
            return
        }
        
        print("PlayerController onCollectionViewLongClick")
        
        //启动图片预览界面
        ImagePreviewController.start(navigationController!, playListManager.data!.banner)
    }
    
    /// 显示黑胶唱片
    @objc func showRecordView() {
        print("PlayerController showRecordView")
        
        //显示出黑胶唱片相关功能
        collectionView.isHidden=false
        recordBackgroundBorder.isHidden=false
        recordThumb.isHidden=false
        
        //使用动画
        UIView.animate(withDuration: 0.5, animations: {
            //动画执行的逻辑
            
            //将黑胶唱片相关控件透明度设置为1
            self.collectionView.alpha=1
            self.recordBackgroundBorder.alpha=1
            self.recordThumb.alpha=1
            
            //将歌词控件透明度设置0
            self.lyricContainer.alpha=0
            
        }) { finished in
            //动画执行完成后的逻辑
            
            //将歌词控件完全隐藏
            self.lyricContainer.isHidden=true
        }
    }
    
    /// 显示歌词View
    @objc func showLyricView() {
        print("PlayerController showLyricView")
        
        //显示歌词控件
        lyricContainer.isHidden = false
        
        //使用动画
        UIView.animate(withDuration: 0.5, animations: {
            //动画执行的逻辑
            
            //将歌词控件透明度改为1
            self.lyricContainer.alpha=1.0
            
            //将黑胶唱片相关控件透明度改为0
            self.collectionView.alpha=0
            self.recordBackgroundBorder.alpha=0
            self.recordThumb.alpha=0
            
        }) { finished in
            //动画执行完成后
            
            //将黑胶唱片完全隐藏
            self.collectionView.isHidden = true
            self.recordThumb.isHidden = true
            self.recordBackgroundBorder.isHidden = true
        }
    }
    
    /// 进入前台了
    @objc func onEnterForeground() {
        print("PlayerController onEnterForeground")
        
        //显示播放数据
        initPlayData()
        
        //滚动到当前位置
        scrollPositionAsync()
        
        //设置播放代理
        musicPlayerManager.delegate = self
    }
    
    /// 进入后台了
    @objc func onEnterBackground() {
        print("PlayerController onEnterBackground")
        
        //取消播放代理
        musicPlayerManager.delegate=nil
    }
    
    // MARK: - 播放相关方法
    
    /// 显示播放数据
    func initPlayData() {
        //显示初始化数据
        showInitData()
        
        //显示播放状态
        showMusicPlayStatus()
        
        //显示音乐时长
        showDuration()
        
        //显示进度
        showProgress()
        
        //显示歌词数据
        showLyricData()
    }
    
    /// 显示播放状态
    func showMusicPlayStatus() {
        if musicPlayerManager.isPlaying() {
            showPauseStatus()
        } else {
            showPlayStatus()
        }
    }
    
    /// 显示初始化数据
    func showInitData() {
        //获取到当前正在播放的音乐
        let data = playListManager.data!
        
        //显示标题
        setTitle("\(data.title!)-\(data.singer.nickname!)")
        
        //显示背景图
        ImageUtil.show(ivBackground, data.banner)
        
        //下载状态
        //根据Id查询当前音乐是否下载
        downloadInfo = downloader.findDownloadInfo(getDownloadInfoId())
        
        if let _ = downloadInfo {
            //有下载任务
            setDownloadCallback()
        }
        
        //不管有没有下载任务
        //都要刷新界面
        refreh()
    }
    
    /// MARK: -  下载相关
    
    /// 根据下载状态刷新界面
    func refreh() {
        if let downloadInfo = downloadInfo {
            //有下载任务
            
            switch downloadInfo.status {
            case .completed:
                //下载完成
                
                btDownload.setImage(UIImage(named: "DownloadSelected"), for: .normal)
            default:
                //其他状态
                //都显示未下载
                //当然也可以监听下载进度
                normalDownloadStatusUI()
            }
            
            //这里不需要知道更详细的下载状态
            //所以就没有判断
            //会在下载管理里面判断
            //打印下载进度
            let start = FileUtil.formatFileSize(downloadInfo.progress)
            let size = FileUtil.formatFileSize(downloadInfo.size)
            print("PlayerController refresh:\(start),\(size)")
        }else {
            //没有下载任务
            normalDownloadStatusUI()
        }
        
        
    }
    
    /// 显示未下载状态UI
    func normalDownloadStatusUI()  {
        btDownload.setImage(UIImage(named: "Download"), for: .normal)
    }
    
    /// 设置下载回调
    func setDownloadCallback() {
        //使用弱引用
        weak var weakSelf = self
        
        downloadInfo!.downloadBlock = {
            downloadInfo in
            weakSelf?.refreh()
        }
    }
    
    /// 获取当前下载任务Id
    ///
    /// - Returns: <#return value description#>
    func getDownloadInfoId() -> String {
        return "\(playListManager.data!.id!)\(PreferenceUtil.userId()!)"
    }
    
    /// 创建下载任务
    func createDownload() {
        let song = playListManager.data!
        let urlString = ResourceUtil.resourceUri(song.uri)
        
        //计算保存的路径
        //保存路径在Documents目录下
        //这里要设置相对路径：CocoaDownloader/用户Id/歌曲uri
        //路径中添加用户Id是实现多用户
        //当然这里很明显的问题是
        //如果多用户都下载一首音乐
        //会导致一首音乐会下载多次
        let path = "CocoaDownloader/\(PreferenceUtil.userId()!)/\(song.uri!)"
        
        print("PlayerController createDownload:\(path)")
        
        //创建下载任务
        downloadInfo = DownloadInfo()
        
        //设置保存路径
        downloadInfo!.path=path
        
        //设置下载的网址
        downloadInfo!.uri=urlString
        
        //设置Id
        downloadInfo!.id=getDownloadInfoId()
        
        //获取系统当前时间
        let date = NSDate(timeIntervalSinceNow: 0)
        
        //转为时间戳
        let currentTimeMillis = date.timeIntervalSince1970
        
        //设置创建时间
        //用于显示下载中
        //下载完成列表排序
        //默认按时间
        downloadInfo!.createdAt=currentTimeMillis
        
        //设置下载回调
        setDownloadCallback()
        
        //开始下载了
        downloader.download(downloadInfo!)
        
        //保存业务数据
        orm.saveSong(song, PreferenceUtil.userId()!)
        
        ToastUtil.short("下载任务添加成功！")
    }
    
    /// 显示时长
    func showDuration() {
        //获取当前音乐总时长
        let duration = musicPlayerManager.data.duration
        
        if duration > 0 {
            //格式化后显示文本
            lbEnd.text = TimeUtil.second2MinuteAndSecond(duration)
            
            //设置到进度条
            sdProgress.maximumValue = duration
        }
    }
    
    /// 显示播放状态
    func showPlayStatus() {
        btPlay.setImage(UIImage(named: "MusicPlay"), for: .normal)
    }
    
    /// 显示暂停状态
    func showPauseStatus() {
        btPlay.setImage(UIImage(named: "MusicPause"), for: .normal)
    }
    
    /// 显示进度
    func showProgress() {
        //获取当前进度
        let progress = musicPlayerManager.data.progress
        
        if progress > 0 {
            //格式化
            lbStart.text = TimeUtil.second2MinuteAndSecond(progress)
            
            //设置到进度条
            sdProgress.value = progress
        }
    }
    
    /// 播放或者暂停
    func playOrPause() {
        if musicPlayerManager.isPlaying() {
            playListManager.pause()
        } else {
            playListManager.resume()
        }
    }
    
    /// 显示播放模式
    func showLoopModel() {
        //获取当前循环模式
        let model = playListManager.getLoopModel()
        
        switch model {
        case .list:
            //列表循环
            btLoopModel.setImage(UIImage(named: "MusicRepeatList"), for: .normal)
        case .random:
            //随机循环
            btLoopModel.setImage(UIImage(named: "MusicRepeatRandom"), for: .normal)
        default:
            //单曲循环
            btLoopModel.setImage(UIImage(named: "MusicRepeatOne"), for: .normal)
        }
    }
    
    // MARK: - 右侧按钮点击事件
    
    /// 右侧分享按钮点击回调
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onShareClick(sender:UIButton) {
        print("PlayerController onShareClick")
    }
    
    /// 右侧更多按钮点击回调
    @objc func onMoreClick()  {
        print("PlayerController onMoreClick")
    }
    
    // MARK: - 迷你控制栏点击事件
    
    /// 下载点击回调
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDownloadClick(_ sender: Any) {
        print("PlayerController onDownloadClick")
        
        //判断是否有下载任务
        if let downloadInfo = downloadInfo {
            //有下载任务
            
            //判断下载状态
            if downloadInfo.status == .completed {
                ToastUtil.short("该歌曲已经下载了！")
            }else {
                //其他状态
                //可能是下载中，等待中
                //下载失败
                ToastUtil.short("已经在下载列表中了！")
            }
        }else {
            //没有下载任务
            createDownload()
        }
    }
    
    // MARK: - 进度相关
    
    /// 当进度条拖拽时
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onProgressChanged(_ sender: UISlider) {
        print("PlayerController onProgressChanged:\(sender.value)")
        
        playListManager.seekTo(sender.value)
    }
    
    // MARK: - 控制栏
    
    /// 循环模式点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLoopModelClick(_ sender: Any) {
        print("PlayerController onLoopModelClick")
        
        playListManager.changeLoopModel()
        
        //显示循环模式
        showLoopModel()
    }
    
    
    /// 上一曲
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPreviousClick(_ sender: Any) {
        print("PlayerController onPreviousClick")
        
        stopReocrdRotate()
        
        playListManager.play(playListManager.previous())
    }
    
    
    /// 播放
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPlayClick(_ sender: Any) {
        print("PlayerController onPlayClick")
        
        playOrPause()
    }
    
    
    
    /// 下一曲
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onNextClick(_ sender: Any) {
        print("PlayerController onNextClick")
        
        stopReocrdRotate()
        
        playListManager.play(playListManager.next())
    }
    
    
    /// 播放列表点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onListClick(_ sender: Any) {
        print("PlayerController onListClick")
        
        //创建一个View
        let musicPlayListView = MusicPlayListView()
        
        //设置尺寸
        musicPlayListView.gk_size = CGSize(width: view.frame.width, height: view.frame.height/1.5)
        
        //设置点击音乐回调
        musicPlayListView.onSongClick = {
            data in
            self.stopReocrdRotate()
            
            self.playListManager.play(data)
        }
        
        //显示这个View
        GKCover.translucentCover(from: view, content: musicPlayListView, animated: true)
    }
    
    // MARK: - 黑胶唱片滚动
    
    /// 异步滚动到当前音乐位置
    func scrollPositionAsync() {
        DispatchQueue.main.async {
            self.scrollPosition()
        }
    }
    
    /// 滚动到当前音乐位置
    func scrollPosition() {
        //获取到播放列表
        let playListOC = playListManager.getPlayList() as! NSArray
        
        //获取当前音乐在播放列表中的位置
        let index = playListOC.index(of: playListManager.data!)
        
        //创建IndexPath
        let indexPath = IndexPath(item: index, section: 0)
        
        //滚动到当前黑胶唱片
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        
        //判断是否需要旋转黑胶唱片
        if musicPlayerManager.isPlaying() {
            DispatchQueue.main.async {
                self.startRecordRotate()
            }
        }
    }
    
    /// 黑胶唱片开始滚动
    /// 指针回到播放位置(默认)
    func startRecordRotate()  {
        //选择黑胶唱片指针
        startRecordThumbRotate()

        //获取当前音乐
        let data = playListManager.data!
        
        print("PlayerController startRecordRotate:\(data.title)")
        
        //发送事件
        SwiftEventBus.post(ON_START_RECORD, sender: data)
    }
    
    /// 黑胶唱片停止滚动
    /// 指针回到暂停状态(-25度)
    func stopReocrdRotate() {
        print("PlayerController stopRecordRotate")
        
        //停止黑胶唱片指针
        stopRecordThumbRotate()
        
        //获取当前音乐
        let data = playListManager.data!
        
        //发送事件
        SwiftEventBus.post(ON_STOP_RECORD, sender: data)
    }
    
    // MARK: - 生命周期
    
    /// 视图即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置标题栏为亮色
        setTitleBarLight()
        
        //设置播放代理
        musicPlayerManager.delegate = self
    }
    
    /// 视图已经显示了
    ///
    /// - Parameter animated: <#animated description#>
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollPositionAsync()
    }
    
    /// 视图即将消失
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //设置标题栏为默认颜色
        setTitleBarDefault()
        
        //取消播放代理
        musicPlayerManager.delegate = nil
    }
    
    //和OC中dealloc差不多
    deinit {
        print("PlayerController deinit")
        
        //停止唱片滚动
        stopReocrdRotate()
    }
    
    // MARK: - 滚动相关
    
    /// 开始拖拽时调用
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("PlayerController scrollViewWillBeginDragging")
        
        if scrollView == tableView {
            //歌词控件
            
            //显示歌词拖拽状态
            showDragView()
        } else {
            //黑胶唱片
        }
    }
    
    /// 滚动中
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //获取滚动距离
        let offsetX = scrollView.contentOffset.x
        let offsetY = scrollView.contentOffset.y
        
        print("PlayerController scrollViewDidScroll offsetX:\(offsetX),offsetY:\(offsetY)")
        
        if scrollView == tableView {
            //歌词列表
            
            if isDrag {
                //只有手动拖拽的时候才处理
                
                //手动计算
                let index = Int((offsetY+tableView.frame.height/2)/44)
                
                //使用系统提供的方法计算
//                let newIndex = tableView.indexPathForRow(at: CGPoint(x: 0, y: offsetY+tableView.frame.height/2))
//
//                print("PlayerController scrollViewDidScroll:\(index),\(newIndex)")
                
                //获取歌词对象
                var lyric:Any!
                if index < 0 {
                    //如果计算出的index小于0
                    //就默认第一个歌词对象
                    lyric = dataArray.first
                }else if index > dataArray.count-1 {
                    //大于最后一个歌词对象（包含填充数据）
                    //就是最后一行数据
                    lyric = dataArray.last
                }else {
                    //如果在列表范围内
                    //就直接去对应位置的数据
                    lyric = dataArray[index]
                }
                
                //判断是否是填充数据
                if lyric is String {
                    //填充数据
                    lbTime.text = ""
                }else {
                    //真实歌词数据
                    //保存到一个字段上
                    scrollSelectedLyricLine = lyric as! LyricLine
                    
                    //将开始时间转为秒
                    let startTime = Float( scrollSelectedLyricLine!.startTime / 1000)
                    
                    //格式化开始时间
                    lbTime.text = TimeUtil.second2MinuteAndSecond(startTime)
                }
            }
        } else {
            //黑胶唱片
            
            if isFirstScroll {
                //停止黑胶唱片滚动
                stopReocrdRotate()
                
                isFirstScroll = false
            }
        }
        
    }
  
    
    /// 结束拖拽
    ///
    /// - Parameters:
    ///   - scrollView: <#scrollView description#>
    ///   - decelerate: <#decelerate description#>
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == tableView {
            //歌词控件
            
            if !decelerate{
                //如果不需要减速
                //就延迟隐藏拖拽效果
                prepareScrollLyricView()
            }
        } else {
            //黑胶唱片
        }
    }
    
    /// 滚动结束（惯性滚动）
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("PlayerController scrollViewDidEndDecelerating")
        
        if scrollView == tableView {
            //歌词控件
            
            //调用延迟启用歌词滚动
            prepareScrollLyricView()
        } else {
            //黑胶唱片
            
            isFirstScroll = true
            
            //获取滚动后的index
            let currentIndex = getCurrentIndex()
            
            //获取滚动后的音乐
            let currentData = playListManager.getPlayList()[currentIndex]
            
            //获取正在播放的这首音乐
            let data = playListManager.data!
            
            //判断滚动后的音乐是否是正在播放的音乐
            if data.id == currentData.id {
                //同一首音乐
                
                if musicPlayerManager.isPlaying() {
                    //如果当前音乐在播放
                    //就开始滚动黑胶唱片
                    startRecordRotate()
                }
            } else {
                //不是同一首音乐
                
                //播放滚动后的这首音乐
                playListManager.play(currentData)
            }
        }
        
        
    }
    
    /// 获取当前位置的IndexPath
    ///
    /// - Returns: <#return value description#>
    func getCurrentIndex() -> Int {
       let indexPath = collectionView.indexPathForItem(at: collectionView.contentOffset)!
        
        print("PlayerController getCurrentIndex:\(indexPath.row)")
        
        return indexPath.row
    }
   
    // MARK: - 唱片指针相关方法
    
    /// 黑胶唱片指针默认状态（播放状态）
    func startRecordThumbRotate() {
        UIView.animate(withDuration: 0.5) {
            self.recordThumb.transform = CGAffineTransform.identity
        }
    }
    
    /// 黑胶唱片指针旋转-25度（暂停状态）
    func stopRecordThumbRotate() {
        UIView.animate(withDuration: 0.5) {
            self.recordThumb.transform=CGAffineTransform(rotationAngle: -0.4363323)
        }
    }
    
    
    // MARK: - 歌词
    
    /// 显示歌词数据
    func showLyricData() {
        //获取当前播放的音乐
        let data = playListManager.data!
        
        //清空歌词列表
        dataArray.removeAll()
        
        if let lyricString = data.lyric {
            //有歌词
            lyricEmpty.isHidden=true
            
            lyric=LyricParser.parse(data.style, lyricString)
            
            //添加占位数据
            addLyricFillData()
            
            //添加歌词到列表
            dataArray=dataArray+lyric!.datas
        
            //添加占位数据
            addLyricFillData()
        }else {
            //没有歌词
            lyricEmpty.isHidden=false
        }
        
        //刷新列表
        tableView.reloadData()
    }
    
    /// 显示歌词进度
    ///
    /// - Parameter progress: <#progress description#>
    func showLyricProgress(_ progress:Float) {
        if lyric == nil {
            //没有歌词
            return
        }
        
        if isDrag {
            //如果正在拖拽歌词
            //就直接返回
            return
        }
        
        //获取当前时间对应的歌词索引
        let newLineNumber = LyricUtil.getLineNumber(lyric!, progress) + lyricPlaceholderSize
        
        if newLineNumber != lineNumber {
            //滚动到当前行
            scrollLyricPosition(newLineNumber)
            
            lineNumber = newLineNumber
        }
        
        //如果是精确到字歌曲
        //还需要将时间分发到Cell中
        //因为要计算唱到那个字了
        if let lyric = lyric, lyric.isAccurate {
            let cell = getLyricCell(lineNumber)
            if let cell = cell {
                //有可能获取不到当前位置的Cell
                //因为上面使用了滚动动画
                //如果不使用滚动动画效果不太好
                cell.show(progress)
            }
        }
    }
    
    /// 滚动到当前歌词行
    ///
    /// - Parameter position: <#position description#>
    func scrollLyricPosition(_ position:Int) {
       let indexPaht = IndexPath(item: position, section: 0)
        tableView.selectRow(at: indexPaht, animated: true, scrollPosition: .middle)
    }
    
    /// 添加歌词占位数据
    func addLyricFillData() {
        //计算占位数
        //tableView.height/2/44
        lyricPlaceholderSize = Int(ceil( Double(tableView.frame.height)/2.0/44.0))
        
        for _ in 0..<lyricPlaceholderSize {
            dataArray.append("fill")
        }
    }
    
    
    
    /// 拖拽状态前面播放按钮点击回调
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLyricPlayClick(_ sender: Any) {
        print("PlayerController onLyricPlayClick")
        
        if let lyricLine = scrollSelectedLyricLine {
            //将开始时间转为秒
            let startTime = Float(lyricLine.startTime)/1000.0
            
            //从当前位置开始播放
            playListManager.seekTo(startTime)
            
            //马上滚动歌词
            enableScrollLyric()
        }
    }
    
    /// 显示歌词拖拽效果
    func showDragView() {
        isDrag = true
        
        dragContainer.isHidden = false
    }
    
    /// 倒计时开启滚动歌词
    func prepareScrollLyricView() {
        cancelTask()
        
        task = DispatchWorkItem {
            self.enableScrollLyric()
        }
        
        //创建一个延迟任务
        DispatchQueue.main.asyncAfter(deadline: .now()+5, execute: task!)
    }
    
    /// 取消倒计时开启滚动歌词延迟
    func cancelTask() {
        if let task = task {
            task.cancel()
            self.task=nil
        }
    }
    
    /// 开启歌词滚动效果
    func enableScrollLyric() {
        isDrag = false
        
        dragContainer.isHidden = true
    }
    
    /// 获取指定位置的歌词Cell
    ///
    /// - Parameter position: <#position description#>
    /// - Returns: <#return value description#>
    func getLyricCell(_ position:Int) -> LyricCell? {
        let indexPath = IndexPath(item: position, section: 0)
        return tableView.cellForRow(at: indexPath) as? LyricCell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension PlayerController {
    
    /// 启动界面
    ///
    /// - Returns: <#return value description#>
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller = navigationController.storyboard!.instantiateViewController(withIdentifier: "Player")
        
        //添加到导航控制器中
        navigationController.pushViewController(controller, animated: true)
    }
}

// MARK: - 播放代理
extension PlayerController: MusicPlayerDelegate {
    //播放器准备完毕了
    func onPrepared(_ data: Song) {
        print("PlayerController onPrepared title:\(data.title),duration:\(data.duration)")
        
        //显示初始化数据
        showInitData()
        
        //显示时长
        showDuration()
        
        //滚动到当前音乐位置
        scrollPositionAsync()
    }
    
    /// 暂停了
    ///
    /// - Parameter data: <#data description#>
    func onPaused(_ data: Song) {
        print("PlayerController onPaused")
        
        showPlayStatus()
        
        stopReocrdRotate()
    }
    
    /// 播放中
    ///
    /// - Parameter data: <#data description#>
    func onPlaying(_ data: Song) {
        print("PlayerController onPlaying:\(playListManager.data!.title)")
        
        showPauseStatus()
        
        startRecordRotate()
    }
    
    /// 进度回调
    ///
    /// - Parameter data: <#data description#>
    func onProgress(_ data: Song) {
        print("PlayerController onProgress:\(data.progress)")
        
        showProgress()
        
        //显示歌词进度
        showLyricProgress(data.progress)
    }
    
    /// 歌词信息改变了
    ///
    /// - Parameter data: <#data description#>
    func onLyricChanged(_ data: Song) {
        print("PlayerController onLyricChanged:\(data.lyric)")
        
        showLyricData()
    }
}

// MARK: - 实现CollectionView数据源和代理
extension PlayerController: UICollectionViewDataSource,UICollectionViewDelegate {
    
    /// 返回有多少个
    ///
    /// - Parameters:
    ///   - collectionView: <#collectionView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playListManager.getPlayList().count
    }
    
    /// 返回当前位置Cell
    ///
    /// - Parameters:
    ///   - collectionView: <#collectionView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //取出数据
        let data = playListManager.getPlayList()[indexPath.row]
        
        //获取Cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL, for: indexPath) as! SongRecordCell
        
        //设置Tag
        cell.tag = indexPath.row
        
        //绑定数据
        cell.bindData(data)
        
        //返回cell
        return cell
    }
    
    
}

// MARK: - 实现UICollectionViewDelegateFlowLayout协议
extension PlayerController: UICollectionViewDelegateFlowLayout {
    
    /// 返回Cell和CollectionView的间距
    ///
    /// - Parameters:
    ///   - collectionView: <#collectionView description#>
    ///   - collectionViewLayout: <#collectionViewLayout description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    /// 返回每一行Cell间距
    ///
    /// - Parameters:
    ///   - collectionView: <#collectionView description#>
    ///   - collectionViewLayout: <#collectionViewLayout description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    /// 返回每列Cell间距
    ///
    /// - Parameters:
    ///   - collectionView: <#collectionView description#>
    ///   - collectionViewLayout: <#collectionViewLayout description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    /// 返回Cell大小
    ///
    /// - Parameters:
    ///   - collectionView: <#collectionView description#>
    ///   - collectionViewLayout: <#collectionViewLayout description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //让Cell和Collection一样大
        return collectionView.frame.size
    }
}

// MARK: - 歌词列表数据源和代理
extension PlayerController:UITableViewDataSource,UITableViewDelegate {
    
    /// 有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath) as! LyricCell
        
        //设置Tag
        cell.tag = indexPath.row
        
        //取出数据
        let data = dataArray[indexPath.row]
        
        //绑定数据
        cell.bindData(data,lyric!.isAccurate)
        
        //返回cell
        return cell
    }
    
    
}
