//
//  LocalMusicController.swift
//  本地音乐界面
//
//  Created by smile on 2019/7/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class LocalMusicController: BaseTitleController {

    /// 按钮容器
    @IBOutlet weak var buttonContainer: UIStackView!
    
    /// 全选
    @IBOutlet weak var btSelectAll: UIButton!
    
    /// 删除
    @IBOutlet weak var btDelete: UIButton!
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 进入/退出编辑模式
    var editBarItem:UIBarButtonItem!
    
    /// 列表数据
    var dataArray:[Song] = []
    
    /// 下载管理器
    var downloader:DownloadManager!
    
    /// 播放列表管理器
    var playListManager:PlayListManager!
    
    override func initViews() {
        super.initViews()
        
        setTitle("本地音乐")
        
        //右上角添加编辑按钮
        editBarItem = UIBarButtonItem(title: "批量编辑", style: .plain, target: self, action: #selector(onEditClick(sender:)))
        
        //将按钮添加到导航栏右侧
        navigationItem.rightBarButtonItem=editBarItem
    }
    
    /// 编辑按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onEditClick(sender:UIButton) {
        print("LocalMusicController onEditClick")
        
        //判断是否有数据
        if dataArray.count == 0{
            ToastUtil.short("没有本地音乐！")
            return
        }
        
        if tableView.isEditing {
            //在编辑模式下
            
            //退出编辑模式
            exitEditMode()
        } else {
            //没有进入编辑模式
            
            //进入编辑模式
            editBarItem.title="取消编辑"
            
            tableView.setEditing(true, animated: true)
            buttonContainer.isHidden=false
        }
        
    
    }
    
    /// 退出编辑模式
    func exitEditMode() {
        editBarItem.title="批量编辑"
        
        //退出编辑模式
        tableView.setEditing(false, animated: true)
        buttonContainer.isHidden=true
        
        //按钮重置为默认状态
        defaultButtonStatus()
        
    }
    
    /// 按钮重置为默认状态
    func defaultButtonStatus() {
        btSelectAll.setTitle("全选", for: .normal)
        
        btDelete.isEnabled=false
    }
    
    override func initDatas() {
        super.initDatas()
        
        //初始化下载管理器
        downloader = DownloadManager.sharedInstance()
        
        //初始化播放列表管理器
        playListManager = PlayListManager.shared
        
        //查询所有已经下载完成的音乐
        let datas = downloader.findAllDownloaded() as? [DownloadInfo]
        
        if datas != nil && datas!.count > 0 {
            //有下载完成的音乐
            dataArray=dataArray+processSong(datas!)
            
            tableView.reloadData()
        } else {
            //没有下载完成的音乐
            
        }
    }
    
    /// 将下载信息转为音乐信息
    ///
    /// - Parameter datas: <#datas description#>
    /// - Returns: <#return value description#>
    func processSong(_ datas:[DownloadInfo]) -> [Song] {
        var results:[Song] = []
        
        for data in datas {
            let song = ORMUtil.shared().findSongById(data.id)!
            results.append(song)
        }
        
        return results
    }
    
    
    /// 选择所有按钮点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSelectAllClick(_ sender: Any) {
        print("LocalMusicController onSelectAllClick")
        
        let selected = isSelected()
        
        for i in 0..<dataArray.count {
            let indexPath = IndexPath(item: i, section: 0)
            
            if selected {
                //有选中
                
                //取消选择
                tableView.deselectRow(at: indexPath, animated: true)
            }else {
                //没有选中
                
                //选择所有
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
            }
        }
        
        //刷新按钮状态
        showButtonStatus()
    }
    
    
    /// 删除点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDeleteClick(_ sender: Any) {
        print("LocalMusicController onDeleteClick")
        
        //获取到选中的IndexPath
        let indexPaths = tableView.indexPathsForSelectedRows!
        
        /// 保存要删除的对象id
        var deleteIndex:[String] = []
        
        for indexPath in indexPaths {
            let data = dataArray[indexPath.row]
            
            //从下载框架中删除
            let downloadInfo = downloader.findDownloadInfo("\(data.id!)\(PreferenceUtil.userId()!)")
            downloader.remove(downloadInfo)
            
            //从数据源中删除
            //dataArray.remove(at: indexPath.row)
            
            //将要删除对象的Id保存到列表中
            deleteIndex.append(data.id)
        }
        
        //删除数据源数据
        dataArray.removeAll { (data) -> Bool in
            return deleteIndex.contains(data.id)
        }
        
        //删除cell
        tableView.deleteRows(at: indexPaths, with: .automatic)
        
        //或者重新加载数据源
//        tableView.reloadData()
        
        //退出编辑模式
        exitEditMode()
    }
    
    /// 显示按钮状态
    func showButtonStatus() {
        if isSelected() {
            //有选中的条目
            
            btSelectAll.setTitle("取消全选", for: .normal)
            btDelete.isEnabled=true
        } else {
            //没有选中的条目
            
            defaultButtonStatus()
        }
    }
    
    /// 是否有选中
    ///
    /// - Returns: <#return value description#>
    func isSelected() -> Bool {
        return tableView.indexPathsForSelectedRows != nil
    }
    
    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建控制器
        let controller = nav.storyboard!.instantiateViewController(withIdentifier: "LocalMusic")
        
        //将控制器放入导航控制器
        nav.pushViewController(controller, animated: true)
    }

}

// MARK: - 列表数据源和代理
extension LocalMusicController:UITableViewDataSource,UITableViewDelegate {
   
    /// 返回有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let song = dataArray[indexPath.row]
        
        //获取cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath)
        
        //设置tag
        cell.tag = indexPath.row
        
        //绑定数据
        //根据Id查询业务数据
//        let song = ORMUtil.shared().findSongById(data.id)
//
//        if let song = song {
            //设置标题
            cell.textLabel?.text=song.title
        
            //设置歌手名称
            cell.detailTextLabel?.text=song.singer.nickname
//        }else {
//            //TODO 如果为空就表示逻辑有问题
//        }
        
        return cell
    }
    
    /// cell选中
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            //如果是编辑状态
            //就不进入播放页面
            showButtonStatus()
            return
        }
        
        let song = dataArray[indexPath.row]
        
        //播放
        
//        //这里是将点击的这首音乐替换播放列表
//        //如果点击一首音乐后
//        //要将所有下载的音乐都设置到播放列表
//        //则需要遍历所有的下载音乐转为song
//        //因为播放列表需要的是song对象
//
//        //根据Id获取业务数据
//        let song = ORMUtil.shared().findSongById(data.id)!
//
//        playListManager.setPlayList([song])
        
        playListManager.setPlayList(dataArray)
        playListManager.play(song)

        //打开播放界面
        PlayerController.start(navigationController!)
    }
    
    /// 取消选中
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        showButtonStatus()
    }
    
    // MARK: - 编辑相关
    
    /// 当前index对应的cell是否可以编辑
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    /// 编辑样式
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if tableView.isEditing {
            //如果在编辑模式下
            
            //要想左侧显示的是多选按钮
            //这里需要返回delete和insert
            //如果只返回delete
            //那么左侧显示的是一个减号
            return UITableViewCell.EditingStyle(rawValue: UITableViewCell.EditingStyle.delete.rawValue | UITableViewCell.EditingStyle.insert.rawValue)!
        } else {
            //单选编辑模式
            return .delete
        }
       
    }
    
    /// 返回编辑button的标题
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "删除"
    }
    
    /// 点击编辑按钮后的回调方法
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - editingStyle: <#editingStyle description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //删除按钮点击回调
            
            //取出当前位置数据
            let data = dataArray[indexPath.row]
            
            //从下载框架中删除
            let downloadInfo = downloader.findDownloadInfo("\(data.id!)\(PreferenceUtil.userId()!)")
            downloader.remove(downloadInfo)
            
            //从当前列表数据源中删除
            dataArray.remove(at: indexPath.row)
            
            //从tableView中删除cell
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            print("LocalMusicController commit edit delete:\(data.title)")
        }
    }
}
