//
//  UserController.swift
//  好友列表；粉丝列表
//
//  Created by smile on 2019/7/16.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class UserController: BaseTitleController {
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 用户Id
    var userId:String!
    
    /// 当前界面类型
    var style:UserStyle!
    
    /// 列表数据源
    var dataArray:[User] = []
    
    override func initViews() {
        super.initViews()
        
        if style == UserStyle.friend {
            setTitle("好友")
            
            //添加 添加好友列表按钮
            createAddFriendMenu()
        } else {
            setTitle("粉丝")
        }
        
        //注册Cell
        tableView.register(UINib(nibName: UserCell.NAME, bundle: nil), forCellReuseIdentifier: UserCell.NAME)
    }
    
    /// 添加 添加好友列表按钮
    func createAddFriendMenu() {
        //创建右侧添加好友按钮
        //通过按钮创建一个BarButtonItem
        let addFriendBarItem=UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onAddFrendClick))
        
        //把按钮添加到导航栏右侧
        navigationItem.rightBarButtonItem=addFriendBarItem
    }
    
    /// 添加好友按钮点击
    @objc func onAddFrendClick() {
        SearchUserController.start(navigationController!)
    }
    
    override func initDatas() {
        super.initDatas()
        
        /// 创建一个变量
        let api:Observable<ListResponse<User>?>!
        
        //根据不同的入口调用不同的api
        if style == UserStyle.friend {
            //好友
            api=Api.shared.friends(userId)
        } else {
            //粉丝
            api=Api.shared.fans(userId)
        }
        
        //请求接口
        api.subscribeOnSuccess { (data) in
            if let data = data?.data?.data{
                self.dataArray=self.dataArray+data
                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
    }

    /// 当前界面类型
    ///
    /// - friend: 好友
    /// - fans: 粉丝
    enum UserStyle {
        case friend
        case fans
    }
}

// MARK: - 启动方法
extension UserController{
    
    /// 启动方法
    ///
    /// - Parameters:
    ///   - nav: <#nav description#>
    ///   - userId: <#userId description#>
    static func start(_ nav:UINavigationController,_ userId:String,_ style:UserStyle) {
        //创建控制器
        let controller=nav.storyboard!.instantiateViewController(withIdentifier: "User") as! UserController
        
        //传递参数
        controller.userId=userId
        controller.style=style
        
        //把控制器添加到导航控制器中
        nav.pushViewController(controller, animated: true)
    }
}

// MARK: - 列表数据源和代理
extension UserController:UITableViewDataSource,UITableViewDelegate{
    
    /// 有多少个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取cell
        return tableView.dequeueReusableCell(withIdentifier: UserCell.NAME, for: indexPath)
    }
    
    /// cell即将显示
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - cell: <#cell description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //转换cell
        guard let cell = cell as? UserCell else {
            return
        }
        
        //取出数据
        let data=dataArray[indexPath.row]
        
        //绑定数据
        cell.bindData(data)
    }
    
    /// cell点击
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //获取data
        let data=dataArray[indexPath.row]
        
        UserDetailController.start(navigationController!, userId: data.id)
    }
}
