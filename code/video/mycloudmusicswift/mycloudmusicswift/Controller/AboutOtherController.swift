//
//  AboutOtherController.swift
//  关于爱学啊（其他Storyboard）
//
//  Created by smile on 2019/7/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class AboutOtherController: BaseTitleController {
    
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    override func initViews() {
        super.initViews()
        
        setTitle("关于爱学啊（其他Storyboard）")
    }
    
    override func initDatas() {
        super.initDatas()
        
        lbTitle.text="这个界面在About.storyboard中\n我是代码中设置的"
    }

    /// 启动方法
    ///
    /// - Parameter nav: <#nav description#>
    static func start(_ nav:UINavigationController) {
        //创建Storyboard
        let storyboard=UIStoryboard(name: "About", bundle: nil)
        
        //创建控制器
        let controller=storyboard.instantiateViewController(withIdentifier: "AboutOther")
        
        //添加到导航控制器
        nav.pushViewController(controller, animated: true)
    }

}
