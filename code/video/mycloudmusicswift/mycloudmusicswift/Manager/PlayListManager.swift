//
//  PlayListManager.swift
//  歌单列表管理器
//  主要是封装了列表相关的操作
//  例如：上一曲，下一曲，循环模式等功能
//
//  Created by smile on 2019/6/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class PlayListManager {
    //单例设计模式
    static var shared = PlayListManager()
    
    /// 播放管理器
    var musicPlayerManager:MusicPlayerManager!
    
    //播放列表
    var datum:[Song] = []
    
    /// 当前播放的音乐
    var data:Song?
    
    /// 是否播放了音乐
    var isPlay = false
    
    /// 循环模式
    var model:MusicPlayerRepeatModel = .list
    
    /// 数据库
    var orm:ORMUtil = ORMUtil.shared()
    
    init() {
        
        //初始化播放管理器
        musicPlayerManager = MusicPlayerManager.shared()
        
        //设置播放完毕回调
        musicPlayerManager.onComplete = {
            data in
            //判断播放循环模式
            if self.getLoopModel() == MusicPlayerRepeatModel.one {
                //单曲循环
                self.play(self.data!)
            } else {
                //其他模式
                self.play(self.next())
            }
        }
        
        //从数据库中恢复播放列表
        initPlayList()
    }
    
    /// 设置播放列表
    ///
    /// - Parameter datum: <#datum description#>
    func setPlayList(_ datum:[Song]) {
        print("PlayListManager setPlayList")
        
        //将原来的播放列表数据中的playList标记清除
        DataUtil.changePlayListFlag(self.datum, false)
        saveAll()
        
        //清空原来的数据
        self.datum.removeAll()
        
        //添加新的数据
        self.datum = self.datum+datum
        
        //更改播放列表标记
        DataUtil.changePlayListFlag(self.datum, true)
        
        //保存到数据库
        saveAll()
    }
    
    /// 获取播放列表
    ///
    /// - Returns: <#return value description#>
    func getPlayList() -> [Song] {
        print("PlayListManager getPlayList")
        return datum
    }
    
    /// 播放当前位置的音乐
    ///
    /// - Parameter position: <#position description#>
    func play(_ position:Int) {
        print("PlayListManager play:\(position)")
        
        let data = datum[position]
        
        play(data)
    }
    
    /// 播放当前音乐
    ///
    /// - Parameter data: <#data description#>
    func play(_ data:Song) {
        self.data = data
        
        //查询是否有下载任务
        let downloadInfo = DownloadManager.sharedInstance().findDownloadInfo("\(data.id!)\(PreferenceUtil.userId()!)")
        
        if downloadInfo != nil && downloadInfo.status == .completed  {
            //下载完成了
            
            //播放本地音乐
            musicPlayerManager.play(downloadInfo.absolutePath(), data)
            
            print("PlayListManager play offline:\(downloadInfo.absolutePath()),\(data.uri)")
        }else {
            //播放在线音乐
            let path = ResourceUtil.resourceUri(data.uri)
            
            musicPlayerManager.play(path, data)
            
            print("PlayListManager play online:\(path),\(data.uri)")
        }
        
        //标记为播放了
        isPlay = true
        
    }
    
    /// 暂停
    func pause() {
        print("PlayListManager pause")
        musicPlayerManager.pause()
    }
    
    /// 继续播放
    func resume() {
        print("PlayListManager resume")
        
        if isPlay {
            //播放管理器已经播放过音乐了
            musicPlayerManager.resume()
        } else {
            //还没有进行播放
            //第一次打开应用点击继续播放
            play(data!)
            
            //继续播放
            if data!.progress > 1 {
                //有播放记录
                //继续播放
                musicPlayerManager.seekTo(data!.progress-1)
            }
        }
    }
    
    /// 上一曲
    ///
    /// - Returns: <#return value description#>
    func previous() -> Song {
        var index = 0
        
        switch model {
        case .random:
            //随机循环
            index = Int(arc4random()) % datum.count
        default:
            //其他模式
            let datumOC = datum as NSArray
            index = datumOC.index(of: data!)
            
            if index != -1 {
                //找到了
                
                //判断当前播放的音乐是不是第一首音乐
                if index == 0{
                    //当前播放的是第一首音乐
                    index = datum.count - 1
                }else {
                    index -= 1
                }
            }else{
                //抛出异常
                //因为正常情况下不会走到了
            }
            
        }
        
        print("PlayListManager previous index:\(index)")
        
        return datum[index]
    }
    
    /// 下一曲
    ///
    /// - Returns: <#return value description#>
    func next() -> Song {
        var index = 0
        
        switch model {
        case .random:
            //随机循环
            
            //在0~datum.size-1范围中
            //产生一个随机数
            index = Int(arc4random()) % datum.count
            
//            print("PlayListManager next random:\(index)")
            
//            return datum[index]

        default:
            //列表循环
            let datumOC = datum as NSArray
            index = datumOC.index(of: data!)
            if index != -1 {
                //找到了
                
                //如果当前播放的音乐是最后一首音乐
                if index == datum.count - 1 {
                    //当前播放的是最后一首音乐
                    index = 0
                }else {
                    index += 1
                }
            }else{
                //抛出异常
                //因为正常情况下不会走到了
            }

        }
        
        print("PlayListManager next index:\(index)")
        
        return datum[index]
    }
    
    /// 切换循环模式
    ///
    /// - Returns: <#return value description#>
    func changeLoopModel() -> MusicPlayerRepeatModel {
        //将当前循环模式转为int
        var model = self.model.rawValue
        
        //循环模式+1
        model += 1
        
        //判断边界
        if model > MusicPlayerRepeatModel.random.rawValue {
            //超出了范围
            model = 0
        }
        
        self.model = MusicPlayerRepeatModel(rawValue: model)!
        
        return self.model
    }
    
    /// 获取当前循环模式
    ///
    /// - Returns: <#return value description#>
    func getLoopModel() -> MusicPlayerRepeatModel {
        return model
    }
    
    /// 从指定位置开始播放
    ///
    /// - Parameter value: <#value description#>
    func seekTo(_ value:Float) {
        musicPlayerManager.seekTo(value)
        
        //如果暂停了就继续播放
        if !musicPlayerManager.isPlaying() {
            resume()
        }
    }
    
    /// 从播放列表中删除这首音乐
    ///
    /// - Parameter data: <#data description#>
    func delete(_ data:Song) {
        //判断删除的音乐是否是当前正在播放的音乐
        if data.id == self.data!.id {
            //当前删除的音乐就是当前播放的音乐
            
            //暂停当前音乐
            if musicPlayerManager.isPlaying() {
                pause()
            }

            //获取下一首音乐
            if datum.count > 0 {
                //还有音乐
                let nextSong = next()
                if nextSong.id == data.id {
                    //要删除的音乐
                    self.data = nil
                    
                    //TODO Bug随机循环的情况下有可能获取到自己
                }else {
                    play(nextSong)
                }
            }else {
                self.data = nil
            }
        }
        
        //删除当前音乐
        let datumOC = datum as NSArray
        let index = datumOC.index(of: data)
        datum.remove(at: index)
        
        //从数据库中删除这首音乐
        orm.deleteSong(data)
    }
    
    /// 保存播放列表
    func saveAll() {
        for song in datum {
            orm.saveSong(song, PreferenceUtil.userId()!)
        }
    }
    
    /// 保存当前音乐信息
    func saveSong() {
        if let data = data {
            //有音乐才保存
            
            //保存当前音乐信息到数据库
            orm.saveSong(data, PreferenceUtil.userId()!)
            
            //保存当前音乐的Id到配置文件
            PreferenceUtil.setLastPlaySongId(data.id)
        }
        
    }
    
    /// 初始化播放列表
    func initPlayList()  {
        //从数据库中查询当前用户的播放列表
        let songs = orm.queryPlayList(PreferenceUtil.userId()!)
        
        //判断有没有播放列表
        if songs.count > 0 {
            //有播放列表
            
            //添加到播放列表
            datum = datum + songs
            
            //获取上一次播放的这首音乐
            let songId = PreferenceUtil.lastPlaySongId()
            
            if let songId = songId {
                //上一次有播放音乐
                
                //找到上一次播放的音乐
                for song in songs {
                    if song.id == songId {
                        data = song
                        break
                    }
 
                }
                
                if let data = data {
                    //找到了
                    setMusicPlayerManagerData(data)
                }else {
                    //没找到
                    //可能各种原因
                    defaultPlaySong()
                }
            }else {
                //设置默认音乐
                defaultPlaySong()
            }
        }
    }
    
    /// 设置默认播放的音乐
    func defaultPlaySong() {
        data = datum[0]
        
        setMusicPlayerManagerData(data!)
    }
    
    /// 设置音乐对象到播放管理器
    ///
    /// - Parameter data: <#data description#>
    func setMusicPlayerManagerData(_ data:Song) {
        musicPlayerManager.data = data
    }
}


/// 循环模式
///
/// - list: 列表循环
/// - one: 单曲循环
/// - random: 随机循环
enum MusicPlayerRepeatModel:Int {
    case list = 0
    case one
    case random
}
