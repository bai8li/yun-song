//
//  LoginControllerTests.swift
//  mycloudmusicswiftUITests
//
//  Created by smile on 2019/6/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import XCTest

class LoginControllerTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    /// 测试方法
    //每个测试方法是独立的
    //也就说执行每个测试方法前都会
    //安装一遍应用
    func testOther() {
        print("LoginControllerTests testOther")
    }
    
    /// 测试登录
    func testLogin() {
        //等待5秒钟
        //因为应用重新运行后
        //有启动界面
        
        //真实项目中会采用其他方法
        //例如：找到跳过广告按钮并点击
        //快速进入到需要测试的界面
        //因为测试时间肯定希望越短越好
        sleep(5)
        
        //现在是登录注册页面
        
        //上下文
        let app = XCUIApplication()
        
        //找到登录按钮
        var btLogin=app.buttons["登 录"]
        
        //点击登录按钮
        btLogin.tap()
        
        //现在是登录页面
        
        //找到登录按钮
        btLogin=app.buttons["登录"]
        
        //---------不输入用户名和密码
        btLogin.tap()
        
        //确认提示是否正确
        XCTAssertTrue(app.staticTexts["请输入用户名！"].exists)
        
        //---------输入1位用户名
        //找到用户名输入框
        let tfNickname=app.textFields["请输入手机号/邮箱"]
        
        //点击用户名输入框
        tfNickname.tap()
        
        //输入用户名
        tfNickname.typeText("i")
        
        //点击登录按钮
        btLogin.tap()
        
        //确认提示是否正确
        XCTAssertTrue(app.staticTexts["用户名格式不正确！"].exists)
        
        //---------输入正确的邮箱
        //清除原来的用户名
        
        //由于我们的实现是只有获取了焦点
        //才显示清除按钮
        //所以这种实现没问题
        app.buttons["Clear text"].tap()
        
        //输入用户名
        tfNickname.typeText("ixueaedu@163.com")
        
        //点击登录按钮
        btLogin.tap()
        
        //确认提示是否正确
        XCTAssertTrue(app.staticTexts["请输入密码！"].exists)
        
        //---------输入1位密码
        //找到密码输入框
        let tfPassword=app.secureTextFields["请输入密码"]
        
        //点击密码输入框
        tfPassword.tap()
        
        //输入密码
        tfPassword.typeText("i")
        
        //点击登录按钮
        btLogin.tap()
        
        //确认提示是否正确
        XCTAssertTrue(app.staticTexts["密码格式不正确！"].exists)
        
        //---------输入和账号匹配的密码
        //清除密码
        app.buttons["Clear text"].tap()
        
        //输入密码
        tfPassword.typeText("ixueaedu")
        
        //点击登录
        btLogin.tap()
        
        //延时3秒
        //因为有网络请求
        sleep(3)
        
        //确认提示是否正确
        
        //判断tabBars里面的按钮否有发现按钮
        //如果有就表示登陆成功
        //并进入了首页
        XCTAssertTrue(app.tabBars.buttons["发现"].exists)
    }

}
