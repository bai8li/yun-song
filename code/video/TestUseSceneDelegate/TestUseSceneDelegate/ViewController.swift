//
//  ViewController.swift
//  TestUseSceneDelegate
//
//  Created by smile on 2020/12/26.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func onButtonClick(_ sender: UIButton) {
        SceneDelegate.shared.toOther()
    }
    
}

