//
//  SceneDelegate.swift
//  TestUseSceneDelegate
//
//  Created by smile on 2020/12/26.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
        
    /// 定义一个静态的计算属性
    /// 返回SceneDelegate对象实例
    open class var shared: SceneDelegate {
        get {
            let scene = UIApplication.shared.connectedScenes.first
            return scene?.delegate as! SceneDelegate
        }
    }
    
    func toOther() {
        setRootViewController(name: "Other")
    }
    
    /// 设置跟控制器
    ///
    /// - Parameter name: <#name description#>
    func setRootViewController(name:String) {
        //获取到Main.storyboard
        let mainStory=UIStoryboard(name: "Main", bundle: nil)
        
        //实例化Guide场景
        //因为场景关联了控制器
        //所以说也可以说实例化了一个控制
        //只是这个过程是系统创建的
        //不是我们手动完成
        let controller=mainStory.instantiateViewController(withIdentifier: name)
        
        //替换掉原来的根控制器
        //目的是，我们不希望用户还能返回到启动界面
        window!.rootViewController=controller
    }


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

