//
//  ViewController.swift
//  TestScreenAdapter
//
//  Created by smile on 2019/7/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //测试获取状态栏高度
        //获取的值是一倍尺寸
        
        //iPhone X:(0.0, 0.0, 375.0, 44.0)
        //iPhone SE:(0.0, 0.0, 320.0, 20.0)
        let statusRect=UIApplication.shared.statusBarFrame
        print("ViewController status rect:\(statusRect)")
        
        //获取导航栏高度
        //iPhone X:(0.0, 44.0, 375.0, 44.0)
        //iPhone SE:(0.0, 20.0, 320.0, 44.0)
        let navigationRect=navigationController?.navigationBar.frame
        print("ViewController navigation rect:\(navigationRect)")
    }

    /// 已经重新布局了
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //获取布局的安全区
        //在该界面iPhone X上
        //top是88：是状态栏+到导航栏的高度
        //bottom是83：49（TabBar高度）+34（底部安全区）
        //获取使用代码布局的时候可以手动偏移这么多距离
        //当然也可以使用自动布局和safeAreaInsets对齐
        
        //iPhone X:(top: 88.0, left: 0.0, bottom: 34.0, right: 0.0)
        //iPhone SE:(top: 64.0, left: 0.0, bottom: 0.0, right: 0.0)
        let safeAreaInsets=view.safeAreaInsets
        print("ViewController safe area insets:\(safeAreaInsets)")
        
    }

}

