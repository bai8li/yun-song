//
//  ViewController.swift
//  TestScreenAdapter
//
//  Created by smile on 2019/4/30.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //测试获取状态栏高度
        //获取的值是一倍尺寸
        let statusRect = UIApplication.shared.statusBarFrame
        print("DiscoveryController status rect:\(statusRect)")
        
        //测试获取导航栏高度
        let navigationRect = self.navigationController?.navigationBar.frame;
        print("DiscoveryController navigation rect:\(navigationRect)")
    }

    /// 已经重新布局
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        //获取布局的安全区
        //在该界面iPhone X上
        //top是88：是状态栏+到导航栏的高度
        //bottom是83：49（TabBar高度）+34（底部安全区）
        //获取使用代码布局的时候可以手动偏移这么多距离
        //当然也可以使用自动布局和safeAreaInsets对齐
        let safeAreaInsets=view.safeAreaInsets
        print("DiscoveryController safe area insets:\(safeAreaInsets)")
    }
}

