//
//  ThemeManagerExtend.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/5/13.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//主题框架
import SwiftTheme

extension ThemeManager {
    
    /// 是否是夜间模式
    ///
    /// - Returns: <#return value description#>
    static func isNight() -> Bool {
        return ThemeManager.currentThemeIndex == 1
    }
}
