//
//  View.swift
//  对UIView的扩展类，扩展了，设置边框
//
//  Created by smile on 2019/4/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit


// MARK: - 这里使用了Swift的扩展语法
//不懂的朋友可以学习下Swift课程
extension UIView{
    
    /// 显示主颜色默认宽度边框
    func showColorPrimaryBorder() {
//        边框为1
        self.layer.borderWidth = CGFloat(SIZE_BORDER)
        
        //        边框颜色
        self.layer.borderColor = UIColor(red: 212/255.0, green: 69/255.0, blue: 73/255.0, alpha: 1.0).cgColor
    }
    
    /// 显示边框
    func showBorder(_ color:UIColor) {
        //        边框为1
        self.layer.borderWidth = CGFloat(SIZE_BORDER)
        
        //        边框颜色
        self.layer.borderColor = color.cgColor
    }
    
    /// 更改View锚点
    /// 会自动修正位置的偏移
    ///
    /// - Parameter anchorPoint: <#anchorPoint description#>
    func setViewAnchorPoint(_ anchorPoint:CGPoint) {
        //原来的锚点
        let originAnchorPoint = layer.anchorPoint
        
        //要偏移的锚点
        let offetPoint = CGPoint(x: anchorPoint.x - originAnchorPoint.x, y: anchorPoint.y - originAnchorPoint.y)
        
        //要偏移的距离
        let offetX=(offetPoint.x) * frame.size.width
        let offetY=(offetPoint.y) * frame.size.height
        
        //设置这个值 说明已经改变了偏移量
        layer.anchorPoint = anchorPoint
        
        //将指定的偏宜量更改回来
        layer.position = CGPoint(x: layer.position.x + offetX, y: layer.position.y + offetY)
    }
}
