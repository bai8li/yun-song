//
//  UIColor.swift
//  对UIColor的扩展；支持从16进制创建UIColor
//
//  Created by smile on 2019/4/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

extension UIColor{
    /// 通过16进制创建UIColor
    /// convenience：表示这是一个便利构造函数
    /// 是Swift语法，这里不讲解
    ///
    /// - Parameter hex: <#hex description#>
    public convenience init(hex: Int){
        self.init(hex:hex,alpha:1)
    }
    
    /// 通过16进制创建UIColor
    /// convenience：表示这是一个便利构造函数
    /// 是Swift语法，这里不讲解
    ///
    /// - Parameter hex: <#hex description#>
    public convenience init(hex: Int,alpha:Int){
        //        self.init()
        
        //        从Int中，通过与运算取出对应的颜色
        //        最高2为表示红色
        //        中间两位表示绿色
        //        最低两位表示蓝色
        let red=(hex & 0xFF0000) >> 16
        let green=(hex & 0xFF00) >> 8
        let blue=(hex & 0xFF)
        
        //        转为0~1区间
        let redFloat=Double(red)/255.0
        let greenFloat=Double(green)/255.0
        let blueFloat=Double(blue)/255.0
        
        //        调用UIColor的初始方法
        self.init(red: CGFloat(redFloat), green: CGFloat(greenFloat), blue: CGFloat(blueFloat), alpha: CGFloat(alpha))
    }
}
