//
//  UITextFieldExtend.swift
//  对UITextField的扩展类
//
//  Created by smile on 2019/4/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

extension UITextField{
    
    /// 显示左侧图标
    ///
    /// - Parameter name: <#name description#>
    func showLeftIcon(name:String) {
        //设置图片
        self.leftView=UIImageView(image: UIImage(named: name))
        
        //设置一直显示
        self.leftViewMode=UITextField.ViewMode.always
    }
}
