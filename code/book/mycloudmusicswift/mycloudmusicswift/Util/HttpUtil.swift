//
//  HttpUtil.swift
//  网络请求相关辅助方法
//
//  Created by smile on 2019/5/4.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//JSON解析为对象
import HandyJSON

// 导入网络框架
import Moya

class HttpUtil {
    
    /// 处理网络请求错误
    ///
    /// - Parameters:
    ///   - baseResponse: <#baseResponse description#>
    ///   - error: <#error description#>
    static func handlerRequest(baseResponse:BaseResponse?=nil,error:Swift.Error?=nil) {
        //如果其他位置也用到了这样的错误码
        //可以写到常量
        //这样由于其他位置都没用到
        //所有可以不用写到常量
        print("handlerRequest:\(baseResponse),\(error)")
        
        if let error = error as? MoyaError {
            //error类型是Moya.MoyaError
            switch error {
            case .imageMapping(let response):
                print("图片解析错误")
                print(response)
            case .jsonMapping(let response):
                print("JSON解析错误")
                print(response)
            case .statusCode(let response):
                print("状态码错误处理")
                
                //响应状态码
                let code=response.statusCode
                
                switch code {
                case 401:
                    //调用本地退出方法
                    AppDelegate.shared.logout()
                    
                    //弹出提示
                    ToastUtil.short("登陆信息过期，请重新登陆!")
                case 403:
                    ToastUtil.short("你没有权限访问!")
                case 404:
                    ToastUtil.short("你访问内容不存在!")
                case 500...599:
                    ToastUtil.short("服务器错误,请稍后再试!")
                default:
                    ToastUtil.short("未知错误，请稍后再试！")
                    break
                }
                
            case .stringMapping(let response):
                print("字符串转换错误")
                print(response)
                
                ToastUtil.short("字符串转换错误，请稍后再试！")
            case .underlying(let nsError as NSError, let response):
                print("这里将错误转为NSNSError:\(nsError)")
                
                switch nsError.code {
                case NSURLErrorNotConnectedToInternet:
                    ToastUtil.short("网络好像不好，请稍后再试！")
                case NSURLErrorTimedOut:
                    ToastUtil.short("连接超时，请稍后再试！")
                    
                //TODO 更多错误判断可以在这里添加
                default:
                    ToastUtil.short("未知错误，请稍后再试！")
                }
                
            case .requestMapping:
                print("请求映射错误！")
            case .objectMapping(_, _):
                print("对象解析错误！")
            case .encodableMapping(_):
                print("对象编码错误！")
            case .parameterEncoding(_):
                print("参数编码错误！")
            }
        }else{
            //判断业务是否请求出错
            if let baseResponse=baseResponse{
                if let message=baseResponse.message {
                    //有错误提示
                    ToastUtil.short(message)
                }else{
                    //没有错误描述
                    ToastUtil.short("未知错误,请稍后再试!")
                }
            }else{
                ToastUtil.short("未知错误，请稍后再试！")
            }
        }
        
    }
}
