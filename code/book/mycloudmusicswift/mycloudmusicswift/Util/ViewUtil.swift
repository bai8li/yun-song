//
//  ViewUtil.swift
//  View相关的工具类，包括：圆角，边框
//
//  Created by smile on 2019/4/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ViewUtil {
    
    /// 设置大圆角
    ///
    /// - Parameter view: 要设置的View
    static func showLargeRadius(view:UIView) {
        view.layer.cornerRadius = CGFloat(SIZE_LARGE_RADIUS)
        
        //        裁剪多余的内容
        //        例如：给ImageView设置了圆角
        //        如果不裁剪多余的内容，就不会生效
        view.clipsToBounds=true
    }
    
    /// 设置小圆角
    ///
    /// - Parameter view: 要设置的View
    static func showSmallRadius(view:UIView) {
        view.layer.cornerRadius = CGFloat(SIZE_SMALL_RADIUS)
        
        //        裁剪多余的内容
        //        例如：给ImageView设置了圆角
        //        如果不裁剪多余的内容，就不会生效
        view.clipsToBounds=true
    }
    
    /// 设置圆角
    ///
    /// - Parameters:
    ///   - view: 要设置的View
    ///   - radius: 圆角半径
    static func showRadius(_ view:UIView,_ radius:Float) {
        view.layer.cornerRadius = CGFloat(radius)
        
        //        裁剪多余的内容
        //        例如：给ImageView设置了圆角
        //        如果不裁剪多余的内容，就不会生效
        view.clipsToBounds=true
    }
    
    /// 从ScrollView截图
    ///
    /// - Parameter scrollView: <#scrollView description#>
    /// - Returns: <#return value description#>
    static func captureScrollView(_ scrollView:UIScrollView) -> UIImage {
        //获取绘图上下文
        //下面的代码不用理解
        //他是iOS中绘图相关的
        UIGraphicsBeginImageContextWithOptions(scrollView.contentSize, false, 0.0)
        
        //获取到ScrollView的偏移
        let savedContentOffset = scrollView.contentOffset
        
        //ScrollView的位置
        let savedFrame = scrollView.frame
        
        //重新设置ScrollView的Frame
        scrollView.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        
        //调用渲染方法
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        //然后就可以获取到图片
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        //恢复ScrollView的偏移
        scrollView.contentOffset = savedContentOffset
        
        //恢复ScrollView的Frame
        scrollView.frame = savedFrame
        
        //标记绘图结束
        UIGraphicsEndImageContext()
        
        //返回图片
        return image!
    }
}
