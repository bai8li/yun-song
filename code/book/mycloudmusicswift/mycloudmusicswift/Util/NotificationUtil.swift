//
//  NotificationUtil.swift
//  通知相关工具类
//
//  Created by smile on 2019/4/22.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

import UserNotifications

class NotificationUtil {
    
    /// 消息聊天消息
    ///
    /// - Parameter data: <#data description#>
    static func showMessage(_ data:JMSGMessage) {
        let user=data.fromUser
        
        //获取用户名
        UserManager
            .shared()
            .getUser(StringUtil.removeUserId(user.username)) { (userDetail) in
            //获取用户信息成功
            //获取通知中心
            let center=UNUserNotificationCenter.current()
                
                //创建通知
                let content=UNMutableNotificationContent()
                
                //携带信息
                //用来点击通知做相应的处理
                content.userInfo=[ACTION_KEY:ACTION_MESSAGE,EXTRA_ID:user.username]
                
                //内容格式为：用户名：消息内容
                content.body="\(userDetail.nickname!): \(MessageUtil.getContent(data))"
                
                //默认提示音
                content.sound = UNNotificationSound.default
                
                //创建一个通知请求
                let request=UNNotificationRequest(identifier: "IxueaMyMusic", content: content, trigger: nil)
                
                //显示通知
                center.add(request, withCompletionHandler: { (error) in
                    if let error = error {
                        print("NotificationUtil showMessage error:\(error)")
                    }else{
                        print("NotificationUtil showMessage success")
                    }
                    
                })
        }
    }
    
}
