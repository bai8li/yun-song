//
//  BundleUtil.swift
//  Bundle相关工具类
//
//  Created by smile on 2019/4/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BundleUtil {

    /// app版本
    /// 在项目-targets-general-indenty-version设置
    ///
    /// - Returns: 一般都是3位：例如：2.0.1
    static func appVersion() -> String {
        //获取到info文件字典
        let infoDictionary=Bundle.main.infoDictionary
        
        //取里面的值并转为字符串
        let versionString=infoDictionary!["CFBundleShortVersionString"] as! String
        return versionString
    }
    
    /// app build版本
    /// 在项目-targets-general-indenty-build设置
    ///
    /// - Returns: 一般是整形，例如：201；int更方便判断大小
    static func appBuildVersion() -> Int {
        let infoDictionary=Bundle.main.infoDictionary
        let versionCode=infoDictionary!["CFBundleVersion"] as! String
        let versionCodeInt=Int(versionCode)
        return versionCodeInt!
    }
}
