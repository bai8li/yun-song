//
//  ShareUtil.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class ShareUtil {
    
    /// 分享歌词文本
    ///
    /// - Parameters:
    ///   - data: 歌曲信息
    ///   - lyric: 歌词文本
    static func shareLyricText(_ data:Song,_ lyric:String) {
        //分享字符串：分享歌词：这是歌词。分享%s的单曲《%s》：http://dev-courses-misuc.ixuea.com/songs/%s (来自@我的云音乐)
        //具体的可以根据业务需求更改
        
        //创建分享参数
        let params = NSMutableDictionary()

        //分享的内容
        let shareContent="分享歌词：\(lyric)。分享\(data.singer.nickname!)的单曲《\(data.title!)》：http://dev-courses-misuc.ixuea.com/songs/\(data.id) (来自@我的云音乐)"

        //真实项目中分享的地址
        //一般是音乐的网页
        //但由于我们这里没有实现网页
        //所以这个地址是随便写的
//        let shareURLString="http://dev-courses-misuc.ixuea.com/songs/\(data.id)"

        //设置分享内容
         params.ssdkSetupShareParams(byText: shareContent, images: nil, url: nil, title:"我的云音乐歌词分享" , type: .text)
        
        share(params)
    }
    
    /// 分享图片
    ///
    /// - Parameter data: <#data description#>
    static func shareImage(_ data:UIImage) {
        //创建分享参数
        let params = NSMutableDictionary()
        
        //设置分享内容
        params.ssdkSetupShareParams(byText: nil, images: data, url: nil, title:"" , type: .image)
        
        share(params)
    }
    
    static func share(_ params:NSMutableDictionary) {
        ShareSDK.showShareActionSheet(nil, items: nil, shareParams: params) { (state : SSDKResponseState, platformType : SSDKPlatformType, userdata: [AnyHashable : Any]?, contentEnity : SSDKContentEntity?, error : Error?, end : Bool) in
            handleShareResult(state,error as? NSError)
        }
    }
    
    static func handleShareResult(_ state:SSDKResponseState,_ error:NSError?) {
        //分享回调
        switch state {
        case .success:
            ToastUtil.short("分享成功！")
        default:
            let alert=UIAlertView(title: "分享失败", message: "错误原因：\(error)", delegate: nil, cancelButtonTitle: "确认")
            alert.show()
        }
    }
}
