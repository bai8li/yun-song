//
//  DataUtil.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class DataUtil {
    
    /// 更改是否在播放列表显示
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - isPlayList: <#isPlayList description#>
    static func changePlayListFlag(_ data:[Song],_ playList:Bool) {
        for song in data {
            song.playList=playList
        }
    }
    
    /// 对聊天消息进行处理
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func processMessage(_ data:[JMSGMessage]) -> [JMSGMessage] {
        let result=data.sorted(by: { (obj1, obj2) -> Bool in
            return obj1.timestamp.intValue<obj2.timestamp.intValue
        })
        
        return result
    }
    
    
    /// 将用户根据昵称首字母分组
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func processUser(_ datas:[User]) -> [UserGroup] {
        var results:[UserGroup]=[]
        
        //根据用户昵称计算出拼音
        var users=processUserPinyin(datas)
        
        //按照第一个字母排序
        users=users.sorted(by: { (obj1, obj2) -> Bool in
            return obj1.frist<obj2.frist
        })
        
        //按照拼音首字母的第一个字母分组
        //这些操作都可以使用谓词
        //这里为了简单使用了最普通的方法
        //因为只要明白了原理
        //使用谓词就是语法不同而已
        var lastUser:User?
        
        var group:UserGroup!
        for user in users {
            //按照UserGroup分组
            if lastUser != nil && lastUser!.frist == user.frist{
                //相等
            }else {
                group=UserGroup()
                group.title=user.frist
                group.datas=[]
                results.append(group)
            }
            
            
            group.datas.append(user)
            
            lastUser=user
            
        }
        
        
        //测试数据
//        let fristGroup=UserGroup()
//        fristGroup.title="a"
//        fristGroup.datas=[]
//
//        results.append(fristGroup)
//
//        for i in 0..<50 {
//            let user=User()
//            user.nickname="爱学啊\(i)"
//            user.frist="a"
//            fristGroup.datas.append(user)
//        }
//
//        let secondGroup=UserGroup()
//        secondGroup.title="w"
//        secondGroup.datas=[]
//
//        for i in 0..<50 {
//            let user=User()
//            user.nickname="我的云音乐\(i)"
//            user.frist="w"
//            secondGroup.datas.append(user)
//        }
//
//        results.append(secondGroup)
        
        
        return results
    }
    
    /// 根据用户昵称计算出拼音
    ///
    /// - Parameter datas: <#datas description#>
    /// - Returns: <#return value description#>
    static func processUserPinyin(_ datas:[User]) -> [User] {
        
        //创建拼音格式化输出对象
        // PinyinToneType: none, toneNumber
        // PinyinVCharType: vCharacter, uUnicode, uAndColon
        // PinyinCaseType: lowercased, uppercased, capitalized
        let outputFormat = PinyinOutputFormat(toneType: .none, vCharType: .vCharacter, caseType: .lowercased)
        
        for data in datas{
            //获取全拼
            //分隔符为空字符串
            //也就是没有分隔符
            data.pinyin=data.nickname!.toPinyin(withFormat: outputFormat, separator: "")
            
            //获取拼音首字母
            //例如"爱学啊"
            //结果为：axa
             data.pinyinFrist=data.nickname!.toPinyinAcronym()
            
            //拼音首字母的首字母
            data.frist=data.pinyinFrist.substring(toIndex: 1)
        }
        
        return datas
    }
    
    
    /// 将组标题放到列表中
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func processUserLetter(_ data:[UserGroup]) -> [String] {
        var results:[String]=[]
        
        for userGroup in data {
            results.append(userGroup.title)
        }
        
        //测试数据
//        var results:[String]=[]
//
//        results.append("a")
//        results.append("w")
        
        return results
    }
    
    /// 根据文本过滤用户
    ///
    /// - Parameters:
    ///   - datas: <#datas description#>
    ///   - searchText: <#searchText description#>
    /// - Returns: <#return value description#>
    static func filterUser(_ datas:[UserGroup],_ searchText:String) -> [UserGroup] {
        var results:[UserGroup]=[]
        
        //转为小写
        let searchText=searchText.lowercased()
        
        if searchText.isEmpty {
            //如果是空字符串
            //就直接返回
            return results
        }
        
        var lastUser:User?
        
        var group:UserGroup!
        
        //使用普通的遍历方式
        for userGroup in datas {
            let users = userGroup.datas
            for user in users! {
                //nickanme是否包含搜索的字符串
                //全拼是否包含
                //首字母是否包含

                //如果需要更多的条件可以在加
                //条件越多
                //就更容易搜索到
                //但结果就越多
                if user.nickname!.containsIgnoringCase(find: searchText)
                  || user.pinyin.containsIgnoringCase(find: searchText)
                  || user.pinyinFrist.containsIgnoringCase(find: searchText){

                    //搜索结果
                    //还是要按照UserGroup分组
                    if lastUser != nil && lastUser!.frist == user.frist{
                        //相等
                    }else {
                        group=UserGroup()
                        group.title=user.frist
                        group.datas=[]
                        results.append(group)
                    }


                    group.datas.append(user)

                    lastUser=user

                }
            }
        }
        
        return results
    }
}
