//
//  ImageUtil.swift
//  实现图片工具类
//
//  Created by smile on 2019/4/12.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class ImageUtil {
    
    /// 显示网络相对图片地址
    ///
    /// - Parameter uri: 图片相对地址
    static func show(_ imageView:UIImageView,_ uri:String) {
        //将图片路径转为绝对地址
        let uri=ResourceUtil.resourceUri(uri)
        
//        //有图片才设置
//        let url=URL(string: uri)
//
//        //占位图片
//        //就是图片还没有加载回来前这段时间
//        //显示的图片
//        let placeHolderImage=UIImage(named: "PlaceHolder")
//
//        //加载图片
//        imageView.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
        
        showFull(imageView, uri)
    }
    
    /// 显示网络绝对图片地址
    ///
    /// - Parameter uri: 图片绝对地址
    static func showFull(_ imageView:UIImageView,_ uri:String) {
        //有图片才设置
        let url=URL(string: uri)
        
        //占位图片
        //就是图片还没有加载回来前这段时间
        //显示的图片
        let placeHolderImage=UIImage(named: "PlaceHolder")
        
        //加载图片
        imageView.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
    }
    
    /// 显示头像
    ///
    /// - Parameter uri: 相对地址或者绝对地址
    static func showAvatar(_ imageView:UIImageView,_ uri:String?) {
        if let avatar = uri {
            //只有有头像才需要处理
            if avatar.hasPrefix("http") {
                //绝对地址

                showFull(imageView, avatar)
            }else{
                //相对地址
                show(imageView, avatar)
            }
            
        }
    }
    
    /// 保存图片到相册
    ///
    /// - Parameter data: <#data description#>
    static func saveImage(_ data:UIImage) {
        UIImageWriteToSavedPhotosAlbum(data, self, #selector(image(image:didFinishSavingWithError:contextInfo:)),nil)
    }
    
    
    /// 保存图片到相册回调
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - error: <#error description#>
    ///   - contextInfo: <#contextInfo description#>
//    @objc static func saveImageCallback(image:UIImage,didFinishSavingWithError error:NSError,contextInfo:AnyObject) {
//        var message=""
//
//        if error==nil {
//            message = "保存成功！"
//
//        } else {
//             message = "保存失败，请稍后再试！"
//        }
//
//        ToastUtil.short(message)
//    }
    
     @objc static func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer){
        var message=""
        
        if error==nil {
            message = "保存成功！"
            
        } else {
            message = "保存失败，请稍后再试！"
        }
        
        ToastUtil.short(message)
    }
    
}
