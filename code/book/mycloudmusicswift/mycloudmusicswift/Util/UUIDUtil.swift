//
//  UUIDUtil.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class UUIDUtil {
    
    /// 生成UUID
    /// 例如：3A32E44543AD42F5B584531ED040C62A
    ///
    /// - Returns: <#return value description#>
    static func getUUID() -> String {
        //删除横线
        return NSUUID().uuidString.replacingOccurrences(of: "-", with: "")
    }
}
