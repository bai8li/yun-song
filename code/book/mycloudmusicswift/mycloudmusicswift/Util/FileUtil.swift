//
//  FileUtil.swift
//  文件相关工具类
//
//  Created by smile on 2019/4/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class FileUtil {
 
    /// 文件大小格式化
    ///
    /// - Returns: 返回值实例：1.22M
    static func formatFileSize(_ data:Int) -> String {
        if data>0 {
            let size=Double(data)
            
            let kiloByte=size/1024
            if kiloByte<1 && kiloByte>0 {
                return String(format: "%.2fByte", size)
            }
            
            let megaByte=kiloByte/1024
            if megaByte<1 {
                return String(format: "%.2fK", kiloByte)
            }
            
            let gigaByte=megaByte/1024
            if gigaByte<1 {
                return String(format: "%.2fM", megaByte)
            }
            
            let teraByte=gigaByte/1024
            if teraByte<1 {
                return String(format: "%.2fG", gigaByte)
            }
            
            
            return String(format: "%.2fT", teraByte)
        }
        
        return "0K"
    }
}
