//
//  AnalysisUtil.swift
//  统计相关方法
//
//  Created by smile on 2019/5/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class AnalysisUtil {
    
    /// 登陆事件
    ///
    /// - Parameters:
    ///   - success: <#success description#>
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - qq_id: qqId；为加密；所以如果泄漏了，别人就可以登陆；真实项目传递到第三方统计的数据最好先加密
    ///   - weibo_id: <#weibo_id description#>
    static func onLogin(success:Bool,phone:String?=nil,email:String?=nil,qq_id:String?=nil,weibo_id:String?=nil) {
        
        let event = JANALYTICSLoginEvent()
        
        //是否登陆成功
        event.success = success
       
        // 返回值类似一个数组
        let info=getMethod()
        
        event.method = info.0
        
        //请勿传递密码
        event.extra = info.1
        
        //记录事件
        JANALYTICSService.eventRecord(event)
    }
    
    /// 注册事件
    ///
    /// - Parameters:
    ///   - avatar: <#avatar description#>
    ///   - nickname: <#nickname description#>
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - qq_id: <#qq_id description#>
    ///   - weibo_id: <#weibo_id description#>
    static func onRegister(success:Bool,avatar:String?=nil,nickname:String,phone:String,email:String,qq_id:String?=nil,weibo_id:String?=nil) {
        
        let event = JANALYTICSRegisterEvent()
        
        //是否注册成功
        event.success = success
        
        // 返回值类似一个数组
        let info=getMethod()
        
        event.method = info.0
        
        //请勿传递密码
        event.extra = info.1
        
        //记录事件
        JANALYTICSService.eventRecord(event)
    }
    
    /// 根据信息计算登陆/注册方式
    ///
    /// - Parameters:
    ///   - phone: <#phone description#>
    ///   - email: <#email description#>
    ///   - qq_id: <#qq_id description#>
    ///   - weibo_id: <#weibo_id description#>
    /// - Returns: <#return value description#>
    static func getMethod(phone:String?=nil,email:String?=nil,qq_id:String?=nil,weibo_id:String?=nil) -> (String,[String:String]) {
        
        //扩展信息
        var extra:[String:String]=[:]
        
        var loginType=""
        
        //判断登陆方式
        if let phone = phone{
            //手机号
            loginType="phone"
            
            extra["phone"]=phone
        }else if let email = email{
            //邮箱
            loginType="email"
            
            extra["email"]=email
            
        }else if let qq_id = qq_id{
            //QQ
            loginType="qq"
            
            extra["qq"]=qq_id
        }else if let weibo_id = weibo_id{
            //微博
            loginType="weibo"
            
            extra["weibo"]=weibo_id
        }
        
        return (loginType,extra)
    }
    
    /// 点击了跳过广告
    ///
    /// - Parameter userId: <#userId description#>
    static func onSkipAd(_ userId:String) {
        let event=JANALYTICSCountEvent()
        
        //自定义事件名
        event.eventID="SkipAd"
        
        //传递了用户Id
        event.extra=["userId":userId]
        
        //记录事件
        JANALYTICSService.eventRecord(event)
    }
    
    /// 购买事件
    ///
    /// - Parameters:
    ///   - success: <#success description#>
    ///   - data: <#data description#>
    static func onPurchase(_ success:Bool,_ data:Order) {
        let event = JANALYTICSPurchaseEvent()
        
        //是否购买成功
        event.success = false
        
        //商品价格
        //            event.price = CGFloat(Float(self.data.price)!)
        
        event.price = 500.99
        
        //商品名称
        event.goodsName = data.book.title
        
        //类型
        //一般有多种商品
        //例如：有课程，有电子书
        event.goodsType = "book"
        
        //商品Id
        event.goodsID = data.book.id
        
        //人民币
        event.currency = .currencyCNY
        
        //数量
        //由于我们这里没有数据
        //所有随便写一个值
        event.quantity = 1228
        
        //自定义扩展信息
        //            event.extra = []
        
        JANALYTICSService.eventRecord(event)
    }
}
