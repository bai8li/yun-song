//
//  PreferenceUtil.swift
//  偏好设置；用来保存，是否登陆了，用户Id，是否显示引导界面等配置
//
//  Created by smile on 2019/4/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class PreferenceUtil {
    /// 判断是否要显示引导界面
    ///
    /// - Returns: return bool true：表示显示引导界面，false：表示不显示
    static func isShowGuide() -> Bool {
//        获取KEY_GUIDE的值
        let isShowGuide=UserDefaults.standard.bool(forKey: PreferenceUtil.KEY_GUIDE)
//        如果不等于true，表示显示引导界面

        if !isShowGuide {
//            如果isShowGuide为nil，就是第一次运行
//            所以返回true，表示要显示引导界面
//            如果存储的是true，就不会来到这里
            return true
        }
        
//       其他情况下，返回false
        return false
    }
    
    
    /// 设置显示了引导界面
    ///
    /// - Parameter isShowGuide: 是否显示了引导界面
    static func setShowGuide(isShowGuide:Bool) {
        //保存值
        UserDefaults.standard.set(isShowGuide, forKey: KEY_GUIDE)
    }
   
    /// 保存用户Id
    ///
    /// - Parameter data: <#data description#>
    static func setUserId(_ data:String) {
        UserDefaults.standard.set(data, forKey: KEY_USER_ID)
    }
    
    
    /// 获取用户Id
    static func userId()->String? {
        return UserDefaults.standard.string(forKey: KEY_USER_ID)
    }
    
    /// 保存用户会话标识
    ///
    /// - Parameter data: <#data description#>
    static func setUserToken(_ data:String) {
        UserDefaults.standard.set(data, forKey: KEY_USER_TOKEN)
    }
    
    
    /// 获取用户会话标识
    ///
    /// - Parameter data: <#data description#>
    static func userToken() ->String {
        return UserDefaults.standard.string(forKey: KEY_USER_TOKEN)!
    }
    
    /// 是否登陆
    ///
    /// - Returns: <#return value description#>
    static func isLogin() -> Bool {
        //直接判断是否有UserId就行了
        if let userId = userId() {
            return true
        }
        
        return false
    }
    
    
    /// 退出登陆
    static func logout()  {
        //清除用户Id
        UserDefaults.standard.removeObject(forKey: KEY_USER_ID)
        
        //清除用户登陆标识
        UserDefaults.standard.removeObject(forKey: KEY_USER_TOKEN)
    }
    
    /// 获取最后播放的音乐Id
    ///
    /// - Returns: <#return value description#>
    static func lastPlaySongId() -> String? {
        return UserDefaults.standard.string(forKey: KEY_LAST_SONG_ID)
    }
    
    /// 设置当前播放音乐的Id
    ///
    /// - Parameter data: <#data description#>
    static func setLastPlaySongId(_ data:String) {
        UserDefaults.standard.set(data, forKey: KEY_LAST_SONG_ID)
    }
    
    static func setLastSongProgress(_ data:Double) {
        UserDefaults.standard.set(data, forKey: KEY_LAST_SONG_PROGRESS)
    }
    
    static func lastSongProgress() -> Double {
        let value = UserDefaults.standard.double(forKey: KEY_LAST_SONG_PROGRESS)
        
        return value
    }
    
    static func isRemoveHeadsetStop() -> Bool {
        if let result=UserDefaults.standard.bool(forKey: KEY_REMOVE_HEADSET_STOP) as? Bool {
            print("PreferenceUtil isRemoveHeadsetStop found:\(result)")
            return result
        }
        
        print("PreferenceUtil isRemoveHeadsetStop not found")
        return true
    }
    
    static func setRemoveHeadsetStop(_ isRemove:Bool) {
        UserDefaults.standard.set(isRemove, forKey: KEY_REMOVE_HEADSET_STOP)
    }
    
    /// 是否是夜间模式
    ///
    /// - Returns: <#return value description#>
    static func isNight() -> Bool {
        //默认为false
        //正符合我们的需求
        //所以不用特殊处理
        return UserDefaults.standard.bool(forKey: KEY_NIGHT)
    }
    
    /// 设置是否是夜间模式
    ///
    /// - Parameter isNight: <#isNight description#>
    static func setNight(_ isNight:Bool) {
        UserDefaults.standard.set(isNight, forKey: KEY_NIGHT)
    }
    

    //由于常量只是内部使用，可以定义到最下面
    //同时改为私有的
    //    是否显示引导界面常量字符串
    private static let KEY_GUIDE = "KEY_GUIDE"
    
    
    /// 用户Id常量
    private static let KEY_USER_ID = "KEY_USER_ID"
    
    /// 用户Token常量
    private static let KEY_USER_TOKEN = "KEY_USER_TOKEN"
    
    /// 最后播放的这首音乐Id
    private static let KEY_LAST_SONG_ID = "KEY_LAST_SONG_ID"
    
    /// 最后播放的这首进度
    private static let KEY_LAST_SONG_PROGRESS = "KEY_LAST_SONG_PROGRESS"
    
    /// 拔掉耳机停止播放
    private static let KEY_REMOVE_HEADSET_STOP = "KEY_REMOVE_HEADSET_STOP"
    
    /// 夜间模式
    private static let KEY_NIGHT = "KEY_NIGHT"
    
}

