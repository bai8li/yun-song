//
//  LRCLyricsParser.swift
//  LRC歌词解析器
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class LRCLyricsParser {
    
    static func parse(_ style:LyricType,_ data:String) -> Lyric {
        // 创建歌词解析结果对象
        let result=Lyric()

        result.datas=[]

        //使用\n拆分歌词
        let strings=data.components(separatedBy: "\n")

        for line in strings {
            //解析每一行
            if let lyricLine=parseLine(line){
                result.datas.append(lyricLine)
            }
        }

        return result;
    }
    
    
    /// 解析每一行歌词
    /// 例如：[00:00.300]爱的代价 - 李宗盛
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func parseLine(_ data:String) -> LyricLine? {
        
        if data.hasPrefix("[0") {
            let lyricLine=LyricLine()
            
            //歌词开始，过滤了前面的元数据
            //移除开始位置[字符串
            let data = data.substring(fromIndex: 1)
            
            //]拆分
            let comands=data.components(separatedBy: "]")
     
            //开始时间
            lyricLine.startTime=TimeUtil.parseToInt(comands[0])
            
            //歌词
            lyricLine.data=comands[1]
            
            return lyricLine
        }
        
        return nil
    }
}
