//
//  LyricParser.swift
//  歌词解析器
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

class LyricParser {
    
    /// 解析歌词
    ///
    /// - Parameters:
    ///   - style: 歌词类型
    ///   - data: 歌词内容
    /// - Returns: 解析后的歌词对象
    static func parse(_ style:LyricType,_ data:String) -> Lyric {
        switch style {
        case .ksc:
            return KSCLyricsParser.parse(style,data)
        default:
            //默认当前LRC处理
            return LRCLyricsParser.parse(style,data)
        }
    }
}
