//
//  File.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class IxueaOSSUtil {
    private static var instance:OSSClient?
    
    static func shared() -> OSSClient {
        if instance==nil {
            //推荐使用OSSAuthCredentialsProvider。token过期可以及时更新
            //我们这里为了课程难度，就直接使用AK和SK
            let credentialProvider = OSSPlainTextAKSKPairCredentialProvider(plainTextAccessKey: ALIYUN_OSS_AK, secretKey: ALIYUN_OSS_SK)
            
            //bucket地址；认证信息
            //格式化为去除网址后面的格式化符
            instance=OSSClient(endpoint: RESOURCE_ENDPOINT, credentialProvider: credentialProvider)
        }
        return instance!
    }

}
