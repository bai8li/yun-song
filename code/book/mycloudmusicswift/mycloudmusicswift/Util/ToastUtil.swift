//
//  ToastUtil.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//


import UIKit

class ToastUtil {
    private static var hud:MBProgressHUD?
    
    /// 显示一个短时间（1秒）的自动提示框
    ///
    /// - Parameter message: 要提示的内容
    static func short(_ message:String) {
        //创建MBProgressHUD
        
        //通过这种方式创建
        //必须要有View
        
        //这里使用的是根控制的View
        //所以理论上只要有界面
        //就一直存储
        //也能实现全局使用
                let hud=MBProgressHUD.showAdded(to:AppDelegate.shared.window!.rootViewController!.view, animated: true)
        
        //这种方式可以全局使用
        //但官方已经不推荐使用
        
        //这种方式在Swift中没效果
//        let hud = MBProgressHUD(window: UIApplication.shared.keyWindow!)
        
        
        //只显示文字
        hud.mode = MBProgressHUDMode.text
        
        //小矩形的背景色
        hud.bezelView.color = UIColor.black
        
        //细节文字
        hud.detailsLabel.text = message
        
        //细节文字颜色
        hud.detailsLabel.textColor=UIColor.white
        
        //自动隐藏
        hud.hide(animated: true, afterDelay: 1)
    }
    
    /// 显示一个加载对话框
    ///
    /// - Parameter message: <#message description#>
    static func showLoading(_ message:String){
        hud=MBProgressHUD.showAdded(to:AppDelegate.shared.window!.rootViewController!.view, animated: true)
        
        //设置菊花颜色
        hud!.activityIndicatorColor=UIColor.white
        
        //设置背景模糊
        hud!.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor
        
        //设置背景为半透明效果
        //UIColor(white：0.0：表示创建一个黑色
        //0.1表示黑色有透明效果；0表示完全透明；1表示完全黑色
        hud!.backgroundView.color=UIColor(white: 0.0, alpha: 0.1)
        
        //小矩形的背景色
        hud!.bezelView.color = UIColor.black
        
        //细节文字
        hud!.detailsLabel.text = message
        
        //细节文字颜色
        hud!.detailsLabel.textColor=UIColor.white
        
        //显示
        hud!.show(animated: true)
        
    }
    
    /// 显示一个加载对话框，使用默认文字
    static func showLoading() {
        showLoading("拼命加载中.")
    }
    
    /// 隐藏加载对话框
    static func hideLoading() {
        if hud != nil {
            hud!.hide(animated: true)
            hud=nil
        }
    }
    
}
