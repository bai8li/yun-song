//
//  MessageUtil.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/22.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class MessageUtil {
    static func getContent(_ data:JMSGMessage) -> String {
        if JMSGContentType.image == data.contentType {
            //图片消息
            return "[图片]"
        }else if JMSGContentType.text == data.contentType{
            //文本消息
            return (data.content as! JMSGTextContent).text
        }
        
        return "其他消息"
    }
    
    /// 获取全局消息未读数
    /// 也可以在用到的地方
    /// 直接调用SDK接口
    /// 这里还写到工具类的好处是
    /// 使用的时候不用在拆包等一些列判断
    ///
    /// - Returns: <#return value description#>
    static func getUnreadMessageCount() -> Int {
        let unreadMessageCountInt=JMSGConversation.getAllUnreadCount()
        if unreadMessageCountInt != nil && unreadMessageCountInt.intValue>0 {
            return unreadMessageCountInt.intValue
        }
        return 0
    }
}
