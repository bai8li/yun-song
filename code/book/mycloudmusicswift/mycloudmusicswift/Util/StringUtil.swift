//
//  StringUtil.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/17.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class StringUtil {

    /// 点击回调方法别名
    public typealias onTagClick = (_ containerView:UIView, _ text:NSAttributedString,_ range:NSRange,_ rect:CGRect) -> Void
    
    
    /// 处理内容
    /// 对关键内容高亮
    /// 并添加点击方法
    ///
    /// - Parameters:
    ///   - data: 内容
    ///   - result: 富文本字符串
    ///   - onMentionClick: mention点击回调方法
    ///   - onHashTagClick: hashTag点击回调方法
    /// - Returns: 处理完成后的富文本
    static func processContent(_ data:String,_ result:NSMutableAttributedString, _ onMentionClick:@escaping onTagClick,_ onHashTagClick:@escaping onTagClick) -> NSMutableAttributedString {
        
        //设置字体大小
        result.yy_font = UIFont.systemFont(ofSize: 17)
        
        //查找@
        let mentionResults = RegUtil.findMentions(data)
        
        //遍历进行处理
        for tag in mentionResults {
            StringUtil.processInner(result,tag.range,onMentionClick)
        }
        
        //匹配话题
        let hashTagResults = RegUtil.findHashTag(data)
        
        for tag in hashTagResults {
            StringUtil.processInner(result,tag.range,onHashTagClick)
        }
        
        return result
    }
    
    /// 移除字符串中首的@
    /// 移除首尾的#
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func removePlaceholderString(_ data:String) -> String {
        if data.hasPrefix("@") {
            //从1开始截取到末尾
            return data.substring(fromIndex: 1)
        }else if data.hasPrefix("#") {
            //截取1~最后一个字符串
            return data[1 ..< data.count-1]
        }
        
        return data
    }
    
    /// 截取用户点击的这段文本
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - range: <#range description#>
    /// - Returns: <#return value description#>
    static func processClickText(_ data:String,_ range:NSRange) -> String {
        //截取出用户点击的这段文字
        var clickText = data[range.location..<range.location+range.length]

        //移除字符串前后的标识符
        clickText=StringUtil.removePlaceholderString(clickText)
        
        return clickText
    }
    
    /// 只是对文本进行高亮
    /// 不添加点击事件
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func processHighlight(_ data:String) ->  NSMutableAttributedString {
        let result=NSMutableAttributedString(string: data)
        
        //设置字体大小
        result.yy_font = UIFont.systemFont(ofSize: 17)
        
        //查找@
        let mentionResults = RegUtil.findMentions(data)
        
        //遍历进行处理
        for tag in mentionResults {
            processHighlightInner(result,tag.range)
        }
        
        //匹配话题
        let hashTagResults = RegUtil.findHashTag(data)
        
        for tag in hashTagResults {
            StringUtil.processHighlightInner(result,tag.range)
        }
        
        return result
    }
    
    /// 内部高亮文本方法
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - range: <#range description#>
    private static func processHighlightInner(_ data:NSMutableAttributedString,_ range:NSRange) {
        //设置颜色
                data.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hex: 0x3385ff), range: range)
    }
    
    /// 内部处理字符串点击方法
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - range: <#range description#>
    ///   - onTagClick: <#onTagClick description#>
    private static func processInner(_ data:NSMutableAttributedString,_ range:NSRange,_ onTagClick:@escaping onTagClick) {
        
        //使用yyText框架提供的方法设置富文本
        data.yy_setTextHighlight(range, color: UIColor(hex: 0x3385ff), backgroundColor: nil) { (containerView, text, range, rect) in
            
            //Tag点击回调
            print("StringUtil processInner click:\(text),\(range)")
            
            //回调传递过来的方法
            onTagClick(containerView,text,range,rect)
        }

    }
    
    /// 给用Id添加填充字符
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func processUserId(_ data:String) -> String {
        return "100\(data)"
    }
    
    /// 将填充字符移除
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func removeUserId(_ data:String) -> String {
        return data.substring(fromIndex: 3)
    }
    
}
