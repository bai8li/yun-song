//
//  TimeUtil.swift
//  mycloudmusicswift
//
//  Created by smile on 2019/4/14.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class TimeUtil {
    
    /// <#Description#>
    ///
    /// - Parameter value: <#value description#>
    /// - Returns: <#return value description#>
    static func formatTime1(_ value:Float) -> String {
        let minute=Int(value/60)
        let second=Int(value)%60
        return String(format: "%02d:%02d", minute,second)
    }
    
    static func format1(_ data:String) -> String {
        //先将字符串转为NSDate
        let date=dateFromISO8801String(data)
        
        //然后在格式化App使用的格式
        return format1(date)
    }
    
    static func format3(_ data:String) -> String {
        //先将字符串转为NSDate
        let date=dateFromISO8801String(data)
        
        let dateFormatter=DateFormatter.init()
        dateFormatter.dateFormat=DATE_TIME_FORMAT
        
        return dateFormatter.string(from: date)
    }
    
    static func formatYMD(_ data:String) -> String {
        //先将字符串转为NSDate
        let date=dateFromISO8801String(data)
        
        let dateFormatter=DateFormatter.init()
        dateFormatter.dateFormat=DATE_TIME_FORMAT_YMD
        
        return dateFormatter.string(from: date)
    }
    
    static func formatYMD(_ date:Date) -> String {

        let dateFormatter=DateFormatter.init()
        dateFormatter.dateFormat=DATE_TIME_FORMAT_YMD
        
        return dateFormatter.string(from: date)
    }
    
    static func formatYMDHHMMSSSSS(_ date:Date) -> String {
        //先将字符串转为NSDate
        
        let dateFormatter=DateFormatter.init()
        dateFormatter.dateFormat=DATE_TIME_FORMAT_YMD_HHMMSS_SSS
        
        return dateFormatter.string(from: date)
    }
    
    static func dateFromISO8801String(_ data:String) -> Date {
        let dateFormatter=DateFormatter.init()
        dateFormatter.timeZone=TimeZone(identifier: "UTC")
        dateFormatter.dateFormat=ISO8601_DATE_TIME_FORMAT
        return dateFormatter.date(from: data)!

    }

    static func format1(_ date:Date) -> String {
        //内容的时间
        let createdAt=date

        //    当前时间
        let now = Date.init()

        // 日历对象 （方便比较两个日期之间的差距）
        let calendar = Calendar.current

        // NSCalendar.Unit 枚举代表想获得哪些值
        let unitFlags:Set<Calendar.Component>=[Calendar.Component.year, Calendar.Component.month,Calendar.Component.weekOfMonth, Calendar.Component.day, Calendar.Component.hour, Calendar.Component.minute, Calendar.Component.second]

        // 计算两个日期之间的差值
        let cmps = calendar.dateComponents(unitFlags, from: createdAt, to: now)

        let result:String!
        if (cmps.second!<=0) {
            result="现在"
        }else if (cmps.second!>0 && cmps.minute!<=0  ) {
            result="\(cmps.second!)秒前"
        }else if (cmps.minute!>0 && cmps.hour!<=0  ) {
            result="\(cmps.minute!)分钟前"
        }else if (cmps.hour!>0 && cmps.day!<=0  ) {
            result="\(cmps.hour!)小时前"
        }else if (cmps.day!>0 && cmps.weekOfMonth!<=0  ) {
            result="\(cmps.day!)天前"
        }else{
            result=format2(createdAt)
        }
        return result;
    }
    
    static func format2(_ date:Date) -> String {
        //实例化一个NSDateFormatter对象
        let dateFormatter = DateFormatter.init()
        
        //设定时间格式,这里可以设置成自己需要的格式
        dateFormatter.dateFormat=LOCAL_DATE_TIME_FROMAT
        
        //格式化时间
        return dateFormatter.string(from: date)
    }

    /// <#Description#>
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    static func parseToInt(_ data:String) -> Int {
        //将:替换为.
        let value=data.replacingOccurrences(of: ":", with: ".")
        
        //.拆分
        let strings=value.components(separatedBy: ".")
        
        let m=Int(strings[0])!
        let s=Int(strings[1])!
        let ms=Int(strings[2])!
        
        return (m*60+s)*1000+ms
    }
}
