//
//  Constants.swift
//  常量类，主要用来保存项目中用到了字符串，颜色，尺寸，网络请求地址
//
//  Created by smile on 2019/4/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

/// 是否是调试模式
let DEBUG=true

// MARK: - 开发环境
/// 网络接口地址
let ENDPOINT="http://dev-my-cloud-music-api-rails.ixuea.com"

/// 资源接口地址
let RESOURCE_ENDPOINT="http://dev-courses-misuc.ixuea.com"

// MARK:- 阿里云OSS配置
///企业项目中也应该区分环境
//AK
let ALIYUN_OSS_AK = "LTAIvvrRFHYlwoEj"

//SK
let ALIYUN_OSS_SK = "xDpTSeBM0PqDUWtlVPQfTfMb97CbPD"

//bucket name
//可以简单理解为上传到那个硬盘（不是目录）
//企业项目中也应该像上面区分环境
let ALIYUN_OSS_BUCKET_NAME = "dev-courses-misuc"

//MARK: - 正式环境
///// 网络接口地址
//let ENDPOINT="http://my-cloud-music-api-rails.ixuea.com"
//
///// 资源接口地址
//let RESOURCE_ENDPOINT="http://courses-misuc.ixuea.com"


// MARK: - 尺寸
///立即体验按钮宽度
let SIZE_BUTTON_ENTER_WIDTH = 130.0

///立即体验按钮高度
let SIZE_BUTTON_ENTER_HEIGHT = 44.0

///大圆角
let SIZE_LARGE_RADIUS=22

///小圆角
let SIZE_SMALL_RADIUS=5

///默认边框
let SIZE_BORDER=1

///评论界面行高
let SIZE_COMMENT_LINE_HEIGHT:CGFloat=27

//小分割线
//let SIZE_SMALL_DIVIDER = 0.5

//分割线
//let SIZE_DIVIDER = 5

///大分割线
let SIZE_LARGE_DIVIDER:CGFloat = 10

///发现界面，title Item高度
let SIZE_TITLE_HEIGHT:CGFloat = 40

///发现界面，Header高度
let SIZE_DISCOVERY_HEADER_HEIGHT:CGFloat = 270

///唱片每16毫秒旋转的弧度
let SIZE_ROTATE:CGFloat = 0.004021238596595

///头部高度
let SIZE_HEADER_HEIGHT = 270

///悬浮(指示器)高度
let SIZE_INDICATOR_HEIGHT = 44

/// 标题Cell高度
/// 用在评论列表，用户详情歌单列表
let SIZE_TITLE_CELL_HEIGHT:CGFloat = 75

// MARK: - 颜色
///全局主色调
///PRIMARY后缀参考了Android中颜色命名规则
let COLOR_PRIMARY = 0xdd0000

/// 深黑字符串
/// 值和xcode不一样
/// 他的值和黑色一样
let COLOR_DARK_BLACK=0x232323

///灰色
/// COLOR_LIGHT_GREY这样的命名规则参考了Xcode颜色命名
let COLOR_LIGHT_GREY = 0xaaaaaa

///分享歌词选中颜色
let COLOR_SHARE_LYRIC_SELECTED = 0x252424

/// 评论中可点击的文本颜色
let COLOR_CLICK = 0x3385ff

// MARK: - 颜色字符串（主要用来实现夜间模式）
/// 白色字符串
let COLOR_STRING_WHITE="#ffffff"

/// TableView分组颜色字符串
let COLOR_STRING_GROUP_TABLE_VIEW_BACKGROUND="#EFEFF4"

/// 灰文本颜色字符串
let COLOR_STRING_LIGHT_GREY="#AAAAAA"

/// 发现页面搜索按钮夜间模式下背景颜色字符串
let COLOR_STRING_SEARCH_BACKGROUND="#2E2E2E"

/// 深黑字符串
/// 值和xcode不一样
/// 他的值和黑色一样
let COLOR_STRING_DARK_BLACK="#232323"

/// 黑色字符串
let COLOR_STRING_BLACK="#000000"

/// 主色调字符串
let COLOR_STRING_PRIMARY = "#dd0000"

/// 主色调深色字符串
/// 用来实现夜间模式
/// 按钮按下
let COLOR_STRING_DARK_PRIMARY = "#7B2222"

// MARK: - 图片
//占位图
let IMAGE_PLACE_HOLDER = "PlaceHolder"

// MARK: - 正则表达式

/// 手机号
/// 移动：134 135 136 137 138 139 147 150 151 152 157 158 159 178 182 183 184 187 188 198
/// 联通：130 131 132 145 155 156 166 171 175 176 185 186
/// 电信：133 149 153 173 177 180 181 189 199
/// 虚拟运营商: 170
let REGX_PHONE="^(0|86|17951)?(13[0-9]|15[012356789]|16[6]|19[89]]|17[01345678]|18[0-9]|14[579])[0-9]{8}$"

//邮箱正则表达式
let REGX_EMAIL="^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$"

//常量字符串
//广告点击后发送的事件
let AD_CLICK = "AD_CLICK"

/// Cell复用字符串
let CELL="Cell"

// MARK: - 日期时间时间
/// ISO8601日期格式
let ISO8601_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSzzz"

/// 日期格式
let DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"

/// YMD日期格式
let DATE_TIME_FORMAT_YMD = "yyyy-MM-dd"

/// YMD HHmmssSSS日期格式
let DATE_TIME_FORMAT_YMD_HHMMSS_SSS = "yyyy-MM-dd HH:mm:ss.SSS"

/// 年月日 时分
let LOCAL_DATE_TIME_FROMAT = "yyyy-MM-dd HH:mm"


// MARK: - 事件名称
/// 选择了话题
let SELECT_TOPIC = "SELECT_TOPIC"

/// 选择了好友
let SELECT_FRIEND = "SELECT_FRIEND"

/// 发布了动态
let PUBLISH_FEED = "PUBLISH_FEED"

///收到消息了
let ON_MESSAGE = "ON_MESSAGE"

///更新了用户资料
let ON_UPDATE_USER_INFO = "ON_UPDATE_USER_INFO"

//通知，用来点击通知，判断动作，跳转到聊天界面
let ACTION_MESSAGE = "ACTION_MESSAGE"

//通知中的动作
let ACTION_KEY = "ACTION_KEY"

//通知中携带Id
let EXTRA_ID = "EXTRA_ID"

// QQ登录
let PLATFORM_QQ=20

// 微信
let PLATFORM_WECHAT=30

// 微博
let PLATFORM_WEIBO=40

//消息数可能更改了通知
//可以细化
//只有消息数更改了
//在通知
let ON_MESSAGE_COUNT_CHANGED = "ON_MESSAGE_COUNT_CHANGED"

/// 创建歌单成功通知
let ON_SHEET_CREATED="ON_SHEET_CREATED"

/// 要关闭播放控制器
let CLOSE_PLAY_PAGE="CLOSE_PLAY_PAGE"

/// 点击了搜索
let ON_SEARCH_CLICK="ON_SEARCH_CLICK"

// MARK: - 支付宝回调
let ON_ALIPAY_CALLBACK = "ON_ALIPAY_CALLBACK"

/// 支付宝支付回调
/// 不能和其他应用有重复
/// 用于支付宝客户端回调我们应用
let ALIPAY_CALLBACK_SCHEME = "ixueacourseswift"

/// 支付成功通知
let ON_PAY_SUCCESS="ON_PAY_SUCCESS"

/// 下载状态通知
let DOWNLOAD_STATUS_CHANGED="DOWNLOAD_STATUS_CHANGED"

//聊天界面每次拉取历史数量
let MSSSAGE_COUNT:Int32 = 20

// MARK: - 融云IM
///融云IM配置
let RONG_IM_APP_KEY = "cpj2xarlct12n"

// MARK: - 极光
//极光App key
let JIGUANG_APP_KEY="ba3f08e44440a942db10b46e"

let JIGUANG_CHANNEL="default"

// MARK: - 腾讯Bugly
//App key
let BUGLY_APP_KEY="d963027715"

// MARK: - QQ第三方登陆，分享
let QQ_APP_KEY="101481482"
let QQ_APP_SECRET="b4304961e7299dde6b7b6964bfbb4a7f"

// MARK: - 微博第三方登陆，分享
let WEIBO_APP_KEY="1638318172"
let WEIBO_APP_SECRET="5e265999bb4eb7c3c1eed34d658e0b0e"
let WEIBO_REDIRECT_URI="http://www.ixuea.com"

// MARK: - 高德地图
/// 高德地图
let AMAP_KEY="b118833fa0ed59f545a571a1d7c620af"

/// 二维码地址
/// 因为我们没有做网页所以随便写一个地址
/// 真实项目中可能会使用主页，或者APP下载界面
let QRCODE_URL="\(ENDPOINT)/v1/monitors/version?u="

//当前应用的AppStore地址
//用来点击版本跳转到AppStore
//由于当前应用没有上传到应用商店
//所有随便选择一个地址
//这里就用节奏大师
let MY_APPSTORE_URL = "itms-apps://itunes.apple.com/cn/app/jie-zou-da-shi/id493901993?mt=8"

/// 最热
let ORDER_HOT=10

//最新
let ORDER_NEW=0

//AES算法
let AES_KEY="wqfrwOSH*gN%I2v6"
let AES_IV="VO*1sxQO5nDkcMyj"
