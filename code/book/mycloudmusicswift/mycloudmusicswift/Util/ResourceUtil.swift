//
//  ResourceUtil.swift
//  资源工具类
//
//  Created by smile on 2019/4/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class ResourceUtil {
    
    
    /// 将相对径资源转为绝对路径
    ///
    /// - Parameter uri: <#uri description#>
    /// - Returns: <#return value description#>
    static func resourceUri(_ uri:String) -> String {
        return "\(RESOURCE_ENDPOINT)/\(uri)"
    }
    
}
