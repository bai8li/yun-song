//
//  MeButton.swift
//  我的界面按钮模型
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ButtonInfo {
    
    /// 按钮的图标
    var icon:String!
    
    /// 按钮显示的文本
    var title:String!
    
    /// 构造方法
    ///
    /// - Parameters:
    ///   - icon: <#icon description#>
    ///   - title: <#title description#>
    init(_ icon:String,_ title:String) {
        self.icon=icon
        self.title=title
    }
}
