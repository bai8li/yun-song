//
//  ListResponse.swift
//  列表网络响应解析类
//
//  Created by smile on 2019/5/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//JSON解析为对象
import HandyJSON

class ListResponse<T:HandyJSON>: BaseResponse {
    
    /// 定义一个列表
    /// 里面的对象使用了泛型
    var data:Array<T>?
    
    /// 分页对象
    /// 不是所有列表都需要分页
    /// 所以他可以为空
    var meta:Meta?
    
}
