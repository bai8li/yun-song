//
//  Title.swift
//  标题模型
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class Title {
    
    /// 标题
    var title:String!
    
    /// 是否显示更多按钮
    var isShowMore:Bool=false
    
    /// 用来区分点击
    var tag:Int!
    
    init(_ title:String,_ isShowMore:Bool,_ tag:Int) {
        self.title=title
        self.isShowMore=isShowMore
        self.tag=tag
    }
}
