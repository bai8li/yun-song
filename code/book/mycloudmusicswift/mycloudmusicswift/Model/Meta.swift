//
//  Meta.swift
//  网络请求响应分页接口模型
//
//  Created by smile on 2019/4/17.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入JSON解析框架
import HandyJSON

class Meta: HandyJSON {
    /**
     当前页，从1开始
     */
    var current_page:Int=0
    
    
    /**
     共有多少页
     */
    var total_pages:Int=0
    
    
    /**
     共有多少条
     */
    var total_count:Int=0
    
    
    /**
     上一页，为空表示没有
     */
    var prev_page:Int?
    
    
    /**
     下一页，为空表示没有
     */
    var next_page:Int?
    
    /// 因为实现了HandyJSON
    /// 他要求有一个init方法
    required init() {}
    
    //根据传递的meta，计算下一页
    //meta有可能为空
    static func nextPage(_ data:Meta?) -> Int {
        if let data = data {
            return data.current_page + 1
        }
        return 1
    }
}
