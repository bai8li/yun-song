//
//  Comment.swift
//  评论模型
//
//  Created by smile on 2019/4/17.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class Comment: BaseModel {
//    - (BOOL)isLiked{
//    return _like_id!=nil;
//    }
    
    /// 评论内容
    var content:String!
    
    
    /**
     喜欢数量
     */

    var likes_count:Int=0
    
    /**
     * 区分评论来源，0:视频,10:单曲,20:专辑,30:歌单
     */
    var style:Int=0
    
    /**
     * 歌单Id
     */
    var sheet_id:Int?
    
    /**
     * 是否点赞，有值表示点赞，null表示没点赞
     */
    var like_id:String?
    
    /**
     * 回复评论的Id
     */
    var parent_id:Int?
    
    /**
     * 回复的评论，服务端返回数据
     */
    var parent:Comment?
    
    /**
     当前评论属于哪个用户
     */
    var user:User!
    
    //@property YYTextLayout *textLayout;
    //
    //@property YYTextLayout *sourceTextLayout;
    
    func isLiked() -> Bool {
        return like_id != nil
    }
    
}
