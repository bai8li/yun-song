//
//  SongLocal.swift
//  Song存储到数据库的模型
//  因为当前版本数据库框架和JSON解析框架放一起
//  有冲突，所以把数据库模型映射单独创建出来
//
//  Created by smile on 2019/4/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//数据库框架
import RealmSwift
import Realm

class SongLocal: Object {
    
    /// 数据库主键
    /// 当前对象Id+用户Id
    @objc dynamic var sid:String=""

    @objc dynamic var id:String=""
    
    @objc dynamic var title:String=""
    @objc dynamic var banner:String=""
    @objc dynamic var uri:String=""
    
    /// 歌手Id
    @objc dynamic var singer_id:String=""
    
    /// 歌手名称
    @objc dynamic var singer_nickname:String=""
    
    /// 歌手头像
    /// 可选值
    @objc dynamic var singer_avatar:String?=nil
    
    /// 用户Id，保存到数据库采用，用来实现多用户
    @objc dynamic var userId:String!
    
    /// 是否在播放列表；true：在
    @objc dynamic var playList:Bool=false
    
    /// 设置主键字段
    ///
    /// - Returns: <#return value description#>
    override static func primaryKey() -> String? {
        return "sid"
    }
    
    func generateId() {
        sid = "\(id)\(userId!)"
    }

    // MARK: -  播放音乐后才有值
    /// 当前音乐播放进度
    @objc dynamic var progress:Float=0
    
    /// 当前音乐时长
    /// 播放后才可以取值
    @objc dynamic var duration:Float=0
    
    /// 将SongLocal转为Song对象
    ///
    /// - Returns: <#return value description#>
    func toSong() -> Song {
        let song=Song()
        
        song.id=id
        song.title=title
        song.banner=banner
        song.uri=uri
        
        let singer=User()
        singer.id=singer_id
        singer.nickname=singer_nickname
        singer.avatar=singer_avatar
        
        song.singer=singer
        
        song.playList=playList
        
        //音乐时长
        song.duration=duration
        
        //音乐播放进度
        song.progress=progress
        
        return song
    }
 
}
