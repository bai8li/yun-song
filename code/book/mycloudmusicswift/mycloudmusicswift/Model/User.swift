//
//  User.swift
//  用户模型
//
//  Created by smile on 2019/4/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

////导入JSON解析框架
//import HandyJSON

class User: BaseModel {
    var nickname:String?
    var avatar:String?
    var description:String?
    
    //计算属性
    //返回格式化后的描述
    var formatDescription:String {
        get {
            if description != nil && !description!.isEmpty {
                return description!
            }

            return "这个人很懒，没有填写个人介绍！"
        }
    }
    
    var gender:Int=0
    var genderFormat:String {
        get {
            switch gender {
            case 1:
                return "男"
            case 2:
                return "女"
            default:
                //0
                return "保密"
            }
        }
    }
    
    
    var birthday:String?
    
    var phone: String?
    var email:String?
    
    var qq_id: String?
    var wechat_id:String?
    var weibo_id:String?
    
    var province:String?
    var province_code:String?
    
    var city:String?
    var city_code:String?
    
    var area:String?
    var area_code:String?
    
    /**
     * 是否关注，1：关注
    在用户详情，动态列表才会返回
     */
    var following:Int?
    
    /**
     * 关注我的人
     */
    var followers_count=0
    
    /**
     * 我关注的人
     */
    var followings_count=0
    
    func isFollowing() -> Bool {
        return following != nil
    }
    
    //本地过滤相关属性
    //当然可以将多个搜索字符串拼接到一个字段上
    /**
     拼音首字母，的首字母
     */
    var frist:String!
    
    /// 昵称全拼
    var pinyin:String!
    
    /// 昵称每个汉字的首字母
    var pinyinFrist:String!
    
}
