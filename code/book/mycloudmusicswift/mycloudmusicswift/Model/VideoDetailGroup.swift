//
//  VideoDetailGroup.swift
//  视频详情分组模型
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoDetailGroup {
    //当前这一组下面的内容
    var datas:[Any]!
    
    //组标题要显示的信息
    //因为该界面组本质显示的
    //就是当前播放的视频所对应的用户信息
    var data:User?
    
}
