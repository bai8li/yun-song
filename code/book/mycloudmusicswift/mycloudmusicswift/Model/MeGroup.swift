//
//  MeGroup.swift
//  我的界面分组模型
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class MeGroup {
    /// 这一组 标题
    var title:String?
    
    /// 这一组 数据
    var datas:[Any]!
}
