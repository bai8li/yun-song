//
//  DetailResponse.swift
//  详情网络响应解析类
//
//  Created by smile on 2019/5/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//JSON解析为对象
import HandyJSON

/// 继承BaseResponse
/// 定义了一个泛型T
/// 泛型必须实现了HandyJSON协议
/// 因为我们这里肯定希望用户传递的对象能解析为对象
/// 所以在这里必须要实现该协议
class DetailResponse<T:HandyJSON>: BaseResponse {
    
    /// 真实数据
    /// 他的类型是泛型
    /// 也就是说在使用的时候指定
    var data:T?
    
}

