//
//  Sheet.swift
//  歌单模型
//
//  Created by smile on 2019/4/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
//import HandyJSON

//{
//    "id": 4,
//    "title": "Android中如何访问网络",
//    "banner": null,
//    "description": "我们希望用户可以手动处理，也可以不处理。所以就要定义不同的方法，有些方法不用传入失败回调block，如果要手动处理就调用传入失败回调block就行了；这和前面显示loading差不多。这是随便添加的，Android和iOS",
//    "clicks_count": 1,
//    "collections_count": 0,
//    "comments_count": 0,
//    "user": {
//        "id": 1,
//        "nickname": "爱学啊Android开发组",
//        "avatar": null,
//        "gender": 0
//    }
//}
//class Sheet: HandyJSON {
//    var id: Int!
//    var title:String!
//    var banner:String?
//    var description:String?
//    var clicks_count:Int=0
//    var collections_count:Int=0
//    var comments_count:Int=0
//    var user:User!
//    var created_at:String?
//    var updated_at:String?
//
//    required init() {}
//
//}

//重构后
//class Sheet: BaseModel {
//
//    /// 歌单Id
//    var id: Int!
//
//    /// 标题
//    var title:String!
//
//    /// 封面
//    var banner:String?
//
//    /// 描述
//    var description:String?
//
//    /// 点击数
//    var clicks_count:Int=0
//
//    /// 收藏数
//    var collections_count:Int=0
//
//    /// 评论数
//    var comments_count:Int=0
//
//    /// 创建歌单的用户
//    var user:User!
//
//    /// 创建时间
//    var created_at:String?
//
//    /// 更新时间
//    var updated_at:String?
//
//}

//重构公共字段后
class Sheet: BaseModel {
    /// 标题
    var title:String!
    
    /// 封面
    var banner:String?
    
    /// 描述
    var description:String?
    
    /// 点击数
    var clicks_count:Int=0
    
    /// 收藏数
    var collections_count:Int=0
    
    /// 评论数
    var comments_count:Int=0
    
    /// 音乐数
    var songs_count:Int=0
    
    /// 创建歌单的用户
    var user:User!
    
    /// 歌单下的歌曲
    var songs:[Song]?
    
    /// 是否收藏，有值就表示收藏
    var collection_id:Int?
    
    /// 是否收藏
    ///
    /// - Returns: true:收藏；false:没收藏
    func isCollection() -> Bool {
        if collection_id != nil {
            return true
        }
        
        return false
    }
}
