//
//  SheetGroup.swift
//  歌单分组模型
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SheetGroup: UIView {

    /// 这一组 标题
    var title:String!
    
    /// 这一组 歌单
    var datas:[Sheet]!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
