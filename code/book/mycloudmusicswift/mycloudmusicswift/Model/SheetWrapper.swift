//
//  SheetWrapper.swift
//  用来测试解析歌单详情网络请求
//
//  Created by smile on 2019/5/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//JSON解析为对象
import HandyJSON

class SheetWrapper:HandyJSON {
    /// 歌单详情
    var data:SheetDetail!
    
    /// 因为实现了HandyJSON
    /// 他要求有一个init方法
    required init() {}
}
