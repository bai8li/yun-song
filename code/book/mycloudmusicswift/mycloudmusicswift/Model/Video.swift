//
//  Video.swift
//  视频模型
//
//  Created by smile on 2019/4/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class Video: BaseModel {
    
    /// 视频标题
    var title:String!
    
    /// 视频地址
    var uri:String!
    
    /// 视频封面
    var banner:String!
    
    /// 点击数
    var clicks_count:Int=0
    
    /// 点赞数
    var likes_count:Int=0
    
    /// 评论列表
    var comments_count:Int=0
    
    /// 视频作者
    var user:User!
    
    // MARK: -  播放后才有值
    /// 当前播放进度
    var progress:Float=0
    
    /// 时长
    /// 播放后才可以取值
    var duration:Float=0
}
