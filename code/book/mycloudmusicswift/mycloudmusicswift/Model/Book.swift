//
//  Book.swift
//  电子书
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class Book: BaseModel {
    var title:String!
    var banner:String!
    var price:String!
    var user:User!
    
    /// 有值表示已经购买
    var buy:Int?
    
    /// 是否购买
    ///
    /// - Returns: true:购买；false:没有购买
    func isBuy() -> Bool {
        if buy != nil {
            return true
        }
        
        return false
    }
}

//{
//    "id": 3,
//    "title": "爱学啊这是电子书1，测试商品",
//    "banner": "banner1.jpg",
//    "price": "0.01",
//    "created_at": "2019-04-10T00:28:45.000Z",
//    "user": {
//        "id": 5,
//        "nickname": "爱学啊",
//        "avatar": "http://thirdqq.qlogo.cn/qqapp/101464132/1A9F7466DF196A10D4F969522B0871D6/100",
//        "gender": 0
//    }
//}
