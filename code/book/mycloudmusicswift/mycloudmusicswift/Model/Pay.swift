//
//  Pay.swift
//  支付参数
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class Pay: BaseModel {
    
    /// 支付渠道
    /// 取值和Order中channel一样
    var channel:Order.Channel!
    
    /// 支付参数
    var pay:String!
    
}
