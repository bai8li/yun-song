//
//  Session.swift
//  会话模型（登陆成功后返回的模型）
//
//  Created by smile on 2019/4/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
//import HandyJSON

class Session: BaseModel {
    
    /// 用户Id
    var user: String!
    
    /// 登陆标识
    var session: String!
    
    /// 第三方聊天用的标识
    var im_token: String!

}
