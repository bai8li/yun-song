//
//  Song.swift
//  歌曲模型
//
//  Created by smile on 2019/4/12.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

//{
//    "id": 1,
//    "title": "爱学啊-这是歌曲1",
//    "banner": "banner1.jpg",
//    "uri": "m1.mp3",
//    "clicks_count": 0,
//    "comments_count": 0,
//    "style": 0,
//    "lyric": null,
//    "created_at": "2019-04-10T00:29:09.000Z",
//    "user": {
//        "id": 5,
//        "nickname": "爱学啊Android开发组",
//        "avatar": null
//    },
//    "singer": {
//        "id": 5,
//        "nickname": "爱学啊Android开发组",
//        "avatar": null
//    }
//}
class Song: BaseModel {
    /// 数据库主键，当前对象Id+用户Id
    var sid:String?
    
    var title:String!
    var banner:String!
    var uri:String!
    
    /// 点击数
    var clicks_count:Int=0
    
    /// 评论数
    var comments_count:Int=0
    
    /// 歌词类型
    var style:LyricType!
    
    /// 歌词内容
    var lyric:String?
    
    /// 创建歌曲的用户
    var user:User!
    
    /// 歌手
    var singer:User!
    
    /// 是否在播放列表；true：在
    var playList:Bool=false
    
    /// 是否是本地音乐
    ///
    /// - Returns: <#return value description#>
    func isLocal() -> Bool {
        return false
    }
    
    // MARK: -  播放后才有值
    /// 当前播放进度
    var progress:Float=0
    
    /// 时长
    /// 播放后才可以取值
    var duration:Float=0
    
    
    /// 将Song转为SongLocal对象
    ///
    /// - Returns: <#return value description#>
    func toSongLocal() -> SongLocal {
        //创建一个对象
        let songLocal=SongLocal()
        
        //将要保存的值赋值到保存对象上
        songLocal.id=id
        songLocal.title=title
        songLocal.banner=banner
        songLocal.uri=uri
        songLocal.singer_id=singer.id
        songLocal.singer_nickname=singer.nickname!
        songLocal.singer_avatar=singer.avatar
        songLocal.playList=playList
        
        //音乐时长
        songLocal.duration=duration
        
        //音乐播放进度
        songLocal.progress=progress
    
        return songLocal
    }
    
}


/// 歌词类型
///
/// - 0: LRC
/// - 10: KSC
enum LyricType:Int,HandyJSONEnum {
    case lrc=0
    case ksc=10
}
