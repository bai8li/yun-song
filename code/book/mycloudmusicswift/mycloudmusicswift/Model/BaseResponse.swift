//
//  BaseResponse.swift
//  通用网络请求响应模型
//
//  Created by smile on 2019/5/3.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//JSON解析为对象
import HandyJSON

class BaseResponse:HandyJSON {
    
    /// 状态码
    /// 只有发生了错误才会有
    var status:Int?
    
    /// 错误信息
    /// 发生了错误不一定有
    var message:String?

    /// 因为实现了HandyJSON
    /// 他要求有一个init方法
    required init() {}
}
