//
//  Lyric.swift
//  解析后的歌词
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

class Lyric {
    /// 每一行歌词
    var datas:[LyricLine]!
    
    /// 是否是精确到字歌词
    var isAccurate = false
}
