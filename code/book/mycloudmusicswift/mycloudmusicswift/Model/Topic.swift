//
//  Topic.swift
//  话题模型
//
//  Created by smile on 2019/4/18.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class Topic: BaseModel {
    var title:String!
    var banner:String!
    
    var desc:String?

    
    /**
     * 参与人数
     */
    var joins_count:Int=0
}
