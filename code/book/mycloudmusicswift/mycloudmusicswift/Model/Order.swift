//
//  Order.swift
//  订单模型
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入JSON解析框架
import HandyJSON

class Order: BaseModel {
    //订单状态：0:待支付,10:支付完成,20:订单关闭
    var status:Status = .wait_pay
    var statusFormat:String {
        get {
            switch status {
            case .payed:
                return "支付完成"
            case .close:
                return "订单关闭"
            default:
                return "待支付"
            }
        }
    }
    
    var price:String!
    
    
    /// 订单来源
    /// 一般订单来源不会返回给客户端
    /// 我们这样返回来只是给大家演示如何显示这些字段而已
    /// 也就是在那个平台创建的订单
    
//    # 订单来源，创建订单传递的来源
//    # 支付来源也会用到，支付时传递的来源
//    # 因为可能有创建订单是web网站
//    # 用户是在手机上支付的
//    # web
//    ORDER_SOURCE_WEB=0
//
//    # android
//    ORDER_SOURCE_ANDROID=10
//
//    # ios
//    ORDER_SOURCE_IOS=20
    var source:Int = 0
    var sourceFormat:String {
        get {
            switch source {
            case 10:
                return "Android客户端"
            case 20:
                return "iOS客户端"
            default:
                return "Web网站"
            }
        }
    }
    
    
    /// 支付来源
    /// 也就是在那个平台支付的
    var origin:Int = 0
    var originFormat:String {
        get {
            switch source {
            case 10:
                return "Android客户端"
            case 20:
                return "iOS客户端"
            default:
                return "Web网站"
            }
        }
    }
    
    
    /// 支付渠道
    var channel:Channel = .unknow
    var channelFormat:String {
        get {
            switch channel {
            case .alipay:
                return "支付宝"
            case .wechat:
                return "微信"
            default:
                return "未知"
            }
        }
    }
    
    var number:String!
    
    var book:Book!
    
    enum Status:Int,HandyJSONEnum {
        case wait_pay=0
        case payed=10
        case close=20
    }
    
    enum Channel:Int,HandyJSONEnum {
        case unknow=0
        case alipay=10
        case wechat=20
    }
    
    enum Source:Int,HandyJSONEnum {
        case web=0
        case android=10
        case ios=20
    }
    
}

//订单详情JSON
//{
//    "data": {
//        "id": 1,
//        "status": 0,
//        "price": "0.01",
//        "source": 0,
//        "origin": 10,
//        "channel": 10,
//        "number": "201904230742252268",
//        "other": null,
//        "created_at": "2019-04-23T07:42:25.000Z",
//        "book": {
//            "id": 3,
//            "title": "爱学啊这是电子书1，测试商品",
//            "banner": "banner1.jpg",
//            "price": "0.01",
//            "created_at": "2019-04-10T00:28:45.000Z",
//            "user": {
//                "id": 5,
//                "nickname": "爱学啊",
//                "avatar": "http://thirdqq.qlogo.cn/qqapp/101464132/1A9F7466DF196A10D4F969522B0871D6/100",
//                "gender": 0
//            }
//        }
//    }
//}
