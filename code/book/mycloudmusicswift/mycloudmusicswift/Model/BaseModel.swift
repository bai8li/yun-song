//
//  BaseModel.swift
//  通用模型类，实现了JSON解析协议
//
//  Created by smile on 2019/4/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//导入JSON解析框架
import HandyJSON

class BaseModel: HandyJSON {
    
    /// id
    var id: String!
    
    /// 创建时间
    var created_at:String=""
    
    /// 更新时间
    var updated_at:String=""
    
    /// 因为实现了HandyJSON
    /// 他要求有一个init方法
    required init() {}
}
