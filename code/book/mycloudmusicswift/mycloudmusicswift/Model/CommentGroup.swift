//
//  CommentGroup.swift
//  评论分组模型
//
//  Created by smile on 2019/4/17.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class CommentGroup {
    /// 这一组 标题
    var title:String!
    
    /// 这一组 评论
    var comments:[Comment]!
}
