//
//  Ad.swift
//  广告模型
//
//  Created by smile on 2019/4/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

////导入JSON解析框架
//import HandyJSON

//class Ad: HandyJSON {
//
//    /// 图片地址
//    var banner: String!
//
//    /// 点击跳转的页面
//    var uri: String!
//
//    /// 广告标题
//    var title: String!
//
//    required init() {}
//}

//重构后
class Ad: BaseModel {
    
    /// 图片地址
    var banner: String!
    
    /// 点击跳转的页面
    var uri: String!
    
    /// 广告标题
    var title: String!

}
