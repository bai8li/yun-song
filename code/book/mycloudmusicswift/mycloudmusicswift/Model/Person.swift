//
//  Person.swift
//  测试数据库模型
//
//  Created by smile on 2019/4/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入数据库框架
import RealmSwift

class Person: Object {
    @objc dynamic var name = ""
    @objc dynamic var age = 0
}
