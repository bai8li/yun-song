//
//  SheetDetail.swift
//  歌单详情外部模型（测试用）
//
//  Created by smile on 2019/5/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//JSON解析为对象
import HandyJSON

class SheetDetail:HandyJSON {
    /// 标题
    var title:String!
    
    /// 封面
    var banner:String?
    
    /// 描述
    var description:String?
    
    /// 点击数
    var clicks_count:Int=0
    
    /// 收藏数
    var collections_count:Int=0
    
    /// 评论数
    var comments_count:Int=0
    
    /// 音乐数
    var songs_count:Int=0
    
    /// 创建歌单的用户
    var user:User!
    
    /// 歌单下的歌曲
    var songs:[Song]?
    
    /// 是否收藏，有值就表示收藏
    var collection_id:Int?
    
    /// 因为实现了HandyJSON
    /// 他要求有一个init方法
    required init() {}
}
