//
//  LogFormat.swift
//  自定义日志格式
//
//  Created by smile on 2019/5/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import Foundation

//日志框架
import CocoaLumberjack

//实现DDLogFormatter协议
//这里要继承NSObject
//因为DDLogFormatter其实是继承NSObject
//如果不继承会要实现很多NSObject里面的方法
class LogFormat: NSObject, DDLogFormatter {
    
    /// 返回日志字符串
    ///
    /// - Parameter logMessage: <#logMessage description#>
    /// - Returns: <#return value description#>
    func format(message logMessage: DDLogMessage) -> String? {
        var logLevel=""
        
        //判断日志等级；转为字符串
        switch logMessage.level {
        case .error:
            logLevel="E"
        case .warning:
            logLevel="W"
        case .info:
            logLevel="I"
        case .debug:
            logLevel="D"
        default:
            logLevel="V"
        }
        
        //格式化时间
        let date=TimeUtil.formatYMDHHMMSSSSS(logMessage.timestamp)
        
        //格式化日志
        return "\(date) \(logLevel) \(logMessage.tag ?? "") \(logMessage.message)"
    }
    
}
