//
//  DiscoveryHeaderView.swift
//  发现界面头部布局
//
//  Created by smile on 2019/4/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class DiscoveryHeaderView: BaseCollectionReusableView,YJBannerViewDelegate,YJBannerViewDataSource {
    static var NAME = "DiscoveryHeaderView"
    
    /// 顶部轮播图
    @IBOutlet weak var bannerView: YJBannerView!
    
    /// 今天几号点击按钮
    @IBOutlet weak var btDay: UIButton!
    
    /// 显示今天是多少号
    @IBOutlet weak var lbDay: UILabel!
    
    /// 每日推荐标题
    /// 在代码中找到他主要是实现夜间模式
    @IBOutlet weak var lbDayTitle: UILabel!
    
    /// 歌单按钮
    @IBOutlet weak var btSheet: UIButton!
    
    /// 歌单标题
    @IBOutlet weak var lbSheet: UILabel!
    
    /// FM按钮
    @IBOutlet weak var btFM: UIButton!
    
    /// FM标题
    @IBOutlet weak var lbFM: UILabel!
    
    /// 排行榜标题
    @IBOutlet weak var btRank: UIButton!
    
    /// 排行榜按钮
    @IBOutlet weak var lbRank: UILabel!
    
    /// 用来存储轮播图图片地址
    var bannerArray:[String]=[]
    
    /// 轮播图点击回调方法
    var onBannerClick:((_ position:Int) -> Void)?
    
    /// 显示轮播图组件
    /// 因为轮播图需要的是图片地址
    /// 而现在服务端返回的对象
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:[Ad]) {
        
        //移除原来的数据
        bannerArray.removeAll()
        
        //循环每个数据，取出图片地址
        //因为服务端返回对象里面还包含其他信息
        //而轮播图控件只需要图片地址
        for ad in data {
            bannerArray.append(ResourceUtil.resourceUri(ad.banner!))
        }
        
        //通知轮播图框架从新加载数据
        bannerView.reloadData()
       
    }
    
    override func initViews()  {
        super.initViews()
        //主题相关配置
        //文本
        lbDayTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        lbSheet.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        lbFM.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        lbRank.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //图片配置
        //每日推荐
        btDay.theme_setImage(["Day","DayGrey"], forState: .normal)
        btDay.theme_setImage(["DaySelected","DaySelectedGrey"], forState: .highlighted)
        
        //歌单
        btSheet.theme_setImage(["Sheet","SheetGrey"], forState: .normal)
        btSheet.theme_setImage(["SheetSelected","SheetSelectedGrey"], forState: .highlighted)
        
        //私人FM
        btFM.theme_setImage(["FM","FMNight"], forState: .normal)
        btFM.theme_setImage(["FMSelected","FMNightSelected"], forState: .highlighted)
        
        //排行榜
        btRank.theme_setImage(["Rank","RankGrey"], forState: .normal)
        btRank.theme_setImage(["RankSelected","RankSelectedGrey"], forState: .highlighted)
        
       
//       初始化轮播图组件
        //        这是轮播图的数据库为当前类
        bannerView.dataSource=self
        
        //        设置轮播图的代理为当前类
        bannerView.delegate=self
        
        //        设置如果显示的图片为空（找不到图片）显示的图片
        bannerView.emptyImage=UIImage (named: IMAGE_PLACE_HOLDER)
        
        //        设置占位图，就是图片还是下载下来前显示的图片
        bannerView.placeholderImage=UIImage (named: IMAGE_PLACE_HOLDER)
        
        //        设置轮播图组件内部显示图片调用什么方法
        //        下面这句话的意思是显示图片的时候
        //        调用：sd_setImageWithURL:placeholderImage:
        //        该方法是SDWebImage框架中的方法
        bannerView.bannerViewSelectorString="sd_setImageWithURL:placeholderImage:"
        
        
        //指示器默认颜色
        bannerView.pageControlNormalColor=UIColor(hex: COLOR_LIGHT_GREY)
        
        //高亮颜色
        bannerView.pageControlHighlightColor=UIColor(hex: COLOR_PRIMARY)
        
        
        //指示器位置
        //        self.bannerView.pageControlAliment = PageControlAlimentRight;
        
        //禁用自动滚动
        //        bannerView.autoScroll=false
        
        //        重新载入数据
        bannerView.reloadData()
        
        //设置BannerView圆角
        ViewUtil.showSmallRadius(view: bannerView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        defaultDayColor()
    }
    
    override func initDatas()  {
        super.initDatas()
        
        //设置每日推荐按钮，天
        
        //获取当前日期
        let now = Date()
        
        //创建一个日期格式化器
        let formatter = DateFormatter()
        
        //只格式化天（2位）
        formatter.dateFormat = "dd"
        
        //格式化
        let time = formatter.string(from: now)
        
        //将格式化的字符串设置到label
        lbDay.text=time
    }
    
    override func initListeners() {
        super.initListeners()
        
    }
    
    // MARK: - 轮播图
    /// banner数据来源
    ///
    /// - Parameter bannerView: 轮播组价
    /// - Returns: 返回要显示的图片
    func bannerViewImages(_ bannerView: YJBannerView!) -> [Any]! {
        return bannerArray
    }
    
    /// 自定义Cell
    /// 这里复写该方法的目的是，设置图片的缩放模式
    ///
    /// - Parameters:
    ///   - bannerView: <#bannerView description#>
    ///   - customCell: 原来的组件
    ///   - index: 当前索引
    /// - Returns: 返回新的cell
    func bannerView(_ bannerView: YJBannerView!, customCell: UICollectionViewCell!, index: Int) -> UICollectionViewCell! {
        
        //        将cell转为YJBannerViewCell
        //        因为我们要给他配置一些属性
        let cell = customCell as! YJBannerViewCell
        
        //按比例，从中心填充，多余的裁掉
        cell.showImageViewContentMode=UIView.ContentMode.scaleAspectFill
        
        return cell
    }
    
    
    
    /// banner点击方法
    ///
    /// - Parameters:
    ///   - bannerView: <#bannerView description#>
    ///   - index: 点击的索引
    func bannerView(_ bannerView: YJBannerView!, didSelectItemAt index: Int) {
        NSLog("discovery controller banner click:%d",index )
        
        if let callback = onBannerClick {
            //回调点击事件方法
            callback(index)
        }
    }
    
    /// 每日推荐标签默认颜色
    func defaultDayColor() {
        if PreferenceUtil.isNight() {
            //夜间模式
            lbDay.textColor=UIColor(hex: COLOR_DARK_BLACK)
        } else {
            //白天模式
            lbDay.textColor=UIColor(hex: COLOR_PRIMARY)
            
        }
    }
    
    // MARK: - 快捷按钮点击事件
    
    /// 每日推荐点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDayClick(_ sender: UIButton) {
        print("DiscoveryHeaderView onclick")
        
        defaultDayColor()
    }
    
    
    /// 歌单点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onListClick(_ sender: UIButton) {
        print("DiscoveryHeaderView onListClick")
    }
    
    /// 私人FM点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onFMClick(_ sender: UIButton) {
        print("DiscoveryHeaderView onFMClick")
    }
    
    /// 排行榜点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onRankClick(_ sender: Any) {
        print("DiscoveryHeaderView onRankClick")
    }
    
    
    /// 每日推荐按钮按下
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDayTouchDown(_ sender: UIButton) {
//        if PreferenceUtil.isNight() {
//            //夜间模式
//            lbDay.textColor=UIColor(hex: 0x232323)
//        } else {
//            //白天模式
//            lbDay.textColor=UIColor(hex: COLOR_PRIMARY)
//        }
    }
    
    
    
}
