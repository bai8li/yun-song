//
//  VideoDetailUserInfoHeaderView.swift
//  视频详情用户信息View
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoDetailUserInfoHeaderView: UITableViewHeaderFooterView {
    
    static var NAME = "VideoDetailUserInfoHeaderView"

    /// 用户头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 用户昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 关注按钮
    @IBOutlet weak var btFollow: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btFollow.showColorPrimaryBorder()
        ViewUtil.showRadius(ivAvatar, 20)
    }
    
    func bindData(_ data:User) {
        ImageUtil.showAvatar(ivAvatar, data.avatar)
        
        lbNickname.text=data.nickname
        
    }
    
    @IBAction func onFollowClick(_ sender: Any) {
        print("VideoDetailUserInfoHeaderView onFollowClick")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
