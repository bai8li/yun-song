//
//  SongListMoreView.swift
//  音乐列表更多View
//
//  Created by smile on 2019/4/27.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SongListMoreView: BaseView {
    @IBOutlet weak var svCollectionSong: UIStackView!
    
    
    @IBOutlet weak var svDeleteSongInSheet: UIStackView!
    var contentView:UIView!
    
    /// 收藏歌曲回调方法
    var onCollectionSongClick:((_ position:Int) -> Void)!
    
    /// 从歌单中删除回调方法
    var onDeleteSongInSheetClick:((_ position:Int) -> Void)?
    
    override func initViews() {
        super.initViews()

        contentView = Bundle.main.loadNibNamed("SongListMoreView", owner: self, options: nil)!.first as! UIView
        
        self.addSubview(contentView)
    }
    
    override func initListeners() {
        super.initListeners()
        
        //收藏到歌单点击
        svCollectionSong.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCollectionClick)))
        
        //从歌单中删除点击
        svDeleteSongInSheet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onDeleteSongClick)))
    }
    
    @objc func onCollectionClick() {
        print("SongListMoreView onCollectionSongClick")
        
        onCollectionSongClick(tag)
    }
    
    @objc func onDeleteSongClick() {
        print("SongListMoreView onDeleteSongClick")
        
        if let onDeleteSongInSheetClick = onDeleteSongInSheetClick {
            onDeleteSongInSheetClick(tag)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        //设置添加的View的frame
        contentView.frame = bounds
    }

}

