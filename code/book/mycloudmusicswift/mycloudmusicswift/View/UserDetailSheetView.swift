//
//  UserDetailSheetView.swift
//  用户详情歌单列表
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

import JXPagingView


class UserDetailSheetView: BaseView {
    
    /// 歌单点击回调方法
    var onSheetClick:((_ data:Sheet) -> Void)!
    
    //当前界面的UserId
    var userId:String!

    var tableView:UITableView!

    var dataArray:[SheetGroup]=[]
    
    /// 列表的回调
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    
    /// 自定义初始化方法
    func customInit() {
        fetchData()
    }
    
    override func initViews() {
        super.initViews()
        
        //创建TableView
        //因为这个View没有用到了Xib
        //是代码实现布局
        tableView=UITableView(frame: self.frame)
        
        //设置Cell高度
        tableView.rowHeight=SIZE_TITLE_CELL_HEIGHT
        
        //注册自定义section
        //因为默认的UI不太适合我们
        //所以真实项目中大部分情况都要自定义
        
        tableView.register(UINib(nibName: CommentHeaderView.NAME, bundle: nil), forHeaderFooterViewReuseIdentifier: CommentHeaderView.NAME)

        //注册歌单Cell
        tableView.register(UINib(nibName:TopicCell.NAME, bundle:nil), forCellReuseIdentifier: TopicCell.NAME)
        
        tableView.delegate=self
        tableView.dataSource=self

       addSubview(tableView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //设置TableView尺寸
        tableView.frame=self.bounds
    }
    
    func fetchData() {
        //获取创建的歌单
        Api.shared
            .createSheets(userId)
            .subscribeOnSuccess { (data) in
                let group=SheetGroup()
                group.title="创建的歌单"
                
                //表示如果data.data为空
                //就是用空数组代理
                //??是三元表达式的简写
                group.datas=data?.data ?? []
                self.dataArray.append(group)
                
                //获取收藏的歌单
                Api.shared
                    .collectSheets(self.userId)
                    .subscribeOnSuccess({ (data) in
                        let group=SheetGroup()
                        group.title="收藏的歌单"
                        group.datas=data?.data ?? []
                        self.dataArray.append(group)

                        self.tableView.reloadData()
                })
        }
        
    }

}

// MARK - JXPagingViewListView代理
extension UserDetailSheetView: JXPagingViewListViewDelegate {
    
    /// 返回列表控件
    ///
    /// - Returns: <#return value description#>
    func listView() -> UIView {
        return self
    }
    
    /// 返回内部的滚动控件
    ///
    /// - Returns: <#return value description#>
    func listScrollView() -> UIScrollView {
        return tableView
    }
    
    /// 将回调保存到当前类中
    ///
    /// - Parameter callback: <#callback description#>
    func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }
    
}


// MARK: - TableView数据源和代理
extension UserDetailSheetView: UITableViewDataSource, UITableViewDelegate {
    
    /// 有多少组
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    /// 该组有多个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = dataArray[section]
        return group.datas.count
    }
    
    
    /// 返回当前Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME) as! TopicCell
        
        let group = dataArray[indexPath.section]
        let data=group.datas[indexPath.row]
        
        cell.bindData(data)
        
        return cell
    }
    
    
    /// 返回自定义的section(header)
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell=tableView.dequeueReusableHeaderFooterView(withIdentifier: CommentHeaderView.NAME) as! CommentHeaderView
        
        let group = dataArray[section]
        
        cell.bindData(group.title)
        
        return cell
    }
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("UserDetailSheetView didSelectRowAt:\(indexPath.row)")
        //取出数据
        let group = dataArray[indexPath.section]
        let data=group.datas[indexPath.row]
        
        onSheetClick(data)
    }
    
    /// scrollView已经滚动了
    /// 将这个scrollView传递到了PageView的回调中
    /// 它内部可能需要用到一些scrollView的状态
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}
