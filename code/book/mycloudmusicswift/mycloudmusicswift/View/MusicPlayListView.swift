//
//  MusicPlayListView.swift
//  音乐播放列表
//  用在音乐播放器控制
//  实现的效果是点击播放列表
//  从屏幕底部弹出的播放列表
//
//  Created by smile on 2019/4/16.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class MusicPlayListView: BaseView  {
    
    /// 播放点击回调方法
    var onSongClick:((_ data:Song) -> Void)!
    
    /// 删除音乐回调
    var onDeleteSongClick:((_ index:Int) -> Void)!
    
    var contentView: UIView!
    
    @IBOutlet weak var contentContaienr: UIView!
    
    /// 播放列表管理器
    var playListManager:PlayListManager!
    
    /// 播放器管理器
    var musicPlayerManager:MusicPlayerManager!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func initViews() {
        super.initViews()
        //从Nib加载View
        contentView = Bundle.main.loadNibNamed("MusicPlayListView", owner: self, options: nil)!.first as! UIView
        
        self.addSubview(contentView)
        
        //设置数据和代理
        tableView.dataSource=self
        tableView.delegate=self
        
        //注册Cell
        tableView.register(UINib(nibName:SongListCell.NAME , bundle: nil), forCellReuseIdentifier: SongListCell.NAME)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //设置内容的尺寸
        contentView.frame=bounds
        
        //设置控制按钮背景
        //左上，右上为圆角
        
        //设置切哪个直角
        //    UIRectCornerTopLeft     = 1 << 0,  左上角
        //    UIRectCornerTopRight    = 1 << 1,  右上角
        //    UIRectCornerBottomLeft  = 1 << 2,  左下角
        //    UIRectCornerBottomRight = 1 << 3,  右下角
        //    UIRectCornerAllCorners  = ~0UL     全部角
        
        //创建遮罩（有点类似PS中蒙版）路径
        let maskPath = UIBezierPath(roundedRect: contentContaienr.bounds, byRoundingCorners: [UIRectCorner.topLeft,.topRight], cornerRadii: CGSize(width: SIZE_LARGE_DIVIDER, height: SIZE_LARGE_DIVIDER))

        //创建layer
        let maskLayer=CAShapeLayer()
        //设置layer的frame为控制按钮组背景frame
        maskLayer.frame=contentContaienr.bounds

        //设置layer路径为上面创建的路径
        maskLayer.path = maskPath.cgPath

        //重新设置到控制组背景控件上
        contentContaienr.layer.mask = maskLayer
        
//                setNeedsLayout()
    }
    
    override func initDatas() {
        super.initDatas()
        
        playListManager=PlayListManager.shared
        musicPlayerManager=MusicPlayerManager.shared()
        
        showCurrentPlayMusicStatus()
    }
    
    
    /// 在列表中显示当前播放的音乐状态
    func showCurrentPlayMusicStatus() {
        //选中当前播放的音乐
        let currentSong = playListManager.getPlaySong()!
        
        //当前播放音乐的位置
        var index=0
        
        //遍历列表
        //获取正在播放的音乐
        //在当前列表中什么位置
        let dataArray = playListManager.getPlayList()!
        for e in dataArray.enumerated() {
            if e.element.id == currentSong.id{
                index=e.offset
                break
            }
        }
        
        //创建一个IndexPath
        let indexPath = IndexPath(item: index, section: 0)
        
        //选择这一行
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
    }
    
    
    
    
    @IBAction func onDeleteAllClick(_ sender: UIButton) {
    }
    
    
}

// MARK: - 列表代理
extension MusicPlayListView:UITableViewDataSource,UITableViewDelegate {
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playListManager.getPlayList()!.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: SongListCell.NAME, for: indexPath) as! SongListCell
        
        //右侧更多按钮显示删除状态
        cell.showDeleteStatus()
        
        //设置Tag
        //目的是Cell中显示位置
        //还有点击Cell更多按钮时知道是点击了那个Cell
        cell.tag=indexPath.row
        
        //取出数据
        let dataArray=playListManager.getPlayList()!
        
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
        
        //设置更多点击回调方法
        cell.onMoreClick={
            position in
            self.processMoreClick(position)
        }
        
        //        //选中当前播放的音乐
        //        let currentSong=playListManager.getPlaySong()
        //        if let currentSong = currentSong {
        //            if currentSong.id==data.id {
        //                //选中当前行
        //                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
        //            }
        //        }
        
        return cell
    }
    
    
    /// 删除当前这首音乐
    ///
    /// - Parameter position: <#position description#>
    func processMoreClick(_ position:Int) {
        let data=playListManager.getPlayList()![position]
        
        //从播放列表中删除这首音乐
        playListManager.delete(data)
        
        //从TableView中删除
        tableView.reloadData()
        
        //回调删除了这首音乐
        onDeleteSongClick(position)
        
        //再次获取当前播放的音乐
        let song=playListManager.getPlaySong()
        
        guard let _ = song else {
            //如果没有音乐
            //播放界面应该关闭
            //但当前是在View中没法直接关闭
            //所以发送一个通知
            //在播放控制器中接受到关闭
            SwiftEventBus.post(CLOSE_PLAY_PAGE, sender: nil)
            
            //guard规定body里面必须要有一个return
            return
        }
        
        //如果还有播放的音乐
        
        //选中当前播放的音乐
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.showCurrentPlayMusicStatus()
        }
    }
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("MusicPlayListView didSelectRowAt:\(indexPath.row)")
        
        //取出数据
        let dataArray=playListManager.getPlayList()!
        
        let data=dataArray[indexPath.row]
        
        onSongClick(data)
        
    }
    
}
