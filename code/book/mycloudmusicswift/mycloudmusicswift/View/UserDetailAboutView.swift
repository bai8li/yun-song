//
//  UserDetailAboutView.swift
//  用户详情关于我们View
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

import JXPagingView

class UserDetailAboutView: BaseView {
    
    @IBOutlet weak var tableView: UITableView!
    
    var contentView: UIView!
    
    //当前界面的UserId
    var userId:String!

    var dataArray:[Any]=[]
    
    /// 列表的回调
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    
    override func initViews() {
        super.initViews()
        
        //从Nib加载View
        contentView = Bundle.main.loadNibNamed("UserDetailAboutView", owner: self, options: nil)!.first as! UIView
        
        self.addSubview(contentView)
        
        //设置数据和代理
        tableView.dataSource=self
        tableView.delegate=self
        
        //注册Cell
        tableView.register(UINib(nibName:TopicCell.NAME, bundle:nil), forCellReuseIdentifier: TopicCell.NAME)
    }
    
    func customInit() {
        initDatas()
        initListeners()
    }
    
    override func initDatas() {
        super.initDatas()
    
        //添加一些测试数据
        //如果要显示真实数据
        //和歌单列表一样
        //直接从网络获取就行了
        for index in 0...100 {
            dataArray.append(Topic())
        }
        
        tableView.reloadData()
    }

    //MARK：自定义方法
    func loadViewFromNib() -> UIView {
        //重点注意，否则使用的时候不会同步显示在IB中，只会在运行中才显示。
        //注意下面的nib加载方式直接影响是否可视化，如果bundle不确切（为nil或者为main）则看不到实时可视化
        let nib = UINib(nibName:String(describing: UserDetailFeedView.self), bundle:nil)
        
        //将类名变为字符串：String(describing: MyView.self) Bundle的参数为type(of: self)也可以。
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }


}

// MARK: - JXPagingViewListView代理
// 如果要在在PageView中显示必须实现
extension UserDetailAboutView: JXPagingViewListViewDelegate {
    
    /// 返回列表控件
    ///
    /// - Returns: <#return value description#>
    func listView() -> UIView {
        return self
    }
    
    /// 返回内部的滚动控件
    /// 以前在OC项目中可以返回nil
    /// 实现的效果就是
    /// 该界面可以不适用TableView这样的控件实现
    /// 但在iOS版本中该方法无法返回nil
    /// 所以如果某个界面不想用UIScrollView
    /// 就只能换其他框架，或者自己实现
    ///
    /// - Returns: <#return value description#>
    func listScrollView() -> UIScrollView {
        return tableView
    }
    
    /// 将回调保存到当前类中
    ///
    /// - Parameter callback: <#callback description#>
    func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }
    
}

// MARK: - TableView数据源和代理
extension UserDetailAboutView: UITableViewDataSource, UITableViewDelegate {
    /// 有多少组
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 该组有多个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME) as! TopicCell
        
        return cell
    }
    
    /// scrollView已经滚动了
    /// 将这个scrollView传递到了PageView的回调中
    /// 它内部可能需要用到一些scrollView的状态
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}
