//
//  SheetDetailHeaderView.swift
//  歌单详情头部
//
//  Created by smile on 2019/4/13.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SheetDetailHeaderView: UITableViewHeaderFooterView {
    static let NAME = "SheetDetailHeaderView"

    /// 歌单封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 歌单标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 歌单创建人的头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 歌单创建人的昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 歌单评论数
    @IBOutlet weak var lbCommentCount: UILabel!
    
    
    /// 歌单分享数
    @IBOutlet weak var lbShareCount: UILabel!
    
    
    /// 歌单下面的快捷按钮容器
    @IBOutlet weak var vwControl: UIView!
    
    
    /// 歌单音乐数
    @IBOutlet weak var lbSongCount: UILabel!
    
    
    /// 收藏歌单按钮
    @IBOutlet weak var btCollection: UIButton!
    
    /// 歌单对象
    var data:Sheet!
    
    /// 评论点击回调方法
    var onCommentClick:(() -> Void)!
    
    /// 收藏点击回调方法
    var onCollectionClick:(() -> Void)!
    
    /// 用户点击回调方法
    var onUserClick:(() -> Void)!
    
    /// 播放全部点击回调方法
    var onPlayAllClick:(() -> Void)!
    
    /// 显示收藏状态
    func showCollectionStatus() {
        //收藏状态
        if data.isCollection() {
            //将按钮的文字改为 取消
            btCollection.setTitle("取消(\(data.collections_count))", for: UIControl.State.normal)
            
            //已经收藏了
            //就需要弱化取消收藏按钮
            //因为我们的本质是想让用户收藏
            //所以去掉背景
            btCollection.backgroundColor=UIColor.clear
            
            //设置文字颜色为灰色
            btCollection.setTitleColor(UIColor.lightGray, for: .normal)
            
        } else {
            //将按钮的文字改为 取消
            btCollection.setTitle("收藏(\(data.collections_count))", for: UIControl.State.normal)
            
            //设置为主色调背景
            btCollection.backgroundColor=UIColor(hex: COLOR_PRIMARY)
            
            //设置文字颜色为白色
            btCollection.setTitleColor(UIColor.white, for: .normal)
        }

    }
    
    /// 评论列表点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCommentListClick(_ sender: UIButton) {
        print("SheetDetailHeaderView onCommentListClick")
        onCommentClick()
    }
    
    
    /// 用户信息点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onUserClick(_ sender: UIButton) {
        print("SheetDetailHeaderView onUserClick")
        
        onUserClick()
    }
    
    
    /// 收藏歌单点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCollectionClick(_ sender: UIButton) {
        print("SheetDetailHeaderView onCollectionClick")
        
        onCollectionClick()
    }
    
    
    /// 播放所有音乐
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPlayAllClick(_ sender: UIButton) {
        print("SheetDetailHeaderView onPlayAllClick")
        
        onPlayAllClick()
    }
    
    override func awakeFromNib() {
        //歌单封面圆角
        ViewUtil.showSmallRadius(view: ivBanner)
        
        //歌单创建者头像圆角
        ViewUtil.showRadius(ivAvatar, 12)
        
        //设置控制按钮背景
        //左上，右上为圆角
        
        //设置切哪个直角
        //    UIRectCornerTopLeft     = 1 << 0,  左上角
        //    UIRectCornerTopRight    = 1 << 1,  右上角
        //    UIRectCornerBottomLeft  = 1 << 2,  左下角
        //    UIRectCornerBottomRight = 1 << 3,  右下角
        //    UIRectCornerAllCorners  = ~0UL     全部角
        
        //创建遮罩（有点类似PS中蒙版）路径
        let maskPath = UIBezierPath(roundedRect: self.vwControl.bounds, byRoundingCorners: [UIRectCorner.topLeft,.topRight], cornerRadii: CGSize(width: SIZE_LARGE_DIVIDER, height: SIZE_LARGE_DIVIDER))
        
        //创建layer
        let maskLayer=CAShapeLayer()
        //设置layer的frame为控制按钮组背景frame
        maskLayer.frame=vwControl.bounds
        
        //设置layer路径为上面创建的路径
        maskLayer.path = maskPath.cgPath
        
        //重新设置到控制组背景控件上
        vwControl.layer.mask = maskLayer
    }
    
    
    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Sheet) {
        //把歌单对象保存到当前类的实例变量上面
        //因为其他位置用到了
        self.data=data
        
        //显示歌单封面
        if let banner = data.banner {
            ImageUtil.show(ivBanner, banner)
        }
        
        //歌单标题
        lbTitle.text=data.title
        
        //歌单创建者头像
        ImageUtil.showAvatar(ivAvatar, data.user.avatar)
        
        //歌单创建者昵称
        lbNickname.text=data.user.nickname
        
        //歌单评论数
        lbCommentCount.text="\(data.comments_count)"
        
        //歌单有多少首音乐
        if let songs = data.songs {
            lbSongCount.text="(共\(songs.count)首)"
        }
        
        //显示收藏状态
        showCollectionStatus()
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
