//
//  BaseView.swift
//  通用View
//  实现一些通用的逻辑
//
//  Created by smile on 2019/4/27.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseView: UIView {

    //MARK：实现初始化构造器
    //使用代码构造此自定义视图时调用
    override init(frame: CGRect) {       //每一步都必须
        super.init(frame: frame)         //实现父初始化
        innerInit()
    }
    //可视化IB初始化调用
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        innerInit()
    }
    
    func innerInit() {
        initViews()
        initDatas()
        initListeners()
    }
    
    /// 初始化控件
    func initViews(){
        
    }
    
    /// 初始化数据
    func initDatas(){
        
    }
    
    /// 初始化监听器
    func initListeners(){
        
    }

}
