//
//  UserDetailFeedView.swift
//  用户详情动态View
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

import JXPagingView

class UserDetailFeedView: BaseView {
    var contentView: UIView!
    
    //当前界面的UserId
    var userId:String!

    @IBOutlet weak var tableView: UITableView!
    
    var dataArray:[Any]=[]
    
    /// 列表的回调
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    
    override func initViews() {
        super.initViews()
        
        //从Nib加载View
        contentView = Bundle.main.loadNibNamed("UserDetailFeedView", owner: self, options: nil)!.first as! UIView
        
        self.addSubview(contentView)
        
        //设置数据和代理
        tableView.dataSource=self
        tableView.delegate=self
        
        //注册Cell
       tableView.register(UINib(nibName:TopicCell.NAME, bundle:nil), forCellReuseIdentifier: TopicCell.NAME)
    }
    
    /// 自定义初始化方法
    func customInit() {
        initDatas()
        initListeners()
    }
    
    override func initDatas() {
        super.initDatas()
        
        //添加一些测试数据
        //如果要显示真实数据
        //和歌单列表一样
        //直接从网络获取就行了
        for index in 0...100 {
            dataArray.append(Topic())
        }
        
        tableView.reloadData()
    }
    
}

extension UserDetailFeedView: JXPagingViewListViewDelegate {
    /// 返回列表控件
    ///
    /// - Returns: <#return value description#>
    func listView() -> UIView {
        return self
    }
    
    /// 返回内部的滚动控件
    ///
    /// - Returns: <#return value description#>
    func listScrollView() -> UIScrollView {
        return tableView
    }
    
    /// 将回调保存到当前类中
    ///
    /// - Parameter callback: <#callback description#>
    func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

}

extension UserDetailFeedView: UITableViewDataSource, UITableViewDelegate {
    /// 有多少组
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 该组有多个
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME) as! TopicCell
        
        return cell
    }
    
    /// scrollView已经滚动了
    /// 将这个scrollView传递到了PageView的回调中
    /// 它内部可能需要用到一些scrollView的状态
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}
