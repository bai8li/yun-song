//
//  BaseCollectionReusableView.swift
//  通用的CollectionReusableView
//  主要是在父类将初始逻辑拆分为三个方法
//  这样更利于代码组织
//  当然也可以添加一些公用的逻辑
//
//  Created by smile on 2019/5/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseCollectionReusableView: UICollectionReusableView {
    /// 当系统创建这个Nib的时候
    /// 就会调用这个方法
    /// 因为Nib关联了这个类
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //这部分也可以像前面
        //定义父类
        initViews()
        initDatas()
        initListeners()
    }
    
    /// 初始化控件
    func initViews(){
        
    }
    
    /// 初始化数据
    func initDatas(){
        
    }
    
    /// 初始化监听器
    func initListeners(){
        
    }
    
}
