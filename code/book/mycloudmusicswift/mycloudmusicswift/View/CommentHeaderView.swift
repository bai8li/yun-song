
//
//  CommentHeaderView.swift
//  评论分组标题
//
//  Created by smile on 2019/4/17.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class CommentHeaderView: UITableViewHeaderFooterView {
    static var NAME = "CommentHeaderView"
    

    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    func bindData(_ data:String) {
        lbTitle.text=data
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
