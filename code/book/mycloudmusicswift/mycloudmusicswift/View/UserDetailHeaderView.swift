//
//  UserDetailHeaderView.swift
//  用户详情头部
//
//  Created by smile on 2019/4/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class UserDetailHeaderView: BaseView {
    static var NAME = "UserDetailHeaderView"
    
    /// 背景
    @IBOutlet weak var ivBackground: UIImageView!
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 信息
    @IBOutlet weak var lbInfo: UILabel!
    
    /// 关注按钮
    @IBOutlet weak var btFollow: UIButton!
    
    /// 发送消息
    @IBOutlet weak var btSendMessage: UIButton!
    
    var data:User!
    
    /// 关注点击回调方法
    var onFollowClick:((_ data:User) -> Void)!
    
    /// 发送消息点击回调方法
    var onSendMessageClick:((_ data:User) -> Void)!
    
    override func initViews() {
        super.initViews()
        
        //加载Nib
        let nib = UINib(nibName:UserDetailHeaderView.NAME, bundle:nil)

        //实例化View
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        view.frame = self.bounds
        
        addSubview(view)
        
        //配置View
        ViewUtil.showRadius(ivAvatar, 45)
        
        ViewUtil.showRadius(btFollow, 20)
        ViewUtil.showRadius(btSendMessage, 20)
        
        btFollow.showColorPrimaryBorder()
        btSendMessage.showColorPrimaryBorder()
        
        //由于服务端没有实现用户背景
        //所以这里显示一个默认的背景
        ImageUtil.showFull(ivBackground, "http://t2.hddhhn.com/uploads/tu/201707/115/52.jpg")
    }
    
    func bindData(_ data:User)  {
        self.data=data

        ImageUtil.showAvatar(ivAvatar, data.avatar)

        lbNickname.text=data.nickname
        lbInfo.text="关注 \(data.followings_count) | 粉丝 \(data.followers_count)"

        //是否显示关注按钮
        if PreferenceUtil.userId()! == data.id.description {
            //自己
            //隐藏关注按钮
            //隐藏发送消息按钮
            btFollow.isHidden=true
            btSendMessage.isHidden=true
        } else {
            btFollow.isHidden=false
            showFollowStatus()
        }
    }

    func showFollowStatus() {
        if data.isFollowing() {
            //已经关注
            btFollow.setTitle("取消关注", for: .normal)
            
            //弱化取消关注
            btFollow.backgroundColor=UIColor.clear
            
            btSendMessage.isHidden=false
        } else {
            //没有关注
            btFollow.setTitle("关注", for: .normal)
           
            //强化关注
            btFollow.backgroundColor=UIColor(hex: COLOR_PRIMARY)
            
            btSendMessage.isHidden=true
        }
    }
    
    /// 关注按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onFollowClick(_ sender: UIButton) {
        onFollowClick(data)
    }
    
    /// 发送消息按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSendMessageClick(_ sender: UIButton) {
        onSendMessageClick(data)
    }

}
