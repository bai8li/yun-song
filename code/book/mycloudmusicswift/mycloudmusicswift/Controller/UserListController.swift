//
//  UserListController.swift
//  搜索界面用户列表
//
//  Created by smile on 2019/5/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

//导入响应式编程框架
import RxSwift

class UserListController: UITableViewController {
    
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag = DisposeBag()

    /// 当前界面列表数据
    var dataArray:[User]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //注册Cell
        tableView.register(UINib(nibName:UserCell.NAME , bundle: nil), forCellReuseIdentifier: UserCell.NAME)
        
        
        //监听点击了搜索
        SwiftEventBus.onMainThread(self, name: ON_SEARCH_CLICK) { result in
            let data = result!.object as! String
            
            print("UserListController search:\(data)")
            
            self.fetchData(data)
        }
    }
    
    func fetchData(_ data:String) {
        //移除原来的数据
        self.dataArray.removeAll()
        
        //搜索
        Api.shared
            .searcheUsers(data)
            .subscribeOnSuccess { (data) in
                if let data = data?.data{
                    self.dataArray=self.dataArray+data
                    self.tableView.reloadData()
                }
        }.disposed(by: disposeBag)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - TableView数据源
extension UserListController {
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: tableView description
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: UserCell.NAME, for: indexPath) as! UserCell
        
        //取出数据
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
        
        return cell
    }
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data=dataArray[indexPath.row]
        
        UserDetailController.start(navigationController!, userId: data.id)
    }
    
    
}
