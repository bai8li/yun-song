//
//  DownloadController.swift
//  音乐下载界面
//
//  Created by smile on 2019/4/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class DownloadController: BaseTitleController {
    
    /// 下载按钮
    @IBOutlet weak var btDownloadClick: UIButton!
    
    /// 删除按钮
    @IBOutlet weak var btDeleteAllClick: UIButton!
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 下载管理器
    var downloader:DownloadManager!
    
    /// orm工具类
    var orm:ORMUtil!
    
    var hasDownloading:Bool=false
    
    var dataArray:[DownloadInfo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
         setTitle("下载管理")
    }
    
    override func initDatas() {
        super.initDatas()
        
        downloader=DownloadManager.sharedInstance()
        orm=ORMUtil.shared()
        
        fetchData()
    }

    /// 获取数据
    func fetchData() {
        //查询所有未完成下载的任务
        let downloadings=downloader.findAllDownloading() as? [DownloadInfo]

        if downloadings != nil && downloadings!.count > 0 {
            //有下载任务
            
            //判断是否有正在下载的任务
            for downloadInfo in downloadings! {
                if DownloadStatus.downloading==downloadInfo.status {
                    //如果有一个的状态是正在下载
                    //按钮就是暂停所有
                    hasDownloading=true
                }
            }
            
            dataArray=dataArray+downloadings!
        } else {
            //没有下载任务
            //可以在界面上显示一些提示信息
            
        }

        tableView.reloadData()
        
        //设置按钮状态
        setPauseOrResumeStatus()
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听下载完成状态
        //目的是要从当前列表移除
        //因为当前列表中只显示除下载完成的任务
        SwiftEventBus.onMainThread(self, name: DOWNLOAD_STATUS_CHANGED) { result in
            self.dataArray.removeAll()
            self.fetchData()
            
        }
    }
    
    
    /// 暂停所有
    /// 开始所有
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDownloadClick(_ sender: Any) {
        print("DownloadController onDownloadClick")
        //无数据
        //可以按照需求来处理
        //原版是没有下载任务不能进入到该界面
        //我们这里就显示一个提示就行了
        
        if dataArray.count==0 {
            ToastUtil.short("没有下载任务！")
            return
        }
        
        pauseOrResumeAll()
    }
    
    
    /// 删除所有除下载完成的任务
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDeleteClick(_ sender: Any) {
        print("DownloadController onDeleteClick")
        if dataArray.count==0 {
            ToastUtil.short("没有下载任务！")
            return
        }
        
        for data in dataArray {
            //删除所有下载任务
            downloader.remove(data)
        }
        
        //删除当前列表
        dataArray.removeAll()
        
        //列表重新加载数据
        tableView.reloadData()
    }
    
    /// 暂停或者继续下载
    func pauseOrResumeAll() {
        if hasDownloading {
            pauseAll()
            hasDownloading = false
        } else {
            resumeAll()
            hasDownloading = true
        }
        
       setPauseOrResumeStatus()
    }

    /// 设置暂停和继续下载按钮状态
    func setPauseOrResumeStatus() {
        if hasDownloading {
            btDownloadClick.setTitle("全部暂停", for: .normal)
        } else {
            btDownloadClick.setTitle("全部开始", for: .normal)
        }
    }
    
    /// 暂停所有
    func pauseAll() {
        downloader.pauseAll()
        tableView.reloadData()
    }
    
    /// 继续下载所有
    func resumeAll() {
        downloader.resumeAll()
        tableView.reloadData()
    }
 
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension DownloadController{
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "Download") as! DownloadController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}


extension DownloadController: UITableViewDataSource,UITableViewDelegate {
    
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataArray[indexPath.row]
        
        let cell=tableView.dequeueReusableCell(withIdentifier: DownloadCell.NAME, for: indexPath) as! DownloadCell
       
        //绑定数据
        cell.bindData(data)
        
        return cell
    }

    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data=dataArray[indexPath.row]
        
        if DownloadStatus.paused == data.status || DownloadStatus.error == data.status {
            //暂停中
            //下载错误
            
            //都是继续下载
            downloader.resume(data)
        }else {
            //其他情况都是暂停
            downloader.pause(data)
        }
       
    }
    
    // MARK: - 列表编辑相关
    /// 当前index是否可以编辑
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    /// 编辑样式
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        //删除
        return .delete
    }
    
    /// 编辑Button的标题
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "删除"
    }
    
    
    /// 点击按钮后的动作
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - editingStyle: <#editingStyle description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle==UITableViewCell.EditingStyle.delete {
            //删除按钮点击
            let data=dataArray[indexPath.row]
            
            //从下载框架中移除
            downloader.remove(data)
            
            //从当前列表中删除
            dataArray.remove(at: indexPath.row)
            
            //从TableView中删除
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            print("DownloadController delete download success:\(data.uri)")
        }
    }
}
