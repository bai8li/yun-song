//
//  SearchResultController.swift
//  搜索结果页面
//
//  Created by smile on 2019/5/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

//类似Android中ViewPager效果
import Tabman

//Tabman依赖他
import Pageboy

class SearchResultController: TabmanViewController {
    
    /// 要显示的控制器
    private var viewControllers = [SheetController(), UserListController(),UIViewController(), UIViewController(),UIViewController(), UIViewController(),UIViewController()]
    
    
    /// 指示器标题
    private var titles=["歌单", "用户", "单曲", "视频", "歌手", "专辑",  "主播电台"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("SearchResultController viewDidLoad")
        
        //监听点击了搜索
        SwiftEventBus.onMainThread(self, name: ON_SEARCH_CLICK) { result in
            let data = result!.object as! String
            
            print("SearchResultController search:\(data)")
        }
        
        /// 设置代理
        self.dataSource = self
        
        //创建指示器
        let bar = TMBar.ButtonBar()
        
        //自定义按钮
        bar.buttons.customize { (button) in
            //默认颜色
            button.tintColor = .black
            
            //选中颜色
            button.selectedTintColor = UIColor(hex: COLOR_PRIMARY)
        }
        
        //指示器颜色
        bar.indicator.tintColor=UIColor(hex: COLOR_PRIMARY)
        
        //指示器里面的布局排列方式
        bar.layout.transitionStyle = .progressive
        
        //指示器到外部的边距
        bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
        
        //将指示器添加到当前控制器中
        //显示位置是在顶部
        addBar(bar, dataSource: self, at: .top)
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 左右滑动控件数据源和代理
extension SearchResultController: PageboyViewControllerDataSource, TMBarDataSource {
    
    /// 有多个列表
    ///
    /// - Parameter pageboyViewController: <#pageboyViewController description#>
    /// - Returns: <#return value description#>
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    /// 返回当前位置控制器
    ///
    /// - Parameters:
    ///   - pageboyViewController: <#pageboyViewController description#>
    ///   - index: <#index description#>
    /// - Returns: <#return value description#>
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index]
    }
    
    /// 默认页面
    ///
    /// - Parameter pageboyViewController: <#pageboyViewController description#>
    /// - Returns: <#return value description#>
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
    
    /// 当前位置指示器
    ///
    /// - Parameters:
    ///   - bar: <#bar description#>
    ///   - index: <#index description#>
    /// - Returns: <#return value description#>
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        return TMBarItem(title: titles[index])
    }
    
    
}

