//
//  VideoDetailController.swift
//  视频详情界面
//  视频界面之所以不封装播放列表管理器
//  是因为我们这里视频列表没有循环模式
//  所以就不用封装
//  真实项目，或者是专业的播放器应该像音乐管理列表那样封装
//  和复用音乐播放列表
//  可以将音乐播放列表实现为何内容无关的g's
//
//  Created by smile on 2019/4/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入媒体框架
//CMTime在该框架
import CoreMedia

// 键盘管理器
// 目的是键盘弹出时
// 相应的控件自动上移避免挡住
import KeyboardLayoutGuide

//导入系统媒体框架
import MediaPlayer

class VideoDetailController: BaseTitleController {
    
    // MARK: - 控件
    /// 视频容器高度自动布局
    @IBOutlet weak var videoContainerHeight: NSLayoutConstraint!
    
    /// 视频容器
    @IBOutlet weak var videoContainer: UIView!
    
    /// 播放按钮
    @IBOutlet weak var btPlay: UIButton!
    
    /// 重新播放按钮
    @IBOutlet weak var btReplyPlay: UIButton!
    
    /// 进度信息
    @IBOutlet weak var lbInfo: UILabel!
    
    /// 播放文本进度条
    @IBOutlet weak var lbProgress: UILabel!
    
    /// 视频时长
    @IBOutlet weak var lbEnd: UILabel!
    
    /// 横竖屏切换按钮
    @IBOutlet weak var btOrientation: UIButton!
    
    /// 进度条
    @IBOutlet weak var sdProgress: UISlider!
    
    /// 播放控制相关容器
    @IBOutlet weak var controlContainer: UIStackView!
    
    /// 列表视图
    @IBOutlet weak var tableView: UITableView!
    
    /// 文本输入框
    @IBOutlet weak var vwInputContainer: UIView!
    
    /// 输入框边框
    @IBOutlet weak var vwInputBorder: UIView!
    
    /// 评论数图片
    @IBOutlet weak var ivCommentCount: UIImageView!
    
    /// 评论数文本
    @IBOutlet weak var lbCommentCount: UILabel!

    // MARK: - 数据
    var id:String!
    
    var data:Video!
    
    /// 播放管理器
    var videoPlayerManager:VideoPlayerManager!
    
    /// 显示视频画面的layer
    var playLayer:AVPlayerLayer!
    
    /// 是否触摸Slide
    var isTouchSlide = false
    
    var isResumePlay = false
    
    //视频容器的高
    //这个值不是随便写的
    //而是在iPhone尺寸上
    //根据1920*1080视频缩放的近似值
    var SIZE_VIDEO_CONTAIENR_HEIGHT:CGFloat = 210
    
    var dataArray:[VideoDetailGroup] = []
    
    
    /// 评论在tableview的位置
    /// 用来滚动到评论
    var commentPosition:Int=0
    
    /// 异步任务
    var task:DispatchWorkItem?
    
    // MARK: - 初始化方法
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func initViews() {
        super.initViews()
        
        //创建右侧按钮
        //分享
        let shareBarItem = UIBarButtonItem(image: UIImage(named: "Share"), style: .plain, target: self, action: #selector(onShareClick(sender:)))
        
        //更多
        let moreBarItem = UIBarButtonItem(image: UIImage(named: "MoreWhite"), style: .plain, target: self, action: #selector(onMoreClick(sender:)))
        
        //将BarItem设置到导航栏右侧
        //排序为：从右到左排序
        navigationItem.rightBarButtonItems=[moreBarItem,shareBarItem]
        
        //进度条
        sdProgress.minimumTrackTintColor=UIColor(hex: COLOR_PRIMARY)
        sdProgress.thumbTintColor = UIColor(hex: COLOR_PRIMARY)
        
        let thumbImage=UIImage(named: "ProgressThumb")
        sdProgress.setThumbImage(thumbImage, for: .normal)
        sdProgress.setThumbImage(thumbImage, for: .highlighted)
        
        //重播按钮圆角
        ViewUtil.showRadius(btReplyPlay, 15)
        
        //重播按钮边框
        btReplyPlay.showColorPrimaryBorder()

        //因为该界面有播放器
        //所以要支持转屏幕
        AppDelegate.shared.orientationMask=UIInterfaceOrientationMask.all
        
        // 禁用返回手势
        //因为拖动进度条的时候
        //很容易被识别为返回
//        navigationController?.interactivePopGestureRecognizer?.isEnabled=false
        
        //设置输入框圆角
        ViewUtil.showSmallRadius(view: vwInputBorder)
        
        //设置输入框边框
        vwInputBorder.showColorPrimaryBorder()
        
        //添加一条自动布局约束
        //含义是：输入框的底部锚点
        //等于当前界面中View的底部锚点
        //也就说这个框架
        vwInputContainer.bottomAnchor.constraint(equalTo: view.keyboardLayoutGuide.topAnchor).isActive = true
        
        //注册视频Cell详情
        tableView.register(UINib(nibName: VideoDetailInfoCell.NAME, bundle:nil), forCellReuseIdentifier: VideoDetailInfoCell.NAME)
        
        //注册标题详情
        tableView.register(UINib(nibName: TitleTableViewCell.NAME, bundle:nil), forCellReuseIdentifier: TitleTableViewCell.NAME)
        
        //相关视频Cell
        tableView.register(UINib(nibName: VideoDetailVideoCell.NAME, bundle:nil), forCellReuseIdentifier: VideoDetailVideoCell.NAME)
        
        //注册评论Cell
        tableView.register(UINib(nibName: CommentCell.NAME, bundle:nil), forCellReuseIdentifier: CommentCell.NAME)
        
        //自定义section
        //因为默认的UI不太适合我们
        //所以真实项目中大部分情况都要自定义
        tableView.register(UINib(nibName: VideoDetailUserInfoHeaderView.NAME, bundle: nil), forHeaderFooterViewReuseIdentifier: VideoDetailUserInfoHeaderView.NAME)
        
        //初始化视频播放管理器
        videoPlayerManager=VideoPlayerManager.shared
        
        playLayer=videoPlayerManager.playLayer
        
        print("VideoDetailController initViews:\(videoContainer.bounds)")
        
        //将layer添加到当前界面视频容器中
        videoContainer.layer.insertSublayer(playLayer, at: 0)
        
        //设置播放代理
        videoPlayerManager.delegate=self
    }
    
    override func initDatas() {
        super.initDatas()
        
        Api.shared
            .videoDetail(id: id)
            .subscribeOnSuccess { (data) in
                if let data = data?.data {
                    self.showData(data)
                }
            }.disposed(by: disposeBag)
        
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func showData(_ data:Video) {
        self.data=data
        print("VideoDetailController showData:\(data.title)")
        
        //播放视频
        play(data.uri)
        
        //准备其他数据
        //包括：相关视频，评论
        prepareData(data)
        
        //准备隐藏播放控制器栏
        startHideController()
    }
    
    /// 准备数据
    ///
    /// - Parameter data: <#data description#>
    func prepareData(_ data:Video) {
        //组装列表数据
        //第一组视频信息
        let fristGroup=VideoDetailGroup()
        
        //第一组不显示标题
        //后面会设置他的高度为0
        fristGroup.data=nil
        
        fristGroup.datas=[data]
        
        dataArray.append(fristGroup)
        
        //第二组
        let secondGroup=VideoDetailGroup()
        secondGroup.data=data.user
        secondGroup.datas=[]
        
        dataArray.append(secondGroup)
        
        secondGroup.datas.append("相关推荐")
        
        //相关推荐
        //由于服务端没有接口所以直接请求视频列表
        Api.shared
            .videos()
            .subscribeOnSuccess { (data) in
                if let data = data?.data {
                    print("VideoDetailController video list success:\(data.count)")
                    
                    secondGroup.datas=secondGroup.datas+data
                    
                    
                    //保存评论位置，根据自己的业务逻辑计算
                    self.commentPosition=1+data.count
                    
                    //添加评论标题
                    secondGroup.datas.append("最热评论")
                    
                    //传递视频Id
                    //获取该视频下的评论
                    //但由于服务端没有实现
                    //所以这里就不传递了
                    
                    //热门评论
                    Api.shared
                        .comments()
                        .subscribeOnSuccess { data in
                            if let data=data?.data {
                                print("VideoDetailController comment list success:\(data.count)")
                                
                                secondGroup.datas=secondGroup.datas+data
                                
                                //显示评论数
                                self.lbCommentCount.text="\(secondGroup.datas.count)"
                                
                                self.tableView.reloadData()
                            }
                    }.disposed(by: self.disposeBag)
                    
                    
                }
            }.disposed(by: disposeBag)
        
    }
    
    override func initListeners() {
        super.initListeners()
        
        //开启监听设备旋转的通知
        //不开启的话设备方向一直是UIInterfaceOrientationUnknown
        if !UIDevice.current.isGeneratingDeviceOrientationNotifications {
            UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        }
        
        //添加屏幕方向变化监听
        NotificationCenter.default.addObserver(self, selector: #selector(onDeviceOrientationChanged(notification:)), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        // 应用切换到后台
        // 也就是按home键
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        // 应用切换到前台
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    /// 进入后台
    @objc func didEnterBackground() {
        print("VideoDetailController didEnterBackground")
        checkIfPause()
    }
    
    /// 进入前台
    @objc func didBecomeActive() {
        print("VideoDetailController didBecomeActive")
        checkIfResume()
    }
    
    /// 已经重新布局
    /// 当切换横竖屏时
    /// 该方法也会调用
    /// 所以直接在该方法中更改播放视频的layer
    /// 就能完成视频播放横竖屏切换
    override func viewDidLayoutSubviews() {
        //重新设置PageView的尺寸和位置
        playLayer.frame = videoContainer.bounds
        
        print("VideoDetailController viewDidLayoutSubviews:\(videoContainer.bounds)")
    }
    
    //状态栏样式
    //虽然这里用了导航栏
    //已经设置了导航栏为白色
    //但当隐藏了导航栏
    //状态栏就为黑色
    //所以还需要再次设置
    override var preferredStatusBarStyle: UIStatusBarStyle{
        //内容为白色
        return UIStatusBarStyle.lightContent
    }
    
    // MARK: - 导航栏右侧按钮
    /// 点击右侧分享按钮
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onShareClick(sender:UIButton) {
        print("PlayerController onShareClick")
    }
    
    /// 点击右侧更多按钮
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onMoreClick(sender:UIButton) {
        print("PlayerController onMoreClick")
    }
    
    // MARK: - 播放相关
    func checkIfPause() {
        if videoPlayerManager.isPlaying() {
            
            isResumePlay=true
            
            //暂停
            pause()
        }
    }
    
    /// 是否需要自动恢复
    func checkIfResume() {
        //判断是否需要自动恢复播放
        if isResumePlay {
            resume()

            isResumePlay=false
        }
    }

    /// 视图即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //设置导航栏样式
        //这里将导航栏背景设置为黑色
        //这样状态栏（有了导航栏，只能通过这种方式设置状态栏颜色）
        //标题就会变成白色
        navigationController!.navigationBar.barStyle=UIBarStyle.black
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.white]
        
        
        //设置返回按钮为白色
        navigationController!.navigationBar.tintColor=UIColor.white
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        
        //去掉导航栏下面的阴影
        //如果大家需要可以留着
        //这个导航栏有层次感
        navigationController?.navigationBar.shadowImage=UIImage()
        
        checkIfResume()
    }
    
    /// 视图即将消失
    ///
    /// - Parameter animated: <#animated description#>
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        checkIfPause()
        
        //取消播放器代理
        videoPlayerManager.delegate=nil
        
        //禁用转屏幕
        AppDelegate.shared.orientationMask=UIInterfaceOrientationMask.portrait
        
        //恢复状态栏颜色
        //因为其他界面可能当前界面样式不一样
        
        //标题就会变成黑色
        navigationController!.navigationBar.barStyle=UIBarStyle.default
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.black]
        
        //设置返回按钮为黑色
        navigationController!.navigationBar.tintColor=UIColor.black
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    deinit {

        //取消倒计时任务
        cancelTask()
    }
    
    /// 播放视频
    ///
    /// - Parameter data: <#data description#>
    func play(_ data:String) {
        //播放本地视频
        //视频文件放到项目根目录
//                let localVideoPath=Bundle.main.path(forResource: "ixueaedu", ofType: ".mp4")!
//
//        videoPlayerManager.play(localVideoPath, self.data)
        
        //播放在线视频
        videoPlayerManager.play(ResourceUtil.resourceUri(data), self.data)
        
        //显示暂停状态
        showPauseStatus()
    }
    
    /// 播放或暂停
    func playOrPause() {
        if videoPlayerManager.isPlaying() {
            pause()
        } else {
            resume()
        }
    }
    
    /// 暂停
    func pause() {
        videoPlayerManager.pause()
    }
    
    /// 显示暂停状态
    func showPauseStatus() {
        btPlay.setImage(UIImage(named: "VideoPause"), for: .normal)
        btReplyPlay.isHidden=true
        lbInfo.isHidden=true
    }
    
    /// 显示播放状态
    func showPlayStatus() {
        btPlay.setImage(UIImage(named: "VideoPlay"), for: .normal)
    }
    
    /// 继续播放
    func resume() {
        videoPlayerManager.resume()
    }
    
    /// 显示或者隐藏控制栏
    func showOrHideController() {
        if controlContainer.isHidden {
            
            if data.progress != data.duration {
                //播放完毕了
                //就不显示控制栏
                controlContainer.isHidden=false
                sdProgress.isHidden=false
                btPlay.isHidden=false
            }
            
            //显示导航栏
            navigationController!.setNavigationBarHidden(false, animated: false)
            
            startHideController()
        } else {
            hideController()
        }
        
    }
    
    /// 开启隐藏播放相关控制
    func startHideController() {
        //如果要取消异步任务就需要这样使用
        //创建一个任务
        task = DispatchWorkItem {
            //这里是异步任务执行时回调
            self.hideController()
        }
        
        //5秒钟后隐藏播放相关控制
        //添加到异步队列中
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: task!)
    }
    
    /// 隐藏状态栏
    func hideController() {
        controlContainer.isHidden=true
        sdProgress.isHidden=true
        btPlay.isHidden=true
        
        //隐藏导航栏
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        cancelTask()
    }
    
    /// 取消倒计时任务
    func cancelTask() {
        if let task = task {
            task.cancel()
        }
        
        task=nil
    }
    
    // MARK: - 播放按钮
    @IBAction func onPlayClick(_ sender: Any) {
        print("VideoDetailController onPlayClick")
        playOrPause()
    }
    
    
    /// 重新播放
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onReplyPlayClick(_ sender: UIButton) {
        print("VideoDetailController onReplyPlayClick")
        play(data.uri)
    }
    
    
    /// 设置屏幕方向
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onOrientationClick(_ sender: Any) {
        print("VideoDetailController onOrientationClick")
        let deviceOrientation=UIDevice.current.orientation
        if UIDeviceOrientation.portrait==deviceOrientation {
            //竖屏的时候点击
            //就切换为横屏
            
            //向左旋转
            //当然也可以设置为向右旋转
            setOrientation(UIDeviceOrientation.landscapeLeft)
        } else {
            //其他方向
            //统一设置为竖屏
            setOrientation(UIDeviceOrientation.portrait)
        }
    }
    
    
    /// 播放控制层点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPlayControlClick(_ sender: Any) {
        print("VideoDetailController onPlayControlClick")
        showOrHideController()
    }
    
    // MARK: - 进度条相关
    @IBAction func onSlideTouchDown(_ sender: UISlider) {
        isTouchSlide=true
    }
    
    @IBAction func onProgressChanged(_ sender: UISlider) {
        //显示用户拖拽的位置
        //这样用户好确定拖拽到那个时间点
        showProgress(sender.value)
    }
    
    
    @IBAction func onSlideTouchUp(_ sender: UISlider) {
        isTouchSlide=false
        
        if sender.value>1 {
            videoPlayerManager.seekTo(sender.value-1)
        }
    }
    
    /// 显示播放进度
    ///
    /// - Parameter data: <#data description#>
    func showProgress(_ data:Float) {
        sdProgress.setValue(data, animated: true)
        let start=TimeUtil.formatTime1(data)
        lbProgress.text="\(start)"
    }
    
    
    /// 播放失败显示
    func onPlayFailed() {
        btReplyPlay.isHidden=true
        lbInfo.isHidden=true
        
        btReplyPlay.setTitle("点击重试", for: .normal)
        lbInfo.text="视频播放失败!"
    }
    
    // MARK: - 列表控制按钮
    @IBAction func onListControlClick(_ sender: Any) {
        print("VideoDetailController onListControlClick")
        
        var indexPath:IndexPath!
        
        if lbCommentCount.isHidden {
            //评论隐藏了
            //表示是返回到顶部
            indexPath=IndexPath(item: 0, section: 0)
            
        } else {
            //滚动到评论
            indexPath=IndexPath(item: commentPosition, section: 1)
        }
        
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
 
    }
    
    /// 显示滚动到定顶部UI效果
    func showScrollTopUI() {
        //显示返回到顶部
        lbCommentCount.isHidden=true
        ivCommentCount.image=UIImage(named: "BackTop")
    }
    
    /// 滚动到评论
    func showScrollTopComment() {
        //显示评论信息
        lbCommentCount.isHidden=false
        ivCommentCount.image=UIImage(named: "CommentCountText")
    }

    // MARK: - 屏幕旋转
    //设备方向改变的处理
    @objc func onDeviceOrientationChanged( notification:NSNotification) {
        //获取屏幕方向
        let deviceOrientation=UIDevice.current.orientation
        switch deviceOrientation {
        case UIDeviceOrientation.landscapeLeft:
            print("屏幕向左横置")
            onDeviceOrientationLandscape()
        case UIDeviceOrientation.landscapeRight:
            print("屏幕向右橫置")
            onDeviceOrientationLandscape()
        case UIDeviceOrientation.portrait:
            print("屏幕直立")
            onDeviceOrientationPortrait()
        default:
            break
        }
    }
    
    
    /// 横屏状态
    /// 横屏状态下
    /// 状态栏会自动隐藏
    func onDeviceOrientationLandscape() {
        //横屏下
        //视频容器的高度就是view的高度
        videoContainerHeight.constant=view.frame.height
        
        //设置图标为切换到竖屏图标
        btOrientation.setImage(UIImage(named: "NormalScreen"), for: .normal)
    }
    
    /// 竖屏状态下
    func onDeviceOrientationPortrait() {
        //更改视频容器高度
        videoContainerHeight.constant=SIZE_VIDEO_CONTAIENR_HEIGHT
        
        //设置图标为切换到横屏图标
        btOrientation.setImage(UIImage(named: "FullScreen"), for: .normal)
    }
    
    /// 设置屏幕方向
    ///
    /// - Parameter data: <#data description#>
    func setOrientation(_ data:UIDeviceOrientation) {
        UIDevice.current.setValue(data.rawValue, forKey: "orientation")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - 启动界面
extension VideoDetailController{
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ id:String) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "VideoDetail") as! VideoDetailController
        
        controller.id=id
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}

// MARK: - 列表数据源
extension VideoDetailController: UITableViewDataSource,UITableViewDelegate {
    
    /// 分组数量
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data=dataArray[section]
        return data.datas.count
    }
    
    /// 返回自定义的section(header)
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let group = dataArray[section]
        
        let cell=tableView.dequeueReusableHeaderFooterView(withIdentifier: VideoDetailUserInfoHeaderView.NAME) as! VideoDetailUserInfoHeaderView
        
        if let data=group.data {
            cell.bindData(data)
        }
        
        return cell
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let group=dataArray[indexPath.section]
        let data=group.datas[indexPath.row]
        
        let type=typeForItemAtIndex(indexPath)
        
        //根据Cell类型创建Cell
        switch type {
        case .videoInfo:
            let cell=tableView.dequeueReusableCell(withIdentifier: VideoDetailInfoCell.NAME, for: indexPath) as! VideoDetailInfoCell
            
            //绑定数据
            cell.bindData(data as! Video)
            
            return cell
        case .title:
            let cell=tableView.dequeueReusableCell(withIdentifier: TitleTableViewCell.NAME, for: indexPath) as! TitleTableViewCell
             
            //绑定数据
            cell.bindData(data as! String)
            
            return cell
        case .video:
            let cell=tableView.dequeueReusableCell(withIdentifier: VideoDetailVideoCell.NAME, for: indexPath) as! VideoDetailVideoCell
            
            //绑定数据
            cell.bindData(data as! Video)
            
            return cell
        default:
            let cell=tableView.dequeueReusableCell(withIdentifier: CommentCell.NAME, for: indexPath) as! CommentCell
            
            //绑定数据
            cell.bindData(data as! Comment)
            
            return cell
        }
        

    }
    
    /// 获取列表类型
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    func typeForItemAtIndex(_ indexPath:IndexPath) -> CellType {
        let group=dataArray[indexPath.section]
        let data=group.datas[indexPath.row]
        
        if indexPath.section==0 && indexPath.row==0 {
            //第一组
            //是视频信息
            return .videoInfo
        }else if data is String {
            //标题
            return .title
        }else if data is Video {
            //相关视频
            return .video
        }
        
        //TODO 更多的类型在这里扩展就行了
        
        ////评论
        return .comment
    }
    
    /// 头部高度
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section==1 {
            //只有用户信息才有高度
            return 50
        }
        
        //其他的组不显示section
        return 0
    }

    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let group=dataArray[indexPath.section]
        let data=group.datas[indexPath.row]
        
        let type=typeForItemAtIndex(indexPath)
        if CellType.video==type {
            //相关视频点击
            let video=data as! Video
            play(video.uri)
        }
        
        //TODO 其他类型条目点击
        
    }
    
    /// 滚动中
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //获取滚动距离
        //如果大于100就显示返回到顶部
        //该值可以根据业务逻辑更改
        let offsetY  = scrollView.contentOffset.y
        if offsetY > 100 {
            showScrollTopUI()
        } else {
            showScrollTopComment()
        }
        
    }
    
    /// Cell类型
    /// 这是一个枚举
    enum CellType {
        //视频信息
        //就是显示视频名称那部分
        case videoInfo
        
        //标题
        //相关推荐
        //精彩评论
        case title
        
        //相关视频
        case video
        
        //评论
        case comment
    }
}

// MARK: - 视频播放管理器代理
extension VideoDetailController:VideoPlayerDelegate{
    /// 进度回调
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - progress: <#progress description#>
    ///   - duration: <#duration description#>
    func onProgress(data: Video, progress: Float, duration: Float) {
        print("VideoDetailController onProgress:\(progress),\(duration)")
        showProgress(data.progress)
    }
    
    /// 暂停了播放
    ///
    /// - Parameter data: <#data description#>
    func onPaused(_ data: Video) {
        print("VideoDetailController onPaused:\(data.title)")
        showPlayStatus()
    }
    
    /// 正在播放
    ///
    /// - Parameter data: <#data description#>
    func onPlaying(_ data: Video) {
        print("VideoDetailController onPlaying:\(data.title)")
        showPauseStatus()
    }
    
    /// 播放器准备完毕；可以获取到音乐市场
    ///
    /// - Parameter data: <#data description#>
    func onPrepared(_ data: Video) {
        print("VideoDetailController onPrepared:\(data.title)")

        sdProgress.maximumValue=data.duration
        lbEnd.text="/\(TimeUtil.formatTime1(data.duration))"
        
        //开启滑块的滑动功能
        sdProgress.isEnabled = true
    }
    
    /// 播放发生了错误
    ///
    /// - Parameter data: <#data description#>
    func onError(_ data: Video, _ error: Error?) {
        print("VideoDetailController onError:\(data.title),\(error)")
        
        onPlayFailed()
    }
    
    /// 播放完毕了
    ///
    /// - Parameter data: <#data description#>
    func onComplete(_ data: Video) {
        //真实项目中
        //播放完毕了
        //可能会自动播放下一首
        //和音乐差不多
        //这里就不实现了
        print("VideoDetailController onComplete:\(data.title)")
        
        hideController()
        btReplyPlay.isHidden=false
        btReplyPlay.setTitle("重新播放", for: .normal)
    }
    
    
}
