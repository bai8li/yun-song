//
//  SplashController.swift
//  第二个启动界面，例如：可以添加判断用户是否登陆从而跳转不同的界面，判断是否显示引导界面，获取广告等功能
//
//  Created by smile on 2019/4/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class SplashController: BaseCommonController {
    
    /// 加载视图成功后调用该方法
    /// 类似Android中Activity的onCreate方法
    override func viewDidLoad() {
        super.viewDidLoad()

        //        延时3秒钟
        //        时间大家可以根据业务需求调整
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            self.next()
        }
        
        
////        测试使用偏好设置
////        获取偏移设置对象
//        let userDefaults=UserDefaults.standard
//
////        保存一个字符串”我们是爱学啊“
////        存储的键为：name
//        userDefaults.set("我们是爱学啊", forKey: "username")
//
////        通过键找到上面存储的值
//        var username=userDefaults.string(forKey: "username")
//
////        打印出来，方便调试
//        print("第一次获取的值：",username)
//
////        删除该key对应的值
//        userDefaults.removeObject(forKey: "username")
//
////        再次获取
//        username=userDefaults.string(forKey: "username")
//
//        print("删除后再次获取的值：",username)
        
    }
   
    
    /// 延时3后调用该方法
    func next() {
        print("SplashController next")
        
//        获取到当前应用的AppDelegate对象
//        全局只有一个
        
//        这里会报错找不到AppDelegate，是正常现象
//        可能是Xcode Bug，但可以正常运行
//        let appDelegate=UIApplication.shared.delegate as! AppDelegate
        
        
//        调用它里面的方法显示引导界面
//        appDelegate.toGuide()
        
        if PreferenceUtil.isShowGuide() {
//            if true {
//            显示引导页
//                    使用自定义计算属性
            AppDelegate.shared.toGuide()
        }else if PreferenceUtil.isLogin(){
            //已经登陆了

            //就显示广告页面；在广告页面再进入主界面
            //可以根据自己的需求来更改
            //同时只有用户登陆了
            //才显示也给用户有更好的体验
            AppDelegate.shared.toAd()

            //直接进入首页
//            AppDelegate.shared.toHome()
        }else{
            //登陆/注册页面
            AppDelegate.shared.toLoginOrRegister()
        }
    }
    
    //状态栏样式
//    override var preferredStatusBarStyle: UIStatusBarStyle{
//        //内容为白色
//        return UIStatusBarStyle.lightContent
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - 页面统计
extension SplashController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //使用极光分析
        //统计页面
        JANALYTICSService.startLogPageView("Splash")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //页面结束
        JANALYTICSService.stopLogPageView("Splash")
    }
}
