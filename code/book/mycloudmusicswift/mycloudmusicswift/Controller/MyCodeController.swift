//
//  MyCodeController.swift
//  我的二维码界面
//
//  Created by smile on 2019/4/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class MyCodeController: BaseTitleController {

    /// 内容容器
    @IBOutlet weak var contentContainer: UIView!
    
    /// 用户头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 显示我的二维码图片
    @IBOutlet weak var ivCode: UIImageView!
    
    override func initViews() {
        super.initViews()
        
        setTitle("我的二维码")
        
        ViewUtil.showSmallRadius(view: contentContainer)
    }
    
    override func initDatas() {
        super.initDatas()
        
        fetchData()
    }
    
    func fetchData() {
        Api.shared
            .userDetail(PreferenceUtil.userId()!)
            .subscribeOnSuccess { data in
                if let data = data?.data {
                    self.showData(data)
                }
        }.disposed(by: disposeBag)
        
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func showData(_ data:User!) {
        /// 显示头像
        ImageUtil.showAvatar(ivAvatar, data.avatar)
        
        /// 显示昵称
        lbNickname.text=data.nickname
        
        //我们这里的二维码的数据
        //就是一个网址
        //真实的数据在网址的查询参数里面
        //http://dev-my-cloud-music-api-rails.ixuea.com/v1/monitors/version?u=
        let qrCodeData="\(QRCODE_URL)\(data!.id!)"
        
        //显示二维码
        showCode(qrCodeData)
    }
    
    /// 显示二维码
    ///
    /// - Parameter data: <#data description#>
    func showCode(_ data:String) {
        //这就是这个二维码框架的固定写法
        
        //根据数据创建一个二维码Image
        let qrImg = LBXScanWrapper.createCode(codeType: "CIQRCodeGenerator", codeString: data, size: ivCode.bounds.size, qrColor: UIColor.black, bkColor: UIColor.white)
        
        //获取应用的图标
        let logoImg = UIImage(named: "AppIcon")
        
        //将应用图标嵌入到二维码中
        ivCode.image = LBXScanWrapper.addImageLogo(srcImg: qrImg!, logoImg: logoImg!, logoSize: CGSize(width: 20, height: 20))
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //恢复状态栏颜色
        //因为其他界面可能当前界面样式不一样
        
        //标题就会变成黑色
        navigationController!.navigationBar.barStyle=UIBarStyle.default
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.black]
        
        //设置返回按钮为黑色
        navigationController!.navigationBar.tintColor=UIColor.black
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    /// 视图即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置导航栏样式
        //这里将导航栏背景设置为黑色
        //这样状态栏（有了导航栏，只能通过这种方式设置状态栏颜色）
        //标题就会变成白色
        navigationController!.navigationBar.barStyle=UIBarStyle.black
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.white]
        
        
        //设置返回按钮为白色
        navigationController!.navigationBar.tintColor=UIColor.white
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        
        //去掉导航栏下面的阴影
        //如果大家需要可以留着
        //这个导航栏有层次感
        navigationController?.navigationBar.shadowImage=UIImage()
    }

}

// MARK: - 启动界面
extension MyCodeController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "MyCode") as! MyCodeController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
