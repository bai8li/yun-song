//
//  SelectLyricController.swift
//  选择歌词界面
//
//  Created by smile on 2019/4/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SelectLyricController: BaseTitleController {

    
    /// 背景
    @IBOutlet weak var ivBackground: UIImageView!
    
    /// 列表
    @IBOutlet weak var tableView: UITableView!
    
    
    /// 分享歌词文本
    @IBOutlet weak var btShareLyric: UIButton!
    
    /// 分享歌词图片
    @IBOutlet weak var btShareLyricImage: UIButton!
    
    /// 分享歌词视频
    @IBOutlet weak var btShareLyricVideo: UIButton!
    
    var dataArray:[LyricLine] = []
    
    var song:Song!
    
    var lyric:Lyric!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        setTitle("分享歌词")
        
        //设置控件圆角
        ViewUtil.showRadius(btShareLyric, 15)
        ViewUtil.showRadius(btShareLyricImage, 15)
        ViewUtil.showRadius(btShareLyricVideo, 15)
        
        //设置控件边框
        btShareLyric.showColorPrimaryBorder()
        btShareLyricImage.showColorPrimaryBorder()
        btShareLyricVideo.showColorPrimaryBorder()
    }
    
    override func initDatas() {
        super.initDatas()
        
        ImageUtil.show(ivBackground, song.banner)
        
        //添加数据到列表
        dataArray=dataArray+lyric.datas
        
        tableView.reloadData()
    }
    
    /// 分享歌词点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onShareLyricClick(_ sender: UIButton) {
        
        let lyrics=getSelectLyricString("，")
        
        print("select lyric controller share text:\(lyrics)")
        
        if lyrics.isEmpty {
            ToastUtil.short("请选择歌词！")
            return
        }
       
        //分享
        ShareUtil.shareLyricText(song,lyrics)
    }
    
    /// 分享歌词图片点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onShareLyricImageClick(_ sender: UIButton) {
        print("select lyric controller share image");
        
        let lyrics=getSelectLyricString("\n")
        
        if lyrics.isEmpty {
            ToastUtil.short("请选择歌词！")
            return
        }
        
        ShareLyricImageController.start(self.navigationController!,song,lyrics)
    }
    
    /// 获取选择的歌词
    ///
    /// - Parameter separator: <#separator description#>
    /// - Returns: <#return value description#>
    func getSelectLyricString(_ separator:String) -> String {
        //将歌词拼接成字符串分享字符串

        var lyricStringArray:[String]=[]
        
        //获取选中的歌词索引
        var indexPaths=tableView.indexPathsForSelectedRows
        
        if indexPaths == nil {
            return ""
        }
        
        //由于该方法获取的indexPath是选择顺序
        //而我们需要的是列表顺序
        //所以按照row从小到大排序
        indexPaths=indexPaths?.sorted(by: { (obj1, obj2) -> Bool in
            return obj1.row<obj2.row
        })
        
        //遍历
        for indexPath in indexPaths! {
            let line=dataArray[indexPath.row]
            
            //将字符串添加到数组中
            lyricStringArray.append(line.data)
        }
  
        //使用分隔符连接字符串
        return lyricStringArray.joined(separator: separator)
    }
    
    /// 视图即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置导航栏样式
        //这里将导航栏背景设置为黑色
        //这样状态栏（有了导航栏，只能通过这种方式设置状态栏颜色）
        //标题就会变成白色
        navigationController!.navigationBar.barStyle=UIBarStyle.black
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.white]
        
        
        //设置返回按钮为白色
        navigationController!.navigationBar.tintColor=UIColor.white
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
    }
    
    /// 视图即将消失
    ///
    /// - Parameter animated: animated description
    override func viewWillDisappear(_ animated: Bool) {
        //恢复状态栏颜色
        //因为其他界面可能当前界面样式不一样
        
        //标题就会变成黑色
        navigationController!.navigationBar.barStyle=UIBarStyle.default
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.black]
        
        //设置返回按钮为黑色
        navigationController!.navigationBar.tintColor=UIColor.black
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ song:Song,_ lyric:Lyric) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "SelectLyric") as! SelectLyricController
        
        //将传递过来的数据设置到controller中
        controller.song=song
        controller.lyric=lyric
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: false)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension SelectLyricController: UITableViewDataSource,UITableViewDelegate {
    
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: SelectLyricCell.NAME, for: indexPath) as! SelectLyricCell
        
        //取出数据
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
        
        return cell
    }
}
