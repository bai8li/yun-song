//
//  ShopDetailController.swift
//  商品详情界面
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class ShopDetailController: BaseTitleController {
    
    /// 商品图片
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 价格
    @IBOutlet weak var lbPrice: UILabel!
    
    /// 控制按钮
    @IBOutlet weak var btControl: UIButton!
    
    var id:String!
    
    var data:Book!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        setTitle("商品详情")
        
        ViewUtil.showSmallRadius(view: ivBanner)
    }
    
    
    override func initDatas() {
        super.initDatas()
        
        Api.shared
            .shopDetail(id)
            .subscribeOnSuccess { (data) in
                if  let data = data?.data {
                    self.showData(data)
                }
            }.disposed(by: disposeBag)
        
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听支付成功
        SwiftEventBus.onMainThread(self, name: ON_PAY_SUCCESS) { result in
            self.showBuySuccess()
        }
    }
    
    func showData(_ data:Book) {
        self.data=data
        
        ImageUtil.show(ivBanner, data.banner)
        lbTitle.text=data.title
        lbPrice.text="优惠价: ￥\(data.price!)"
        
        if data.isBuy() {
            showBuySuccess()
        }
        
    }
    
    /// 显示购买成功的样式
    func showBuySuccess() {
        //已经购买了
        btControl.setTitle("已经购买了", for: .normal)
        btControl.setTitleColor(UIColor(named: "ColorPass"), for: .normal)
    }

    /// 购买按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onControlClick(_ sender: UIButton) {
        print("ShopDetailController onControlClick")
        
        if data.isBuy() {
            //已经购买了
            ToastUtil.short("已经购买了！")
            
            //真实项目中
            //如果是虚拟物品
            //例如：电子书，视频，课程
            //在这里就是学习入口
            
            //当然为了安全
            //就算这里是学习入口
            //里面接口也要判断
            //只有服务端判断是购买了才返回数据
            //好处是如果有人破解了客户端
            //能进入学习界面
            //但里面的接口不会返回数据
        } else {
            //创建订单
            //跳转到订单详情
            createOrder()
        }
    }
    
    /// 创建订单
    func createOrder() {
        Api.shared
            .createOrder(data.id)
            .subscribeOnSuccess { (data) in
                if let data = data?.data {
                    OrderDetailController.start(self.navigationController!, data.id)
                }
        }.disposed(by: disposeBag)
        
    }
    


}

// MARK: - 启动界面
extension ShopDetailController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ id:String) {
        //创建控制器
                let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "ShopDetail") as! ShopDetailController
 
        controller.id=id
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
