//
//  LocalMusicController.swift
//  下载完成的音乐
//
//  Created by smile on 2019/4/29.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class LocalMusicController: BaseTitleController {
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 控件容器
    @IBOutlet weak var buttonContainer: UIStackView!
    
    /// 全选按钮
    @IBOutlet weak var btSelectAll: UIButton!
    
    /// 删除按钮
    @IBOutlet weak var btDelete: UIButton!
    
    /// 播放列表管理器
    var playListManager:PlayListManager!
    
    /// 下载管理器
    var downloader:DownloadManager!
    
    var dataArray:[DownloadInfo] = []
    
    /// 右上角编辑按钮
    var editBarItem:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        setTitle("本地音乐")
        
        //创建右侧编辑按钮
        editBarItem=UIBarButtonItem(title: "编辑", style: .plain, target: self, action: #selector(onEditClick(sender:)))
        
        //将BarItem设置到导航栏右侧
        navigationItem.rightBarButtonItem=editBarItem
        
    }
    
    /// 编辑点击
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onEditClick(sender:UIButton) {
        print("LocalMusicController onEditClick")
        if dataArray.count == 0{
            //没有数据
            ToastUtil.short("没有本地音乐！")
            return
        }
        
        if tableView.isEditing {
            //如果已经在编辑模式下
            //就是退出
            exitEditMode()
        } else {
            //进入编辑模式
            editBarItem.title="取消"
            
            tableView.setEditing(true, animated: true)
            buttonContainer.isHidden=false
            
        }
    }
    
    /// 退出编辑模式
    func exitEditMode() {
        editBarItem.title="编辑"
        
        //退出编辑模式
        tableView.setEditing(false, animated: true)
        buttonContainer.isHidden=true
        
        //按钮重置为默认状态
        defaultButtonStatus()
    }
    
    override func initDatas() {
        super.initDatas()
        
        //创建播放列表管理器
        playListManager=PlayListManager.shared
        
        //获取下载管理器
        downloader=DownloadManager.sharedInstance()
        
        fetchData()
    }
    
    func fetchData() {
        //查询所有未完成下载的任务
        let datas=downloader.findAllDownloaded() as? [DownloadInfo]
        
        if datas != nil && datas!.count > 0 {
            //有下载任务
            dataArray=dataArray+datas!
            
             tableView.reloadData()
        } else {
            //没有下载任务
            //可以在界面上显示一些提示信息
            
        }
        
    }
    
    /// 显示按钮状态
    func showButtonStatus() {
        if isSelected(){
            //有选择项目
            btSelectAll.setTitle("取消全选", for: .normal)
            
            btDelete.isEnabled=true
        }else{
            defaultButtonStatus()
        }
    }
    
    /// 是否有选中的Cell
    ///
    /// - Returns: <#return value description#>
    func isSelected() -> Bool {
        return tableView.indexPathsForSelectedRows != nil
    }
    
    /// 默认按钮状态
    func defaultButtonStatus() {
        //没有选择项目
        btSelectAll.setTitle("全选", for: .normal)
        
        btDelete.isEnabled=false
    }
    
    /// 选择所有按钮点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSelectAllClick(_ sender: Any) {
        print("LocalMusicController onSelectAllClick")
        
        for i in 0..<dataArray.count {
            //变量所有数据
            let indexPath=IndexPath(item: i, section: 0)
            
            if isSelected() {
                //有选中的Cell
                //就是取消选择
                tableView.deselectRow(at: indexPath, animated: true)
            }else{
                //否则就是选择
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
            }
        }
        
        //选择后
        //还要更新按钮状态
        showButtonStatus()
    }
    
    
    /// 删除按钮点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onDeleteClick(_ sender: Any) {
        print("LocalMusicController onDeleteClick")
        
        //因为判断了
        //所有调用这个方法的时候
        //一定有选择的条目
        let indexPaths=tableView.indexPathsForSelectedRows!
        
        for indexPath in indexPaths {
            let downloadInfo=dataArray[indexPath.row]
            
            //从下载框架中删除
            downloader.remove(downloadInfo)
            
            //从数据源中删除
            dataArray.remove(at: indexPath.row)
            
        }
        
        //从新加载数据源
        tableView.reloadData()
        
        //退出编辑模式
        exitEditMode()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension LocalMusicController{
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "LocalMusic") as! LocalMusicController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}

extension LocalMusicController: UITableViewDataSource,UITableViewDelegate {
    
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataArray[indexPath.row]
        
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath)
        
        //设置Tag
        //目的是Cell中显示位置
        //还有点击Cell更多按钮时知道是点击了那个Cell
        cell.tag=indexPath.row
        
        //绑定数据
        
        //根据Id获取业务数据
        let song=ORMUtil.shared().findSongByUri(data.id)
        
        if let song = song {
            //设置标题
            cell.textLabel!.text=song.title
            
            //设置歌手名称
            cell.detailTextLabel!.text=song.singer.nickname
        }else{
            //TODO 如果为空就表示逻辑有问题
        }
        
        return cell
    }
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("LocalMusicController didSelectRowAt:\(indexPath)")
        
        let data=dataArray[indexPath.row]
        
        if tableView.isEditing {
            //如果是编辑状态
            //就不进入播放页面
            showButtonStatus()
            return
        }
        
        //播放
        
        //这里是将点击的这首音乐替换播放列表
        //如果点击一首音乐后
        //要将所有下载的音乐都设置到播放列表
        //则需要遍历所有的下载音乐转为song
        //因为播放列表需要的是song对象
        
        //根据Id获取业务数据
        let song=ORMUtil.shared().findSongByUri(data.id)!
        
        playListManager.setPlayList([song])
        playListManager.play(song)
        
        //跳转到播放界面
        PlayerController.start(navigationController!)
    }
    
    /// 取消选择
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("LocalMusicController didDeselectRowAt:\(indexPath)")
        showButtonStatus()
    }
    
    // MARK: - 列表编辑相关
    
    /// 当前index是否可以编辑
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    
    /// 编辑样式
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if tableView.isEditing {
            //如果在编辑模式下
            
            //要想左侧显示的是多选按钮
            //这里需要返回delete和insert
            //如果只返回delete
            //那么左侧显示的是一个减号
            return UITableViewCell.EditingStyle(rawValue: UITableViewCell.EditingStyle.delete.rawValue | UITableViewCell.EditingStyle.insert.rawValue)!
        } else {
            //删除
            return .delete
        }
        
    }
    
    /// 编辑Button的标题
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "删除"
    }
    
    
    /// 点击按钮后的动作
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - editingStyle: <#editingStyle description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle==UITableViewCell.EditingStyle.delete {
            //删除按钮点击
            let data=dataArray[indexPath.row]
            
            //从下载框架中移除
            downloader.remove(data)
            
            //从当前列表中删除
            dataArray.remove(at: indexPath.row)
            
            //从TableView中删除
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            print("LocalMusicController delete download success:\(data.uri)")
        }
    }

}
