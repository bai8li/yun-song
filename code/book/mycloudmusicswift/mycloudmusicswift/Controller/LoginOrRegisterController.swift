//
//  LoginOrRegisterController.swift
//  登陆/注册界面
//
//  Created by smile on 2019/4/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class LoginOrRegisterController: BaseLoginController {

    /// 注册按钮
    @IBOutlet weak var btRegister: UIButton!
    
    
    /// 登录按钮
    @IBOutlet weak var btPhoneLogin: UIButton!
    
    /// 昵称
    private var nickname:String?
    
    /// 头像
    private var avatar:String?
    
    /// 第三方登录后的Id
    private var openId:String?
    
    /// 登录类型
    private var type:SSDKPlatformType?
    
    //子类不用复写该方法
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//    }
    
    override func initViews() {
        //去掉返回按钮文字
//        UIBarButtonItem.appearance().set
//        let barButtonItem=UIBarButtonItem.appearance()
//        barButtonItem.setTitleTextAttributes([.foregroundColor: UIColor.clear], for: UIControl.State.normal)
//
//        barButtonItem.setTitleTextAttributes([.foregroundColor: UIColor.clear], for: UIControl.State.highlighted)
        
//        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateNormal];
//        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateHighlighted];
        
        //        圆角
        ViewUtil.showLargeRadius(view: btPhoneLogin)
        ViewUtil.showLargeRadius(view: btRegister)
        
        //        边框
        btPhoneLogin.showColorPrimaryBorder()
        btRegister.showColorPrimaryBorder()
        
    }
    
    
    /// 按下；更改背景颜色
    ///
    /// - Parameter sender: 那个按钮
    @IBAction func touchDown(_ sender: UIButton) {
        //更改为按下的颜色
        //因为想实现按下改变背景颜色，而默认只能更改图片
        //所有可以这样手动实现
        sender.backgroundColor=UIColor(hex: COLOR_PRIMARY)
    }
    
    
    /// 抬起（在按钮内部抬起）；还原背景颜色
    /// 按钮的类型要改为Custom
    ///
    /// - Parameter sender: 那个按钮
    @IBAction func touchUp(_ sender: UIButton) {
        touchUpOutside(sender)
        
        //将两个Button都关联了相同的方法
        //所以需要判断才知道是那个
        //当然也可以关联不同的方法
        if btPhoneLogin==sender {
            //手机号登陆按钮
            toPhoneLogin()
        } else {
            //注册
            toRegister()
        }
    }
    
    /// 抬起（在按钮外面抬起）；还原背景颜色
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func touchUpOutside(_ sender: UIButton) {
        //更改为白色
        sender.backgroundColor=UIColor.white
    }
    
    
    /// 跳转到手机号登陆界面
    func toPhoneLogin() {
        print("LoginOrRegisterController toPhoneLogin")
        
//        ToastUtil.short("这是提示框！")
        
        let controller=storyboard!.instantiateViewController(withIdentifier: "Login")
        
        // 将控制器压入导航控制器
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    /// 跳转到注册界面
    func toRegister() {
        print("LoginOrRegisterController toRegister")
        
        //实例化场景，因为场景有控制器
        //所以也可以说实例化控制器，但实例化是系统完成的
        //不是我们手动创建的
        
        //因为我们现在是在Controller中
        //所以就能访问到当前界面中的storyboard
        //就不用像前面那样在创建storyboard了
        let controller=storyboard!.instantiateViewController(withIdentifier: "Register") as! RegisterController
        
        //传递参数
        //如果不是第三方登陆
        //这些参数没有值
        controller.nickname=nickname
        controller.avatar=avatar
        controller.openId=openId
        controller.type = type
       
        // 将控制器压入导航控制器
        navigationController?.pushViewController(controller, animated: true)
    }
    

    // MARK: - 第三方登陆
    
    /// 微信登陆按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onWechatLoginClick(_ sender: UIButton) {
        print("LoginOrRegisterController onWechatLoginClick")
    }
    
    
    /// QQ登陆按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onQQLoginClick(_ sender: UIButton) {
        print("LoginOrRegisterController onQQLoginClick")
        
//        //获取到用户信息
//        ShareSDK.getUserInfo(SSDKPlatformType.typeQQ) { (state:SSDKResponseState, user:SSDKUser?, error:Error?) in
//            if SSDKResponseState.success==state {
//                //QQ登陆成功
//                //这里可以获取到昵称，头像，OpenId(用户Id)
//                let nickname=user?.nickname
//                let avatar=user?.icon
//                let openId=user?.credential.token
//
//                print("login or register qq lgoin succes nickname:\(nickname) avatar:\(avatar) openId:\(openId)")
//            }else{
//                let nsError=error as! NSError
//                print("login or register qq lgoin failed:\(nsError)")
//                ToastUtil.short("登陆失败，请稍后再试！")
//            }
//        }
        
        //用模拟数据登陆
        
        //用户未注册
        //跳转到补充用户资料界面（也可以理解为注册界面）
        
        //大家测试的时候，将openId随便更改一点
        //因为我们已经注册了
        //其他的信息可以直接用我们的
//        self.otherLogin(SSDKPlatformType.typeQQ,"这是昵称爱学啊","http://thirdqq.qlogo.cn/g?b=oidb&k=ISTTJtSxoO85Qq5kiamdvFw&s=100","13438ulijdgfgdg")
        
        //完整的登陆
//        ShareSDK.getUserInfo(SSDKPlatformType.typeQQ) { (state:SSDKResponseState, user:SSDKUser?, error:Error?) in
//            if SSDKResponseState.success==state {
//                //QQ登陆成功
//                //这里可以获取到昵称，头像，OpenId(用户Id)
//                let nickname=user?.nickname
//                let avatar=user?.icon
//                let openId=user?.credential.token
//
//                //调用继续登陆方法
//                self.continueLogin(SSDKPlatformType.typeQQ, nickname!, avatar!, openId!)
//            }else{
//                let nsError=error as! NSError
//                print("login or register qq lgoin failed:\(nsError)")
//                ToastUtil.short("登陆失败，请稍后再试！")
//            }
//        }
    
        //继续登陆
//        continueLoginType(SSDKPlatformType.typeQQ,"昵称1","头像","openId2")
        
        //使用重构后的方法
        otherLogin(SSDKPlatformType.typeQQ)
        
    }
    
    
    /// 微博登陆按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onWeiboLoginClick(_ sender: Any) {
        print("LoginOrRegisterController onWeiboLoginClick")
        
        //获取到用户信息
//        ShareSDK.getUserInfo(SSDKPlatformType.typeSinaWeibo) { (state:SSDKResponseState, user:SSDKUser?, error:Error?) in
//            if SSDKResponseState.success==state {
//                //微博登陆成功
//                //这里可以获取到昵称，头像，OpenId(用户Id)
//                let nickname=user?.nickname
//                let avatar=user?.icon
//                let openId=user?.credential.token
//
//                print("login or register weibo lgoin succes nickname:\(nickname) avatar:\(avatar) openId:\(openId)")
//            }else{
//                let nsError=error as! NSError
//                print("login or register weibo lgoin failed:\(nsError)")
//                ToastUtil.short("登陆失败，请稍后再试！")
//            }
//        }
        
        //微博登陆
//                ShareSDK.getUserInfo(SSDKPlatformType.typeSinaWeibo) { (state:SSDKResponseState, user:SSDKUser?, error:Error?) in
//                    if SSDKResponseState.success==state {
//                        //微博登陆成功
//                        //这里可以获取到昵称，头像，OpenId(用户Id)
//                        let nickname=user?.nickname
//                        let avatar=user?.icon
//                        let openId=user?.credential.token
//
//                        //调用继续登陆方法
//                        self.continueLogin(SSDKPlatformType.typeSinaWeibo, nickname!, avatar!, openId!)
//                    }else{
//                        let nsError=error as! NSError
//                        print("login or register weibo lgoin failed:\(nsError)")
//                        ToastUtil..short("登陆失败，请稍后再试！")
//                    }
//                }

        //使用重构后的方法
        otherLogin(SSDKPlatformType.typeSinaWeibo)
    }
    
    /// 跳转到第三方登录后，补充用户资料界面
    ///
    /// - Parameters:
    ///   - type: 登录类型
    ///   - nickname: 昵称
    ///   - avatar: 头像
    ///   - openId: 第三方登录后用户Id
//    func otherLogin(_ type:SSDKPlatformType,_ nickname:String,_ avatar:String,_ openId:String) {
//        //将参数保存到当前控制器中
//        self.nickname=nickname
//        self.avatar=avatar
//        self.openId=openId
//        self.type = type
//
//        //调用跳转到注册界面方法
//        self.toRegister()
//    }
    
    /// 第三方登陆
    ///
    /// - Parameter type: <#type description#>
    func otherLogin(_ type:SSDKPlatformType) {
        ShareSDK.getUserInfo(type) { (state:SSDKResponseState, user:SSDKUser?, error:Error?) in
            if SSDKResponseState.success==state {
                //登陆成功
                //这里可以获取到昵称，头像，OpenId(用户Id)
                let nickname=user?.nickname
                let avatar=user?.icon
                let openId=user?.credential.token
                
                //调用继续登陆方法
                self.continueLogin(type, nickname!, avatar!, openId!)
            }else if SSDKResponseState.cancel==state{
                //TODO 登陆取消处理
            }else{
                let nsError=error as! NSError
                print("login or register \(type) lgoin failed:\(nsError)")
                ToastUtil.short("登陆失败，请稍后再试！")
            }
        }
    }
    
    /// 网易邮箱按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onNeteaseLoginClick(_ sender: UIButton) {
        print("LoginOrRegisterController onNeteaseLoginClick")
    }
    
    
    /// 继续登陆
    ///
    /// - Parameters:
    /// - Parameters:
    ///   - type: 登陆类型
    ///   - nickname: 昵称
    ///   - avatar: 头像
    ///   - openId: 第三方登陆后用户Id
    func continueLogin(_ type:SSDKPlatformType,_ nickname:String,_ avatar:String,_ openId:String) {
        //打印日志，方便调试
        print("login or register \(type) continueLogin nickname:\(nickname) avatar:\(avatar) openId:\(openId)")
        
        
        var qq_id:String?
        var weibo_id:String?
        
        if SSDKPlatformType.typeQQ==type {
            //QQ第三方登陆
            qq_id=openId
        }else{
            //微博第三方登陆
            weibo_id=openId
        }
        
        //先调用登陆接口
        //如果返回用户不存在
        //就调用注册接口
        Api.shared
            .login(qq_id: qq_id, weibo_id: weibo_id)
            
            //密码是错误的
            //目的是测试手动错误处理是否正确
//            .login(phone:"13141111111",password:"test1")
            .subscribe({ data in
                //登陆成功
                //表示用户已经注册了
                self.onLoginSucces(data!.data!)
                
                //第三方登陆成功事件
                AnalysisUtil.onLogin(success: true,qq_id: qq_id, weibo_id: weibo_id)
            }) { (baseResponse, error) -> Bool in
                //登陆失败
                //判断具体的错误类型
                if let baseResponse = baseResponse{
                    //第三方登陆失败事件
                    AnalysisUtil.onLogin(success: false,qq_id: qq_id, weibo_id: weibo_id)
                    
                    //请求成功
                    //并且服务端返回了错误信息
                    
                    //有状态码
                    //表示登陆失败
                    if 1010==baseResponse.status{
                        //用户未注册
                        self.type=type
                        self.nickname=nickname
                        self.avatar=avatar
                        self.openId=openId
                        self.toRegister()
                        
                        //返回true
                        //表示我们手动处理错误
                        return true
                    }
                    
                }
               
                //让父类自动处理错误
                return false
        }
        
    }
    
//    /// 视图即将可见
//    ///
//    /// - Parameter animated: <#animated description#>
//    override func viewWillAppear(_ animated: Bool) {
//        //隐藏导航栏
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//
//        super.viewWillAppear(animated)
//
//    }
//
//
//    /// 视图即将消失
//    ///
//    /// - Parameter animated: <#animated description#>
//    override func viewWillDisappear(_ animated: Bool) {
//        //显示导航栏，因为其他界面要用
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
//
//        super.viewWillDisappear(animated)
//
//    }
    
    
    /// 使用自定义方法，隐藏当前界面的导航栏
    ///
    /// - Returns: <#return value description#>
    override func hideNavigationBar() -> Bool {
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
