//
//  SheetDetailController.swift
//  歌单详情
//
//  Created by smile on 2019/4/12.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SheetDetailController: BaseMusicPlayerController,UITableViewDataSource,UITableViewDelegate {
    
    /// 背景图片
    @IBOutlet weak var ivBackground: UIImageView!
    
    /// 背景容器的高度
    @IBOutlet weak var backgroundContainerHeight: NSLayoutConstraint!
    
    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    /// 头部歌单详情
    var header:SheetDetailHeaderView!
    
    /// 当前界面列表数据
    var dataArray:[Song]=[]
    
    /// 歌单详情Id
    var id:String!
    
    /// 歌单
    var data:Sheet!
    
    var songPosition = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func initViews() {
        super.initViews()
        
        //模拟一个崩溃
        //print(nil!)
        
        setTitle("歌单")
        
        //设置数据和代理
        //这里使用的代码设置
        //在发现页面使用的是可视化
        //两者功能上没什么区别
        //大家可以根据需要选择
        tableView.dataSource=self
        tableView.delegate=self
        
        //注册Cell
        tableView.register(UINib(nibName:SongListCell.NAME , bundle: nil), forCellReuseIdentifier: SongListCell.NAME)
        
        //注册Header
        tableView.register(UINib(nibName:SheetDetailHeaderView.NAME , bundle: nil), forHeaderFooterViewReuseIdentifier: SheetDetailHeaderView.NAME)
        
        //创建右侧按钮
        //分享
        
        //创建自定义类型按钮
        let btShare = UIButton(type: UIButton.ButtonType.custom)
        
        //添加点击事件回调
        btShare.addTarget(self, action: #selector(onShareClick(sender:)), for: UIControl.Event.touchUpInside)
        
        //设置图片
        btShare.setImage(UIImage(named: "Share"), for: UIControl.State.normal)

        //设置按钮的大小包裹内容
        btShare.sizeToFit()
        
        //通过按钮创建一个BarButtonItem
        let shareBarItem=UIBarButtonItem(customView:btShare)
        
        //更多
        let btMore=UIButton(type: UIButton.ButtonType.custom)
        
        //添加点击事件回调
        btMore.addTarget(self, action: #selector(onMoreClick(sender:)), for: .touchUpInside)

        //设置图片
        btMore.setImage(UIImage(named: "MoreWhite"), for: UIControl.State.normal)
        
        //设置按钮的大小包裹内容
        btMore.sizeToFit()

        //通过按钮创建一个BarButtonItem
        let moreBarItem=UIBarButtonItem(customView: btMore)

        //将BarItem设置到导航栏右侧
        //排序为：从右到左排序
        navigationItem.rightBarButtonItems=[createPlayingBar(),moreBarItem,shareBarItem]
    }
    
    override func getPlayingBarImagePrefix() -> String {
        
        return "TatbarPlayingWhite"
    }
    
    /// 点击右侧分享按钮
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onShareClick(sender:UIButton) {
        print("SheetDetailController onShareClick")
    }
    
    /// 点击右侧更多按钮
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onMoreClick(sender:UIButton) {
        print("SheetDetailController onMoreClick")
    }
    
    override func initDatas() {
        super.initDatas()
        
        fetchData()
    }
    
    /// 获取数据
    func fetchData()  {
        dataArray.removeAll()
        
        Api.shared
            .sheetDetail(id: id)
            .subscribeOnSuccess { (data) in
            if let data = data?.data {
                self.showData(data)
            }
        }.disposed(by: disposeBag)
        
    }
    
    /// 显示数据
    ///
    /// - Parameter data: <#data description#>
    func showData(_ data:Sheet) {
        self.data=data
        
        //显示背景
        if let banner = data.banner {
            ImageUtil.show(ivBackground, banner)
        }
        
        //显示头部数据
        header.bindData(data)
       
        
        if let songs = data.songs {
            
//            for i in 0...100 {
                //将请求的数据添加到集合
                dataArray=dataArray+songs
//            }
            
            //让列表控件重新加载数据
            tableView.reloadData()
        }
        
        
        showCurrentPlayMusicStatus()
    }
    
    /// 在列表中显示当前播放的音乐状态
    func showCurrentPlayMusicStatus() {
        if dataArray.count>0 {
            //选中当前播放的音乐
            let currentSong=playListManager.getPlaySong()
            if let currentSong = currentSong {
                //有播放的音乐
                
                //当前播放音乐的位置
                var index=0
                
                //遍历列表
                //获取正在播放的音乐
                //在当前列表中什么位置
                for e in dataArray.enumerated() {
                    if e.element.id == currentSong.id{
                        index=e.offset
                        break
                    }
                }
                
                //创建一个IndexPath
                let indexPath = IndexPath(item: index, section: 0)
                
                //选择这一行
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
            }
        }
    }
    
    func processMoreClick(_ position:Int) {
        let contentView=SongListMoreView()

        contentView.tag=position
       
        //不是我们的歌单就隐藏删除按钮
        contentView.svDeleteSongInSheet.isHidden = data.user.id! != PreferenceUtil.userId()!

        contentView.gk_size=CGSize(width: view.frame.size.width,height: view.frame.size.height/1.5)

        //收藏歌曲回调方法
        contentView.onCollectionSongClick={
            position in
            self.processCollectionSong(position)
        }

        //从歌单中删除回调方法
        contentView.onDeleteSongInSheetClick={
            position in
            self.processDeleteSongInSheet(position)
        }

//        GKCover.translucentCover(from: view, content: contentView, animated: true)
        GKCover.translucentCover(from: view, content: contentView, animated: true, show: {
            
        }) {
            print("SheetDetailController song more close")
            
            if self.songPosition != -1{
                //显示歌单列表
                //用户选择要收藏的歌单
                //完成歌曲到歌单的收藏
                self.showSelectSheetView(self.songPosition)
            }
        }

    }
    
    func processCollectionSong(_ position:Int) {
        
        GKCover.hideView()
        
        print("SheetDetailController processCollectionSong:\(position)")
        
        songPosition=position
    }
    
    func showSelectSheetView(_ position:Int) {
        let contentView=SheetListView()
        
        contentView.tag=position
        
        contentView.gk_size=CGSize(width: view.frame.size.width,height: view.frame.size.height/1.5)
        
        //选择歌单回调方法
        contentView.onSheetClick={
            data in
            self.processSelectSheet(position,data)
        }
        
        GKCover.translucentCover(from: view, content: contentView, animated: true, show: {
            
        }) {
            self.resetPosition()
        }
    }
    
    func processSelectSheet(_ position:Int,_ sheet:Sheet)  {
        GKCover.hideView()
        
        print("SheetDetailController showSelectSheetView onSheetClick:\(sheet)")
        
        //要操作的音乐
        let song=dataArray[position]
        
        resetPosition()
        
        Api.shared
            .addSongToSheet(sheet.id, song.id)
            .subscribeOnSuccess { (data) in
                print("SheetDetailController showSelectSheetView add song:\(song.id) to sheet:\(sheet.id) success")
                ToastUtil.short("收藏成功！")
                
            }.disposed(by: disposeBag)
        
    }
    
    func resetPosition() {
        //用完position后重置
        self.songPosition = -1
    }
    
    func processDeleteSongInSheet(_ position:Int) {
        GKCover.hideView()
        
        print("SheetDetailController processDeleteSongInSheet:\(position)")
        
        //要操作的音乐
        let song=dataArray[position]
        
        Api.shared
            .deleteSongInSheet(data.id, song.id)
            .subscribeOnSuccess { (data) in
                ToastUtil.short("取消收藏成功！")
                
                //重新获取数据
                //也可以在列表中把这首音乐移除
                //并且更新数量
                self.fetchData()
                
            }.disposed(by: disposeBag)
        
    }

    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: SongListCell.NAME, for: indexPath) as! SongListCell
        
        //设置Tag
        //目的是Cell中显示位置
        //还有点击Cell更多按钮时知道是点击了那个Cell
        cell.tag=indexPath.row
        
        //取出数据
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
        
        //设置更多点击回调方法
        cell.onMoreClick={
            position in
            self.processMoreClick(position)
        }
        return cell
    }
    
    
    /// 返回Header
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        header=tableView.dequeueReusableHeaderFooterView(withIdentifier: SheetDetailHeaderView.NAME) as! SheetDetailHeaderView
        
        //设置评论列表点击回调方法
        header.onCommentClick={
           //启动评论界面
            CommentController.start(self.navigationController!, sheet: self.id)
        }
        
        //设置收藏点击回调方法
        header.onCollectionClick={
            self.processCollectionClick()
        }
        
        //用户详情点击回调方法
        header.onUserClick={
            //启动用户详情界面
            UserDetailController.start(self.navigationController!, userId: self.data.user!.id)
        }
        
        //播放全部点击回调方法
        header.onPlayAllClick={
            //从第一首音乐开始播放
            self.play(0)
        }
        
        return header
    }
    
    /// 处理收藏和取消收藏逻辑
    func processCollectionClick() {
        if data.isCollection() {
            //已经收藏
            //取消收藏
            Api.shared
                .deleteCollect(data.id)
                .subscribeOnSuccess { (_) in
                    //取消收藏成功
                    self.data.collection_id=nil
                    
                    //收藏数+1
                    //收藏数，可以不用那么精确
                    self.data.collections_count -= 1
                    
                    //刷新收藏状态
                    self.header.showCollectionStatus()
                }.disposed(by: disposeBag)
            
        } else {
            //没有收藏
            //收藏
            Api.shared
                .collect(data.id)
                .subscribeOnSuccess { _ in
                    //收藏成功
                    //收藏状态变更后
                    //可以重写点击歌单详情界面
                    //获取收藏状态
                    //但对于收藏来说，收藏数可能没那么重要
                    //所以不用及时刷新
                    self.data.collection_id=1
                    
                    self.data.collections_count += 1
                    
                    //刷新收藏状态
                    self.header.showCollectionStatus()
                }.disposed(by: disposeBag)
            
        }
    }
    
    /// 返回header高度
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 308
    }
    
    
    /// UITableView滚动的时候回调
    ///
    /// - Parameter scrollView: <#scrollView description#>
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //垂直方向的滚动距离
        //不滚动为0
        //在0位置向上滚动为正
        //在0位置向下滚动为负
        let offsetY  = scrollView.contentOffset.y;
        print("SheetDetailController scrollViewDidScroll:\(offsetY)");
        
        //状态栏+导航栏+header的高度=350（大概计算）
        backgroundContainerHeight.constant=350+(-offsetY)
    }
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //简单音乐播放界面
        //SimpletPlayerController.start(self.navigationController!)
        
        play(indexPath.row)
 
    }
    
    /// 视图即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置导航栏样式
        //这里将导航栏背景设置为黑色
        //这样状态栏（有了导航栏，只能通过这种方式设置状态栏颜色）
        //标题就会变成白色
        navigationController!.navigationBar.barStyle=UIBarStyle.black
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.white]
        
        
        //设置返回按钮为白色
        navigationController!.navigationBar.tintColor=UIColor.white
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        showCurrentPlayMusicStatus()
    }
    
    /// 视图即将消失
    ///
    /// - Parameter animated: animated description
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //恢复状态栏颜色
        //因为其他界面可能当前界面样式不一样
        
        //标题就会变成黑色
        navigationController!.navigationBar.barStyle=UIBarStyle.default
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.black]
        
        //设置返回按钮为黑色
        navigationController!.navigationBar.tintColor=UIColor.black
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    
    /// 播放这首音乐
    ///
    /// - Parameter position: <#position description#>
    func play(_ position:Int) {
        playListManager.setPlayList(dataArray)
        playListManager.play(position)
        pushMusicPlayerController()
    }

    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ id:String) {
        //获取Storyboard
        let storyboard=UIStoryboard(name: "Main", bundle: nil)
        
        //创建控制器
        let controller=storyboard.instantiateViewController(withIdentifier: "SheetDetail") as! SheetDetailController
        
        //将传递过来的数据设置到controller中
        controller.id=id
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 页面统计
extension SheetDetailController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //使用极光分析
        //统计页面
        JANALYTICSService.startLogPageView("SheetDetail")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //页面结束
        JANALYTICSService.stopLogPageView("SheetDetail")
    }
}
