//
//  FriendController.swift
//  好友控制器
//  动态列表
//
//  Created by smile on 2019/4/18.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class FriendController: BaseTitleController {

    /// 动态列表控件
    @IBOutlet weak var feedTableView: UITableView!
    
    /// 附近列表控件
    @IBOutlet weak var nearbyTableView: UITableView!
    
    
    /// 动态容器
    @IBOutlet weak var feedContainer: UIStackView!
    
    /// 动态控制按钮容器
    @IBOutlet weak var feedControlContainer: UIStackView!
    
    /// 动态列表
    var feedDataArray:[Feed] = []
    
    /// 附近列表
    var nearbyDataArray:[String] = []
    
    var images:[UIImage]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func initViews() {
        super.initViews()
        
        //分段控件
        let segmentedControl=TwicketSegmentedControl(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        
        //未选中文本颜色
        segmentedControl.defaultTextColor=UIColor(hex: COLOR_PRIMARY)
        
        //选择后文本颜色
        segmentedControl.highlightTextColor=UIColor.white
        
        //未选中背景颜色
//        segmentedControl.segmentsBackgroundColor=UIColor
        
        //选中背景颜色
        segmentedControl.sliderBackgroundColor=UIColor(hex: COLOR_PRIMARY)
        
        //是否显示阴影
        segmentedControl.isSliderShadowHidden=true
        
        //设置代理
        segmentedControl.delegate=self
        
        //显示的标题
        let titles = ["动态", "附近"]
        
        //设置标题
        segmentedControl.setSegmentItems(titles)
       
        //将分段控件设置到导航栏
        navigationController?.navigationBar.topItem?.titleView=segmentedControl
        
        //注册Cell
        //文本动态
        feedTableView.register(UINib(nibName:FeedTextCell.NAME , bundle: nil), forCellReuseIdentifier: FeedTextCell.NAME)
        
        //文本图片动态
        feedTableView.register(UINib(nibName:FeedImageCell.NAME , bundle: nil), forCellReuseIdentifier: FeedImageCell.NAME)
    }
    
    override func initDatas() {
        super.initDatas()
        
        fetchData()
        
        //附近，由于没有什么难点，就随便创建一点模拟数据
        //让界面显示出来就行了
        nearbyDataArray.append("由于该界面没有什么难点，所以就随便创建一点模拟数据")
        
        nearbyDataArray.append("你好，这是附近人的界面！")
        nearbyDataArray.append("人生苦短，我们只做好课！")
        nearbyDataArray.append("我们是爱学啊！")
        nearbyDataArray.append("学习成就更好的自己！")
        
        nearbyTableView.reloadData()
    }

    override func initListeners() {
        super.initListeners()
        
        //监听发布了动态
        SwiftEventBus.onMainThread(self, name: PUBLISH_FEED) { result in
            self.feedDataArray.removeAll()
            self.fetchData()
           
        }
    }
    
    /// 加载首页数据
    func fetchData() {
        Api.shared
            .feeds()
            .subscribeOnSuccess { data in
                if let data = data?.data {
                    //将请求回来的数据添加到集合
                    self.feedDataArray=self.feedDataArray+data
                    
                    //通知界面刷新
                    self.feedTableView.reloadData()
                    
                }
            }.disposed(by: disposeBag)
        
        
    }
    
    /// 获取列表类型
    ///
    /// - Parameter data: <#data description#>
    /// - Returns: <#return value description#>
    func typeForItemAtData(_ data:Feed) -> CellType {
        
        if let _ = data.images{
            //有图片
            return .Image
        }
        //    else if (data.video){
        //        //TODO 有视频
        //    }
        
        //TODO 更多的类型，在这里扩展就行了
        
        //文字
        return .Text
    }
    
    /// 视图即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置导航栏样式
        //这里将导航栏背景设置为黑色
        //这样状态栏（有了导航栏，只能通过这种方式设置状态栏颜色）
        //标题就会变成白色
//        navigationController!.navigationBar.barStyle=UIBarStyle.black
        
        //设置标题字体颜色
//        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.white]
        
        
        //设置返回按钮为白色
//        navigationController!.navigationBar.tintColor=UIColor.white
        
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        //去掉导航栏下面的阴影
        //如果大家需要可以留着
        //这个导航栏有层次感
        navigationController?.navigationBar.shadowImage=UIImage()
        
    }
    
    /// 视图即将消失
    ///
    /// - Parameter animated: animated description
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //恢复状态栏颜色
        //因为其他界面可能当前界面样式不一样
        
        //标题就会变成黑色
        navigationController!.navigationBar.barStyle=UIBarStyle.default
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.black]
        
        //设置返回按钮为黑色
        navigationController!.navigationBar.tintColor=UIColor.black
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
    }

    /// Cell类型；这是一个枚举
    enum CellType {
        //文本动态
        case Text
        
        //带图片动态
        case Image
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 列表数据源和代理
extension FriendController:UITableViewDataSource,UITableViewDelegate {
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: tableView description
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView==feedTableView {
            //动态
            return feedDataArray.count
        } else {
            //附近
            return nearbyDataArray.count
        }
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView==feedTableView {
            //动态
            let data=feedDataArray[indexPath.row]
            
            //获取当前对象的类型
            let type=typeForItemAtData(data)
            
            switch type {
            case .Image:
                let cell=tableView.dequeueReusableCell(withIdentifier:String(describing: FeedImageCell.self) , for: indexPath) as! FeedImageCell
                
                cell.bindData(data)
                
                //动态中图片点击回调
                cell.onImageClick={
                    images,index in
                    self.imagePreview(images,index)
                }
                
                //用户头像点击回调
                cell.onAvatarClick={
                    data in
                    self.onAvatarClick(data)
                }
                
                return cell
            default:
                let cell=tableView.dequeueReusableCell(withIdentifier:String(describing: FeedTextCell.self) , for: indexPath) as! FeedTextCell
                
                cell.bindData(data)
                
                //用户头像点击回调
                //和上面Cell重复
                //可以重构
                cell.onAvatarClick={
                    data in
                    self.onAvatarClick(data)
                }
                
                return cell
            }
        } else {
            //附近
            let data=nearbyDataArray[indexPath.row]
            
            let cell=tableView.dequeueReusableCell(withIdentifier:CELL , for: indexPath)
            
            cell.textLabel?.text=data
            
            return cell
        }
        
    }
}

// MARK: - Cell上按钮点击回调方法
extension FriendController {
    
    /// 点击了图片动态中的图片回调
    ///
    /// - Parameters:
    ///   - images: <#images description#>
    ///   - index: <#index description#>
    func imagePreview(_ images:[UIImage],_ index:Int) {
        self.images=images
        
        let galleryViewController=GalleryViewController(startIndex: index, itemsDataSource: self)
        
        presentImageGallery(galleryViewController)
    }
    
    /// 发动态的用户头像点击回调
    ///
    /// - Parameter data: <#data description#>
    func onAvatarClick(_ data:User) {
        UserDetailController.start(self.navigationController!, userId: data.id)
    }
}

// MARK: - 导航栏分段控件代理
extension FriendController:TwicketSegmentedControlDelegate {
    
    /// 分段控件切换回调
    ///
    /// - Parameter segmentIndex: <#segmentIndex description#>
    func didSelect(_ segmentIndex: Int) {
        print("FriendController segmented didSelect:\(segmentIndex)")
        
        if segmentIndex==0 {
            //动态列表
            feedContainer.isHidden=false
            nearbyTableView.isHidden=true
        } else {
            //附近列表
            feedContainer.isHidden=true
            nearbyTableView.isHidden=false
        }
    }
    
}

// MARK: - 图片预览框架代理
extension FriendController:GalleryItemsDataSource{
    /// 返回预览的图片数量
    ///
    /// - Returns: <#return value description#>
    func itemCount() -> Int {
        return images!.count
    }
    
    /// 返回当前位置的UIImage
    ///
    /// - Parameter index: <#index description#>
    /// - Returns: <#return value description#>
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        print("FriendController provideGalleryItem:\(index)")
        
        //测试
        //这里返回的是本地图片
        //        return GalleryItem.image {
        //            let image=UIImage(named: "Guide1")
        //            $0(image)
        //        }
        
        //返回从Cell中获取的图片
        return GalleryItem.image {
            let image=self.images![index]
            $0(image)
        }
        
    }
}
