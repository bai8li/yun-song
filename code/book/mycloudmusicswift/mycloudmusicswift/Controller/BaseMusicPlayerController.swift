//
//  BaseMusicPlayerController.swift
//  通用音乐播放控制器
//  主要用来显示右上角播放状态动画
//
//  Created by smile on 2019/4/14.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//主题框架
import SwiftTheme

class BaseMusicPlayerController: BaseTitleController {

    var playListManager:PlayListManager!
    var musicPlayerManager:MusicPlayerManager!
    
    var ivPlaying:UIImageView!
    
    var vwPlayContainer:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func createPlayingBar() -> UIBarButtonItem {
        //创建自定义类型按钮
        let frame=CGRect(x: 0, y: 0, width: 22, height: 22)
        
        //多个按钮时，这里的大小有点问题
        //因为多个按钮是系统使用的是StackView
        vwPlayContainer=UIView(frame: frame)
        ivPlaying=UIImageView(frame: frame)
        
        let btPlaying = UIButton(type: UIButton.ButtonType.custom)
        btPlaying.frame=frame
        
        vwPlayContainer.addSubview(ivPlaying)
        vwPlayContainer.addSubview(btPlaying)
        
        //添加点击事件回调
        btPlaying.addTarget(self, action: #selector(onPlayingClick(sender:)), for: UIControl.Event.touchUpInside)
        
        //设置按钮的大小包裹内容
//        btPlaying.sizeToFit()
        
        //通过按钮创建一个BarButtonItem
        let playingBarItem=UIBarButtonItem(customView:vwPlayContainer)
        
        
        //将BarItem设置到导航栏右侧
//        navigationItem.rightBarButtonItem=playingBarItem
        
        return playingBarItem
    }
    
    /// 点击右侧分享按钮
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onPlayingClick(sender:UIButton) {
        print("BaseMusicPlayerController onPlayingClick")
        pushMusicPlayerController()
    }
    
    override func initDatas() {
        super.initDatas()
        
        playListManager=PlayListManager.shared
        musicPlayerManager=MusicPlayerManager.shared()
    }
    
    /// 进入到播放界面
    func pushMusicPlayerController() {
        //简单播放器界面
//        SimpletPlayerController.start(self.navigationController!)
        
        //黑胶唱片播放界面
        PlayerController.start(self.navigationController!)
    }
    
    /// 显示播放状态动画
    func showPlayingBarStatus() {
        //播放状态，动画，可以用自定义控件
        //这样方式更耗费资源
        //无限循环
        ivPlaying.animationRepeatCount=0
        
        //将动画图片放到数组中
        var animationImages:[UIImage]=[]
        
        for i in 1...6 {
            //顺着添加图片
            let imageName="\(getPlayingBarImagePrefix())\(i)"
            animationImages.append(UIImage(named: imageName)!)
        }
        
        for i in (1...6).reversed() {
            //倒着添加图片
            let imageName="\(getPlayingBarImagePrefix())\(i)"
            animationImages.append(UIImage(named: imageName)!)
        }
        
        ivPlaying.animationImages   = animationImages
        
        //开始动画
        ivPlaying.startAnimating()
    }
    
    /// 获取播放状态图标前缀
    ///
    /// - Returns: <#return value description#>
    func getPlayingBarImagePrefix() -> String {
        if ThemeManager.isNight() {
            //夜间模式
            return "TatbarPlayingNight"
        }else{
            //日间模式
            return "TatbarPlaying"
        }
        
    }

    /// 显示播放暂停图片
    func showPauseBarStatus() {
        stopPlayingAnimation()
        
        //暂停状态，设置为一张静态图片
        ivPlaying.image=UIImage(named: "\(getPlayingBarImagePrefix())1")
    }
    
    /// 停止播放状态动画
    func stopPlayingAnimation() {
        if ivPlaying.isAnimating {
            ivPlaying.stopAnimating()
        }
    }
    
    
    /// 界面即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("BaseMusicPlayerController viewWillAppear")
        
        if vwPlayContainer != nil {
            let playList=playListManager.getPlayList()
            if playList != nil && playList!.count>0 {
                //有音乐
                vwPlayContainer.isHidden=false
                
                if musicPlayerManager.isPlaying(){
                    //正在播放音乐
                    //就开启动画
                    self.showPlayingBarStatus()
                }else{
                    //没有播放音乐
                    //就停止动画
                    self.showPauseBarStatus()
                }
            }else{
                vwPlayContainer.isHidden=true
            }
        }
    }

    
    /// 视图即将消失
    ///
    /// - Parameter animated: animated description
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("BaseMusicPlayerController viewWillDisappear")
        
        //视图看不见了
        //所以直接停止动画
        //因为都看不见了
        //在播放动画是没多大意义
        //还消耗资源
        stopPlayingAnimation()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

