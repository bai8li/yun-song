//
//  OrderController.swift
//  订单列表
//
//  Created by smile on 2019/4/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class OrderController: UITableViewController {

    var dataArray:[Order] = []
    
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //设置标题
        navigationItem.title="订单列表"
        
        //隐藏底部tabbar
        hidesBottomBarWhenPushed=true
        
        //注册Cell
        
        tableView.register(UINib(nibName: OrderCell.NAME, bundle:nil), forCellReuseIdentifier: OrderCell.NAME)
        
        //分割线间距
        tableView.separatorInset=UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        Api.shared
            .orders()
            .subscribeOnSuccess { data in
                if let data = data?.data {
                    self.dataArray=self.dataArray+data
                    self.tableView.reloadData()
                    
                }
            }.disposed(by: disposeBag)
        
    }

}

// MARK: - 列表数据源和代理
extension OrderController {
    
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: OrderCell.NAME, for: indexPath) as! OrderCell
        
        //获取数据
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
        
        return cell
    }
    
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data=dataArray[indexPath.row]
        
        OrderDetailController.start(navigationController!,data.id)
    }
}

// MARK: - 启动界面
extension OrderController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller = OrderController(style: UITableView.Style.plain)
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
