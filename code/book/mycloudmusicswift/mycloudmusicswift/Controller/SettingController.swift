//
//  SettingController.swift
//  设置控制器
//
//  Created by smile on 2019/4/29.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SettingController: BaseTitleController {
    
    /// 拔掉耳机后是否停止音乐播放的滑块
    @IBOutlet weak var stRemoveHeadsetStop: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        setTitle("设置")
        
        //从偏好设置中恢复状态
        stRemoveHeadsetStop.setOn(PreferenceUtil.isRemoveHeadsetStop(), animated: true)
    }
    
    
    /// 拔掉耳机后是否停止音乐播放滑块回调事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onRemoveHeadsetStopChanged(_ sender: UISwitch) {
        print("SettingController onRemoveHeadsetStopChanged:\(sender.isOn)")
        
        //将值设置到偏好设置
        PreferenceUtil.setRemoveHeadsetStop(sender.isOn)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension SettingController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "Setting") as! SettingController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
