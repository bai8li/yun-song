//
//  VideoController.swift
//  视频列表控制器
//
//  Created by smile on 2019/4/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoController: BaseTitleController {

    /// 列表控件
    @IBOutlet weak var tableView: UITableView!
    
    var dataArray:[Video] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        setTitle("视频")
    }
    
    override func initDatas() {
        super.initDatas()
        
        //获取视频列表
        Api.shared
            .videos()
            .subscribeOnSuccess { data in
                if let data = data?.data {
                    self.dataArray=self.dataArray+data
                    self.tableView.reloadData()
                }
                
            }.disposed(by: disposeBag)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 列表数据源
extension VideoController: UITableViewDataSource,UITableViewDelegate {
    
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataArray[indexPath.row]
       
        let cell=tableView.dequeueReusableCell(withIdentifier: VideoCell.NAME, for: indexPath) as! VideoCell
       
        //绑定数据
        cell.bindData(data)
        
        return cell
    }
    
    /// 点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = dataArray[indexPath.row]
        
        VideoDetailController.start(navigationController!, data.id)
    }
}
