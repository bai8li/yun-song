//
//  NewOrderController.swift
//  订单列表
//  主要用来测试网络接口数据签名和加密
//
//  Created by smile on 2019/5/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class NewOrderController: BaseTitleController {

    /// 显示接口信息
    @IBOutlet weak var lbInfo: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        setTitle("接口数据签名和加密")
    }
    
    // MARK: - 按钮事件
    
    /// 订单列表（响应签名）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onOrderListSignClick(_ sender: Any) {
        print("NewOrderController onOrderListSignClick")
        
        Api.shared
            .ordersV2()
            .subscribeOnSuccess { (data) in
                if let data = data?.data{
                    self.lbInfo.text="(签名接口)订单数量：\(data.count)"
                }
        }.disposed(by: disposeBag)
    }
    
    /// 创建订单（参数签名）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCreateOrderSign(_ sender: Any) {
        print("NewOrderController onCreateOrderSign")
        
        Api.shared
            .createOrderV2(book_id:"1")
            .subscribeOnSuccess { (data) in
                if let data = data?.data{
                    self.lbInfo.text="(签名接口)订单创建成功！"
                }
            }.disposed(by: disposeBag)
    }
    
    /// 订单列表（响应加密）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onOrderListEncryptClick(_ sender: Any) {
        print("NewOrderController onOrderListEncryptClick")
        
        Api.shared
            .ordersV3()
            .subscribeOnSuccess { (data) in
                if let data = data?.data{
                    self.lbInfo.text="(加密接口)订单数量：\(data.count)"
                }
            }.disposed(by: disposeBag)
    }
    
    /// 创建订单（参数加密）点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCreateOrderEncrypt(_ sender: Any) {
        print("NewOrderController onCreateOrderEncrypt")
        
        Api.shared
            .createOrderV3(book_id:"1")
            .subscribeOnSuccess { (data) in
                if let data = data?.data{
                    self.lbInfo.text="(加密接口)订单创建成功！"
                }
            }.disposed(by: disposeBag)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension NewOrderController{
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "NewOrder") as! NewOrderController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
   
}
