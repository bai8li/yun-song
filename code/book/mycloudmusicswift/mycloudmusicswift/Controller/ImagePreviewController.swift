//
//  ImagePreviewController.swift
//  图片预览界面
//
//  Created by smile on 2019/4/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ImagePreviewController: BaseTitleController {

    var data:String!
    
    /// 背景
    @IBOutlet weak var ivBackground: UIImageView!
    
    /// 封面
    @IBOutlet weak var ivCover: UIImageView!
    
    /// 关闭按钮
    @IBOutlet weak var btClose: UIButton!
    
    /// 保存图片按钮
    @IBOutlet weak var btSaveImage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func initViews() {
        super.initViews()
        //设置边框
        btClose.showColorPrimaryBorder()
        btSaveImage.showColorPrimaryBorder()
    }
    
    override func initDatas() {
        super.initDatas()
        
        //显示图片
        ImageUtil.show(ivBackground, data)
        ImageUtil.show(ivCover, data)
    }
    
    /// 关键按钮点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCloseClick(_ sender: Any) {
        self.navigationController!.popViewController(animated: false)
    }
    
    /// 保存图片按钮点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSaveImageClick(_ sender: Any) {
        //直接从ImageView中拿到Image
        saveImage(ivCover.image!)
    }
    
    /// 保存图片到相册
    ///
    /// - Parameter data: <#data description#>
    func saveImage(_ data:UIImage) {
        UIImageWriteToSavedPhotosAlbum(data, self, #selector(image(image:didFinishSavingWithError:contextInfo:)),nil)
    }
    
    
    /// 保存图片到相册回调
    /// 经测试写到工具类中（静态和实例）
    /// 回调找不到方法
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - error: <#error description#>
    ///   - contextInfo: <#contextInfo description#>
    //    @objc static func
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer){
        var message=""
        
        if error==nil {
            message = "保存成功！"
            
        } else {
            message = "保存失败，请稍后再试！"
        }
        
        ToastUtil.short(message)
    }
    
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ data:String) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "ImagePreview") as! ImagePreviewController
        
        //将传递过来的数据设置到controller中
        controller.data=data
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: false)
        
    }

    /// 视图即将可见
    ///
    /// - Parameter animated: animated description
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //隐藏导航栏
        navigationController!.setNavigationBarHidden(true, animated: false)
        
    }
    
    /// 视图即将消失
    ///
    /// - Parameter animated: animated description
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //显示导航栏，因为其他界面要用
        navigationController!.setNavigationBarHidden(false, animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
