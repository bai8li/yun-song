//
//  ShopController.swift
//  商品列表界面
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class ShopController: UITableViewController {
    var dataArray:[Book] = []
    
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //设置标题
        navigationItem.title="商品列表"
        
        //隐藏底部tabbar
        hidesBottomBarWhenPushed=true

        //注册Cell
        tableView.register(UINib(nibName: ShopCell.NAME, bundle:nil), forCellReuseIdentifier: ShopCell.NAME)
        
        //分割线间距
        tableView.separatorInset=UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        //请求数据
        Api.shared
            .shops()
            .subscribeOnSuccess { data in
                if let data = data?.data {
                    self.dataArray=self.dataArray+data
                    self.tableView.reloadData()
                    
                }
            }.disposed(by: disposeBag)
    
    }

}

// MARK: - 列表数据源和代理
extension ShopController {

    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }

    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: ShopCell.NAME, for: indexPath) as! ShopCell

        //获取数据
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
        
        return cell
    }


    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data=dataArray[indexPath.row]

        ShopDetailController.start(navigationController!,data.id)

    }
}

// MARK: - 启动界面
extension ShopController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller = ShopController(style: UITableView.Style.plain)
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
