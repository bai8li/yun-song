//
//  ScanController.swift
//  扫描二维码界面
//
//  Created by smile on 2019/4/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

// 导入AVFoundation
// 主要是设置码类型
import AVFoundation

class ScanController: BaseTitleController {

    /// 扫描容器
    @IBOutlet weak var scanContainer: UIView!
    
    /// 我的二维码容器
    @IBOutlet weak var btMyCode: UIButton!
    
    //相机启动提示文字
    public var readyString:String! = "加载中."
    
    /// 扫描二维码的View
    var scanView:LBXScanView?
    
    /// 对二维码扫描进行了一次包装
    /// 开启扫描和停止扫描直接操作他
    open var scanWrapper: LBXScanWrapper?
    
    /// 扫描样式
    open var scanStyle: LBXScanViewStyle!
    
    /// 识别码的类型
    /// 指定识别几种码
    public var arrayCodeType=[AVMetadataObject.ObjectType.qr as NSString ,AVMetadataObject.ObjectType.ean13 as NSString ,AVMetadataObject.ObjectType.code128 as NSString] as [AVMetadataObject.ObjectType]
    
    override func initViews() {
        super.initViews()
        setTitle("扫一扫")
        
        ViewUtil.showRadius(btMyCode, 20)
    }
    
    override func initDatas() {
        super.initDatas()
        
        //创建扫描样式
        scanStyle = LBXScanViewStyle()
        
        //设置扫描时显示的动画图片
        scanStyle.animationImage = UIImage(named: "QRScanLine")
    }
    
    /// 视图已经可见了
    ///
    /// - Parameter animated: <#animated description#>
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //绘制扫描的View
        drawScanView()
        
        //延迟0.3秒后开始扫描
        perform(#selector(ScanController.startScan), with: nil, afterDelay: 0.3)
    }
    
    /// 绘制扫描的View
    open func drawScanView(){
        if scanView == nil{
            //为空才创建
            scanView = LBXScanView(frame: self.scanContainer.frame,vstyle:scanStyle! )
            self.scanContainer.addSubview(scanView!)
            
        }
        
        //开始准备扫描
        scanView?.deviceStartReadying(readyStr: readyString)
    }
    
    /// 视图即将消失
    ///
    /// - Parameter animated: animated description
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //取消前面的定时
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        
        //停止扫描动画
        scanView?.stopScanAnimation()
        
        //停止扫描
        scanWrapper?.stop()
        
        //恢复状态栏颜色
        //因为其他界面可能当前界面样式不一样
        
        //标题就会变成黑色
        navigationController!.navigationBar.barStyle=UIBarStyle.default
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.black]
        
        //设置返回按钮为黑色
        navigationController!.navigationBar.tintColor=UIColor.black
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    /// 开始扫描
    @objc func startScan() {
        if (scanWrapper == nil){
            var cropRect = CGRect.zero
            
            //裁剪矩形
                cropRect = LBXScanView.getScanRectWithPreView(preView: self.scanContainer, style:scanStyle! )
            
            //创建一个包括对象
            //设置预览的控件
            //扫描的类型
            //裁剪的区域
            //成功回调
            scanWrapper = LBXScanWrapper(videoPreView: self.scanContainer,objType:arrayCodeType, isCaptureImg: false,cropRect:cropRect, success: { [weak self] (arrayResult) -> Void in
                
                if let strongSelf = self
                {
                    //扫描成功
                    //停止扫描动画
                    strongSelf.scanView!.stopScanAnimation()
                    
                    //处理扫描结果
                    strongSelf.handleCodeResult(arrayResult: arrayResult)
                }
            })
        }
        
        //结束相机等待提示
        scanView!.deviceStopReadying()
        
        //开始扫描动画
        scanView!.startScanAnimation()
        
        //相机运行
        scanWrapper!.start()
    }

    /// 处理扫码结果
    /// 如果是继承本控制器的
    /// 可以重写该方法
    /// 作出相应地处理
    /// 或者设置delegate作出相应处理
    open func handleCodeResult(arrayResult:[LBXScanResult]){
//        for result:LBXScanResult in arrayResult
//        {
//            print("%@",result.strScanned ?? "")
//        }
        
        //取第一个数据就行了
        let result = arrayResult[0]
        if let str = result.strScanned {
            if str.isEmpty{
                //内容为空
                
                //显示不支持该类型
                self.showUnsupportFormat()
                return
            }
            
            //有内容
            //就处理他
            handleScanString(str)
        }else{
            //显示不支持该类型
            self.showUnsupportFormat()
        }
    }
    
    /// 处理扫描结果
    ///
    /// - Parameter data: <#data description#>
    func handleScanString(_ data:String) {
        //从扫描的字符串中解析我们需要的参数
        //我们的二维码格式如下：
        //http://dev-my-cloud-music-api-rails.ixuea.com/v1/monitors/version?u=6
        
        //表示用我的云音乐软件扫描后
        //跳转到Id为6用户详情
        
        //其实就是我们应用的下载地址
        //我们要用到的参数放到了查询参数
        //生成网址的好处是
        //如果使用其他软件扫描的时候
        //会自动打开网址
        //大部分二维码扫描软件（QQ，微信）都会这样
        
        //这样就可以引导用户下载我们的应用
        //另外在真实项目中
        //会将上面的网址转短一点
        //因为二维码内容太多了
        //生成的二维码格子就会很小
        //如果显示的图片过小
        //就会非常难识别
        
        //另外不同的方式生成二维码
        //也可能不同
        
        //将URL解析为每一个组件
        let component=URLComponents(string: data)
        
        if let component = component {
            //如果解析成功
            if let queryItems=component.queryItems{
                //如果有查询参数
                var userId:String?
                
                for queryItem in queryItems {
                    if queryItem.name=="u" {
                        //遍历找到u属性
                        
                        //他的值就是userId
                        userId=queryItem.value
                    }
                }
                
                //如果没找相关参数
                //就提示不支持该格式
                if let userId = userId
                {
                    //参数没问题
                    //跳转到用户详情
                    processUserCode(userId)
                    return
                }
                
            }
        }
        
        //显示不支持该类型
        showUnsupportFormat()
    }
    
    
    /// 处理用户的二维码
    ///
    /// - Parameter data: <#data description#>
    func processUserCode(_ data:String) {
        //关闭当前界面
        navigationController?.popViewController(animated: true)
        
        //打开用户详情
        UserDetailController.start(navigationController!, userId: data)
    }
    
    /// 显示不支持该类型
    func showUnsupportFormat() {
        ToastUtil.short("暂时不支持改格式！")
        
        //继续扫描
        startScan()
    }
    
    /// 视图即将可见
    ///
    /// - Parameter animated: <#animated description#>
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //设置导航栏样式
        //这里将导航栏背景设置为黑色
        //这样状态栏（有了导航栏，只能通过这种方式设置状态栏颜色）
        //标题就会变成白色
        navigationController!.navigationBar.barStyle=UIBarStyle.black
        
        //设置标题字体颜色
        navigationController!.navigationBar.titleTextAttributes=[.foregroundColor:UIColor.white]
        
        
        //设置返回按钮为白色
        navigationController!.navigationBar.tintColor=UIColor.white
        
        //导航栏透明
        //这里设置导航透明后，就没有上面的黑色了
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        
        //去掉导航栏下面的阴影
        //如果大家需要可以留着
        //这个导航栏有层次感
        navigationController?.navigationBar.shadowImage=UIImage()
    }
    
    
    /// 我的二维码点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onMyCodeClick(_ sender: Any) {
        MyCodeController.start(navigationController!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension ScanController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "Scan") as! ScanController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
