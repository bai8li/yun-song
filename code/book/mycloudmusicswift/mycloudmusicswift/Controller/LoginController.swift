//
//  LoginController.swift
//  登录界面
//
//  Created by smile on 2019/4/8.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class LoginController: BaseLoginController {

    /// 用户名
    @IBOutlet weak var tfUsername: UITextField!
    
    /// 密码
    @IBOutlet weak var tfPassword: UITextField!
    
    /// 登录按钮
    @IBOutlet weak var btLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        //设置标题
        setTitle("登录")
        
        //配置输入框
        tfUsername.showLeftIcon(name: "LoginItemPhone")
        tfPassword.showLeftIcon(name: "LoginItemPhone")
        
        //按钮圆角
        ViewUtil.showLargeRadius(view: btLogin)
        
    }
    
    
    /// 登录按钮点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLoginClick(_ sender: UIButton) {
        print("LoginController onLoginClick")
        
        //获取用户名
        let username=tfUsername.text?.trim()
        
        if username!.isEmpty {
            ToastUtil.short("请输入用户名！")
            return
        }
        
        guard username!.isPhone() || username!.isEmail() else {
            //如果用户名，既不是手机号
            //也不是邮箱，就是格式错误
            ToastUtil.short("用户名格式不正确！")
            return
        }
        
        //获取密码
        let password=tfPassword.text?.trim()
        
        if password!.isEmpty {
            ToastUtil.short("请输入密码！")
            return
        }
        
        guard password!.isPassword() else {
            ToastUtil.short("密码格式不正确！")
            return
        }
        
        var phone:String?
        var email:String?
        
        //判断是手机号还是邮箱
        if username!.isPhone(){
            //手机号
            phone=username
        }else{
            //邮箱
            email=username
        }

        //调用父类的登录方法
        self.login(phone: phone, email: email, password: password!)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
