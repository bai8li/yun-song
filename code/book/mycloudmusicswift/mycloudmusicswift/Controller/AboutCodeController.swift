//
//  AboutCodeController.swift
//  关于爱学啊（代码自动布局）
//  使用SnapKit来使用AutoLayout布局
//
//  Created by smile on 2019/4/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class AboutCodeController: BaseTitleController {

    /// 按钮版本
    var btVersion:UIButton!
    
    /// 版本
    var lbVersion:UILabel!
    
    /// 新手指南按钮
    var btGuide:UIButton!
    
    /// 关于我们按钮
    var btAbout:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func initViews() {
        super.setTitle("关于爱学啊（代码自动布局）")
        
        //设置背景颜色
        view.backgroundColor=UIColor.groupTableViewBackground
        
        //使用该框架
        //一定要在控件添加到view后
        
        //创建View
        //Logo
        let aboutLogo=UIImage(named: "LoginLogo")
        let aboutImageView=UIImageView(image: aboutLogo)
        view.addSubview(aboutImageView)
        
        //设置Logo圆角
        ViewUtil.showSmallRadius(view: aboutImageView)
        
        //当前版本
        btVersion = UIButton(type: .system)
        btVersion.setTitle("当前版本", for: .normal)
        btVersion.setTitleColor(UIColor.black, for: .normal)
        
        //文本居左
        btVersion.contentHorizontalAlignment = .left
        
        //左边padding
        btVersion.titleEdgeInsets=UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        btVersion.backgroundColor=UIColor.white
        view.addSubview(btVersion)
        
        //右侧版本文本
        lbVersion=UILabel.init()
        lbVersion.textColor=UIColor.darkGray
        lbVersion.font=UIFont.systemFont(ofSize: 14)
        view.addSubview(lbVersion)
        
//        lbVersion.text="V1.0.0"
        
        //我的云音乐新手指南
        btGuide = UIButton(type: .system)
        btGuide.setTitle("我的云音乐新手指南", for: .normal)
        btGuide.setTitleColor(UIColor.black, for: .normal)
        
        //文本居左
        btGuide.contentHorizontalAlignment = .left
        
        //左边padding
        btGuide.titleEdgeInsets=UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        btGuide.backgroundColor=UIColor.white
        view.addSubview(btGuide)
        
        //右侧更多图标
        let imageGuideRightMore=UIImage(named: "ArrowRight")
        let ivGuideRight=UIImageView(image: imageGuideRightMore)
        view.addSubview(ivGuideRight)
        
        //关于我们
        btAbout = UIButton(type: .system)
        btAbout.setTitle("关于爱学啊", for: .normal)
        btAbout.setTitleColor(UIColor.black, for: .normal)
        
        //文本居左
        btAbout.contentHorizontalAlignment = .left
        
        //左边padding
        btAbout.titleEdgeInsets=UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        btAbout.backgroundColor=UIColor.white
        view.addSubview(btAbout)
        
        //右侧更多图标
        let imageAboutRightMore=UIImage(named: "ArrowRight")
        let ivAboutRightMore=UIImageView(image: imageAboutRightMore)
        view.addSubview(ivAboutRightMore)

        //设置约束
        
        //Logo
        aboutImageView.snp.makeConstraints { (make) in
            //宽高为85
            make.width.equalTo(80)
            make.height.equalTo(80)
            
            //顶部距离
            //equalTo后面是对象
            //距离View的顶部安全区
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(50)
            
            //水平居中
            make.centerX.equalTo(self.view)
        }
        
        //当前版本
        btVersion.snp.makeConstraints { (make) in
            //宽为父布局宽度
            make.width.equalTo(self.view)
            
            //高为50
            make.height.equalTo(50)
            
            //顶部距离
            //Logo向下30
            make.top.equalTo(aboutImageView.snp.bottom).offset(30)
            
            //如果这样写，是aboutImageView的top向下30
            //make.top.equalTo(aboutImageView).offset(30)
        }
        
        //版本
        lbVersion.snp.makeConstraints { (make) in
            //右侧位置为
            //btCurrentVersion宽度-10
            make.right.equalTo(self.btVersion).offset(-10)
            
            //在btVersion中垂直居中
            make.centerY.equalTo(self.btVersion)
        }
       
        //我的云音乐新手指南
        //距离上面button 10
        //由于背景是灰色
        //所以距离10就有分割线了
        btGuide.snp.makeConstraints { (make) in
            //宽为父布局宽度
            make.width.equalTo(self.view)
            
            //高为50
            make.height.equalTo(50)
            
            //顶部距离
            //上一个Button底部向下10
            make.top.equalTo(btVersion.snp.bottom).offset(10)

        }
        
        //右侧更多图标
        ivGuideRight.snp.makeConstraints { (make) in
            //宽高
            make.width.equalTo(22)
            make.height.equalTo(22)
            
            //位置和右侧文本一样，只是相对的view不一样
            //这部分可以重构到方法
            //水平，为btCurrentVersion宽度-10
            make.right.equalTo(btGuide).offset(-10)
            
            //右侧位置为
            //btCurrentVersion宽度-10
            make.right.equalTo(btGuide).offset(-10)
            
            //在btGuide垂直居中
            make.centerY.equalTo(self.btGuide)
        }
        
        //关于爱学啊
        //距离上面button 1
        btAbout.snp.makeConstraints { (make) in
            //宽为父布局宽度
            make.width.equalTo(self.view)
            
            //高为50
            make.height.equalTo(50)
            
            //顶部距离
            //上一个Button底部向下1
            make.top.equalTo(btGuide.snp.bottom).offset(1)
            
        }
        
        //右侧更多图标
        ivAboutRightMore.snp.makeConstraints { (make) in
            //宽高
            make.width.equalTo(22)
            make.height.equalTo(22)
            
            make.right.equalTo(self.btAbout).offset(-10)
            
            make.right.equalTo(btAbout).offset(-10)
            
            make.centerY.equalTo(self.btAbout)
        }
        
    }
    
    override func initDatas() {
        super.initDatas()
        
        let version=BundleUtil.appVersion()
        let buildVersion=BundleUtil.appBuildVersion()
        
        lbVersion.text="V\(version).\(buildVersion)"
    }
    
    override func initListeners() {
        super.initListeners()
        
        //版本按钮点击
        btVersion.addTarget(self, action: #selector(onVersionClick), for: .touchUpInside)
        
        //关于我们按钮点击
        btAbout.addTarget(self, action: #selector(onAboutClick), for: .touchUpInside)
    }
    
    /// 版本点击
    @objc func onVersionClick() {
        ToastUtil.short("你点击当前版本！")
        
        
        //如果要上架App Store不能内部检查是否有新版本
        //否则审核可能不会通过
        //跳转到App Store中当前App地址
        
        //跳转到当前App Store地址，真机才能测试
        UIApplication.shared.openURL(URL(string: MY_APPSTORE_URL)!)
    }
    
    /// 关于我们的点击
    @objc func onAboutClick() {
        WebController.start(navigationController!, "关于我们", "http://www.ixuea.com/posts/3")
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: -  启动界面
extension AboutCodeController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=AboutCodeController.init()
        
        //进入当前界面的时候
        //隐藏底部的tabBar
        controller.hidesBottomBarWhenPushed=true
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
