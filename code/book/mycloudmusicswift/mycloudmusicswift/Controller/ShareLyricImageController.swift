//
//  ShareLyricImageController.swift
//  分享歌词图片界面
//
//  Created by smile on 2019/4/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ShareLyricImageController: BaseTitleController {
    
    /// 容器控件
    @IBOutlet weak var svContainer: UIScrollView!
    
    /// 封面图片
    @IBOutlet weak var ivCover: UIImageView!
    
    /// 显示歌词的控件
    @IBOutlet weak var lbLyric: UILabel!
    
    /// 歌曲名称
    @IBOutlet weak var lbSongName: UILabel!
    
    /// 分享图片按钮
    @IBOutlet weak var btShareLyricImage: UIButton!
    
    
    /// 保存歌词图片到相册
    @IBOutlet weak var btSaveLyricImage: UIButton!
    
    var song:Song!
    
    var lyric:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func initViews() {
        super.initViews()
        
        setTitle("分享歌词图片")
        
        btShareLyricImage.showColorPrimaryBorder()
        btSaveLyricImage.showColorPrimaryBorder()
    }
    
    override func initDatas() {
        super.initDatas()
        
        //显示封面
        ImageUtil.show(ivCover, song.banner)
        
        //显示歌词
//        lbLyric.text=lyric
        
        //使用富文本设置行高
        let attrString = NSMutableAttributedString(string: lyric)
        
        let paragraphStyle = NSMutableParagraphStyle.init()
        
        //行高是多少倍
        paragraphStyle.lineHeightMultiple = 1.5
        
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0,length: lyric.count))
        
        lbLyric.attributedText=attrString
        
        //设置歌曲名称
        lbSongName.text="——「\(song.title!)」"
    }
    
    
    /// 分享歌词图片点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onShareLyricImageClick(_ sender: UIButton) {
        print("ShareLyricImageController onShareLyricImageClick")
        
        //将界面转为图片
        let image=ViewUtil.captureScrollView(svContainer)
        
        ShareUtil.shareImage(image)
    }
    
    
    /// 保存歌词图片到相册点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSaveLyricImageClick(_ sender: UIButton) {
        print("ShareLyricImageController onSaveLyricImageClick")
        
        let image=ViewUtil.captureScrollView(svContainer)
        saveImage(image)
    }
    
    /// 保存图片到相册
    ///
    /// - Parameter data: <#data description#>
    func saveImage(_ data:UIImage) {
        UIImageWriteToSavedPhotosAlbum(data, self, #selector(image(image:didFinishSavingWithError:contextInfo:)),nil)
    }
    
    
    /// 保存图片到相册回调
    /// 经测试写到工具类中（静态和实例）
    /// 回调找不到方法
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - error: <#error description#>
    ///   - contextInfo: <#contextInfo description#>
    //    @objc static func
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer){
        var message=""
        
        if error==nil {
            message = "保存成功！"
            
        } else {
            message = "保存失败，请稍后再试！"
        }
        
        ToastUtil.short(message)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - 启动界面
extension ShareLyricImageController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ song:Song,_ lyric:String) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "ShareLyricImage") as! ShareLyricImageController
        
        //将传递过来的数据设置到controller中
        controller.song=song
        controller.lyric=lyric
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
