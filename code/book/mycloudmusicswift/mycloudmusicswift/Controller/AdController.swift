//
//  AdController.swift
//  启动界面广告
//  根控件不能使用StackView
//  因为他有一些边距设置
//  就算去除了里面的内容还有边距
//
//  Created by smile on 2019/4/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class AdController: BaseCommonController {
    /// 显示广告图片的控件
    @IBOutlet weak var ivAd: UIImageView!
    
    /// 点击广告的按钮
    @IBOutlet weak var btAd: UIButton!
    
    /// 跳过按钮
    @IBOutlet weak var btSkipAd: UIButton!
    
    /// 异步任务
    var task:DispatchWorkItem!
    
    
    /// 广告地址
    /// 只有点击了启动页广告才有值
    var adUri:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func initViews() {
        super.initViews()
        
        //设置跳过广告按钮圆角
        ViewUtil.showRadius(btSkipAd,15)
    }
    
    override func initDatas() {
        super.initDatas()
        
        //TODO 设置广告
        //如果通过服务端获取到广告图片
        //可以在这设置到ImageView
        
        //如果要取消异步任务就需要这样使用
        //创建一个任务
        task = DispatchWorkItem {
            //这里是异步任务执行时回调
            self.next()
        }
        
        //添加到异步队列中
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0, execute: task)
    }
    
    /// 延时3后调用该方法
    func next() {
        NSLog("%@", "ad controller next")
        
        AppDelegate.shared.toHome(adUri)
    }

    /// 点击了广告按钮
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onAdClick(_ sender: UIButton) {
        print("ad controller onAdClick")
        
        //正常情况，该界面的广告来自一个接口
        //他应该包含显示的图片，点击跳转的网址
        //这里就写死
        
        //由于当前界面和首页的导航控制器不是同一个
        //所以不能再这里启动显示广告的页面
        //而应该在显示首页后，在显示广告页面
        //因为我们希望用户看完广告，返回的是首页，而不是广告页面
        
        adUri="http://www.ixuea.com/posts/13"
        
        onSkipAdClick(sender)
    }
    
    /// 点击了跳过广告按钮
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSkipAdClick(_ sender: UIButton) {
        print("ad controller onSkipAdClick")
        
        //统计跳过广告次数
        AnalysisUtil.onSkipAd(PreferenceUtil.userId()!)
        
        //取消延时
        task.cancel()
        
        //马上进入下一步操作
        next()
    }
    
    //状态栏样式
    override var preferredStatusBarStyle: UIStatusBarStyle{
        //内容为白色
        return UIStatusBarStyle.lightContent
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 页面统计
extension AdController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //使用极光分析
        //统计页面
        JANALYTICSService.startLogPageView("Ad")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //页面结束
        JANALYTICSService.stopLogPageView("Ad")
    }
}
