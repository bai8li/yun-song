//
//  SelectTopicController.swift
//  选择话题
//  该界面使用SwiftEventBus框架
//  来实现选择完话题后传递到评论界面
//
//  Created by smile on 2019/4/18.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class SelectTopicController: BaseTitleController {
    
    @IBOutlet weak var tableView: UITableView!
    
    /// 当前界面列表数据
    var dataArray:[Topic]=[]

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func initViews() {
        super.initViews()
        
        //注册Cell
        tableView.register(UINib(nibName:TopicCell.NAME , bundle: nil), forCellReuseIdentifier: TopicCell.NAME)
    }
    
    override func initDatas() {
        super.initDatas()
        
        fetchData()
    }
    
    /// 获取数据
    func fetchData()  {
        Api.shared
            .topics()
            .subscribeOnSuccess { data in
                if let data = data?.data {
                    self.dataArray = self.dataArray + data
                    self.tableView.reloadData()
                    
                }
            }.disposed(by: disposeBag)
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SelectTopicController:UITableViewDataSource,UITableViewDelegate {
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: tableView description
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: TopicCell.NAME, for: indexPath) as! TopicCell
        
        //取出数据
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
        
        return cell
    }
    
    /// Cell点击回调
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data=dataArray[indexPath.row];
        
        //发送通知
        SwiftEventBus.post(SELECT_TOPIC, sender: data)
        
        //关闭当前界面
        navigationController?.popViewController(animated: true)
    }
    
    
}

// MARK: - 启动界面
extension SelectTopicController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "SelectTopic") as! SelectTopicController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
