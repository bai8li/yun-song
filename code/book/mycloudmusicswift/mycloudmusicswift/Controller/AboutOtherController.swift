//
//  AboutOtherController.swift
//  关于爱学啊界面
//  本界面主要是演示项目中多个storyboard的使用
//  因为在真实项目中多个编辑storyboard经常会冲突
//  所以真实项目一般会按照模块将界面拆分到多个storyboard
//  例如：用户登陆注册，是一个
//  订单相关是一个
//  我们这里就将关于我们界面拆分到About.storyboard
//  当然有些项目中，不用storyboard
//  而是使用SnipKit这样的布局框架
//  该知识点我们也讲解了
//
//  Created by smile on 2019/4/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class AboutOtherController: BaseTitleController {
    
    /// 文本控件
    @IBOutlet weak var lbTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        //设置内容
        lbTitle.text="这个界面在About.storyboard中\n我是代码中设置的"
    }

    
}

// MARK: - 启动界面
extension AboutOtherController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建storyboard
        let storyboard=UIStoryboard(name: "About", bundle: nil)
        
        //创建控制器
        let controller=storyboard.instantiateViewController(withIdentifier: "AboutOther") as! AboutOtherController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
