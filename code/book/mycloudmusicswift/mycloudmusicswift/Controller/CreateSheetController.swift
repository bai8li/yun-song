//
//  CreateSheetController.swift
//  创建歌单控制器
//
//  Created by smile on 2019/4/27.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class CreateSheetController: BaseTitleController {

    @IBOutlet weak var tfContent: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        setTitle("创建歌单")
        
        //创建保存按钮
        //通过按钮创建一个BarButtonItem
        let saveBarItem = UIBarButtonItem(title: "保存", style: .plain, target: self, action: #selector(onSaveClick(sender:)))
       
        navigationItem.rightBarButtonItem=saveBarItem
    }
    
    /// 保存点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onSaveClick(sender:UIButton) {
        print("CreateSheetController onSaveClick")
        
        let title=tfContent.text!.trim()
        if title!.isEmpty {
            ToastUtil.short("请输入歌单名称.")
            return
        }
        
        Api.shared
            .createSheet(title!)
            .subscribeOnSuccess { (data) in
                //发送通知
                //列表界面刷新
                //这样才能查看到新创建的歌单
                SwiftEventBus.post(ON_SHEET_CREATED, sender: data)
                
                self.navigationController!.popViewController(animated: true)
        }.disposed(by: disposeBag)
 
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 启动界面
extension CreateSheetController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "CreateSheet") as! CreateSheetController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
