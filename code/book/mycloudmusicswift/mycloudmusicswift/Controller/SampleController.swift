//
//  SampleController.swift
//  讲解项目中常用，并且使用比较复杂的框架用户
//  目的是让大家在学习项目前对这些框架有一个大概认识
//
//  Created by smile on 2019/5/2.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SampleController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SampleController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "Sample") as! SampleController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
