//
//  BaseController.swift
//  所有控制器通用父类
//  实现的功能是：将viewDidLoad方法的职责
//  拆分为三个方法，目的是让方法的职责单一
//
//  Created by smile on 2019/4/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
        initDatas()
        initListeners()
    }
    
    /// 初始化控件
    func initViews(){
        
    }
    
    /// 初始化数据
    func initDatas(){
        
    }
    
    /// 初始化监听器
    func initListeners(){
        
    }

}
