//
//  SelectFriendController.swift
//  选择好友控制器
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class SelectFriendController: BaseTitleController {

    /// 列表控件
    @IBOutlet weak var tableView: UITableView!

    /// 搜索控制器
    private var searchController:UISearchController!
    
    /// 当前界面列表数据
    var dataArray:[UserGroup]=[]
    
    /// 搜索结果列表
    var resultDataArray:[UserGroup]=[]
    
    /// 数据首字母列表
    var letterDataArray:[String]=[]
    
    /// 搜索结果首字母列表
    var resultLetterDataArray:[String]=[]
    
    override func initViews() {
        super.initViews()
        
        setTitle("选择好友")
        
        //设置右侧索引字体颜色
        tableView.sectionIndexColor = UIColor(hex: COLOR_PRIMARY)
        
        //设置右侧索引背景色
//        tableView.sectionIndexBackgroundColor = UIColor.lightGray
        
        //初始化搜索控制器
        searchController=UISearchController(searchResultsController: nil)
        
        //设置搜索控制器代理
        searchController.delegate = self
        
        //设置搜索控制器结果回调代理
        searchController.searchResultsUpdater = self
        
        //搜索时背景变暗色
        searchController.dimsBackgroundDuringPresentation = false
        
        //搜索时背景变模糊
        searchController.obscuresBackgroundDuringPresentation = false
        
        //进入搜索状态后隐藏导航栏
        searchController.hidesNavigationBarDuringPresentation = false
        
        // 添加 searchbar 到 headerview
        tableView.tableHeaderView = self.searchController.searchBar
        
        //注册Cell
        tableView.register(UINib(nibName: UserCell.NAME, bundle:nil), forCellReuseIdentifier: UserCell.NAME)
        
        
    }
    
    override func initDatas() {
        super.initDatas()
        
        Api.shared
            .friends(PreferenceUtil.userId()!)
            .subscribeOnSuccess { data in
                if let data = data?.data {

                    //使用测试数据
                    self.dataArray=self.dataArray+DataUtil.processUser(self.getTestData())
                    
//                    let groups=DataUtil.processUser(data)
                    
                    //使用真实数据
//                    self.dataArray=self.dataArray+DataUtil.processUser(data)
                   
                    //字母
                    self.letterDataArray=self.letterDataArray+DataUtil.processUserLetter(self.dataArray)
                    
                    self.tableView.reloadData()
                }
            }.disposed(by: disposeBag)

    }
    
    
    /// 返回测试数据
    /// 由于服务端的数据比较少
    /// 所以这里返回一些测试数据
    ///
    /// - Returns: <#return value description#>
    func getTestData() -> [User] {
        var results:[User]=[]
        
        for i in 0..<50 {
            let user=User()
            user.nickname="我的云音乐\(i)"
            results.append(user)
        }
        
        for i in 0..<50 {
            let user=User()
            user.nickname="爱学啊\(i)"
            results.append(user)
        }
        
        return results
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - 列表数据源和代理
extension SelectFriendController: UITableViewDataSource,UITableViewDelegate {
    // MARK:- TableView数据源
    /// 多少组
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive {
            return resultDataArray.count
        }else{
            return dataArray.count
        }
    }
    
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        var group:UserGroup!
//
//        if searchController.isActive {
//            group=resultDataArray[section]
//        } else {
//            group=dataArray[section]
//        }
        
        return getGroup(section).datas.count
    }
    
    func getGroup(_ section:Int) -> UserGroup {
        var group:UserGroup!
        
        if searchController.isActive {
            group=resultDataArray[section]
        } else {
            group=dataArray[section]
        }
        
        return group
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: UserCell.NAME, for: indexPath) as! UserCell
        
//        let group=dataArray[indexPath.section]
//        let data=group.datas[indexPath.row]
        
        let data=getGroup(indexPath.section).datas[indexPath.row]
        cell.bindData(data)
        
        return cell
    }
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //退出搜索状态
        
        let data=getGroup(indexPath.section).datas[indexPath.row]
        
        //发送通知
        SwiftEventBus.post(SELECT_FRIEND, sender: data)
        
        //关闭当前界面
        navigationController?.popViewController(animated: true)
        
    }
    
    
    /// 组标题
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let group=getGroup(section)
        
        return group.title
    }
    
    
    /// TableView右侧索引标题
    ///
    /// - Parameter tableView: <#tableView description#>
    /// - Returns: <#return value description#>
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if searchController.isActive {
            return resultLetterDataArray
        } else {
            return letterDataArray
        }
    }
    
    /// 返回section（分组）与右侧字母索引的对应关系
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - title: <#title description#>
    ///   - index: <#index description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        //我们这里因为关系是一样的
        //所以不用复写这个方法
        return index
    }
}

// MARK: - 启动界面
extension SelectFriendController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "SelectFriend") as! SelectFriendController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}

// MARK: - 搜索状态
//都是可选的协议
//只有需要的时候才重写
extension SelectFriendController:UISearchControllerDelegate{
    
    /// 即将进入搜索状态
    ///
    /// - Parameter searchController: <#searchController description#>
    func willPresentSearchController(_ searchController: UISearchController) {
        print("SelectFriendController willPresentSearchController")
    }
    
    /// 已经进入搜索状态
    ///
    /// - Parameter searchController: <#searchController description#>
    func didPresentSearchController(_ searchController: UISearchController) {
        print("SelectFriendController didPresentSearchController")
    }
    
 
    /// 即将退出搜索状态
    ///
    /// - Parameter searchController: <#searchController description#>
    func willDismissSearchController(_ searchController: UISearchController) {
        print("SelectFriendController willDismissSearchController")
    }
    
    /// 已经退出搜索状态
    ///
    /// - Parameter searchController: <#searchController description#>
    func didDismissSearchController(_ searchController: UISearchController) {
        print("SelectFriendController didDismissSearchController")
    }
    
    /// 点击了搜索框
    ///
    /// - Parameter searchController: <#searchController description#>
    func presentSearchController(_ searchController: UISearchController) {
        print("SelectFriendController presentSearchController")
    }
}


// MARK: - 搜索结果回调
extension SelectFriendController:UISearchResultsUpdating{
    
    /// 搜索输入框内容改变了
    ///
    /// - Parameter searchController: <#searchController description#>
    func updateSearchResults(for searchController: UISearchController) {
        
        //去除空格
        let searchString = searchController.searchBar.text?.trim()
      
        print("SelectFriendController updateSearchResults:\(searchString)");
        
        //清除原来的数据
        resultDataArray.removeAll()
        resultLetterDataArray.removeAll()
        
        //使用方法过滤
        resultDataArray = DataUtil.filterUser(dataArray,searchString!)
        
        //从搜索的结果中
        //获取索引
        resultLetterDataArray = DataUtil.processUserLetter(resultDataArray)
        
        //刷新列表
        tableView.reloadData()
    }
    
}
