//
//  WebController.swift
//  通用的显示网页界面
//
//  Created by smile on 2019/4/11.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//WKWebView到这个模块中
import WebKit

class WebController: BaseCommonController,WKUIDelegate,WKNavigationDelegate {
    
    /// 要显示的地址
    var uri:String!

    @IBOutlet weak var wv: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        //设置UI代理
        //例如：触摸
        wv.uiDelegate=self
        
        //导航代理
        //例如：返回
        wv.navigationDelegate=self
    }
    

    override func initDatas() {
        super.initDatas()
        //将字符串创建为URL
        let url=URL(string: uri)!
        
        //创建request
        let request = URLRequest(url: url)
        
        //使用WebView加载
        wv.load(request)
    }
    
    
    /// 启动网页界面
    ///
    /// - Parameters:
    ///   - navigationController: 导航控制器
    ///   - title: 要显示的标题
    ///   - uri: 要显示的网页地址
    static func start(_ navigationController:UINavigationController,_ title:String,_ uri:String) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "Web") as! WebController
        
        //将传递过来的数据设置到controller中
        controller.title=title
        controller.uri=uri
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
