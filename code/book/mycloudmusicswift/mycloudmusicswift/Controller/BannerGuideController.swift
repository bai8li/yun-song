//
//  BannerGuideController.swift
//  引导界面轮播图（使用扩展的方式，将轮播图代码拆分到这里了）
//
//  Created by smile on 2019/4/6.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

extension GuideController:YJBannerViewDelegate,YJBannerViewDataSource{
    /// banner数据来源
    ///
    /// - Parameter bannerView: 轮播组价
    /// - Returns: 返回要显示的图片
    func bannerViewImages(_ bannerView: YJBannerView!) -> [Any]! {
        return ["Guide1","Guide2","Guide3","Guide4","Guide5"]
    }
    
    
    
    /// 自定义Cell
    /// 这里复写该方法的目的是，设置图片的缩放模式
    ///
    /// - Parameters:
    ///   - bannerView: <#bannerView description#>
    ///   - customCell: 原来的组件
    ///   - index: 当前索引
    /// - Returns: 返回新的cell
    func bannerView(_ bannerView: YJBannerView!, customCell: UICollectionViewCell!, index: Int) -> UICollectionViewCell! {
        
        //        将cell转为YJBannerViewCell
        //        因为我们要给他配置一些属性
        let cell = customCell as! YJBannerViewCell
        
        //按比例，从中心填充，多余的裁掉
        cell.showImageViewContentMode=UIView.ContentMode.scaleAspectFill
        
        return cell
    }
    
    
    
    /// banner点击方法
    ///
    /// - Parameters:
    ///   - bannerView: <#bannerView description#>
    ///   - index: 点击的索引
    func bannerView(_ bannerView: YJBannerView!, didSelectItemAt index: Int) {
        NSLog("guide controller banner click:%d",index )
    }
}
