//
//  TopicDetailController.swift
//  话题详情
//
//  Created by smile on 2019/4/17.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TopicDetailController: BaseTitleController {

    @IBOutlet weak var lbTitle: UILabel!
    
    /// 话题Id
    var id:String?
    
    /// 话题标题
    var topicTitle:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initDatas() {
        super.initDatas()
        
        setTitle("话题")
        
        lbTitle.text=topicTitle!
    }
    
    /// 通过用户Id启动界面
    ///
    static func start(_ navigationController:UINavigationController,id:String) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "TopicDetail") as! TopicDetailController
        
        controller.id=id
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
    
    /// 通过话题名称启动界面
    ///
    static func start(_ navigationController:UINavigationController,title:String) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "TopicDetail") as! TopicDetailController
        
        controller.topicTitle=title
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
