//
//  OrderDetailController.swift
//  订单详情界面
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//发布订阅框架
import SwiftEventBus

class OrderDetailController: BaseTitleController {

    /// 显示成功信息
    @IBOutlet weak var lbPaySuccess: UILabel!
    
    /// 订单号
    @IBOutlet weak var lbNumber: UILabel!
    
    /// 订单状态
    @IBOutlet weak var lbStatus: UILabel!
    
    /// 商品图
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 商品标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 订单创建时间
    @IBOutlet weak var lbCreated: UILabel!
    
    /// 订单来源
    @IBOutlet weak var lbSource: UILabel!
    
    /// 订单平台
    @IBOutlet weak var lbPlatform: UILabel!
    
    /// 订单渠道
    @IBOutlet weak var lbChannel: UILabel!
    
    /// 支付方式容器
    @IBOutlet weak var svPayMethodContainer: UIStackView!
    
    /// 支付宝选中状态图片控件
    @IBOutlet weak var ivAlipay: UIImageView!
    
    /// 微信选中状态图片控件
    @IBOutlet weak var ivWechat: UIImageView!
    
    /// 支付宝容器
    @IBOutlet weak var svAlipay: UIStackView!
    
    /// 微信容器
    @IBOutlet weak var svWechat: UIStackView!
    
    /// 要支付的价格
    @IBOutlet weak var lbPrice: UILabel!
    
    /// 支付按钮容器
    @IBOutlet weak var lbPayContainer: UIStackView!
    
    var id:String!
    
    /// 支付渠道
    /// 默认是支付宝
    var channel:Order.Channel = .alipay
    
    var data:Order!
    
    override func initViews() {
        super.initViews()
        
        setTitle("订单详情")
    }
    
    override func initDatas() {
        super.initDatas()
        
        Api.shared
            .orderDetail(id)
            .subscribeOnSuccess { (data) in
                if let data = data?.data {
                   self.showData(data)
                }
            }.disposed(by: disposeBag)
        
    }
    
    /// 显示订单数据
    ///
    /// - Parameter data: <#data description#>
    func showData(_ data:Order) {
        self.data=data
        
        //订单号
        lbNumber.text=data.number
        
        //商品图片
        ImageUtil.show(ivBanner, data.book.banner)
        
        //商品标题
        lbTitle.text=data.book.title
        
        //创建订单日志
        lbCreated.text="下单日期: \(TimeUtil.format3(data.created_at))"
        
        //订单来源
        lbSource.text="订单来源: \(data.sourceFormat)"
        
        //订单价格
        lbPrice.text="￥\(data.price!)"
        
        //显示订单状态
        showOrderStatus(data)
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听支付宝支付状态
        SwiftEventBus.onMainThread(self, name: ON_ALIPAY_CALLBACK) { result in
            let data = result!.object as! [String : Any]
            
            //调用支付宝支付结果出来方法
            self.processAlipayResult(data)
        }
        
        //支付宝点击
        svAlipay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onAlipayClick)))
        
        //微信点击
        svWechat.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onWechatClick)))
    }
    
    /// 支付宝点击
    @objc func onAlipayClick() {
        print("OrderDetailController onAlipayClick")
        
        //选中支付宝
        ivAlipay.image=UIImage(named: "CheckBoxSelected")
        
        //取消选中微信
        ivWechat.image=UIImage(named: "CheckBox")
        
        //将支付渠道设置为支付宝
        channel = .alipay
    }
    
    
    /// 微信点击
    @objc func onWechatClick() {
        print("OrderDetailController onWechatClick")
        
        ivAlipay.image=UIImage(named: "CheckBox")
        ivWechat.image=UIImage(named: "CheckBoxSelected")
        
        channel = .wechat
    }
    
    /// 支付按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPayClick(_ sender: UIButton) {
        print("OrderDetailController onPayClick")
        
//        processAlipay("替换成支付宝支付参数")
        
        fetchPayData()
        
        
    }
    
    /// 获取支付参数
    /// 这里是按照真实项目开发来的
    /// 也就是支付的参数全部在后台生成
    /// 客户端直接设置到支付sdk中
    /// 客户端不能修改
    /// 如果修改了签名就会错误
    func fetchPayData() {
        Api.shared
            .orderPay(id,channel.rawValue)
            .subscribeOnSuccess { (data) in
                if let data = data?.data {
                     self.processPay(data)
                }
            }.disposed(by: disposeBag)

    }
    
    /// 处理支付
    ///
    /// - Parameter data: <#data description#>
    func processPay(_ data:Pay) {
        switch data.channel! {
        case .alipay:
            //支付宝支付
            processAlipay(data.pay)
        case .wechat:
            //微信支付
            processWechat(data.pay)
        default:
            break
        }
    }

    
    /// 支付宝支付
    ///
    /// - Parameter data: <#data description#>
    func processAlipay(_ data:String) {
        //支付宝官方开发文档：https://docs.open.alipay.com/204/105295/
        AlipaySDK.defaultService()!.payOrder(data, fromScheme: ALIPAY_CALLBACK_SCHEME) { (data) in
            print("OrderDetailController onPayClick callback:\(data)")
            
            //如果手机中没有安装支付宝客户端
            //会跳转H5支付页面
            //支付相关的信息会通过这个方法回调
            
            self.processAlipayResult(data as! [String : Any])
        }
    }
    
    
    /// 处理支付宝支付结果
    ///
    /// - Parameter data: <#data description#>
    func processAlipayResult(_ data:[String:Any]) {
        let resultStatus = data["resultStatus"] as! String
//        if true {
            if resultStatus == "9000" {
            //本地支付成功
            //不能依赖本地支付结果
            //必须以服务端为准
            ToastUtil.showLoading("支付确认中，请稍后...")
            
            //延时3秒钟
            //延迟是因为可能有支付回调有延迟
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.next()
            }
                
                //这里就不根据服务端判断了
                //购买成功统计
                AnalysisUtil.onPurchase(true,self.data)
        }else{
            ToastUtil.short("支付失败，请稍后再试！")
                
                //购买失败统计
                AnalysisUtil.onPurchase(false,self.data)
        }
    }
    
    /// 微信支付
    ///
    /// - Parameter data: <#data description#>
    func processWechat(_ data:String) {
        //TODO 微信支付
        
    }
    
    /// 下一步
    /// 确认订单是否支付成功
    /// 本地回调的成功只是界面做处理
    /// 具体是否支付成功以服务端为准
    func next() {
        //调用服务端订单接口
        //查询订单状态
        Api.shared
            .orderDetail(id)
            .subscribe({ (data) in
                //不管是否支付成功都要关闭等待对话框
                ToastUtil.hideLoading()
                
                if let data = data?.data {
                    self.showOrderStatus(data)
                }else{
                    ToastUtil.short("支付失败，请稍后再试！")
                }
                
            }) { (baseResponse, error) -> Bool in
                //不管是否支付成功都要关闭等待对话框
                ToastUtil.hideLoading()
                
                //返回false
                //表示让父类自动处理错误
                //我们这里复写只是要关闭对话框而已
                return false
        }.disposed(by: disposeBag)
    
    }
    
    
    /// 显示订单状态
    ///
    /// - Parameter data: <#data description#>
    func showOrderStatus(_ data:Order) {
        //订单状态
        lbStatus.text=data.statusFormat
        
        //支付平台
        lbPlatform.text="支付平台: \(data.originFormat)"
        
        //支付渠道
        lbChannel.text="支付渠道: \(data.channelFormat)"
        
        switch data.status {
        case .payed:
            //支付成功
            print("OrderDetailController order pay success:\(data.id)")
            lbStatus.textColor=UIColor(named: "ColorPass")
            
            //显示支付成功提示
            lbPaySuccess.isHidden=false
            
            //隐藏支付渠道
            svPayMethodContainer.isHidden=true
            
            //隐藏支付按钮
            lbPayContainer.isHidden=true
            
            //发送支付成功通知
            SwiftEventBus.post(ON_PAY_SUCCESS)
        case .close:
            //订单关闭了
            print("OrderDetailController order close:\(data.id)")
            
        default:
            //等待支付
            print("OrderDetailController order wait pay:\(data.id)")
            
            //等待支付显示支付按钮
            lbPayContainer.isHidden=false
        }
        
        
    }
    
}

// MARK: - 启动界面
extension OrderDetailController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ id:String) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "OrderDetail") as! OrderDetailController
        
        //将传递过来的数据设置到controller中
        controller.id=id
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
