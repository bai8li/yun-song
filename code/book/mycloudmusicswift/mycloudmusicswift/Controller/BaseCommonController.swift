//
//  BaseCommonController.swift
//  通用的业务控制器；添加了一些常用的方法，例如：显示提示框
//  显示等待对话框
//
//  Created by smile on 2019/4/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class BaseCommonController: BaseController {
    
    //负责对象销毁
    //这个功能类似NotificationCenter的removeObserver
    let disposeBag = DisposeBag()

}
