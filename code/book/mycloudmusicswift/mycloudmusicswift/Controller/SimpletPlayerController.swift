//
//  SimpletPlayerController.swift
//  简单的播放器实现
//  主要测试音乐播放相关逻辑
//  因为黑胶唱片界面的逻辑比较复杂
//  如果在和播放相关逻辑混一起，不好实现
//  所有我们可以先使用一个简单的播放器
//  从而把播放器相关逻辑实现完成
//  然后在对接的黑胶唱片，就相对来说简单一点
//
//  Created by smile on 2019/4/14.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SimpletPlayerController: BaseTitleController {
    
    /// 播放按钮
    @IBOutlet weak var btPlay: UIButton!
    
    /// 音乐标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 进度条
    @IBOutlet weak var sdProgress: UISlider!
    
    /// 音乐播放进度
    @IBOutlet weak var lbStart: UILabel!
    
    /// 音乐总时长
    @IBOutlet weak var lbEnd: UILabel!
    
    
    /// 上一曲
    @IBOutlet weak var btPreviouse: UIButton!
    
    
    /// 循环模式按钮
    @IBOutlet weak var btLoopModel: UIButton!
    
    /// 列表视图
    @IBOutlet weak var tableView: UITableView!
    
    var playListManager:PlayListManager!
    var musicPlayerManager:MusicPlayerManager!
    
    var isTouchSlide = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func initViews() {
        super.initViews()
        
        setTitle("简单播放器")
    }
    
    override func initDatas() {
        super.initDatas()
    
        //测试单例模式
        //可以发现他们两次的内存地址都是一样
        //说明单例模式生效了
        let o1=PlayListManager.shared
        let o2=PlayListManager.shared
        print("TestPlayerController initDatas")
        
//        //获取播放管理器
//        musicPlayerManager=MusicPlayerManager.shared()
        
        //测试播放音乐
        //由于现在没有获取数据
        //所以创建一个测试数据
//        let songUrl="http://dev-courses-misuc.ixuea.com/assets/s1.mp3"
//
//        let song=Song()
//        song.uri=songUrl
//
//        musicPlayerManager.play(songUrl, song)
        
        playListManager=PlayListManager.shared
        musicPlayerManager=MusicPlayerManager.shared()
        
        //第一次进入界面
        //要显示这首音乐的初始化数据
        
        //显示音乐信息
        //包括标题等信息
        showInitData()
        
        //显示音乐时长
        showDuration()
        
        //显示播放进度
        showProgress()
        
        //显示播放状态
        showMusicPlayStatus()

        //显示循环模式
        showLoopModel()
        
        //第一次设置代理
        onEnterForeground()
    }
    
    override func initListeners() {
        super.initListeners()
        
        //监听APP进入前台
        //进入该界面不会执行
        //所以应该在合适的位置设置代理
        NotificationCenter.default.addObserver(self, selector: #selector(onEnterForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        //监听APP进入后台
        NotificationCenter.default.addObserver(self, selector: #selector(onEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    /// 进入了前台
    @objc func onEnterForeground() {
        print("SimpletPlayerController enterForeground")
        
        //设置播放代理
        musicPlayerManager.delegate=self
    }
    
    /// 进入了后台
    @objc func onEnterBackground() {
        print("SimpletPlayerController enterBackground")
        
        //移除播放代理
        musicPlayerManager.delegate=nil
    }
    
    /// 设置音乐的一些初始化数据
    func showInitData() {
        
        let song=playListManager.currentSong
        
        lbTitle.text=song?.title
    }
    
    /// 上一曲点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPreviousClick(_ sender: UIButton) {
        print("SimpletPlayerController onPreviousClick")

        if let song = playListManager.previous() {
           playListManager.play(song)
   
        }else{
            //TODO 正常情况下不能能走到这里
            //因为播放界面只有播放列表有数据时才能进入
            ToastUtil.short("没有可播放的音乐！")
        }

    }
    
    /// 播放点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onPlayClick(_ sender: UIButton) {
        print("SimpletPlayerController onPlayClick")
        playOrPause()
    }
    
    /// 下一曲点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onNextClick(_ sender: UIButton) {
        print("SimpletPlayerController onNextClick")
 
        if let song = playListManager.next() {
            playListManager.play(song)

        }else{
            ToastUtil.short("没有可播放的音乐！")
        }
    }
  
    /// 播放或者暂停
    func playOrPause() {
        if musicPlayerManager.isPlaying() {
            playListManager.pause()
            
        } else {
            playListManager.resume()
           
        }
    }
    
    /// 显示暂停状态
    func showPauseStatus() {
       btPlay.setTitle("暂停", for: .normal)
    }
    
    /// 显示播放状态
    func showPlayStatus() {
         btPlay.setTitle("播放", for: .normal)
    }
    
    /// 显示时长
    func showDuration() {
        let end=playListManager.currentSong!.duration
        
        if end > 0 {
            lbEnd.text=TimeUtil.formatTime1(end)
            sdProgress.maximumValue=Float(end)
        }
        
    }
    
    /// 显示进度
    func showProgress() {
        let progress=playListManager.currentSong!.progress
        
        if progress > 0 {
            if(!isTouchSlide){
                //当用户没有触摸进度条的时候才设置进度
                lbStart.text = TimeUtil.formatTime1(progress)
                sdProgress.value=Float(progress)
            }
            
        }
    }
    
    
    /// 显示音乐播放按钮等状态
    func showMusicPlayStatus() {
        if musicPlayerManager.isPlaying() {
            showPauseStatus()
        } else {
            showPlayStatus()
        }
    }
    
    /// 视图已经可见了
    ///
    /// - Parameter animated: <#animated description#>
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollPosition()
    }
    
    /// 滚动到当前音乐位置
    func scrollPosition() {
        //选中当前播放的音乐
        let currentSong=playListManager.getPlaySong()
        let playListOC=(playListManager.getPlayList()! as NSArray)
        let index=playListOC.index(of: currentSong)
        let indexPath=IndexPath(item: index, section: 0)
        
        //延时是因为前面可能执行的是删除歌曲
        //删除后TableView结构变更后才能选中
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            if index != -1 {
                //选中当前行
                self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableView.ScrollPosition.middle)
            }
        }
    }
    
    
    /// 音乐循环模式点击
    ///
    /// - Parameter sender: sender description
    @IBAction func onLoopModelClick(_ sender: UIButton) {
        print("SimpletPlayerController onLoopModelClick")
        playListManager.changeLoopModel()
        showLoopModel()
    }
    
    /// 显示循环模式
    func showLoopModel() {
        let model=playListManager.getLoopModel()
        switch model {
        case .list:
            btLoopModel.setTitle("列表循环", for: .normal)
        case .random:
            btLoopModel.setTitle("随机模式", for: .normal)
        default:
            btLoopModel.setTitle("单曲循环", for: .normal)
        }
    }

    // MARK: - 进度条相关
    /// 拖动进度条
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onChangeProgress(_ sender: UISlider) {
        print("SimpletPlayerController onChangeProgress:\(sender.value)")
        
        //拖拽进度的时候
        //将进度显示到文本上
        //这样用户好确定拖拽位置
        lbStart.text = TimeUtil.formatTime1(sender.value)
    }
    
    
    /// 按下进度
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSlideTouchDown(_ sender: UISlider) {
        print("SimpletPlayerController onSlideTouchDown")
        isTouchSlide=true
    }
    
    
    /// 抬起进度条
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onSlideTouchUp(_ sender: UISlider) {
        print("SimpletPlayerController onSlideTouchUp")
        isTouchSlide=false
        
        if(sender.value>1){
            //减一是为了用户能听清前一秒
            //音乐可能没那么明细
            //但视频继续播放的时候最好减一秒
            musicPlayerManager.seekTo(sender.value-1)
        }
    }

}

// MARK: - UITableView相关
extension SimpletPlayerController:UITableViewDataSource,UITableViewDelegate {
    /// 共有少个Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playListManager.getPlayList()!.count
    }
    
    /// 返回当前位置Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data=playListManager.getPlayList()![indexPath.row]
        
        let cell=tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath)
        
        cell.textLabel?.text=data.title
        
        return cell
    }
    
    /// Cell点击
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("SimpletPlayerController didSelectRowAt:\(indexPath.row)")
        
        //获取这首音乐
        let data=playListManager.getPlayList()![indexPath.row]
        
        playListManager.play(data)
    }
    
    // MARK: - 列表编辑相关
    
    /// 当前index是否可以编辑
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    /// 编辑的样式
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        //删除
        return UITableViewCell.EditingStyle.delete
    }
    
    
    /// 编辑Button的标题
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "删除"
    }
    
    
    /// 触发编辑按钮后的动作
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - editingStyle: <#editingStyle description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        print("SimpletPlayerController edit:\(indexPath.row)")
        
        //获取这首音乐
        let data=playListManager.getPlayList()![indexPath.row]
        
        //从播放列表中删除这首音乐
        playListManager.delete(data)
        
        tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        
        //再次获取当前播放的音乐
        let song = playListManager.getPlaySong()
        if song == nil {
            //关闭播放界面
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
}

// MARK: - 播放管理器代理
extension SimpletPlayerController:MusicPlayerDelegate{
    func onProgress(data: Song, progress: Float, duration: Float) {
        print("SimpletPlayerController onProgress:\(progress),\(duration)")
        showProgress()
    }
    
    func onPaused(_ data: Song) {
        showPlayStatus()
        
    }
    
    func onPlaying(_ data: Song) {
        showPauseStatus()
        
    }
    
    func onPrepared(_ data: Song) {
        print("SimpletPlayerController onPrepared:\(data.duration)")
        
        showInitData()
        
        showDuration()
        
        scrollPosition()
    }
    
    func onLyricChanged(_ data: Song) {
        print("SimpletPlayerController onLyricChanged:\(data.lyric)")
    }
    
    func onError(_ data: Song) {
        
    }
    
}

// MARK: - 启动界面
extension SimpletPlayerController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "SimpletPlayer") as! SimpletPlayerController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
