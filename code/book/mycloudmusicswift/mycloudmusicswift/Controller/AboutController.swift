//
//  AboutController.swift
//  关于爱学啊（可视化布局）
//  使用storyboard布局界面
//
//  Created by smile on 2019/4/27.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class AboutController: BaseTitleController {

    /// 版本
    @IBOutlet weak var lbVersion: UILabel!
    
    /// 版本容器
    @IBOutlet weak var vwVersion: UIView!
    
    /// 关于我们容器
    @IBOutlet weak var vwAbout: UIView!

    override func initViews() {
        super.initViews()
        
        setTitle("关于爱学啊（可视化布局）")
    }
    
    override func initDatas() {
        super.initDatas()
        let version=BundleUtil.appVersion()
        
        let buildVersion=BundleUtil.appBuildVersion()
        
        /// 显示版本
        lbVersion.text="V\(version).\(buildVersion)"
    }
    
    override func initListeners() {
        super.initListeners()
        
        //版本点击
        vwVersion.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onVersionClick)))
        
        //关于爱学啊
        vwAbout.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onAboutClick)))
    }
    
    /// 版本按钮点击
    @objc func onVersionClick() {
        ToastUtil.short("你点击当前版本！")
        
        
        //如果要上架App Store不能内部检查是否有新版本
        //否则审核可能不会通过
        //跳转到App Store中当前App地址
        
        //跳转到当前App Store地址，真机才能测试
        UIApplication.shared.openURL(URL(string: MY_APPSTORE_URL)!)
    }
    
    /// 关于我们点击
    @objc func onAboutClick() {
        print("AboutController onAboutClick")
        WebController.start(navigationController!, "关于我们", "http://www.ixuea.com/posts/3")
    }


}

// MARK: - 启动界面
extension AboutController {
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "About") as! AboutController
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}
