//
//  UserController.swift
//  好友，粉丝界面
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

class UserController: BaseTitleController {
    
    @IBOutlet weak var tableView: UITableView!
    
    /// 界面样式
    var style:UserStyle!
    
    var userId:String!
    
    /// 当前界面列表数据
    var dataArray:[User]=[]
    
    override func initViews() {
        super.initViews()
        
        //根据入口判断显示什么样的标题
        if UserStyle.friend==style {
            setTitle("好友")
            
            createAddFriendMenu()
            
        }else{
            setTitle("粉丝")
        }
        
        //注册Cell
        tableView.register(UINib(nibName: UserCell.NAME, bundle:nil), forCellReuseIdentifier: UserCell.NAME)
    }
    
    /// 创建添加好友按钮
    func createAddFriendMenu() {
        //创建右侧添加好友按钮
        //通过按钮创建一个BarButtonItem
        let addFriendBarItem=UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onAddFrendClick(sender:)))
        
        navigationItem.rightBarButtonItem=addFriendBarItem
    }
    
    /// 点击右侧添加好按钮
    ///
    /// - Parameter sender: <#sender description#>
    @objc func onAddFrendClick(sender:UIButton) {
        print("UserController onAddFrendClick")
        
        SearchUserController.start(navigationController!)
    }
    
    override func initDatas() {
        super.initDatas()
        
        let api:Observable<ListResponse<User>?>!
        
        //根据不同的入口显示不同的数据
        if UserStyle.friend==style {
            //好友
            api=Api.shared.friends(userId)
        }else{
            //粉丝
            api=Api.shared.fans(userId)
        }
 
        api.subscribeOnSuccess { data in
            if let data = data?.data {
                self.dataArray=self.dataArray+data
                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
     
    }
    
    
    /// 当前界面功能
    ///
    /// - friend: 好友列表
    /// - fans: 粉丝列表
    enum UserStyle {
        case friend
        case fans
    }

}


// MARK: - 启动界面
extension UserController{
    /// 启动界面
    ///
    static func start(_ navigationController:UINavigationController,_ userId:String,_ style:UserStyle) {
        //创建控制器
        let controller=navigationController.storyboard?.instantiateViewController(withIdentifier: "User") as! UserController
        
        controller.style=style
        controller.userId=userId
        
        //将控制器压入导航控制中
        navigationController.pushViewController(controller, animated: true)
        
    }
}

// MARK: - 列表数据源和代理
extension UserController: UITableViewDataSource,UITableViewDelegate {
    
    // MARK:- TableView数据源
    /// 返回有多个条目
    /// 和CollectionView差不多
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - section: <#section description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// 返回当前位置的Cell
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    /// - Returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //获取Cell
        let cell=tableView.dequeueReusableCell(withIdentifier: UserCell.NAME, for: indexPath) as! UserCell
        
        return cell
    }
    
    /// Cell即将显示
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - cell: <#cell description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //取出数据
        guard let cell = cell as? UserCell else {
            return
        }
        
        let data=dataArray[indexPath.row]
        
        cell.bindData(data)
    }
    
    /// Item点击事件
    ///
    /// - Parameters:
    ///   - tableView: <#tableView description#>
    ///   - indexPath: <#indexPath description#>
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data=dataArray[indexPath.row]
        
        UserDetailController.start(navigationController!, userId: data.id)
        
    }
}
