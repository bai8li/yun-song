//
//  BaseChatTextCell.swift
//  通用文本Cell
//
//  Created by smile on 2019/4/22.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseChatTextCell: BaseChatCell {
    
    /// 消息内容
    @IBOutlet weak var lbContent: UILabel!
    
    override func bindData(_ data: JMSGMessage) {
        super.bindData(data)
        //消息内容
        let content = data.content as! JMSGTextContent
        
        //显示消息内容
        lbContent.text=content.text
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
