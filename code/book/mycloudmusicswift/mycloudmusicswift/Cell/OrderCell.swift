//
//  OrderCell.swift
//  订单Cell
//
//  Created by smile on 2019/4/24.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
    static  let NAME = "OrderCell"
    
    /// 订单号
    @IBOutlet weak var lbNumber: UILabel!
    
    /// 订单状态
    @IBOutlet weak var lbStatus: UILabel!
    
    /// 商品图片
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 商品标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 商品价格
    @IBOutlet weak var lbPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ViewUtil.showSmallRadius(view: ivBanner)
    }
    
    func bindData(_ data:Order) {
        lbNumber.text=data.number
        
        lbStatus.text=data.statusFormat
        if data.status == .payed {
            //已经支付
            lbStatus.textColor=UIColor(named: "ColorPass")
        }else{
            lbStatus.textColor=UIColor(named: "ColorPrimary")
        }
        
        ImageUtil.show(ivBanner, data.book.banner)
        lbTitle.text=data.book.title
        lbPrice.text="￥\(data.book.price!)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
}
