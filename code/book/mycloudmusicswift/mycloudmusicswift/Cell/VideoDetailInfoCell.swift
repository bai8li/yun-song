//
//  VideoDetailTitleCell.swift
//  视频详情视频信息
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

import TagListView

class VideoDetailInfoCell: BaseTableViewCell {
    static var NAME = "VideoDetailInfoCell"
    
    /// 视频标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 创建时间
    @IBOutlet weak var lbCreateAt: UILabel!
    
    /// 播放数
    @IBOutlet weak var lbPlayCount: UILabel!
    
    @IBOutlet weak var tlTag: TagListView!
    
    /// 评论数
    @IBOutlet weak var lbLikeCount: UILabel!
    
    override func initListeners() {
        super.initListeners()
        
        tlTag.delegate=self
    }
    
    func bindData(_ data:Video) {
        
        lbTitle.text=data.title
        
        let createdAtString=TimeUtil.format3(data.created_at)
        lbCreateAt.text="发布: \(createdAtString)"
        
        lbPlayCount.text="播放: \(data.clicks_count)"
        
        //点赞数
        //其他设置上就行了
        lbLikeCount.text="\(data.likes_count)"
        
        //显示tag，由于服务端没有返回，所以写死
//        self.tvs.tags=[[NSArray alloc] initWithObjects:@"创意音乐",@"幽默",@"说唱", nil];
        
        //重新设置高度,5为上，下padding
        //因为还要设置cell高度，所以要在计算Cell高度时就计算他
//        self.tagHeightConstraint.constant=self.tvs.contentHeight+5*2;
        
        //先移除原来的Tag
        tlTag.removeAllTags()
        
        //添加一个Tag
        tlTag.addTag("爱学啊")
        
        //添加多个Tag
        tlTag.addTags(["创意音乐","幽默","说唱"])
        tlTag.addTags(["流行音乐","搞笑","古典音乐"])
        tlTag.addTags(["乡村","大闹","民谣音乐"])
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension VideoDetailInfoCell:TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("VideoDetailInfoCell click:\(title)")
    }
}
