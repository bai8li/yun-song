//
//  ImageCell.swift
//  图片Cell
//
//  Created by smile on 2019/4/18.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ImageCell: BaseCollectionViewCell {
    static var NAME = "ImageCell"
    
    @IBOutlet weak var ivImage: UIImageView!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(view: ivImage)
    }
    
    func bindData(_ data:Resouce) {
        ImageUtil.show(ivImage, data.uri)
    }

}
