//
//  TitleTableViewCell.swift
//  我的界面TableView标题Cell
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TitleTableViewCell: BaseTableViewCell {
    static var NAME = "TitleTableViewCell"
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    
    /// 更多按钮
    @IBOutlet weak var btMore: UIButton!
    
    /// 更多点击回调方法
    var onMoreClick:(() -> Void)?
    
    func bindData(_ data:Title) {
        lbTitle.text=data.title
        
        btMore.isHidden = !data.isShowMore
    }
    
    func bindData(_ data:String) {
        lbTitle.text=data
        btMore.isHidden=true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    /// 更多按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onMoreClick(_ sender: UIButton) {
        print("TitleTableViewCell onMoreClick")
        
        if let onMoreClick = onMoreClick {
            onMoreClick()
        }
    }
    
}
