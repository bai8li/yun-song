//
//  UserCell.swift
//  用户Cell
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    static let NAME = "UserCell"
    
    @IBOutlet weak var ivAvatar: UIImageView!
    
    @IBOutlet weak var lbNickname: UILabel!
    
    @IBOutlet weak var lbInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ViewUtil.showRadius(ivAvatar, 30)
    }
    
    func bindData(_ data:User) {
        ImageUtil.showAvatar(ivAvatar, data.avatar)
        
        self.lbNickname.text=data.nickname!
        
        lbInfo.text = data.formatDescription
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
