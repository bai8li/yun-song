//
//  TopicCell.swift
//  话题Cell
//
//  Created by smile on 2019/4/18.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TopicCell: BaseTableViewCell {
    static var NAME = "TopicCell"

    @IBOutlet weak var ivBanner: UIImageView!
    
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var lbDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ViewUtil.showSmallRadius(view: ivBanner)
    }
    
    /// 显示话题
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Topic) {
        ImageUtil.show(ivBanner, data.banner)
        lbTitle.text="#\(data.title!)#"
        lbDescription.text="\(data.joins_count)人参与"
    }
    
    /// 显示歌单
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Sheet) {
        if let banner = data.banner {
            ImageUtil.show(ivBanner, banner)
        }
        
        lbTitle.text=data.title
        lbDescription.text="\(data.songs_count)首"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
