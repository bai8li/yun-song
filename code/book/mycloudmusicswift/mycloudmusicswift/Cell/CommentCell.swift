//
//  CommentCell.swift
//  评论Cell
//
//  Created by smile on 2019/4/17.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class CommentCell: BaseTableViewCell {
    static var NAME = "CommentCell"
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 时间
    @IBOutlet weak var lbTime: UILabel!
    
    /// 点赞数
    @IBOutlet weak var lbLikeCount: UILabel!
    
    /// 点赞按钮
    @IBOutlet weak var btLike: UIButton!
    
    /// 评论内容
    @IBOutlet weak var lbContent: YYLabel!
    
    /// 被回复的评论内容
    @IBOutlet weak var lbReplyContent: YYLabel!
    
    
    /// 回复评论这部分到顶部距离的自动约束
    /// 为什么要找到他呢？
    /// 是因为在iOS中没有像Android中隐藏控件
    /// 并不占用位置的功能
    /// 而我们这里，回复评论这部分距离顶部有10
    /// 在有回复评论的时候，这个距离没问题
    /// 没有回复评论的时候底部就会多出10
    /// 解决方法是没有回复评论的时候设置为0
    @IBOutlet weak var replyContentMarginTop: NSLayoutConstraint!
    
    var data:Comment!
    
    /// 点赞点击回调方法
    var onLikeClick:((_ data:Comment) -> Void)!
    
    /// 头像点击回调方法
    var onAvatarClick:((_ data:User) -> Void)!
    
    /// 昵称点击回调方法
    var onNicknameClick:((_ data:String) -> Void)!
    
    /// 话题点击回调方法
    var onHashTagClick:((_ data:String) -> Void)!

    override func initViews() {
        super.initViews()
        
        //设置头像圆角
        ViewUtil.showRadius(ivAvatar, 17.5)
        
        //自动换行
        lbContent.numberOfLines = 0
        lbReplyContent.numberOfLines = 0
        
        //设置行高
        var modifier = YYTextLinePositionSimpleModifier()
        modifier.fixedLineHeight=SIZE_COMMENT_LINE_HEIGHT
        lbContent.linePositionModifier=modifier
        
        modifier = YYTextLinePositionSimpleModifier()
        modifier.fixedLineHeight=SIZE_COMMENT_LINE_HEIGHT
        lbReplyContent.linePositionModifier=modifier
        
        //使用该控件后，如果要自动换行，就要确定该View宽度
        //20+45是左侧，20是右侧
        
        //这部分可以通过设置一个变量，只设置一次就行了
        //因为对于当前界面来说，Cell宽度不会动态改变
        lbContent.preferredMaxLayoutWidth = self.frame.width - 20 - 45 - 20
        
        lbReplyContent.preferredMaxLayoutWidth = self.frame.width - 20 - 45 - 2 - 10 - 20
    }
    
    func bindData(_ data:Comment) {
        self.data=data
        
        ImageUtil.showAvatar(ivAvatar, data.user.avatar)
        
        lbNickname.text=data.user.nickname
        lbTime.text=TimeUtil.format1(data.created_at)
        
        lbLikeCount.text="\(data.likes_count)"
  
        //喜欢状态
        if data.isLiked() {
            lbLikeCount.textColor=UIColor(hex: COLOR_PRIMARY)
            btLike.setImage(UIImage(named: "CommentLiked"), for: .normal)
        } else {
            lbLikeCount.textColor=UIColor.lightGray
            btLike.setImage(UIImage(named: "CommentLike"), for: .normal)
        }
        

//        lbContent.text=data.content
        
        //用来保存修改的字符串
        let result=NSMutableAttributedString(string: data.content)
        lbContent.attributedText=processContent(data.content,result)
        
        if let parent = data.parent {
//            lbReplyContent.text=parent.content
            let replayConent="@\(parent.user.nickname!): \(parent.content!)"
            
            //用来保存修改的字符串
            let result=NSMutableAttributedString(string: replayConent)
            
            //被回复的评论设置颜色为灰色
            result.yy_color=UIColor.lightGray
            
            lbReplyContent.attributedText=processContent(replayConent,result)
            
            //有回复评论设置距离顶部距离为10
            replyContentMarginTop.constant=10
        }else{
            lbReplyContent.text=""
            
            //没有回复评论设置距离顶部距离为0
            replyContentMarginTop.constant=0
        }
    }
    
    /// 处理文本点击事件
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - result: <#result description#>
    /// - Returns: <#return value description#>
    func processContent(_ data:String,_ result:NSMutableAttributedString) -> NSMutableAttributedString {
        //重构到字符串工具类中
        StringUtil.processContent(data,result, { (containerView, text, range, rect) in
            
            //使用工具类方法再次重构
            let clickText = StringUtil.processClickText(data,range)
            
            print("CommentCell bindData on mention click:\(clickText)")
            
            self.onNicknameClick(clickText)
        }, { containerView, text, range, rect in
            //使用工具类方法再次重构
            let clickText = StringUtil.processClickText(data,range)
            
            print("CommentCell bindData on hash tag click:\(clickText)")
            
            self.onHashTagClick(clickText)
        })
        
        return result
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    /// 头像点击
    /// 真实项目中，可能还会实现
    /// 用户名也可以点击
    /// 或者顶部除了点赞那部分
    /// 其他的位置点击都是查看用户信息
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onAvatarClick(_ sender: UIButton) {
        print("CommentCell onAvatarClick:\(data.user.nickname)")
        
        onAvatarClick(data.user)
    }
    
    /// 点击点击方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLikeClick(_ sender: UIButton) {
        print("CommentCell onLikeClick:\(data.content)")
        
        onLikeClick(data)
    }
}
