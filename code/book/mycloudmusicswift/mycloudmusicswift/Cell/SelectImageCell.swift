//
//  SelectImageCell.swift
//  选择图片Cell
//
//  Created by smile on 2019/4/19.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SelectImageCell: BaseCollectionViewCell {
    
    @IBOutlet weak var ivImage: UIImageView!
    
    @IBOutlet weak var btDelete: UIButton!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(view: ivImage)
    }
    
    /// 删除图片按钮点击回调方法
    var deleteImageClick:((_ index:Int) -> Void)!
    
    func bindData(_ data:Any) {
        if data is String {
            //添加按钮
            btDelete.isHidden=true
            ivImage.image=UIImage(named: data as! String)
        }else{
            //真实的图片
            btDelete.isHidden=false
            ivImage.image=data as! UIImage
        }
    }
    
    @IBAction func onDeleteClick(_ sender: UIButton) {
        deleteImageClick(tag)
    }
}
