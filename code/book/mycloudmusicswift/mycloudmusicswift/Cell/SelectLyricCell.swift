//
//  SelectLyricCell.swift
//  选择歌词界面歌词Cell
//
//  Created by smile on 2019/4/21.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SelectLyricCell: BaseTableViewCell {
    static var NAME = "SelectLyricCell"

    /// 选中标识
    @IBOutlet weak var ivSelected: UIImageView!
    
    /// 歌词标题
    @IBOutlet weak var lbTitle: UILabel!
    
    override func initViews() {
        super.initViews()
        
        selectedBackgroundView = UIView(frame: frame)
        
        //设置背景为半透明效果
        //UIColor(white：1：表示创建一个白色
        //0.1表示白色有透明效果；0表示完全透明；1表示完全白色
        selectedBackgroundView!.backgroundColor = UIColor(white: 1, alpha: 0.1)
    }
    
    func bindData(_ data:LyricLine) {
        lbTitle.text=data.data
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if (selected) {
            ivSelected.isHidden=false
        } else {
            ivSelected.isHidden=true
        }
    }

}
