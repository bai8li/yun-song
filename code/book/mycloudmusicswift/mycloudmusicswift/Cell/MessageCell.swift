//
//  MessageCell.swift
//  消息Cell
//  会话Cell
//
//  Created by smile on 2019/4/22.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class MessageCell: BaseTableViewCell {
    static var NAME = "MessageCell"

    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 时间
    @IBOutlet weak var lbTime: UILabel!
    
    /// 消息
    @IBOutlet weak var lbMessage: UILabel!
    
    /// 未读消息数据
    @IBOutlet weak var lbMessageCount: UILabel!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showRadius(ivAvatar, 30)
        ViewUtil.showRadius(lbMessageCount, 7.5)
    }
    
    func bindData(_ data:JMSGConversation) {
        
        /// 获取最后一条消息
        if let latestMessage = data.latestMessage {
            //将毫秒时间戳转为秒时间戳
            let time = latestMessage.timestamp.intValue / 1000
            
            //将时间戳解析为date
            let date = Date(timeIntervalSince1970: TimeInterval(time))
            
            //格式化日期
            lbTime.text = TimeUtil.format1(date)
        } else {
            lbTime.text = ""
        }
        
        //显示最后一条消息内容
        lbMessage.text = data.latestMessageContentText()
        
        let user = data.target as! JMSGUser
        
        //获取用户名和头像
        UserManager.shared().getUser(StringUtil.removeUserId(user.username)) { (data) in
            self.lbNickname.text=data.nickname!
            ImageUtil.showAvatar(self.ivAvatar, data.avatar)
            
        }
        
        //未读消息
        if data.unreadCount != nil && (data.unreadCount?.intValue)! > 0 {
            //有未读消息
            lbMessageCount.text="\(data.unreadCount!.intValue)"
            
            lbMessageCount.isHidden=false
        } else {
            //没有
            lbMessageCount.isHidden=true
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
