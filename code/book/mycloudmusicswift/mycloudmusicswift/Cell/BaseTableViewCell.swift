//
//  BaseTableViewCell.swift
//  通用TableViewCell
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        initViews()
        initDatas()
        initListeners()
    }
    
    /// 初始化控件
    func initViews(){
        //去掉默认选中的灰色
        selectionStyle=SelectionStyle.none
        
    }
    
    /// 初始化数据
    func initDatas(){
        
    }
    
    /// 初始化监听器
    func initListeners(){
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
