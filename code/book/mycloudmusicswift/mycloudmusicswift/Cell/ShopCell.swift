//
//  ShopCell.swift
//  商品Cell
//
//  Created by smile on 2019/4/23.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ShopCell: BaseTableViewCell {
    static  let NAME = "ShopCell"

    /// 商品图片
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 价格
    @IBOutlet weak var lbPrice: UILabel!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(view: ivBanner)
    }
    
    func bindData(_ data:Book) {
        ImageUtil.show(ivBanner, data.banner)
        lbTitle.text=data.title
        lbPrice.text="￥\(data.price!)"
    }

}
