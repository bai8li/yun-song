//
//  SheetCell.swift
//  歌单Cell
//
//  Created by smile on 2019/4/12.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SheetCell: BaseCollectionViewCell {
    
    static var NAME = "SheetCell"
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    
    /// 歌单封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    
    /// 点击量
    @IBOutlet weak var lbCount: UILabel!
    
    /// 点击数
    @IBOutlet weak var ivClickCount: UIImageView!
    
    override func initViews() {
        super.initViews()
        //主题配置
        //标题
        lbTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //点击数文本
        lbCount.theme_textColor=[COLOR_STRING_WHITE,COLOR_STRING_LIGHT_GREY]
        
        //点击数图标
        ivClickCount.theme_image=["ClickCount","ClickCountNight"]
        
        //设置封面图圆角
        ViewUtil.showSmallRadius(view: ivBanner)
    }

    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Sheet) {
        //设置标题
        lbTitle.text=data.title
        
        //设置点击量
        lbCount.text="\(data.clicks_count)"
        
        //设置图片
        if let imageUri = data.banner {
            
//            //将图片路径转为绝对地址
//            let imageUri=ResourceUtil.resourceUri(imageUri)
//
//            //有图片才设置
//            let url=URL(string: imageUri)
//
//            //占位图片
//            //就是图片还没有加载回来前这段时间
//            //显示的图片
//            let placeHolderImage=UIImage(named: "PlaceHolder")
//
//            //加载图片
//            ivBanner!.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [], completed: nil)
            
            //使用封装的工具类
            ImageUtil.show(ivBanner, imageUri)
        }else {
            //如果没有图片
            //设置为默认图片
            ivBanner.image=UIImage(named: "PlaceHolder")
        }
        
    }
}
