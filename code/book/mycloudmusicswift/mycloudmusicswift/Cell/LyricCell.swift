//
//  LyricCell.swift
//  歌词Cell
//
//  Created by smile on 2019/4/20.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class LyricCell: BaseTableViewCell {
    //这一行歌词
    @IBOutlet weak var lbTitle: UILabel!

    /// 这一行歌词（自定义）
    @IBOutlet weak var line: LyricLineView!
    
    
    func bindData(_ data:Any,_ accurate:Bool) {
//        //Label实现
//        if data is String {
//            //字符串
//            //用来填充占位符
//            lbTitle.text=""
//        } else {
//            let lyricLine=data as! LyricLine
//            lbTitle.text=lyricLine.data
//        }
        
        //自定义控件
        if data is String {
            //字符串
            //用来填充占位符
            line.data=nil
            line.isAccurate=false
        } else {
            line.data=data as! LyricLine
            line.isAccurate=accurate
        }

    }

    /// 当Cell选中状态发送变更时调用
    ///
    /// - Parameters:
    ///   - selected: <#selected description#>
    ///   - animated: <#animated description#>
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //逐行歌词实现
        //        if selected {
        //            //选中状态
        //            //颜色为主色调
        //            lbTitle.textColor=UIColor(hex: COLOR_PRIMARY)
        //        } else {
        //            //未选中
        //            //默认颜色
        //            lbTitle.textColor=UIColor.lightGray
        //        }
        
        //通过自定义View实现的
        line.isSelected=selected

        //会触发异步调用drawRect方法
        line.setNeedsDisplay()
        
    }
    
    /// 播放时间回调
    ///
    /// - Parameter time: <#time description#>
    func show(_ time:Float) {
        line.show(time)
        
        line.setNeedsDisplay()
    }

}
