//
//  ButtonCell.swift
//  我的界面；按钮Cell
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class ButtonCell: BaseTableViewCell {
    static var NAME = "ButtonCell"

    /// 按钮图标
    @IBOutlet weak var ivIcon: UIImageView!
    
    /// 按钮标题
    @IBOutlet weak var lbTitle: UILabel!
    
    
    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:ButtonInfo) {
        ivIcon.image=UIImage(named: data.icon)
        lbTitle.text=data.title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
