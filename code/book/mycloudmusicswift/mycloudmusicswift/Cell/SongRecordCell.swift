//
//  SongRecordCell.swift
//  歌曲黑胶唱片Cell
//
//  Created by smile on 2019/4/15.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SongRecordCell: BaseCollectionViewCell {
    
    /// 黑胶唱片
    @IBOutlet weak var background: UIImageView!
    
    /// 歌曲封面
    @IBOutlet weak var banner: UIImageView!
    
    var data:Song!
    
    var displayLink:CADisplayLink?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //设置封面为圆形
        self.banner.layer.cornerRadius = self.banner.frame.size.width/2.0
        self.banner.clipsToBounds=true
        self.banner.layer.masksToBounds = true
        
        //黑胶唱片不用设置为圆形
        //是因为图片就是原型
    }
    
    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Song) {
        self.data=data
        
        ImageUtil.show(banner, data.banner)
    }
    
    /// 开始转动
    func startCoverRotate(){
        stopCoverRotate()
        
        //创建一个定时器
        self.displayLink = CADisplayLink(target: self, selector: #selector(runRotate))
        
        //放入主循环
        //这样界面每帧都会调用
        displayLink?.add(to: RunLoop.main, forMode: RunLoop.Mode.default)
       
    }
    
    /// 停止转动
    func stopCoverRotate(){
        if let displayLink=displayLink {
            displayLink.invalidate()
            self.displayLink = nil
        }
    }
    
    /// 旋转，每帧调用
    @objc func runRotate() {
//        print("SongRecordCell runRotate:\(data.title)")
        //在已有的基础上，旋转指定的弧度
        //角度转弧度：度数 * (π / 180）
        //在Android项目中我们测得每16毫秒转0.2304度(用秒表测转一圈的时间)
        //0.2304*(PI/180)=0.004021238596595
        
        //在原来的基础上没16毫秒，顺时针选中0.004021238596595弧度
       
        //滚动背景
        //concatenating：表示在原来的基础上进行变换
        background.transform=CGAffineTransform(rotationAngle: SIZE_ROTATE).concatenating(background.transform)
       
        //滚动封面
        banner.transform=CGAffineTransform(rotationAngle: SIZE_ROTATE).concatenating(banner.transform)
    }

}
