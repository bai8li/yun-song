//
//  VideoCell.swift
//  视频Cell
//
//  Created by smile on 2019/4/25.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoCell: BaseTableViewCell {
    static let NAME = "VideoCell"
    
    /// 视频封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 播放数
    @IBOutlet weak var lbPlayCount: UILabel!
    
    /// 时长
    @IBOutlet weak var lbTime: UILabel!
    
    /// 视频类型
    @IBOutlet weak var btType: UIButton!
    
    /// 标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 视频作者头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 视频作者昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 点赞数按钮
    @IBOutlet weak var btLikeCount: UIButton!
    
    /// 评论数按钮
    @IBOutlet weak var btCommentCount: UIButton!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(view: ivBanner)
        ViewUtil.showRadius(ivAvatar, 20)
        
        btType.showColorPrimaryBorder()
        ViewUtil.showRadius(btType, 11)
    }
    
    func bindData(_ data:Video) {
        //显示封面
        ImageUtil.show(ivBanner, data.banner)
        
        //显示播放量
        lbPlayCount.text=" \(data.clicks_count)"
        
        //服务端存储的是毫秒
        //所以这里先转为秒
        lbTime.text=TimeUtil.formatTime1(data.duration/1000.0)
        
        //标题
        lbTitle.text=data.title
        
        //用户头像
        ImageUtil.showAvatar(ivAvatar, data.user.avatar)
        
        //视频作者
        lbNickname.text=data.user.nickname
        
        //点赞数
        btLikeCount.setTitle(" \(data.likes_count)", for: .normal)
        
        //评论数
        btCommentCount.setTitle(" \(data.comments_count)", for: .normal)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    /// 点赞按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onLikeClick(_ sender: Any) {
        print("VideoCell onLikeClick")
    }
    
    
    /// 评论按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onCommentClick(_ sender: Any) {
        print("VideoCell onCommentClick")
    }
    
    /// 更多按钮点击
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onMoreClick(_ sender: Any) {
        print("VideoCell onMoreClick")
    }
    
}
