//
//  BaseFeedCell.swift
//  动态通用Cell
//
//  Created by smile on 2019/4/18.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseFeedCell: BaseTableViewCell {
    
    /// 头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    /// 时间
    @IBOutlet weak var lbTime: UILabel!
    
    /// 内容
    @IBOutlet weak var lbContent: UILabel!
    
    var data:Feed!
    
    /// 头像点击回调方法
    var onAvatarClick:((_ data:User) -> Void)!
    
    override func initViews() {
        super.initViews()
        //设置头像圆角
        ViewUtil.showRadius(ivAvatar, 17.5)
    }
    
    func bindData(_ data:Feed) {
        self.data=data
        
        //头像
        ImageUtil.showAvatar(ivAvatar, data.user.avatar)

        //设置昵称
        lbNickname.text=data.user.nickname
        
        //设置发送时间
        lbTime.text=TimeUtil.format1(data.created_at)
        
//        lbContent.text=data.content
        //设置内容行高
        //由于Label没有直接设置的属性
        //所以只能通过第三方框架或者使用属性文本
        
        //创建属性文本
        let contentAttributed = NSMutableAttributedString(string: data.content)
        
        //判断是否有地理位置
        if let province=data.province{
            contentAttributed.yy_appendString("\n来自：\(province).\(data.city!)")
        }
        
        let paragraphStyle = NSMutableParagraphStyle.init()
        
        //设置行高
        paragraphStyle.lineSpacing = 8.0

        //设置到属性文本中
        contentAttributed.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: contentAttributed.length))
        
        //设置到控件上
        lbContent.attributedText=contentAttributed
    }

    /// 头像点击回调方法
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onAvatarClick(_ sender: UIButton) {
        onAvatarClick(self.data.user)
    }

}
