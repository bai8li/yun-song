//
//  BaseCollectionViewCell.swift
//  通用CollectionViewCell
//
//  Created by smile on 2019/5/28.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initViews()
        initDatas()
        initListeners()
    }
    
    /// 初始化控件
    func initViews(){

    }
    
    /// 初始化数据
    func initDatas(){
        
    }
    
    /// 初始化监听器
    func initListeners(){
        
    }
}
