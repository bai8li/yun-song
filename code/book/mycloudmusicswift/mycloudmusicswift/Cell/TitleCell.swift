//
//  TitleCell.swift
//  标题Cell
//
//  Created by smile on 2019/4/10.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class TitleCell: BaseCollectionViewCell {
    static var NAME = "TitleCell"
    
    /// 显示标题
    @IBOutlet weak var lbTitle: UILabel!

    override func initViews() {
        super.initViews()
        //主题配置
        lbTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
    }
    
    
    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:String) {
        lbTitle.text=data
    }
}
