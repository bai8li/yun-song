//
//  VideoDetailVideoCell.swift
//  视频详情 相关视频Cell
//
//  Created by smile on 2019/4/26.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class VideoDetailVideoCell: BaseTableViewCell {
    static var NAME = "VideoDetailVideoCell"
    
    /// 视频图片
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 视频标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 视频信息
    @IBOutlet weak var lbInfo: UILabel!
    
    override func initViews() {
        super.initViews()
        ViewUtil.showSmallRadius(view: ivBanner)
    }
    
    func bindData(_ data:Video) {
        ImageUtil.show(ivBanner, data.banner)
        lbTitle.text=data.title
        
        let timeString=TimeUtil.formatTime1(data.duration/1000.0)
        lbInfo.text="\(timeString)，by \(data.user.nickname!)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
