//
//  SongListCell.swift
//  歌曲列表Cell
//
//  Created by smile on 2019/4/13.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SongListCell: BaseTableViewCell {
    static var NAME = "SongListCell"
    
    /// 正在播放提示
    @IBOutlet weak var ivPlay: UIImageView!
    
    /// 第几首音乐
    @IBOutlet weak var lbPosition: UILabel!
    
    /// 音乐标题
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 音乐信息
    @IBOutlet weak var lbInfo: UILabel!
    
    /// 更多信息按钮
    @IBOutlet weak var btMore: UIButton!
    
    /// 更多点击回调方法
    var onMoreClick:((_ position:Int) -> Void)!
    
    /// 显示为删除状态
    func showDeleteStatus() {
        btMore.setImage(UIImage(named: "DeleteAllGrey"), for: .normal)
    }

    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Song) {
        //设置位置
        lbPosition.text="\(tag+1)"
        
        //设置标题
        lbTitle.text=data.title
        
        //设置歌手名称
        lbInfo.text=data.singer.nickname
    }
    
    func bindData(_ data:DownloadInfo) {
        //根据Id获取业务数据
        let song=ORMUtil.shared().findSongByUri(data.id)
        
        if let song = song {
            //设置位置
            lbPosition.text="\(tag+1)"
            
            lbTitle.text=song.title
            lbInfo.text=song.singer.nickname
        }else{
            //TODO 如果为空就表示逻辑有问题
        }
        
        
    }
    
    /// 选中和取消选中会调用该方法
    ///
    /// - Parameters:
    ///   - selected: <#selected description#>
    ///   - animated: <#animated description#>
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        //自定义选中的状态
        if (selected) {
            //选中（其实就是正在播放这首音乐）

            //隐藏显示歌曲数量
            lbPosition.isHidden=true

            //显示正在播放图标
            ivPlay.isHidden=false

            //标题变为主色调
            lbTitle.textColor=UIColor(hex: COLOR_PRIMARY)
        } else {
            lbPosition.isHidden=false
            ivPlay.isHidden=true

            lbTitle.textColor=UIColor.darkText
        }
    }
    
    /// 更多按钮点击事件
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func onMoreClick(_ sender: UIButton) {
        //这里的弹窗会在播放界面实现
        //所以这里就不在重复实现
        print("SongListCell onMoreClick:\(tag)")
        
        onMoreClick(tag)
    }
    
}
