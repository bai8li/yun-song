//
//  BaseChatImageCell.swift
//  通用聊天图片Cell
//
//  Created by smile on 2019/4/22.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class BaseChatImageCell: BaseChatCell {

    /// 图片
    @IBOutlet weak var ivContent: UIImageView!
    
    override func initViews() {
        super.initViews()
        
        ViewUtil.showSmallRadius(view: ivContent)
    }
    
    override func bindData(_ data: JMSGMessage) {
        super.bindData(data)
        
        let content = data.content as! JMSGImageContent
        
        //获取图片
        content.largeImageData(progress: nil, completionHandler: { (imageData, msgId, error) in
            if msgId == data.msgId {
                //防止图片错位
                //判断下Id是否一样
                
                if let imageData = imageData {
                    let image = UIImage(data: imageData)
                    self.ivContent.image = image
                }
            }
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
