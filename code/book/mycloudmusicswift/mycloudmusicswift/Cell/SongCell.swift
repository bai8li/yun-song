//
//  SongCell.swift
//  单曲Cell
//
//  Created by smile on 2019/4/12.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

class SongCell: BaseCollectionViewCell {
    static var NAME = "SongCell"
    
    /// 歌曲封面
    @IBOutlet weak var ivBanner: UIImageView!
    
    /// 歌曲名称
    @IBOutlet weak var lbTitle: UILabel!
    
    /// 点击数
    @IBOutlet weak var lbCount: UILabel!
    
    /// 歌手头像
    @IBOutlet weak var ivAvatar: UIImageView!
    
    /// 歌手昵称
    @IBOutlet weak var lbNickname: UILabel!
    
    @IBOutlet weak var vwDivider: UIView!
    
    @IBOutlet weak var ivClickCount: UIImageView!
    
    override func initViews() {
        super.initViews()
        //主题配置
        //标题
        lbTitle.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //昵称
        lbNickname.theme_textColor=[COLOR_STRING_BLACK,COLOR_STRING_LIGHT_GREY]
        
        //分割线
        vwDivider.theme_backgroundColor=[COLOR_STRING_GROUP_TABLE_VIEW_BACKGROUND,COLOR_STRING_SEARCH_BACKGROUND]
        
        //设置歌曲封面圆角
        ViewUtil.showSmallRadius(view: ivBanner)
        
        //歌手头像圆角
        ViewUtil.showRadius(ivAvatar, 12.5)
    }

    /// 绑定数据
    ///
    /// - Parameter data: <#data description#>
    func bindData(_ data:Song) {
        //单曲封面
        ImageUtil.show(ivBanner, data.banner)
        
        //设置标题
        lbTitle.text=data.title
        
        //设置点击量
        lbCount.text="\(data.clicks_count)"
        
        ImageUtil.showAvatar(ivAvatar, data.singer.avatar)
        
        //歌手昵称
        lbNickname.text=data.singer.nickname
        
    }
}
