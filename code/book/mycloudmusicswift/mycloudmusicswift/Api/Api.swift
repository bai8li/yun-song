//
//  Api.swift
//  网络请求接口包装类
//  对外提供与框架无关的接口
//
//  Created by smile on 2019/4/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import UIKit

//导入响应式编程框架
import RxSwift

//JSON解析为对象
import HandyJSON

// 导入网络框架
import Moya

class Api {
    /// 单例设计模式
    static let shared = Api()
    
    /// MoyaProvider
    private let provider:MoyaProvider<Service>!
    
    /// 私有构造方法
    private init() {
        //插件列表
        var plugins:[PluginType]=[]
        
        /// 显示网络请求等待对话框
        let networkActivityPlugin=NetworkActivityPlugin(networkActivityClosure: { changeType, targetType in
            //changeType类型是NetworkActivityChangeType
                //通过它能监听到开始请求和结束请求
    
                //targetType类型是TargetType
                //就是我们这里的service
                //通过它能判断是那个请求
    
                if NetworkActivityChangeType.began==changeType{
                    //开始请求
                    print("began request:\(targetType.path)")
                    
                    let targetType=targetType as! Service
                    
                    switch targetType {
                    case .login, .sheetDetail:
                        //这里只实现了
                        //登陆，歌单详情
                        //显示网络请求等待对话框
                        //如果大家要实现其他请求也显示
                        //只需要在这里添加就行了
                        ToastUtil.showLoading()
                        
                    default:
                        break
                    }
                }else{
                    //结束请求
                    print("end request:\(targetType.path)")
                    
                    //请求结束这里就不判断了
                    ToastUtil.hideLoading()
                }
        })
        
        plugins.append(networkActivityPlugin)
        
        //网络请求签名加密插件
        plugins.append(NetworkPlugin())
        
        if DEBUG {
            //调试模式
            //添加网络请求日志插件
            plugins.append(NetworkLoggerPlugin())
        }
        
        /// 创建Request的闭包
        /// 在这里能修改body
        /// 修改请求头
        let requestClosure = {(endpoint: Endpoint, closure: MoyaProvider.RequestResultClosure) in
            
            do {
                //获取到request
                var request: URLRequest = try endpoint.urlRequest()
                
                //处理request
                //主要是实现参数签名和加密
                let url=request.url!.absoluteString
                let method=request.httpMethod!
                
                print("Api requestClosure:\(url)")
                
                if url.hasSuffix("v2/orders.json") && method.hasSuffix("POST") {
                    //该接口需要添加参数签名到请求头
                    
                    //将body转为字符串
                    let bodyString = String(data: request.httpBody!, encoding: String.Encoding.utf8)!
                    
                    //计算签名
                    let sign=DigestUtil.sha1(bodyString)
                    
                    //添加到请求中
                    request.allHTTPHeaderFields!["Sign"]=sign
                    
                    print("Api requestClosure sign params success:\(method),\(url)")
                }else if url.hasSuffix("v3/orders.json") && method.hasSuffix("POST") {
                    //该接口参数需要加密
                    
                    //将body转为字符串
                    let bodyString = String(data: request.httpBody!, encoding: String.Encoding.utf8)!
                    
                    //将参数加密
                    let encryptBodyString=DigestUtil.encryptAES(bodyString)
                    
                    //重新创建body
                    let newBody=encryptBodyString?.data(using: .utf8)
                    
                    //设置到request上
                    request.httpBody=newBody
                    
                    //更改header
                    //因为对于加密的数据
                    //有些服务端框架会自动处理
                    //例如：我们后台用的Rails框架
                    //如果传递的JSON他会解析为Hash
                    //类似iOS中的字典
                    //由于我们参数加密了
                    //其实就是一段字符串
                    //所以要更改Content-Type为文本
                    //不然服务端后出错
                    request.allHTTPHeaderFields!["Content-Type"]="application/plain"

                    print("Api requestClosure encrypt params success:\(method),\(url)")
                }
                
                //end 处理request
                
                //回调成功
                closure(.success(request))
            } catch MoyaError.requestMapping(let url) {
                closure(.failure(MoyaError.requestMapping(url)))
            } catch MoyaError.parameterEncoding(let error) {
                closure(.failure(MoyaError.parameterEncoding(error)))
            } catch {
                closure(.failure(MoyaError.underlying(error, nil)))
            }
        }
        
        //初始化MoyaProvider
        provider = MoyaProvider<Service>(requestClosure:requestClosure,plugins:plugins)
        
    }
    
    /// 歌单列表
    ///
    /// - Returns: <#return value description#>
    func sheets() -> Observable<ListResponse<Sheet>?>{
        return provider
        .rx
        .request(.sheets)
        .filterSuccessfulStatusCodes()
        .asObservable()
        .mapString()
        .mapObject(ListResponse<Sheet>.self)
    }
    
    /// 歌单详情
    ///
    /// - Returns: <#return value description#>
    func sheetDetail(id:String) -> Observable<DetailResponse<Sheet>?>{
        return provider
            .rx
            .request(.sheetDetail(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Sheet>.self)
    }
    
    /// 创建歌单
    ///
    /// - Returns: <#return value description#>
    func createSheet(_ title:String) -> Observable<DetailResponse<Sheet>?>{
        return provider
            .rx
            .request(.createSheet(title: title))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Sheet>.self)
    }
    
    /// 添加音乐到歌单（收藏音乐）
    ///
    /// - Returns: <#return value description#>
    func addSongToSheet(_ id:String,_ song_id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.addSongToSheet(id:id,song_id: song_id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 从歌单中删除一首音乐（取消收藏）
    ///
    /// - Returns: <#return value description#>
    func deleteSongInSheet(_ id:String,_ song_id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.deleteSongInSheet(id:id,song_id:song_id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    
    /// 歌曲详情
    ///
    /// - Returns: <#return value description#>
    func songDetail(id:String) -> Observable<DetailResponse<Song>?>{
        return provider
            .rx
            .request(.songDetail(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Song>.self)
    }
    
    /// 视频列表
    ///
    /// - Returns: <#return value description#>
    func videos() -> Observable<ListResponse<Video>?>{
        return provider
            .rx
            .request(.videos)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Video>.self)
    }
    
    /// 视频详情
    ///
    /// - Returns: <#return value description#>
    func videoDetail(id:String) -> Observable<DetailResponse<Video>?>{
        return provider
            .rx
            .request(.videoDetail(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Video>.self)
    }
    
    /// 话题列表
    ///
    /// - Returns: <#return value description#>
    func topics() -> Observable<ListResponse<Topic>?>{
        return provider
            .rx
            .request(.topics)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Topic>.self)
    }
    
    /// 动态列表
    ///
    /// - Returns: <#return value description#>
    func feeds() -> Observable<ListResponse<Feed>?>{
        return provider
            .rx
            .request(.feeds)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Feed>.self)
    }
    
    /// 商品列表
    ///
    /// - Returns: <#return value description#>
    func shops() -> Observable<ListResponse<Book>?>{
        return provider
            .rx
            .request(.shops)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Book>.self)
    }
    
    /// 商品详情
    ///
    /// - Returns: return value description
    func shopDetail(_ id:String) -> Observable<DetailResponse<Book>?>{
        return provider
            .rx
            .request(.shopDetail(id:id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Book>.self)
    }
    
    
    /// 广告列表
    ///
    /// - Returns: <#return value description#>
    func ads() -> Observable<ListResponse<Ad>?>{
        return provider
            .rx
            .request(.ads)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Ad>.self)
    }
    
    /// 歌曲列表
    ///
    /// - Returns: <#return value description#>
    func songs() -> Observable<ListResponse<Song>?>{
        return provider
            .rx
            .request(.songs)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Song>.self)
    }
    
    /// 用户详情
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func userDetail(_ id:String,_ nickname:String?=nil) -> Observable<DetailResponse<User>?>{
        return provider
            .rx
            .request(.userDetail(id: id,nickname:nickname))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<User>.self)
    }
    
    /// 更新用户
    ///
    /// - Parameters:
    ///   - id: 用户Id
    ///   - nickname: 昵称
    ///   - avatar: 头像
    ///   - desc: 描述
    ///   - gender: 性别
    ///   - birthday: 生日
    ///   - province: 省
    ///   - province_code: 省代码
    ///   - city: 城市
    ///   - city_code: 城市代码
    ///   - area: 区
    ///   - area_code: 区代码
    /// - Returns: <#return value description#>
    func updateUser(id:String,nickname:String?=nil,avatar:String?=nil,desc:String?=nil,gender:Int?=nil,birthday:String?=nil,province:String?=nil,province_code:String?=nil,city:String?=nil,city_code:String?=nil,area:String?=nil,area_code:String?=nil) -> Observable<DetailResponse<User>?>{
        return provider
            .rx
            .request(.updateUser(id:id,nickname:nickname,avatar:avatar,desc:desc,gender:gender,birthday:birthday,province:province,province_code:province_code,city:city,city_code:city_code,area:area,area_code:area_code))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<User>.self)
    }
    
    
    /// 绑定第三方平台账号
    ///
    /// - Parameters:
    ///   - account: 第三方平台Id
    ///   - platform: 平台类型；取值在常量文件中
    /// - Returns: <#return value description#>
    func bindAccount(_ account:String,_ platform:Int) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.bindAccount(account: account,platform:platform))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 解绑第三方平台账号
    ///
    /// - Parameters:
    ///   - platform: 平台类型；取值在常量文件中
    /// - Returns: <#return value description#>
    func unbindAccount(_ platform:Int) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.unbindAccount(platform:platform))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 发送短信验证码
    ///
    /// - Parameter phone: <#phone description#>
    /// - Returns: <#return value description#>
    func sendSMSCode(phone:String) -> Observable<DetailResponse<BaseModel>?>{
        //这里不解析对象
        //可以随便传递模型
        //因为该接口成功
        //不会返回内容
        return provider
            .rx
            .request(.sendSMSCode(phone: phone))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 发送邮件验证码
    ///
    /// - Parameter phone: <#phone description#>
    /// - Returns: <#return value description#>
    func sendEmailCode(email:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.sendEmailCode(email: email))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 重置密码
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func resetPassword(phone:String?,email:String?,code:String,password:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.resetPassword(phone: phone,email:email, code:code,password:password))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 关注用户
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func follow(_ id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.follow(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 取消关注用户
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func deleteFollow(_ id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.deleteFollow(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 评论点赞
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func like(_ comment_id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.like(comment_id: comment_id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 取消评论点赞
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func deleteLike(_ id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.deleteLike(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    
    /// 创建评论
    ///
    /// - Parameters:
    ///   - content: <#content description#>
    ///   - sheet_id: <#sheet_id description#>
    ///   - parent_id: <#parent_id description#>
    /// - Returns: <#return value description#>
    func createComment(content:String,sheet_id:String?=nil,parent_id:String?=nil) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.createComment(content: content, sheet_id: sheet_id, parent_id: parent_id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 创建动态
    ///
    /// - Parameters:
    ///   - content: 动态内容
    ///   - province: 省
    ///   - city: 市
    ///   - longitude: 经度
    ///   - latitude: 纬度
    ///   - images: 图片地址
    /// - Returns: <#return value description#>
    func createFeed(content:String,province:String?=nil,city:String?=nil,longitude:Double?=nil,latitude:Double?=nil,images:[Dictionary<String,String>]?=nil) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.createFeed(content: content, province: province, city: city,longitude:longitude,latitude:latitude,images:images))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 收藏歌单
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func collect(_ id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.collect(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 取消收藏歌单
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func deleteCollect(_ id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.deleteCollect(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 好友列表
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func friends(_ id:String) -> Observable<ListResponse<User>?>{
        return provider
            .rx
            .request(.friends(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<User>.self)
    }
    
    /// 粉丝列表
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func fans(_ id:String) -> Observable<ListResponse<User>?>{
        return provider
            .rx
            .request(.fans(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<User>.self)
    }
    
    /// 创建用户（注册）
    ///
    /// - Parameters:
    ///   - avatar: 头像
    ///   - nickname: 昵称
    ///   - phone: 手机号
    ///   - email: 邮箱
    ///   - password: 密码
    ///   - qq_id: qq第三方登陆Id
    ///   - weibo_id: 微博第三方登陆Id
    /// - Returns: <#return value description#>
    func createUser(avatar:String?=nil,nickname:String,phone:String,email:String,password:String,qq_id:String?=nil,weibo_id:String?=nil) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.createUser(avatar: avatar, nickname: nickname, phone: phone, email: email, password: password, qq_id: qq_id, weibo_id: weibo_id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    /// 登陆
    ///
    /// - Parameters:
    ///   - phone: 手机号
    ///   - email: 邮箱
    ///   - password: 密码
    ///   - qq_id: qq第三方登陆Id
    ///   - weibo_id: 微博第三方登陆Id
    /// - Returns: <#return value description#>
    func login(phone:String?=nil,email:String?=nil,password:String?=nil,qq_id:String?=nil,weibo_id:String?=nil) -> Observable<DetailResponse<Session>?>{
        return provider
            .rx
            .request(.login(phone: phone, email: email, password: password, qq_id: qq_id, weibo_id: weibo_id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Session>.self)
    }

    /// 评论列表
    ///
    /// - Parameters:
    ///   - sheet_id: 歌单Id
    ///   - order: 排序；0：最新；10：最热
    /// - Returns: <#return value description#>
    func comments(sheet_id:String?=nil,order:Int=ORDER_NEW,page:Int=1) -> Observable<ListResponse<Comment>?> {
        return provider
            .rx
            .request(.comments(sheet_id: sheet_id, order: order,page:page))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Comment>.self)
    }
    
    
    /// 获取搜索建议
    ///
    /// - Parameter query: <#query description#>
    /// - Returns: <#return value description#>
    func searcheSuggests(_ query:String ) -> Observable<DetailResponse<Suggest>?> {
        return provider
            .rx
            .request(.searcheSuggests(query: query))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Suggest>.self)
    }
    
    /// 歌单搜索
    ///
    /// - Parameter query: <#query description#>
    /// - Returns: <#return value description#>
    func searcheSheets(_ query:String ) -> Observable<ListResponse<Sheet>?> {
        return provider
            .rx
            .request(.searcheSheets(query: query))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Sheet>.self)
    }
    
    /// 用户搜索
    ///
    /// - Parameter query: <#query description#>
    /// - Returns: <#return value description#>
    func searcheUsers(_ query:String ) -> Observable<ListResponse<User>?> {
        return provider
            .rx
            .request(.searcheUsers(query: query))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<User>.self)
    }
    
    /// 用户创建的歌单
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func createSheets(_ id:String) -> Observable<ListResponse<Sheet>?> {
        return provider
            .rx
            .request(.createSheets(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Sheet>.self)
    }
    
    /// 用户收藏的歌单
    ///
    /// - Parameter id: <#id description#>
    /// - Returns: <#return value description#>
    func collectSheets(_ id:String) -> Observable<ListResponse<Sheet>?> {
        return provider
            .rx
            .request(.collectSheets(id: id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Sheet>.self)
    }
    
    
    func ordersV2() -> Observable<ListResponse<Order>?> {
        return provider
            .rx
            .request(.ordersV2)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Order>.self)
    }
    
    func ordersV3() -> Observable<ListResponse<Order>?> {
        return provider
            .rx
            .request(.ordersV3)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Order>.self)
    }
    
    func createOrderV2(book_id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.createOrderV2(book_id: book_id, source: Order.Source.ios.rawValue))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
    
    func createOrderV3(book_id:String) -> Observable<DetailResponse<BaseModel>?>{
        return provider
            .rx
            .request(.createOrderV3(book_id: book_id, source: Order.Source.ios.rawValue))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<BaseModel>.self)
    }
 
    /// 创建订单
    ///
    /// - Returns: return value description
    func createOrder(_ book_id:String) -> Observable<DetailResponse<Order>?>{
        return provider
            .rx
            .request(.createOrder(book_id:book_id, source: Order.Source.ios.rawValue))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Order>.self)
    }
    
    /// 订单列表
    ///
    /// - Returns: return value description
    func orders() -> Observable<ListResponse<Order>?>{
        return provider
            .rx
            .request(.orders)
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(ListResponse<Order>.self)
    }
    
    /// 订单详情
    ///
    /// - Returns: return value description
    func orderDetail(_ id:String) -> Observable<DetailResponse<Order>?>{
        return provider
            .rx
            .request(.orderDetail(id:id))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Order>.self)
    }
    
    /// 获取支付参数
    ///
    /// - Returns: return value description
    func orderPay(_ id:String,_ channel:Int) -> Observable<DetailResponse<Pay>?>{
        return provider
            .rx
            .request(.orderPay(id:id,channel:channel,origin:Order.Source.ios.rawValue))
            .filterSuccessfulStatusCodes()
            .asObservable()
            .mapString()
            .mapObject(DetailResponse<Pay>.self)
    }
    
    
}
