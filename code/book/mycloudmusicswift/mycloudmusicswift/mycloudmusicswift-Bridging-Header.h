//
//  mycloudmusicswift-Bridging-Header.h
//  OC和Swift混合编程桥接文件
//  由于我们项目是Swift编写，这里引用了OC写的框架
//  所以要在这里将OC框架的头文件导入进来
//
//  Created by smile on 2019/4/5.
//  Copyright © 2019 ixuea. All rights reserved.
//

//轮播图组件（OC实现）
#import "YJBannerView.h"
#import "YJBannerViewCell.h"

//提示框
#import "MBProgressHUD/MBProgressHUD.h"

//ShareSDK
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>

//发送验证码，倒计时框架
#import "CountDown.h"

//图片加载框架
#import "SDWebImage/UIImageView+WebCache.h"

//遮罩视图
#import "GKCover.h"

//富文本控件
#import "YYText/YYText.h"

//阿里云OSS
//用来上传发布带图片动态
#import "AliyunOSSiOS/AliyunOSSiOS.h"

//指示器
#import "JXCategoryView/JXCategoryView.h"

//极光
// 引入 JSHARE 功能所需头文件
//#import "JShare/JSHAREService.h"

// 引入 JPush 功能所需头文件
//#import "JPUSHService.h"
//
//// iOS10 注册 APNs 所需头文件
//#ifdef NSFoundationVersionNumber_iOS_9_x_Max
//#import <UserNotifications/UserNotifications.h>
//#endif

//// 如果需要使用 idfa 功能所需要引入的头文件（可选）
//#import <AdSupport/AdSupport.h>

//极光IM
#import <JMessage/JMessage.h>

// 引入JAnalytics功能所需头文件
#import "JANALYTICSService.h"

// 如果需要使用idfa功能所需要引入的头文件（可选）
#import <AdSupport/AdSupport.h>

//快速给View添加红点
#import <RKNotificationHub/RKNotificationHub.h>

//支付宝支付
#import <AlipaySDK/AlipaySDK.h>

//高德地图定位
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>

//自定义CollectionView的Layout
#import <UICollectionViewLeftAlignedLayout/UICollectionViewLeftAlignedLayout.h>

//图片裁剪
#import <RSKImageCropper/RSKImageCropper.h>

//下载框架
#import "CocoaDownloader.h"

//搜索界面框架
#import <PYSearch/PYSearch.h>

//腾讯Bugly
#import <Bugly/Bugly.h>
