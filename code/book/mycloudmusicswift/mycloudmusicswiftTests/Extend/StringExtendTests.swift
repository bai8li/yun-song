//
//  StringExtendTests.swift
//  字符串扩展单元测试
//
//  Created by smile on 2019/5/7.
//  Copyright © 2019 ixuea. All rights reserved.
//

import XCTest

class StringExtendTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /// 测试手机号格式
    func testIsPhone() {
        //这是一个正确的手机号格式
        //所以用断言判断结果为true
        //只有结果为true才表示测试通过
        //也就表示我们的代码没问题
        
        //这里的assert其实是Swift中的
        assert("13141111111".isPhone())
        
        //这是一个错误的手机号格式
        //所以用断言判断结果为false
        
        //XCTAssertFalse是测试框架中的
        //测试中推荐用测试框架中的代码
        XCTAssertFalse("ixuea".isPhone())
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
