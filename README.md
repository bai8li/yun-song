# 简介

这是一门使用Swift语言，从0开发一个iOS平台，接近企业级商业级的项目（我的云音乐），课程包含了基础内容，高级内容，项目封装，项目重构等知识，99%代码为手写；因为这是项目课程；所以不会深入到源码讲解某个知识点，以及原理，但会粗略的讲解下基础原理；主要是讲解如何使用系统功能，流行的第三方框架，第三方服务，完成接近企业级商业级项目，目的是让大家，学到真正的企业级商业级项目开发技术。

## 测试编译环境

macOS:11.1 
xcode:12.3
iOS:14.3

## Git分支

```
dev-video-sp：使用Spring Boot API课程的项目，最新的功能都在这个分支，推荐查看这个，例如：微信登录，微信支付，苹果推送等，需要付费开发者账号才能运行
dev-video-sp-not-paid-account：移除了需要付费开发者账号功能，免费账号就可以运行
master：使用Rails API课程的项目
```

## 目录结构

```
api：用到的API目录
code：代码
	video：录制视频时写的源码(推荐查看这一个)
		mycloudmusicswift：课程项目
		MyCloudMusicSwiftNew：新版xcode创建的项目，主要是讲解如何使用新版xcode创建项目
		TestRemoveSceneDelegate：测试如何将新版项目结构移除SceneDelegate，以兼容旧版
		TestScreenAdapter：测试屏幕适配的项目
		TestUseSceneDelegate：测试新版项目结构下（有SceneDelegate），界面跳转功能
		XZWeather：演示上架写的天气应用
resource：一些资源文件，或截图
```

## 运行方法

### 安装依赖

因为mycloudmusicswift项目用到CocoaPods了，所以运行方法是在有Podfile的目录里面，执行如下命令安装依赖：

```
pod install
```

这个步骤，可能不较慢，大家可以使用代理。

### 打开项目

依赖安装完成后，打开xcworkspace文件；其他项目打开xcodeproj文件

### 运行模拟器

打开后，选择相应的模拟器，执行运行即可。

### 运行到真机

要更改BundleId，然后在选择iCloud账号，然后在运行。

## 许可证

### 图片

除了部分自己设计的图片，其他的来自iconfont。

项目的交互，参考了网易云音乐，QQ音乐，酷狗音乐；交互请勿用于商业用途。

### 音乐

来自网络。

### 视频

来自音悦台。

### 代码

除了第三方框架，其他的代码均自己编写，可用于项目中；其他第三方框架请参考第三方框架的许可证。
